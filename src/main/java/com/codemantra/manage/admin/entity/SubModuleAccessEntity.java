/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

public class SubModuleAccessEntity{
	
	private String accessId;
	private List<String> activityAccess;
	
	public String getAccessId() {
		return accessId;
	}
	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}
	public List<String> getActivityAccess() {
		return activityAccess;
	}
	public void setActivityAccess(List<String> activityAccess) {
		this.activityAccess = activityAccess;
	}
}

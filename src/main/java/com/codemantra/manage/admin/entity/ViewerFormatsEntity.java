package com.codemantra.manage.admin.entity;

public class ViewerFormatsEntity {

	public String formatId;
	public String viewerType;
	
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getViewerType() {
		return viewerType;
	}
	public void setViewerType(String viewerType) {
		this.viewerType = viewerType;
	}
}

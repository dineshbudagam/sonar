/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mPartnerGroup")
public class PartnerGroupEntity extends MBaseEntity{

	@Id
	private String id;
	
	private String partnerGroupId;
	private String partnerGroupName;	
	private String partnerTypeId;
	private List<String> partnerId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartnerGroupId() {
		return partnerGroupId;
	}
	public void setPartnerGroupId(String partnerGroupId) {
		this.partnerGroupId = partnerGroupId;
	}
	public String getPartnerGroupName() {
		return partnerGroupName;
	}
	public void setPartnerGroupName(String partnerGroupName) {
		this.partnerGroupName = partnerGroupName;
	}
	public String getPartnerTypeId() {
		return partnerTypeId;
	}
	public void setPartnerTypeId(String partnerTypeId) {
		this.partnerTypeId = partnerTypeId;
	}
	public List<String> getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(List<String> partnerId) {
		this.partnerId = partnerId;
	}
	
	
	
}

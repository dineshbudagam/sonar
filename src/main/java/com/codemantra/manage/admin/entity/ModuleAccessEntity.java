/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

public class ModuleAccessEntity{
	
	private String moduleId;
	private List<SubModuleAccessEntity> submodule;
	
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public List<SubModuleAccessEntity> getSubmodule() {
		return submodule;
	}
	public void setSubmodule(List<SubModuleAccessEntity> submodule) {
		this.submodule = submodule;
	}	
	
}

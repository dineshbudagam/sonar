package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CustomFieldsStringEntity {

	@Field("metaDataFieldName")
	String metaDataFieldName;
	
	@Field("operation")
	String operation;
	
	@Field("startIndex")
	Integer startIndex;
	
	
	@Field("endIndex")
	Integer endIndex;
	
	@Field("substituteText")
	String substituteText;

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public String getOperation() {
		return operation;
	}

	public Integer getStartIndex() {
		return startIndex;
	}

	public Integer getEndIndex() {
		return endIndex;
	}

	public String getSubstituteText() {
		return substituteText;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public void setEndIndex(Integer endIndex) {
		this.endIndex = endIndex;
	}

	public void setSubstituteText(String substituteText) {
		this.substituteText = substituteText;
	}

	
}
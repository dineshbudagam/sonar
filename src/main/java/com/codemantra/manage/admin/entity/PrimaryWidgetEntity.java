/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
20-11-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "defaultWidgetConfig")
public class PrimaryWidgetEntity extends MBaseEntity{
	
	@Id
	private String id;
	
	private String widgetId;
	private String widgetName;
	/*private List<String> roleId;
	private String query;
	private List<String> queryParams;*/
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(String widgetId) {
		this.widgetId = widgetId;
	}
	public String getWidgetName() {
		return widgetName;
	}
	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
	/*public List<String> getRoleId() {
		return roleId;
	}
	public void setRoleId(List<String> roleId) {
		this.roleId = roleId;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public List<String> getQueryParams() {
		return queryParams;
	}
	public void setQueryParams(List<String> queryParams) {
		this.queryParams = queryParams;
	}	*/

}

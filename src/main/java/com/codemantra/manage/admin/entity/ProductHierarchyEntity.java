package com.codemantra.manage.admin.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mProductHierarchy")
public class ProductHierarchyEntity {
	

	@Field("_id")
	private String id;
	
	@Field("productHierarchyName")
	private String productHierarchyName;
	
	
	@Field("parent_id")
	private String parent_id;
	
	@Field("finalProductHierarchyIds")
	private List<String> finalProductHierarchyIds;
	
	@Field("isActive")
	private boolean isActive;
	
	@Field("isDeleted")
	private boolean isDeleted;
	
	@Field("createdBy")
	private String createdBy;
	
	@Field("group")
	private String group;
	
	@Field("jsonPath")
	private String jsonPath;
	
	@Field("autoGenJsonPath")
	private String autoGenJsonPath;
	
	@Field("titlePath")
	private String titlePath;
	
	@Field("uidPath")
	private String uidPath;
	
	@Field("displayOrderNo")
	private String displayOrderNo;
	
	@Field("autoGenField")
	private String autoGenField;
	
	@Field("createdOn")
	private String createdOn;
	
	@Field("syncField")
	private String syncField;
	
	@Field("displayName")
	private String displayName;
	
	@Field("isMenuDisplay")
	private boolean isMenuDisplay;
	
	@Field("isQuickCreate")
	private boolean isQuickCreate;
	
    @Field("jsonPathISSN")
	private String jsonPathISSN;

    @Field("currentProductISSN")
	private String currentProductISSN;
    
    @Field("isCreateFinalProduct")
	private boolean isCreateFinalProduct;
        
    @Field("tagCreation")
    private boolean tagCreation;
        
    @Field("viewProduct")
    private boolean viewProduct;   
    
    
    @Field("updateISBN")
    private boolean updateISBN;

	@Field("isDistributableLevel")
	private boolean isDistributableLevel;
    
    @Field("isbnPath")
    private String isbnPath;
    
    @Field("multiCreateList")
    private List<Map<String,Object>> multiCreateList;
    
    @Field("abbr")
    private String abbr;   
    
	public String getProductHierarchyName() {
		return productHierarchyName;
	}

	public void setProductHierarchyName(String productHierarchyName) {
		this.productHierarchyName = productHierarchyName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	
	public boolean isCreateFinalProduct() {
		return isCreateFinalProduct;
	}

	public void setCreateFinalProduct(boolean isCreateFinalProduct) {
		this.isCreateFinalProduct = isCreateFinalProduct;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	
	public List<String> getFinalProductHierarchyIds() {
		return finalProductHierarchyIds;
	}

	public void setFinalProductHierarchyIds(List<String> finalProductHierarchyIds) {
		this.finalProductHierarchyIds = finalProductHierarchyIds;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getAutoGenField() {
		return autoGenField;
	}

	public void setAutoGenField(String autoGenField) {
		this.autoGenField = autoGenField;
	}

	public String getJsonPath() {
		return jsonPath;
	}

	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}

	public String getDisplayOrderNo() {
		return displayOrderNo;
	}

	public void setDisplayOrderNo(String displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}

	public String getAutoGenJsonPath() {
		return autoGenJsonPath;
	}

	public void setAutoGenJsonPath(String autoGenJsonPath) {
		this.autoGenJsonPath = autoGenJsonPath;
	}

	public String getTitlePath() {
		return titlePath;
	}

	public void setTitlePath(String titlePath) {
		this.titlePath = titlePath;
	}

	public String getUidPath() {
		return uidPath;
	}

	public void setUidPath(String uidPath) {
		this.uidPath = uidPath;
	}

	public String getSyncField() {
		return syncField;
	}

	public void setSyncField(String syncField) {
		this.syncField = syncField;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isMenuDisplay() {
		return isMenuDisplay;
	}

	public void setMenuDisplay(boolean isMenuDisplay) {
		this.isMenuDisplay = isMenuDisplay;
	}

	public boolean isQuickCreate() {
		return isQuickCreate;
	}

	public void setQuickCreate(boolean isQuickCreate) {
		this.isQuickCreate = isQuickCreate;
	}

    /**
     * @return the jsonPathISSN
     */
    public String getJsonPathISSN() {
        return jsonPathISSN;
    }

    /**
     * @param jsonPathISSN the jsonPathISSN to set
     */
    public void setJsonPathISSN(String jsonPathISSN) {
        this.jsonPathISSN = jsonPathISSN;
    }

    /**
     * @return the currentProductISSN
     */
    public String getCurrentProductISSN() {
        return currentProductISSN;
    }

    /**
     * @param currentProductISSN the currentProductISSN to set
     */
    public void setCurrentProductISSN(String currentProductISSN) {
        this.currentProductISSN = currentProductISSN;
    }

	public boolean isTagCreation() {
		return tagCreation;
	}

	public void setTagCreation(boolean tagCreation) {
		this.tagCreation = tagCreation;
	}

	public boolean isViewProduct() {
		return viewProduct;
	}

	public void setViewProduct(boolean viewProduct) {
		this.viewProduct = viewProduct;
	}

	public boolean isUpdateISBN() {
		return updateISBN;
	}

	public void setUpdateISBN(boolean updateISBN) {
		this.updateISBN = updateISBN;
	}

	public String getIsbnPath() {
		return isbnPath;
	}

	public void setIsbnPath(String isbnPath) {
		this.isbnPath = isbnPath;
	}

	public List<Map<String, Object>> getMultiCreateList() {
		return multiCreateList;
	}

	public void setMultiCreateList(List<Map<String, Object>> multiCreateList) {
		this.multiCreateList = multiCreateList;
	}

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public boolean isDistributableLevel() {
		return isDistributableLevel;
	}

	public void setDistributableLevel(boolean distributableLevel) {
		isDistributableLevel = distributableLevel;
	}
}

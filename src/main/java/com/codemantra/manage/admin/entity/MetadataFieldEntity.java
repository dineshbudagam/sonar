/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.codemantra.manage.admin.model.MetaDataFields;

@Document(collection = "metaDataFieldConfig")
public class MetadataFieldEntity extends MBaseEntity{

	@Id
	private String id;
	
	private String metaDataFieldId;
	private String metaDataFieldName;
	private String displayOrderNo;
	private String groupId;
	private String fieldDisplayName;
	private String fieldType;
	private String jsonPath;
	private String jsonPathIndex;
	private Boolean isDisplay;
	private Boolean isEdit;
	private String referencePath;
	private Map<String,Object> uiFlags;
	public String productHierarchyId;
	public String referenceTable;
	public String referenceValue;
	public String[] pageName;
	public String isMandatory;
	public String lookUp;
	private Object fieldDataType;
	public String jsonPathResult;
	public Integer jsonPathResultIndex;
	public List<MetaDataFields> metaDataFieldsData;
	public String jsonDbPathList;
	public String jsonDbPathElement;
	public List<JsonDbPathConditionEntity> jsonDbPathCondition;
	private Boolean unwind;
	private String exactReferencePath; 
	
	/** Added for lookup & master fields **/
	private Boolean lookupField;
	private String lookupCollection;
	public List<JsonDbPathConditionEntity> lookupCriteria;
	private String lookupCodeField;
	private String lookupDescField;
	private List<Map> lookupConditions;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMetaDataFieldId() {
		return metaDataFieldId;
	}
	public void setMetaDataFieldId(String metaDataFieldId) {
		this.metaDataFieldId = metaDataFieldId;
	}
	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}
	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}
	public String getDisplayOrderNo() {
		return displayOrderNo;
	}
	public void setDisplayOrderNo(String displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getFieldDisplayName() {
		return fieldDisplayName;
	}
	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getJsonPath() {
		return jsonPath;
	}
	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}
	public String getJsonPathIndex() {
		return jsonPathIndex;
	}
	public void setJsonPathIndex(String jsonPathIndex) {
		this.jsonPathIndex = jsonPathIndex;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	public Boolean getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}
	public String getReferencePath() {
		return referencePath;
	}
	public void setReferencePath(String referencePath) {
		this.referencePath = referencePath;
	}
	public Map<String, Object> getUiFlags() {
		return uiFlags;
	}
	public void setUiFlags(Map<String, Object> uiFlags) {
		this.uiFlags = uiFlags;
	}
	public Object getFieldDataType() {
		return fieldDataType;
	}
	public void setFieldDataType(Object fieldDataType) {
		this.fieldDataType = fieldDataType;
	}
	public Boolean getUnwind() {
		return unwind;
	}
	public void setUnwind(Boolean unwind) {
		this.unwind = unwind;
	}
	public String getExactReferencePath() {
		return exactReferencePath;
	}
	public void setExactReferencePath(String exactReferencePath) {
		this.exactReferencePath = exactReferencePath;
	}
	public Boolean getLookupField() {
		return lookupField;
	}
	public void setLookupField(Boolean lookupField) {
		this.lookupField = lookupField;
	}
	public String getLookupCollection() {
		return lookupCollection;
	}
	public void setLookupCollection(String lookupCollection) {
		this.lookupCollection = lookupCollection;
	}
	public String getLookupCodeField() {
		return lookupCodeField;
	}
	public void setLookupCodeField(String lookupCodeField) {
		this.lookupCodeField = lookupCodeField;
	}
	public String getLookupDescField() {
		return lookupDescField;
	}
	public void setLookupDescField(String lookupDescField) {
		this.lookupDescField = lookupDescField;
	}
	public List<Map> getLookupConditions() {
		return lookupConditions;
	}
	public void setLookupConditions(List<Map> lookupConditions) {
		this.lookupConditions = lookupConditions;
	}
	public String getProductHierarchyId() {
		return productHierarchyId;
	}
	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}
	public String getReferenceTable() {
		return referenceTable;
	}
	public void setReferenceTable(String referenceTable) {
		this.referenceTable = referenceTable;
	}
	public String getReferenceValue() {
		return referenceValue;
	}
	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}
	public String[] getPageName() {
		return pageName;
	}
	public void setPageName(String[] pageName) {
		this.pageName = pageName;
	}
	public String getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getLookUp() {
		return lookUp;
	}
	public void setLookUp(String lookUp) {
		this.lookUp = lookUp;
	}
	public String getJsonPathResult() {
		return jsonPathResult;
	}
	public void setJsonPathResult(String jsonPathResult) {
		this.jsonPathResult = jsonPathResult;
	}
	public Integer getJsonPathResultIndex() {
		return jsonPathResultIndex;
	}
	public void setJsonPathResultIndex(Integer jsonPathResultIndex) {
		this.jsonPathResultIndex = jsonPathResultIndex;
	}
	public List<MetaDataFields> getMetaDataFieldsData() {
		return metaDataFieldsData;
	}
	public void setMetaDataFieldsData(List<MetaDataFields> metaDataFieldsData) {
		this.metaDataFieldsData = metaDataFieldsData;
	}
	public String getJsonDbPathList() {
		return jsonDbPathList;
	}
	public void setJsonDbPathList(String jsonDbPathList) {
		this.jsonDbPathList = jsonDbPathList;
	}
	public String getJsonDbPathElement() {
		return jsonDbPathElement;
	}
	public void setJsonDbPathElement(String jsonDbPathElement) {
		this.jsonDbPathElement = jsonDbPathElement;
	}
	public List<JsonDbPathConditionEntity> getJsonDbPathCondition() {
		return jsonDbPathCondition;
	}
	public void setJsonDbPathCondition(List<JsonDbPathConditionEntity> jsonDbPathCondition) {
		this.jsonDbPathCondition = jsonDbPathCondition;
	}
	public List<JsonDbPathConditionEntity> getLookupCriteria() {
		return lookupCriteria;
	}
	public void setLookupCriteria(List<JsonDbPathConditionEntity> lookupCriteria) {
		this.lookupCriteria = lookupCriteria;
	}
	
	
}

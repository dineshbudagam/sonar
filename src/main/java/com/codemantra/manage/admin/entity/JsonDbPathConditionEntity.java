/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
25-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
29-12-2017			v1.1       	   	 Shahid	  				Search Service Rework
***********************************************************************************************************************/


package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class JsonDbPathConditionEntity {
	
	@Field("field")
	public String field;
	
	@Field("condition")
	public String condition;
	
	@Field("value")
	public Object value;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}

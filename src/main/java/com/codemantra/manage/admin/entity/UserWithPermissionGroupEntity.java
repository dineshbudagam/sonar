/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-11-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.entity;

import java.util.Date;

public class UserWithPermissionGroupEntity extends MBaseEntity{
	
	private String userId;	
	private String emailId;	
	private String permissionGroupId;
	private String firstName;
	private String lastName;
	private String contactNo;
	private String address;	
	private String city;
	private String zipCode;
	private String profilePicName;
	private String password;
	private Date passwordExpiryDate;
	private Boolean isPasswordExpired;
	private Boolean isRegistered;
	private Date registeredOn;
	private Date invitationMailSentOn;
	
	private PermissionGroupEntity permissionGroup;
	private String onlineStatus;
	
	private Boolean isLocked;
	private Integer lockCount;
	private Date lockedTime;
	private String firstNameFirst;
	private String firstNameLast;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getProfilePicName() {
		return profilePicName;
	}

	public void setProfilePicName(String profilePicName) {
		this.profilePicName = profilePicName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	public Boolean getIsPasswordExpired() {
		return isPasswordExpired;
	}

	public void setIsPasswordExpired(Boolean isPasswordExpired) {
		this.isPasswordExpired = isPasswordExpired;
	}

	public Boolean getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public Date getInvitationMailSentOn() {
		return invitationMailSentOn;
	}

	public void setInvitationMailSentOn(Date invitationMailSentOn) {
		this.invitationMailSentOn = invitationMailSentOn;
	}

	public PermissionGroupEntity getPermissionGroup() {
		return permissionGroup;
	}

	public void setPermissionGroup(PermissionGroupEntity permissionGroup) {
		this.permissionGroup = permissionGroup;
	}

	public String getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	public Integer getLockCount() {
		return lockCount;
	}

	public void setLockCount(Integer lockCount) {
		this.lockCount = lockCount;
	}

	public Date getLockedTime() {
		return lockedTime;
	}

	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}

	public String getFirstNameFirst() {
		return firstNameFirst;
	}

	public void setFirstNameFirst(String firstNameFirst) {
		this.firstNameFirst = firstNameFirst;
	}

	public String getFirstNameLast() {
		return firstNameLast;
	}

	public void setFirstNameLast(String firstNameLast) {
		this.firstNameLast = firstNameLast;
	}
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mAccount")
public class AccountEntity extends MBaseEntity{

	@Id
	private String id;
	
	@Field("accountId")
	private String accountId;
	
	@Field("accountName")
	private String accountName;
	
	@Field("metadataMappedField")
	private List<AccountMappedField> metadataMappedField;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public List<AccountMappedField> getMetadataMappedField() {
		return metadataMappedField;
	}

	public void setMetadataMappedField(List<AccountMappedField> metadataMappedField) {
		this.metadataMappedField = metadataMappedField;
	}
	
	
}

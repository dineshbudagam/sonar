/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "cMetadataGroups")
public class CMetadataGroupEntity{

	@Id
	private String id;
	
	@Field("groupId")
	private String groupNameId;
	
	@Field("groupDisplayName")
	private String groupName;
	
	@Field("groupDisplayOrder")
	private String groupNameDisplayOrder;
	
	@Field("isDisplay")
	private Boolean isDisplay;
	
	@Field("isActive")
	private Boolean isActive;
	
	@Field("isDeleted")
	private Boolean isDeleted;
	
	@Field("createdBy")
	private String createdBy;
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Field("modifiedBy")
	private String modifiedBy;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupNameId() {
		return groupNameId;
	}
	public void setGroupNameId(String groupNameId) {
		this.groupNameId = groupNameId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupNameDisplayOrder() {
		return groupNameDisplayOrder;
	}
	public void setGroupNameDisplayOrder(String groupNameDisplayOrder) {
		this.groupNameDisplayOrder = groupNameDisplayOrder;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	
	
	
}

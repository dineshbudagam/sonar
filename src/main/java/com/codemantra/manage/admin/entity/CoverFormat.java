package com.codemantra.manage.admin.entity;

public class CoverFormat {
	String formatId;
	Integer formatOrder;
	String formatNaming;
	String formatNamingPrefix;
	String formatNamingSuffix;
	String fieldDisplayName;
	Boolean compressedIfMultiple;
	
	
	public String getFieldDisplayName() {
		return fieldDisplayName;
	}
	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}
	public Boolean getCompressedIfMultiple() {
		return compressedIfMultiple;
	}
	public void setCompressedIfMultiple(Boolean compressedIfMultiple) {
		this.compressedIfMultiple = compressedIfMultiple;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public Integer getFormatOrder() {
		return formatOrder;
	}
	public void setFormatOrder(Integer formatOrder) {
		this.formatOrder = formatOrder;
	}
	public String getFormatNaming() {
		return formatNaming;
	}
	public String getFormatNamingPrefix() {
		return formatNamingPrefix;
	}
	public String getFormatNamingSuffix() {
		return formatNamingSuffix;
	}
	public void setFormatNaming(String formatNaming) {
		this.formatNaming = formatNaming;
	}
	public void setFormatNamingPrefix(String formatNamingPrefix) {
		this.formatNamingPrefix = formatNamingPrefix;
	}
	public void setFormatNamingSuffix(String formatNamingSuffix) {
		this.formatNamingSuffix = formatNamingSuffix;
	}
	
	
}

/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
16-06-2017			v1.0      	    Sinduja                 Initial version
***********************************************************************************************************************/

package com.codemantra.manage.admin.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mPassword")
public class PasswordEntity extends MBaseEntity {
	
	@Field("userId")
	private String userId;

	@Field("emailId")
	private String emailId;
	
	@Field("passwordLink")
	private String passwordLink;
	
	@Field("accessKey")
	private String accessKey;	
	
	@Field("password")
	private String password;
	
	@Field("expiryOn")
	private Date expiryOn;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPasswordLink() {
		return passwordLink;
	}

	public void setPasswordLink(String passwordLink) {
		this.passwordLink = passwordLink;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getExpiryOn() {
		return expiryOn;
	}

	public void setExpiryOn(Date expiryOn) {
		this.expiryOn = expiryOn;
	}
	
}
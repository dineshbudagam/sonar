/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.LinkedHashMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "manageConfig")
public class ManageConfigEntity extends MBaseEntity{

	@Id
	private String id;
	
	@Field("config")
	private LinkedHashMap<String, Object> config;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public LinkedHashMap<String, Object> getConfig() {
		return config;
	}

	public void setConfig(LinkedHashMap<String, Object> config) {
		this.config = config;
	}

	
}

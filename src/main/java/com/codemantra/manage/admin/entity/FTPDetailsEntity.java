package com.codemantra.manage.admin.entity;

import java.net.URLEncoder;
import java.text.MessageFormat;

public class FTPDetailsEntity {
	
	String ftpType;
	String protocol;
	String host;
	String port;
	String userName;
	String password;
	String ftpPath;
	Integer connectionTimeout;
	Integer dataTimeout;
	Boolean  isPassive=true;

	private static final String FTP_PATTERN = "{0}://{1}:{3}@{2}:{4}/";
	
	public FTPDetailsEntity() {
		super();
	}
	
	public String getFtpType() {
		return ftpType;
	}
	
	
	public void setFtpType(String ftpType) {
		this.ftpType = ftpType;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFtpPath() {
		return ftpPath;
	}

	public void setFtpPath(String ftpPath) {
		this.ftpPath = ftpPath;
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(Integer connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}
	
	public Integer getDataTimeout() {
		return dataTimeout;
	}

	public void setdataTimeout(Integer dataTimeout) {
		this.dataTimeout = dataTimeout;
	}
	public Boolean getIsPassive() {
		return isPassive;
	}

	public void setIsPassive(Boolean isPassive) {
		this.isPassive = isPassive;
	}

	/*@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FTPDetails [");
		if (ftpType != null)
			builder.append("ftpType=").append(ftpType).append(", ");
		if (protocol != null)
			builder.append("protocol=").append(protocol).append(", ");
		if (host != null)
			builder.append("host=").append(host).append(", ");
		if (port != null)
			builder.append("port=").append(port).append(", ");
		if (userName != null)
			builder.append("userName=").append(userName).append(", ");
		if (password != null)
			builder.append("password=").append(password).append(", ");
		if (ftpPath != null)
			builder.append("ftpPath=").append(ftpPath).append(", ");
		if (connectionTimeout != null)
			builder.append("connectionTimeout=").append(connectionTimeout).append(", ");
		if (dataTimeout != null)
			builder.append("dataTimeout=").append(dataTimeout).append(", ");
		if (isPassive != null)
			builder.append("isPassive=").append(isPassive);
		builder.append("]");
		return builder.toString();
	}*/

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		return MessageFormat.format(FTP_PATTERN,
                this.getProtocol(),  //0
                URLEncoder.encode(this.getUserName()), //1
                this.getHost(), //2
                URLEncoder.encode(this.getPassword()) //3
                ,this.getProtocol() //4
                );
	}
}

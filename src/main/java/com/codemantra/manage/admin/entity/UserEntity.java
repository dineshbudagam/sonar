/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
05-08-2017			v1.0       	   Shahid ul Islam  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.admin.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mUser")

public class UserEntity extends MBaseEntity {
	@Id
	private String id;
	
	@Field("userId")
	private String userId;	

	@Field("emailId")
	private String emailId;
	
	@Field("permissionGroupId")
	private String permissionGroupId;

	@Field("firstName")
	private String firstName;

	@Field("lastName")
	private String lastName;

	@Field("contactNo")
	private String contactNo;

	@Field("address")
	private String address;	

	@Field("city")
	private String city;

	@Field("zipCode")
	private String zipCode;
	
	@Field("profilePicName")
	private String profilePicName;	

	@Field("password")
	private String password;

	@Field("passwordExpiryDate")
	private Date passwordExpiryDate;

	@Field("isPasswordExpired")
	private Boolean isPasswordExpired;
	
	@Field("isRegistered")
	private Boolean isRegistered;
	
	@Field("registeredOn")
	private Date registeredOn;
	
	@Field("invitationMailSentOn")
	private Date invitationMailSentOn;
	
	@Field("userType")
	private String userType;
	
	@Field("userExpiryDate")
	private Date userExpiryDate;
	
	@Field("rights")
	private List<String> rights;
	
	@Field("onlineStatus")
	private String onlineStatus;
	
	private Boolean isLocked;
	private Integer lockCount;
	private Date lockedTime;
	private String firstNameFirst;
	private String firstNameLast;
	
	
/*	@Field("role")
	private String role;*/
	
	/*@Field("screens")
	private List<ScreenAccess> screens;
	
	@Field("permissions")
	private List<PermissionAccess> permissions;
	
	@Field("applications")
	private List<ApplicationAccess> applications;
	
	@Field("activities")
	private List<ActivityAccess> activities;
	
	@Field("formats")
	private List<FormatAccess> formats;
	
	@Field("accounts")
	private List<AccountAccess> accounts;*/
	
/*	@Field("account")
	private List<String> account;
	
	@Field("metadataType")
	private List<String> metadataType;
	
	@Field("imprint")
	private List<String> imprint;
	
	@Field("productCategory")
	private List<String> productCategory;
	
	@Field("metadataGroup")
	private List<String> metadata;
	
	@Field("partnerType")
	private List<String> partner;
	
	@Field("vendor")
	private List<String> vendor;
	
	@Field("application")
	private List<String> application;*/

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

/*	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}*/

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getProfilePicName() {
		return profilePicName;
	}

	public void setProfilePicName(String profilePicName) {
		this.profilePicName = profilePicName;
	}

	public Boolean getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public Date getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public Date getInvitationMailSentOn() {
		return invitationMailSentOn;
	}

	public void setInvitationMailSentOn(Date invitationMailSentOn) {
		this.invitationMailSentOn = invitationMailSentOn;
	}

	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	public Boolean getIsPasswordExpired() {
		return isPasswordExpired;
	}

	public void setIsPasswordExpired(Boolean isPasswordExpired) {
		this.isPasswordExpired = isPasswordExpired;
	}

	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Date getUserExpiryDate() {
		return userExpiryDate;
	}

	public void setUserExpiryDate(Date userExpiryDate) {
		this.userExpiryDate = userExpiryDate;
	}

	public List<String> getRights() {
		return rights;
	}

	public void setRights(List<String> rights) {
		this.rights = rights;
	}

	public String getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	public Integer getLockCount() {
		return lockCount;
	}

	public void setLockCount(Integer lockCount) {
		this.lockCount = lockCount;
	}

	public Date getLockedTime() {
		return lockedTime;
	}

	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}

	public String getFirstNameFirst() {
		return firstNameFirst;
	}

	public void setFirstNameFirst(String firstNameFirst) {
		this.firstNameFirst = firstNameFirst;
	}

	public String getFirstNameLast() {
		return firstNameLast;
	}

	public void setFirstNameLast(String firstNameLast) {
		this.firstNameLast = firstNameLast;
	}


/*	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}*/

/*	public Boolean isActive() {
		return isActive;
	}

	public void setActive(Boolean isActive) {
		this.isActive = isActive;
	}*/

/*	public Boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}*/

/*	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}*/

/*	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}*/

/*	public List<ScreenAccess> getScreens() {
		return screens;
	}

	public void setScreens(List<ScreenAccess> screens) {
		this.screens = screens;
	}

	public List<PermissionAccess> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionAccess> permissions) {
		this.permissions = permissions;
	}

	public List<ApplicationAccess> getApplications() {
		return applications;
	}

	public void setApplications(List<ApplicationAccess> applications) {
		this.applications = applications;
	}

	public List<ActivityAccess> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityAccess> activities) {
		this.activities = activities;
	}

	public List<FormatAccess> getFormats() {
		return formats;
	}

	public void setFormats(List<FormatAccess> formats) {
		this.formats = formats;
	}

	public List<AccountAccess> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountAccess> accounts) {
		this.accounts = accounts;
	}*/
/*	public List<String> getAccount() {
		return account;
	}

	public void setAccount(List<String> account) {
		this.account = account;
	}

	public List<String> getMetadataType() {
		return metadataType;
	}

	public void setMetadataType(List<String> metadataType) {
		this.metadataType = metadataType;
	}

	public List<String> getImprint() {
		return imprint;
	}

	public void setImprint(List<String> imprint) {
		this.imprint = imprint;
	}

	public List<String> getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(List<String> productCategory) {
		this.productCategory = productCategory;
	}

	public List<String> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<String> metadata) {
		this.metadata = metadata;
	}

	public List<String> getPartner() {
		return partner;
	}

	public void setPartner(List<String> partner) {
		this.partner = partner;
	}

	public List<String> getVendor() {
		return vendor;
	}

	public void setVendor(List<String> vendor) {
		this.vendor = vendor;
	}

	public List<String> getApplication() {
		return application;
	}

	public void setApplication(List<String> application) {
		this.application = application;
	}*/

}
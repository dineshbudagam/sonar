package com.codemantra.manage.admin.entity;

import java.net.URL;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "tDownload")
public class DownloadEntity {
	
	@Field("uniqueId")
	private String uniqueId;

	@Field("requestedDate")
	private Date requestedDate;

	@Field("downloadLink")
	private URL downloadLink;

	@Field("userId")
	private String userId;

	@Field("versionId")
	private String versionId;

	@Field("downloadStatus")
	private String downloadStatus;

	@Field("expiryDays")
	private int expiryDays;

	@Field("expiryDate")
	private Date expiryDate;

	@Field("downloadType")
	private String downloadType;

	@Field("ftpUser")
	private String ftpUser;

	@Field("ftpEmailds")
	private List<String> ftpEmailds;

	@Field("createdBy")
	private String createdBy;

	@Field("createdDate")
	private Date createdDate;

	@Field("modifiedBy")
	private String modifiedBy;

	@Field("modifiedDate")
	private Date modifiedDate;

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public URL getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(URL downloadLink) {
		this.downloadLink = downloadLink;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getDownloadStatus() {
		return downloadStatus;
	}

	public void setDownloadStatus(String downloadStatus) {
		this.downloadStatus = downloadStatus;
	}

	public int getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDownloadType() {
		return downloadType;
	}

	public void setDownloadType(String downloadType) {
		this.downloadType = downloadType;
	}

	public String getFtpUser() {
		return ftpUser;
	}

	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}

	public List<String> getFtpEmailds() {
		return ftpEmailds;
	}

	public void setFtpEmailds(List<String> ftpEmailds) {
		this.ftpEmailds = ftpEmailds;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}

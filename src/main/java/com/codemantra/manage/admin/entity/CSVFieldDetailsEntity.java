package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public class CSVFieldDetailsEntity {

	@Field("metaDataFieldName")
	String metaDataFieldName;
	
	@Field("headerDisplayName")
	String headerDisplayName;
	
	@Field("SheetName")
	String sheetName;
	
	@Field("DisplayOrderNo")
	Integer displayOrderNo;
	
	@Field("DateFormat")
	String dateFormat;
	
	@Field("isActive")
	Boolean isActive;
	
	@Field("isDeltaMandatory")
	Boolean isDeltaMandatory;
	
	@Field("isDeltaRemove")
	Boolean isDeltaRemove;
	
	@Field("lookup")
	String lookup;
	
	@Field("default")
	String defaultValue;
	

	@Field("prefix")
	String prefix;
	
	@Field("Prefix1")
	String prefix1;
	
	@Field("Suffix")
	String suffix;
	
	@Field("Suffix1")
	String suffix1;
	
	@Field("fieldType")
	String fieldType;
	
	
	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getHeaderDisplayName() {
		return headerDisplayName;
	}

	public void setHeaderDisplayName(String headerDisplayName) {
		this.headerDisplayName = headerDisplayName;
	}


	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getSheetName() {
		return sheetName;
	}

	public Integer getDisplayOrderNo() {
		return displayOrderNo;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public void setDisplayOrderNo(Integer displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLookup() {
		return lookup;
	}

	public void setLookup(String lookup) {
		this.lookup = lookup;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public String getPrefix1() {
		return prefix1;
	}

	public void setPrefix1(String prefix1) {
		this.prefix1 = prefix1;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getSuffix1() {
		return suffix1;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public void setSuffix1(String suffix1) {
		this.suffix1 = suffix1;
	}

	public Boolean getIsDeltaMandatory() {
		return isDeltaMandatory;
	}

	public void setIsDeltaMandatory(Boolean isDeltaMandatory) {
		this.isDeltaMandatory = isDeltaMandatory;
	}

	public Boolean getIsDeltaRemove() {
		return isDeltaRemove;
	}

	public void setIsDeltaRemove(Boolean isDeltaRemove) {
		this.isDeltaRemove = isDeltaRemove;
	}

	
	
}

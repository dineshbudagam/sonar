package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.codemantra.manage.admin.entity.MBaseEntity;



@Document(collection = "mMenu")
public class MenuEntity  extends MBaseEntity{

	@Id
	private String id;

	@Field("menuId")
	private String menuId;
	
	@Field("menuName")
	private String menuName;
	
	@Field("menuCode")
	private String menuCode;
	
	@Field("displayOrder")
	private Integer displayOrder;
	
	@Field("navigation")
	private String navigation;
	
	@Field("icon")
	private String icon;
	
	
	@Field("subMenu") 
	private List<SubMenuEntity> subMenu;
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public String getNavigation() {
		return navigation;
	}
	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public List<SubMenuEntity> getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(List<SubMenuEntity> subMenu) {
		this.subMenu = subMenu;
	}
	
	
	
	

}

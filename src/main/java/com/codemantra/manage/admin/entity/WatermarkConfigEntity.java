/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mWatermark")
public class WatermarkConfigEntity{

	/*@Id
	private String id;*/
	
	private String formatMap;
	private String formatId;
	private String displayFormatId;
	private String watermarkType;
	private Boolean isActive;
	
	/*public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}*/
	public String getFormatMap() {
		return formatMap;
	}
	public void setFormatMap(String formatMap) {
		this.formatMap = formatMap;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getDisplayFormatId() {
		return displayFormatId;
	}
	public void setDisplayFormatId(String displayFormatId) {
		this.displayFormatId = displayFormatId;
	}
	public String getWatermarkType() {
		return watermarkType;
	}
	public void setWatermarkType(String watermarkType) {
		this.watermarkType = watermarkType;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	
}

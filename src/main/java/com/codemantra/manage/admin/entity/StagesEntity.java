package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Document;

//import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cMetadataStages")
public class StagesEntity {
	
	private String stageId;
	
	private String stageDisplayName;
	
	private String  productHierarchyId;
	
	private String  stageName;
	
	private String stageDisplayOrder;
	
	private Boolean isDisplay;
	
	private Boolean isActive;
	
	private Boolean isDeleted;
	
	private String modifiedBy;
	
	private String createdBy;
	
	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getStageDisplayName() {
		return stageDisplayName;
	}

	public void setStageDisplayName(String stageDisplayName) {
		this.stageDisplayName = stageDisplayName;
	}

	public String getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getStageDisplayOrder() {
		return stageDisplayOrder;
	}

	public void setStageDisplayOrder(String stageDisplayOrder) {
		this.stageDisplayOrder = stageDisplayOrder;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreateProductHierarchyId() {
		return createProductHierarchyId;
	}

	public void setCreateProductHierarchyId(String createProductHierarchyId) {
		this.createProductHierarchyId = createProductHierarchyId;
	}

	private String modifiedOn;
	
	private String createdOn;
	
	private String createProductHierarchyId;

	
	

}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mPartner")
public class PartnerEntity extends MBaseEntity{

	@Id
	private String id;
	
	private String partnerId;
	private String accountId;	
	private String partnerTypeId;	
	private String channelName;	
	private String partnerName;	
	private List<String> formatId;
	private List<String> emailId;	
	private List<String> productHierarchyIds;

	
	/** FTP Details Fields **/
	
	private String ftpUrl;
	private String ftpUserName;
	private String ftpPassword;
	private String ftpPort;
	private String ftpFolderName;
	private String ftpInstructions;
	
	private String erpepswr;
	private String accountName;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getPartnerTypeId() {
		return partnerTypeId;
	}
	public void setPartnerTypeId(String partnerTypeId) {
		this.partnerTypeId = partnerTypeId;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public List<String> getFormatId() {
		return formatId;
	}
	public void setFormatId(List<String> formatId) {
		this.formatId = formatId;
	}
	public List<String> getEmailId() {
		return emailId;
	}
	public void setEmailId(List<String> emailId) {
		this.emailId = emailId;
	}
	public String getFtpUrl() {
		return ftpUrl;
	}
	public void setFtpUrl(String ftpUrl) {
		this.ftpUrl = ftpUrl;
	}
	public String getFtpUserName() {
		return ftpUserName;
	}
	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}
	public String getFtpPassword() {
		return ftpPassword;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public String getFtpPort() {
		return ftpPort;
	}
	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}
	public String getFtpFolderName() {
		return ftpFolderName;
	}
	public void setFtpFolderName(String ftpFolderName) {
		this.ftpFolderName = ftpFolderName;
	}
	public String getFtpInstructions() {
		return ftpInstructions;
	}
	public void setFtpInstructions(String ftpInstructions) {
		this.ftpInstructions = ftpInstructions;
	}
	public String getErpepswr() {
		return erpepswr;
	}
	public void setErpepswr(String erpepswr) {
		this.erpepswr = erpepswr;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public List<String> getProductHierarchyIds() {
		return productHierarchyIds;
	}
	public void setProductHierarchyIds(List<String> productHierarchyIds) {
		this.productHierarchyIds = productHierarchyIds;
	}
	
	
	
}

package com.codemantra.manage.admin.entity;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;



@Document(collection="cMetaRefresh")
public class CMetaRefreshEntity extends MBaseEntity{
	
	@Field
	private String _id;
	private String partnerId;
	private Integer interval;
	private Date startDate;
	private Date lastRun;
	private Date nextRun;
	
	
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public Integer getInterval() {
		return interval;
	}
	public Date getStartDate() {
		return startDate;
	}
	public Date getLastRun() {
		return lastRun;
	}
	public Date getNextRun() {
		return nextRun;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setLastRun(Date lastRun) {
		this.lastRun = lastRun;
	}
	public void setNextRun(Date nextRun) {
		this.nextRun = nextRun;
	}
	
	
} 

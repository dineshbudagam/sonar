/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "metaDataGroupConfig")
public class MetadataGroupEntity extends MBaseEntity{

	@Id
	private String id;
	
	private String groupNameId;
	private String groupName;
	private String groupNameDisplayOrder;
	private Boolean isDisplay;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupNameId() {
		return groupNameId;
	}
	public void setGroupNameId(String groupNameId) {
		this.groupNameId = groupNameId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupNameDisplayOrder() {
		return groupNameDisplayOrder;
	}
	public void setGroupNameDisplayOrder(String groupNameDisplayOrder) {
		this.groupNameDisplayOrder = groupNameDisplayOrder;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	
	
	
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "standardDashboardSummary")
public class StandardDashboardSummaryEntity extends MBaseEntity{
	
	@Id
	private String id; 
	private Integer year;
	private Double totalFreeHrs;
	private Date startDate;
	private Date endDate;
	/*private Float freeHrsUsed;
	private Float paidHrsUsed;*/
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Double getTotalFreeHrs() {
		return totalFreeHrs;
	}
	public void setTotalFreeHrs(Double totalFreeHrs) {
		this.totalFreeHrs = totalFreeHrs;
	}
	/*public Float getFreeHrsUsed() {
		return freeHrsUsed;
	}
	public void setFreeHrsUsed(Float freeHrsUsed) {
		this.freeHrsUsed = freeHrsUsed;
	}
	public Float getPaidHrsUsed() {
		return paidHrsUsed;
	}
	public void setPaidHrsUsed(Float paidHrsUsed) {
		this.paidHrsUsed = paidHrsUsed;
	}*/
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
}

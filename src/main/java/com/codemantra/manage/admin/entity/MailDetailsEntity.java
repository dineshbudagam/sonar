/**********************************************************************************
Date          	Version       Modified By      			Description  
***********		********     *************				*************	
19-06-2017		v1.0       	 Bharath Prasanna Y V	  	Initial Version.
23-06-2017		v1.1       	 Shahid ul Islam	        Added User Master Service Code
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "tMailDetails")
public class MailDetailsEntity {

	@Field("userId")
	private String userId;
	@Field("toEmail")
	private String toEmail;
	@Field("ccEmail")
	private String ccEmail;
	@Field("contents")
	private String contents;
	@Field("subject")
	private String subject;
	@Field("emailFrom")
	private String emailFrom;
	@Field("header")
	private String header;
	@Field("headerValue")
	private String headerValue;
	@Field("companyEmail")
	private String companyEmail;
	@Field("templateData")
	private Map<String, Object> templateData;
	@Field("templateName")
	private String templateName;
	@Field("updateFlag")
	private int updateFlag;
	@Field("createdBy")
	private String createdBy;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public String getCcEmail() {
		return ccEmail;
	}
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getHeaderValue() {
		return headerValue;
	}
	public void setHeaderValue(String headerValue) {
		this.headerValue = headerValue;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public Map<String, Object> getTemplateData() {
		return templateData;
	}
	public void setTemplateData(Map<String, Object> templateData) {
		this.templateData = templateData;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public int getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(int updateFlag) {
		this.updateFlag = updateFlag;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}

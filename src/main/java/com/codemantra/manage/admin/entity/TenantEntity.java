package com.codemantra.manage.admin.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mTenant")
public class TenantEntity {
	
	private String tenantId;
	private String tenantCode;
	private String tenantName;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

}

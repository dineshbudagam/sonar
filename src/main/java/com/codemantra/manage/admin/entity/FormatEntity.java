/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mFormat")
public class FormatEntity extends MBaseEntity{

	@Id
	private String id;
	
	private String formatId;
	private String formatName;
	private String filePurpose;
	private List<String> extension;
	private String namingConvention;
	private String subfolder;
	private String formatTypeId;
	private Integer preference;
	private Boolean singleFile;
	private Boolean isEpubFormat;
	private Boolean isVirtualViewerFormat;
	private Boolean createWidget;
	private String editFormatId;
	private Boolean isEditFormat;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public String getFilePurpose() {
		return filePurpose;
	}
	public void setFilePurpose(String filePurpose) {
		this.filePurpose = filePurpose;
	}
	public List<String> getExtension() {
		return extension;
	}
	public void setExtension(List<String> extension) {
		this.extension = extension;
	}
	public String getNamingConvention() {
		return namingConvention;
	}
	public void setNamingConvention(String namingConvention) {
		this.namingConvention = namingConvention;
	}
	public String getSubfolder() {
		return subfolder;
	}
	public void setSubfolder(String subfolder) {
		this.subfolder = subfolder;
	}
	public String getFormatTypeId() {
		return formatTypeId;
	}
	public void setFormatTypeId(String formatTypeId) {
		this.formatTypeId = formatTypeId;
	}
	public Integer getPreference() {
		return preference;
	}
	public void setPreference(Integer preference) {
		this.preference = preference;
	}
	public Boolean getSingleFile() {
		return singleFile;
	}
	public void setSingleFile(Boolean singleFile) {
		this.singleFile = singleFile;
	}
	public Boolean getIsEpubFormat() {
		return isEpubFormat;
	}
	public void setIsEpubFormat(Boolean isEpubFormat) {
		this.isEpubFormat = isEpubFormat;
	}
	public Boolean getIsVirtualViewerFormat() {
		return isVirtualViewerFormat;
	}
	public void setIsVirtualViewerFormat(Boolean isVirtualViewerFormat) {
		this.isVirtualViewerFormat = isVirtualViewerFormat;
	}
	public Boolean getCreateWidget() {
		return createWidget;
	}
	public void setCreateWidget(Boolean createWidget) {
		this.createWidget = createWidget;
	}
	public String getEditFormatId() {
		return editFormatId;
	}
	public void setEditFormatId(String editFormatId) {
		this.editFormatId = editFormatId;
	}
	public Boolean getIsEditFormat() {
		return isEditFormat;
	}
	public void setIsEditFormat(Boolean isEditFormat) {
		this.isEditFormat = isEditFormat;
	}
	
	
}

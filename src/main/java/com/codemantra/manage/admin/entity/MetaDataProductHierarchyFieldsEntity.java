package com.codemantra.manage.admin.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="cMetadataType")
public class MetaDataProductHierarchyFieldsEntity {
	
	@Field(value="lookUp")
	private String lookUp;
	@Field(value="minlength")
    private String minlength;
	@Field(value="metaDataFieldId")
    private String metaDataFieldId;
	@Field(value="groupId")
    private String groupId;
	@Field(value="referenceValuePath")
    private String referenceValuePath;
	@Field(value="jsonType")
    private String jsonType;
	@Field(value="isActive")
    private Boolean isActive;
	@Field(value="fieldDisplayName")
    private String fieldDisplayName;
	@Field(value="createdOn")
    private String createdOn;
	@Field(value="modifiedOn")
    private String modifiedOn;
	@Field(value="displayOrderNo")
    private String displayOrderNo;
	@Field(value="isDeleted")
    private Boolean isDeleted;
	@Field(value="metaDataFieldName")
    private String metaDataFieldName;
	@Field(value="permissionGroupField")
    private Boolean permissionGroupField;
	@Field(value="modifiedBy")
    private String modifiedBy;
	@Field(value="isMandatory")
    private Boolean isMandatory;
	@Field(value="referenceValue")
    private String referenceValue;
	
	@Field(value="referenceList")
    private String referenceList;
	
	@Field(value="referenceField")
    private String referenceField;
	
	@Field("isDefault")
	private String isDefault;
	

	@Field("defaultValue")
	private String defaultValue;
	
	@Field(value="dependentFields")
    public Object dependentFields;
    @Field(value="displayDelim")
    public Object displayDelim;
	
	@Field(value="maxlength")
    private String maxlength;
	@Field(value="jsonPath")
    private String jsonPath;
	@Field(value="isDisplay")
    private Boolean isDisplay;
	@Field(value="isEdit")
	public boolean isEdit;
	@Field(value="createdBy")
    private String createdBy;
	@Field(value="subGroupId")
    private String subGroupId;
	@Field(value="referencePath")
    private String referencePath;
	@Field(value="permissionGroupValue")
    private String permissionGroupValue;
	@Field(value="stageId")
    public String stageId;
	@Field(value="productHierarchyId")
    public String productHierarchyId;
	@Field(value="editProductHierarchyId")
	public List<String> editProductHierarchyId;
	@Field(value="createProductHierarchyId")
	public List<String> createProductHierarchyId;
	@Field(value="jsonDbPathCondition")
    public List<Object> jsonDbPathCondition;
	@Field(value="fieldDataType")
	public Object fieldDataType;
	@Field(value="supportFieldName")
	public String supportFieldName;
	@Field(value="supportLookupName")
	public String supportLookupName;
	@Field(value="supportUIFieldName")
	public String supportUIFieldName;
	@Field(value="additionalFields")
	public Object additionalFields;
	
	@Field(value="isCascade")
	public boolean isCascade;
	

	@Field(value="isSeries")
	public boolean isSeries;
	
	public boolean getIsCascade() {
		return this.isCascade;
	}
	
	public void setIsCascade(boolean isCascade) {
		this.isCascade = isCascade;
	}
	
	public boolean getIsSeries() {
		return this.isSeries;
	}
	
	public void setIsSeries(boolean isSeries)
	{
		this.isSeries = isSeries;
	}
	public String getSupportLookupName() {
		return supportLookupName;
	}

	public void setSupportLookupName(String supportLookupName) {
		this.supportLookupName = supportLookupName;
	}

	public String getSupportFieldName() {
		return supportFieldName;
	}

	public void setSupportFieldName(String supportFieldName) {
		this.supportFieldName = supportFieldName;
	}

	public boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(boolean isEdit) {
		this.isEdit = isEdit;
	}

	public List<Object> getJsonDbPathCondition() {
		return jsonDbPathCondition;
	}

	public void setJsonDbPathCondition(List<Object> jsonDbPathCondition) {
		this.jsonDbPathCondition = jsonDbPathCondition;
	}

	

	
	   
	 

    public List<String> getEditProductHierarchyId() {
		return editProductHierarchyId;
	}

	public void setEditProductHierarchyId(List<String> editProductHierarchyId) {
		this.editProductHierarchyId = editProductHierarchyId;
	}

	

	

	public  Object getFieldDataType() {
		return fieldDataType;
	}

	public void setFieldDataType( Object fieldDataType) {
		this.fieldDataType = fieldDataType;
	}

	public String getProductHierarchyId() {
		return productHierarchyId;
	}

	public void setProductHierarchyId(String productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}

	public List<String> getCreateProductHierarchyId() {
		return createProductHierarchyId;
	}

	public void setCreateProductHierarchyId(List<String> createProductHierarchyId) {
		this.createProductHierarchyId = createProductHierarchyId;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getLookUp ()
    {
        return lookUp;
    }

    public void setLookUp (String lookUp)
    {
        this.lookUp = lookUp;
    }

    public String getMinlength ()
    {
        return minlength;
    }

    public void setMinlength (String minlength)
    {
        this.minlength = minlength;
    }

    public String getMetaDataFieldId ()
    {
        return metaDataFieldId;
    }

    public void setMetaDataFieldId (String metaDataFieldId)
    {
        this.metaDataFieldId = metaDataFieldId;
    }

    public String getGroupId ()
    {
        return groupId;
    }

    public void setGroupId (String groupId)
    {
        this.groupId = groupId;
    }

    public String getReferenceValuePath ()
    {
        return referenceValuePath;
    }

    public void setReferenceValuePath (String referenceValuePath)
    {
        this.referenceValuePath = referenceValuePath;
    }

    public String getJsonType ()
    {
        return jsonType;
    }

    public void setJsonType (String jsonType)
    {
        this.jsonType = jsonType;
    }

    public Boolean getIsActive ()
    {
        return isActive;
    }

    public void setIsActive (Boolean isActive)
    {
        this.isActive = isActive;
    }

    public String getFieldDisplayName ()
    {
        return fieldDisplayName;
    }

    public void setFieldDisplayName (String fieldDisplayName)
    {
        this.fieldDisplayName = fieldDisplayName;
    }

    public String getCreatedOn ()
    {
        return createdOn;
    }

    public void setCreatedOn (String createdOn)
    {
        this.createdOn = createdOn;
    }

    public String getModifiedOn ()
    {
        return modifiedOn;
    }

    public void setModifiedOn (String modifiedOn)
    {
        this.modifiedOn = modifiedOn;
    }

    public String getDisplayOrderNo ()
    {
        return displayOrderNo;
    }

    public void setDisplayOrderNo (String displayOrderNo)
    {
        this.displayOrderNo = displayOrderNo;
    }

    public Boolean getIsDeleted ()
    {
        return isDeleted;
    }

    public void setIsDeleted (Boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getMetaDataFieldName ()
    {
        return metaDataFieldName;
    }

    public void setMetaDataFieldName (String metaDataFieldName)
    {
        this.metaDataFieldName = metaDataFieldName;
    }

    public Boolean getPermissionGroupField ()
    {
        return permissionGroupField;
    }

    public void setPermissionGroupField (Boolean permissionGroupField)
    {
        this.permissionGroupField = permissionGroupField;
    }

    public String getModifiedBy ()
    {
        return modifiedBy;
    }

    public void setModifiedBy (String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

 /*   public String getFieldDataType ()
    {
        return fieldDataType;
    }

    public void setFieldDataType (String fieldDataType)
    {
        this.fieldDataType = fieldDataType;
    }*/

    public Boolean getIsMandatory ()
    {
        return isMandatory;
    }

    public void setIsMandatory (Boolean isMandatory)
    {
        this.isMandatory = isMandatory;
    }

    public String getReferenceValue ()
    {
        return referenceValue;
    }

    public void setReferenceValue (String referenceValue)
    {
        this.referenceValue = referenceValue;
    }

    public String getMaxlength ()
    {
        return maxlength;
    }

    public void setMaxlength (String maxlength)
    {
        this.maxlength = maxlength;
    }

    public String getJsonPath ()
    {
        return jsonPath;
    }

    public void setJsonPath (String jsonPath)
    {
        this.jsonPath = jsonPath;
    }

    public Boolean getIsDisplay ()
    {
        return isDisplay;
    }

    public void setIsDisplay (Boolean isDisplay)
    {
        this.isDisplay = isDisplay;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getSubGroupId ()
    {
        return subGroupId;
    }

    public void setSubGroupId (String subGroupId)
    {
        this.subGroupId = subGroupId;
    }

    public String getReferencePath ()
    {
        return referencePath;
    }

    public void setReferencePath (String referencePath)
    {
        this.referencePath = referencePath;
    }

    public String getPermissionGroupValue ()
    {
        return permissionGroupValue;
    }

    public void setPermissionGroupValue (String permissionGroupValue)
    {
        this.permissionGroupValue = permissionGroupValue;
    }

	public String getReferenceList() {
		return referenceList;
	}

	public void setReferenceList(String referenceList) {
		this.referenceList = referenceList;
	}

	public String getReferenceField() {
		return referenceField;
	}

	public void setReferenceField(String referenceField) {
		this.referenceField = referenceField;
	}

	public Object getDependentFields() {
		return dependentFields;
	}

	public void setDependentFields(Object dependentFields) {
		this.dependentFields = dependentFields;
	}

	public Object getDisplayDelim() {
		return displayDelim;
	}

	public void setDisplayDelim(Object displayDelim) {
		this.displayDelim = displayDelim;
	}

	public Object getAdditionalFields() {
		return additionalFields;
	}

	public void setAdditionalFields(Object additionalFields) {
		this.additionalFields = additionalFields;
	}

	public String getSupportUIFieldName() {
		return supportUIFieldName;
	}

	public void setSupportUIFieldName(String supportUIFieldName) {
		this.supportUIFieldName = supportUIFieldName;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


}

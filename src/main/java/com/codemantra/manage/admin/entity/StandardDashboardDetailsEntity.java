/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "standardDashboardDetails")
public class StandardDashboardDetailsEntity extends MBaseEntity{
	
	@Id
	private String id;
	private String requestId;
	//private Integer year;
	private String projectName;
	private String projectDescription;
	private Double freeHrsUsed;
	private Double paidHrsUsed;
	private String requestedBy;
	private Date releasedOn;
	
	private String requestedByName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/*public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}*/
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public Double getFreeHrsUsed() {
		return freeHrsUsed;
	}
	public void setFreeHrsUsed(Double freeHrsUsed) {
		this.freeHrsUsed = freeHrsUsed;
	}
	public Double getPaidHrsUsed() {
		return paidHrsUsed;
	}
	public void setPaidHrsUsed(Double paidHrsUsed) {
		this.paidHrsUsed = paidHrsUsed;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public Date getReleasedOn() {
		return releasedOn;
	}
	public void setReleasedOn(Date releasedOn) {
		this.releasedOn = releasedOn;
	}
	public String getRequestedByName() {
		return requestedByName;
	}
	public void setRequestedByName(String requestedByName) {
		this.requestedByName = requestedByName;
	}
	
}

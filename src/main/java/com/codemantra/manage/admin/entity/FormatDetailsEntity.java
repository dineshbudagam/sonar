package com.codemantra.manage.admin.entity;


public class FormatDetailsEntity {
	
	String formatId;
	String formatNaming;
	String formatNamingPrefix;
	String formatNamingSuffix;
	String appleFileType;
	Boolean compressedIfMultiple;
	Boolean epubValidator;
	String ePubVersion;
	String fieldDisplayName;
	Boolean isEpubFormat;
	Boolean isActive;
	private String isDeleted;
	private String createdOn;
	private String createdBy;
	private String modifiedOn;
	private String modifiedBy;

	public FormatDetailsEntity() {
		//TO DO
	}
		
	public FormatDetailsEntity(String formatId, String formatNaming, String formatNamingPrefix, String formatNamingSuffix,
			String appleFileType, Boolean compressedIfMultiple, Boolean epubValidator, String ePubVersion,
			String fieldDisplayName, Boolean isEpubFormat,Boolean isActive, String isDeleted, String createdOn, String createdBy, String modifiedOn,
			String modifiedBy) {
		super();
		this.formatId = formatId;
		this.formatNaming = formatNaming;
		this.formatNamingPrefix = formatNamingPrefix;
		this.formatNamingSuffix = formatNamingSuffix;
		this.appleFileType = appleFileType;
		this.compressedIfMultiple = compressedIfMultiple;
		this.epubValidator = epubValidator;
		this.ePubVersion = ePubVersion;
		this.fieldDisplayName = fieldDisplayName;
		this.isEpubFormat = isEpubFormat;
		this.isActive = isActive;
		this.isDeleted = isDeleted;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
	}

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}

	public String getFormatNaming() {
		return formatNaming;
	}

	public void setFormatNaming(String formatNaming) {
		this.formatNaming = formatNaming;
	}

	public String getFormatNamingPrefix() {
		return formatNamingPrefix;
	}

	public void setFormatNamingPrefix(String formatNamingPrefix) {
		this.formatNamingPrefix = formatNamingPrefix;
	}

	public String getFormatNamingSuffix() {
		return formatNamingSuffix;
	}

	public void setFormatNamingSuffix(String formatNamingSuffix) {
		this.formatNamingSuffix = formatNamingSuffix;
	}

	public String getAppleFileType() {
		return appleFileType;
	}

	public void setAppleFileType(String appleFileType) {
		this.appleFileType = appleFileType;
	}

	public Boolean getCompressedIfMultiple() {
		return compressedIfMultiple;
	}

	public void setCompressedIfMultiple(Boolean compressedIfMultiple) {
		this.compressedIfMultiple = compressedIfMultiple;
	}

	public String getePubVersion() {
		return ePubVersion;
	}

	public void setePubVersion(String ePubVersion) {
		this.ePubVersion = ePubVersion;
	}

	
	
	public Boolean getEpubValidator() {
		return epubValidator;
	}

	public Boolean getIsEpubFormat() {
		return isEpubFormat;
	}

	public void setEpubValidator(Boolean epubValidator) {
		this.epubValidator = epubValidator;
	}

	public void setIsEpubFormat(Boolean isEpubFormat) {
		this.isEpubFormat = isEpubFormat;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.List;
import java.util.Map;

import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.entity.CPartnerConfigEntity;
import com.codemantra.manage.admin.model.ReadExcelData;


public interface GdsService {

	public Map<String, Object> savePartnerConfig(CPartnerConfigEntity modelObj, String loggedUser);

	public Map<String, Object> getPartnerConfigById(String id);

	public Map<String, Object> editPartnerConfig(CPartnerConfigEntity modelObj, String id, String loggedUser);

	public Map<String, Object> deletePartnerConfig(String id, String loggedUser);

	public Map<String, Object> getAllpartnerconfig(String loggedUser);

	public Map<String, Object> retrieveAllGdsConfig(String loggedUser);

	public Boolean readLookupExcel(ReadExcelData modelObj) throws Exception;

	APIResponse<Object> customTemplateMData(List<String> custMdata) throws Exception;

	public Map<String, Object> getAllGdsLookupNames(String loggedUser);
	
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.PermissionGroup;

public interface PermissionGroupService {

	public Map<String, Object> save(PermissionGroup pg, String loggedUser);
	
	public Map<String, Object> edit(PermissionGroup pg, String id, String loggedUser);
	
	public Map<String, Object> delete(String id, String loggedUser);
	
	public Map<String, Object> retrieveAll();
	
	public Map<String, Object> retrieve(String id);

	public Map<String, Object> getAppPermission(String configId, String loggedUser);
	
}

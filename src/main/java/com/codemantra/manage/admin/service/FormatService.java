/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.CApprovalEligibleFormats;
import com.codemantra.manage.admin.model.Format;
import com.codemantra.manage.admin.model.FormatType;

public interface FormatService {
	
	public Map<String, Object> retrieveAllFormatTypes(String loggedUser);
	
	public Map<String, Object> saveFormatType(FormatType modelObj, String loggedUser);

	public Map<String, Object> editFormatType(FormatType modelObj, String id, String loggedUser);

	public Map<String, Object> deleteFormatType(String id, String loggedUser);

	public Map<String, Object> retrieveFormatType(String id, String loggedUser);
	
	public Map<String, Object> retrieveAllFormats(String loggedUser);
	
	public Map<String, Object> saveFormat(Format modelObj, String loggedUser);

	public Map<String, Object> editFormat(Format modelObj, String id, String loggedUser);

	public Map<String, Object> deleteFormat(String id, String loggedUser);

	public Map<String, Object> retrieveFormat(String id, String loggedUser);
	
	public Map<String, Object> retrieveAllCApprovalEligibleFormats(String loggedUser);

	public Map<String, Object> saveCApprovalEligibleFormats(CApprovalEligibleFormats modelObj, String loggedUser);

	public Map<String, Object> editCApprovalEligibleFormats(CApprovalEligibleFormats modelObj, String id, String loggedUser);

	public Map<String, Object> deleteCApprovalEligibleFormats(String id, String loggedUser);

	public Map<String, Object> retrieveCApprovalEligibleFormats(String id, String loggedUser);

	
}

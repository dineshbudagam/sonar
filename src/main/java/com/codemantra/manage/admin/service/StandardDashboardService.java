/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.StandardDashboardDetails;

public interface StandardDashboardService {

	public Map<String, Object> save(StandardDashboardDetails modelObj, String loggedUser);
	
	public Map<String, Object> edit(StandardDashboardDetails modelObj, String id, String loggedUser);
	
	public Map<String, Object> retrieveAllDetailsForYear(Integer year, String loggedUser);
	
	public Map<String, Object> getUserSuggestions(String searchText, String loggedUser);
	
}

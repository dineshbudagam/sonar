/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.List;
import java.util.Map;

import com.codemantra.manage.admin.model.ProductCategory;

public interface ProductCategoryService {
	
	public Map<String, Object> saveMetadataMapping(String metadataFieldName, String loggedUser);
	
	public Map<String, Object> save(ProductCategory modelObj, String loggedUser);
	
	public Map<String, Object> edit(ProductCategory modelObj, String id, String loggedUser);
	
	public Map<String, Object> delete(String id, String loggedUser);
	
	public Map<String, Object> retrieveAll(List<String> accounts, List<String> metadatatype, String loggedUser);
	
	public Map<String, Object> retrieve(String id, String loggedUser);
	
}

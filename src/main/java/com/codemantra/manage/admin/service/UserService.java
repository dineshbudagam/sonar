/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.User;

public interface UserService {

	public Map<String, Object> save(User user, String loggedUser);

	public Map<String, Object> edit(User user, String id, String loggedUser);

	public Map<String, Object> delete(String id, String loggedUser);

	public Map<String, Object> retrieveAll(String loggedUser);

	public Map<String, Object> retrieve(String id);

	// Additional Methods

	public Map<String, Object> activateDeactivateUser(User user, String id, Integer activeStatus, String loggedUser);

	public Map<String, Object> updateUserInfo(User user, String id);

	public Map<String, Object> updateProfilePic(User user, String loggedUser);

	public Map<String, Object> resendInvitationMail(User user, String id, String loggedUser);

	public Map<String, Object> userExists(User user, String loggedUser);

	public Map<String, Object> getSuggestions(User user, String loggedUser);

	public Map<String, Object> retrieveAllWithFilters(User user, String loggedUser);

	public Map<String, Object> pgExists(User user, String loggedUser);

	public Map<String, Object> unlockUser(String id, String loggedUser);

	public boolean sendInvitaiontoAllActiveUsers(String userId);

	public Map<String, Object> retrieveAllSkip(String loggedUser, Integer skip);


}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.Partner;
import com.codemantra.manage.admin.model.PartnerGroup;
import com.codemantra.manage.admin.model.PartnerType;

public interface PartnerService {

	public Map<String, Object> savePartnerType(PartnerType modelObj, String loggedUser);
	
	public Map<String, Object> editPartnerType(PartnerType modelObj, String id, String loggedUser);
	
	public Map<String, Object> deletePartnerType(String id, String loggedUser);
	
	public Map<String, Object> retrievePartnerType(String id, String loggedUser);
	
	public Map<String, Object> retrieveAllPartnerTypes(String loggedUser);
	
	public Map<String, Object> savePartner(Partner modelObj, String loggedUser);
	
	public Map<String, Object> editPartner(Partner modelObj, String id, String loggedUser);
	
	public Map<String, Object> deletePartner(String id, String loggedUser);
	
	public Map<String, Object> retrievePartner(String id, String loggedUser);
	
	public Map<String, Object> retrieveAllPartners(Boolean isExport, String loggedUser);
	
	public Map<String, Object> activateDeactivatePartner(Partner modelObj, String id, Integer activeStatus,String loggedUser);
	
	public Map<String, Object> savePartnerGroup(PartnerGroup modelObj, String loggedUser);
	
	public Map<String, Object> editPartnerGroup(PartnerGroup modelObj, String id, String loggedUser);
	
	public Map<String, Object> deletePartnerGroup(String id, String loggedUser);
	
	public Map<String, Object> retrievePartnerGroup(String id, String loggedUser);
	
	public Map<String, Object> retrieveAllPartnerGroups(String loggedUser);
	
	public Map<String, Object> changeFtpPassword(Partner modelObj, String id, String loggedUser);
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.List;
import java.util.Map;

import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.model.DistributionStatusUpdate;

public interface BaseService {
	
	public <T> Map<String, Object> retrieveAll(T entityObj, String loggedUser);
	
	public <T> Map<String, Object> retrieveImprints(List<String> accounts, String loggedUser);
	
	public Map<String, Object> retrieveViewerEligibleFormats(String loggedUser);
	
	public Map<String, Object> getEligibleFormats(String loggedUser);
	
	public <T> Map<String, Object> retrieveImprintsFromPG(List<String> accounts, String loggedUser);
	
	public Map<String, Object> updateDistributionStatus(DistributionStatusUpdate modelObj, String loggedUser);

	public APIResponse<Object> getStatusbyType(String type);
	
}

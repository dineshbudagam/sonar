/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.model.Role;

public interface RoleService {

	public Map<String, Object> save(Role modelObj, String loggedUser);
	
	public Map<String, Object> edit(Role modelObj, String id, String loggedUser);
	
	public Map<String, Object> delete(String id, String loggedUser);
	
	public Map<String, Object> retrieveAll(String loggedUser);
	
	public Map<String, Object> retrieve(String id, String loggedUser);
	
	public Map<String, Object> activateDeactivateRole(Role modelObj, String id, Integer activeStatus,String loggedUser);
	
	public Map<String, Object> roleExists(Role role, String loggedUser);

	public APIResponse<Object> getAllProductHierarchiesStages();
	
}

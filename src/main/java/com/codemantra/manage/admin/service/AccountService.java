/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.service;

import java.util.Map;

import com.codemantra.manage.admin.model.Account;

public interface AccountService {

	public Map<String, Object> save(Account account, String loggedUser);
	
	public Map<String, Object> retrieve(String id, String loggedUser);
	
	public Map<String, Object> saveMappedFieldValues(Account account, String id, String loggedUser);
	
	public Map<String, Object> edit(Account account, String id, String loggedUser);
	
	public Map<String, Object> delete(String id, String loggedUser);
	
	public Map<String, Object> deleteMappedFieldValue(String id, String mappedFieldValue, String loggedUser);
	
	public Map<String, Object> retrieveAll(String loggedUser);
	
	public Map<String, Object> addNewMappingValue(String value, String loggedUser);	
	
}

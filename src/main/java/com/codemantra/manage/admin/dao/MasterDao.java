/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.dao;

import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.codemantra.manage.admin.entity.AwsAPIEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.StagesEntity;
import com.codemantra.manage.admin.entity.StandardDashboardDetailsEntity;
import com.codemantra.manage.admin.entity.StandardDashboardSummaryEntity;
import com.codemantra.manage.admin.entity.StatusEntity;
import com.codemantra.manage.admin.entity.UserEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CodeDescription;
import com.codemantra.manage.admin.model.KeyValue;

public interface MasterDao {

	public <T> T getEntityObject(T masterObj, String criteriaFieldName, String criteriaFieldValue);
	
	public <T> T getEntityObject(T masterObj, Map<String, Object> criteriaMap);
	
	public <T> T getEntityObject(T masterObj, Map<String, Object> inCriteriaMap, Map<String, Object> ninCriteriaMap);
	
	public String getNextSequenceId(String key);
	
	public <T> boolean saveEntityObject(T masterObj) throws Exception;
	
	public <T> List<?> getAllEntityObjects(T masterObj);
	
	public <T> List<?> getAllEntityObjects(T masterObj, Map<String, Object> criteriaMap);
	
	public <U> List<KeyValue> getKeyValueList(String collection, String keyField, String valueField, String criteriaFieldName, U criteriaFieldValue, String unwindField);
	
	public <U> List<KeyValue> getKeyValueList(String collection, String keyField, String valueField, Map<String, Object> criteriaMap, String unwindField);
		
	public <T> boolean updateEntityObject(T masterObj);	
	
	public <T> boolean updateCollectionDocument(String collection, String criteriaFieldName, String criteriaFieldValue, String updateFieldName, T updateFieldValue);
	
	public <T> boolean updateCollectionDocument(String collection, Map<String, Object> criteriaMap, Map<String, Object> updateMap);
	
	public <T> boolean updateCollectionDocument(String collection, Map<String, Object> criteriaMap, Map<String, Object> updateMap, Map<String, Object> updatePushMap);

	public Integer getCountOfDocuments(String collection, Map<String, Object> criteriaMap);

	public <T> boolean saveEntityObjectList(Collection<T> entityObjList) throws Exception;
	
	public <T> List<?> joinCollections(T masterObj, String collection, Map<String, Object> criteriaMap, LookupOperation lookupOperation,  String unwindField);
	
	public <T> List<?> joinCollections(T masterObj, String collection, List<Criteria> criteriaList, LookupOperation lookupOperation,  String unwindField);
		
	public <T> List<?> runNativeMongoQuery(T masterObj, String query, Object... parameters);
	
	public UserWithPermissionGroupEntity getUserEntity (String loggedUser);
	
	public boolean checkPermission(String moduleId, String accessId, Object activityAccess, UserWithPermissionGroupEntity userEntityObj);
	
	public List<String> getModuleAccessList(String moduleId, String accessId, UserWithPermissionGroupEntity userEntityObj);
	
	public <T> boolean  isNull(T obj);
	
	public List<String> getDistinctFieldValues(String collectionName, Map<String, Object> criteriaMap, String fieldName);
	
	public List<String> getDistinctFieldValues(String collectionName, List<Criteria> criteriaList, String fieldName);

	public List<CodeDescription> getColumnFilterSuggestions(String collection, String codeField, String descField, List<Criteria> criteriaList, String filterText);

	public AwsAPIEntity getAwsCredentials();
	
	public URL uploadFileToS3(AwsAPIEntity apiEntity, String inputFilePath, String s3FileName,
			Date urlExpiryDate, String s3Path);
	
	public List<String> getDistinctFieldValues(String collectionName, Map<String, Object> criteriaMap, String fieldName, String sortBy);
	
	public List<String> getDistinctFieldValues(String collectionName, List<Criteria> criteriaList, String fieldName, String sortBy);
	
	public StandardDashboardSummaryEntity checkStandardDashboardValidDate(Date dateVal);
	
	public List<StandardDashboardDetailsEntity> getStandardDashboardDetails(StandardDashboardSummaryEntity entityObj);
	
	public List<UserEntity> getUserSuggestionsForStandardDashboard(String searchText);

	public List<StagesEntity> getAllProductHierarchiesStages();

	public <T> List<?> joinCollectionsWithSkipLimitSkip(T masterObj, String collection, Map<String, Object> criteriaMap,
			LookupOperation lookupOperation, String unwindField, Integer skip, Integer limit);

	public <T> List<?> joinCollectionsWithSkipLimitSkip(T masterObj, String collection, List<Criteria> criteriaList,
			LookupOperation lookupOperation, String unwindField, Integer skip , Integer limit);

	public Integer getCountOfDocuments(String collection, List<Criteria> criteriaMap);
	
	public <T> List<?> joinCollectionsWithSkipLimit(T masterObj, String collection, Map<String, Object> criteriaMap,
			LookupOperation lookupOperation, String unwindField, Integer skip, Integer limit);

	public <T> List<?> joinCollectionsWithSkipLimit(T masterObj, String collection, List<Criteria> criteriaList,
			LookupOperation lookupOperation, String unwindField, Integer skip , Integer limit);


	public Map<String, String> gethierarchyIdNameMap();
	
	public List<String> getAssetFormatIdList();

	public boolean addFormatPg(String pgId, String collectionName, String formatId, String permissionType);

	public MetadataFieldEntity getMetadataFieldEntity(String metaDataFieldName);

	public List<StatusEntity> getStatusbyType(String type);
	
	
}

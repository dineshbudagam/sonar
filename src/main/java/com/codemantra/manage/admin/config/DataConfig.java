/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
22-05-2017			v1.0       	   Bharath Prasanna	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.admin.config;

import java.util.Arrays;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.web.client.RestTemplate;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;


@Configuration
@ComponentScan(basePackages = "com.codemantra.manage")
@PropertySource("classpath:manage_resources.properties")

public class DataConfig {

	@Value("${spring.data.mongodb.host}")
	private String mongoDBServer ;
	
	@Value("${spring.data.mongodb.database}")
	private String mongoDBName ;
	
	@Value("${spring.data.mongodb.port}")
	private String mongoDBPort ;
	
	@Value("${spring.data.mongodb.username}")
	private String mongoDBUserName ;
	
	@Value("${spring.data.mongodb.password}")
	private String mongoDBPassword ;
	
	
	@Autowired
	public MongoDBCredentials mongoDBCredentials;
	
	@Bean
	public MongoTemplate mongoTemplate() {
		mongoDBCredentials.setPwd(mongoDBPassword);
		mongoDBCredentials.setUser(mongoDBUserName);
		mongoDBCredentials.setPort(mongoDBPort);
		mongoDBCredentials.setHost(mongoDBServer);

		MongoCredential mongoCredential = MongoCredential.createScramSha1Credential(mongoDBUserName,
				mongoDBCredentials.getDefaultDatabaseName(), mongoDBPassword.toCharArray());
		MongoClient mongoClient = new MongoClient(new ServerAddress(mongoDBServer, Integer.valueOf(mongoDBPort)),
				Arrays.asList(mongoCredential));
		SimpleMongoDbFactory dbFact = new SimpleMongoDbFactory(mongoClient,mongoDBCredentials.getDefaultDatabaseName());
		
		return new MongoTenantTemplate(dbFact);
	}
	
    @Bean
    public RestTemplate restTemplate() throws Exception {
        RestTemplate mongoTemplate = new RestTemplate( );
        return mongoTemplate;
    }

    @Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
                  
}
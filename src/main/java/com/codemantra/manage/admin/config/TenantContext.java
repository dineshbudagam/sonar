/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codemantra.manage.admin.config;

import org.springframework.util.StringUtils;

public class TenantContext {
	
	private static final String DEFAULT_TENANT = "manage";
	
	private static final ThreadLocal<String> tenant = new ThreadLocal<>();
	
	public static final String getTenant() {
		String tenantCode = tenant.get();
		if(StringUtils.isEmpty(tenantCode)) {
			tenant.set(DEFAULT_TENANT);
			tenantCode = tenant.get();
		}
		return tenantCode;
	}
	
	public static final void setTenant(String tenantCode) {
		tenant.set(tenantCode);
	}
	
	public static final void remove() {
		tenant.remove();
	}

}


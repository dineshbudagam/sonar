/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
20-11-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.config;

import org.jongo.Jongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;

import com.mongodb.DB;

/**
 * {@link org.springframework.boot.autoconfigure.EnableAutoConfiguration Auto configuration} for {@code Jongo} support.
 *
 * @author Cyril Schumacher
 * @version 1.0
 * @since 2014-12-23
 */
@Configuration
@ConditionalOnClass({Jongo.class})
public class JongoAutoConfiguration {

    //<editor-fold desc="Fields section.">

    /**
     * Mongo
     */
    @Autowired
    protected MongoDbFactory mongo;

    /**
     * Jongo properties.
     */
    @Autowired
    protected MongoProperties properties;

    //</editor-fold>

    //<editor-fold desc="Methods section.">

    /**
     * Create a instance of the {@code Jongo} class.
     *
     * @return The instance of {@code Jongo} class.
     */
    @Bean
    public Jongo jongo() {
        final DB database = mongo.getDb();
        return new Jongo(database);
    }

    //</editor-fold>

}
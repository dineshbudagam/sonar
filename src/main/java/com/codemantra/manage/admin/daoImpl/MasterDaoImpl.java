/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.daoImpl;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.jongo.Jongo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.entity.AwsAPIEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.PermissionGroupEntity;
import com.codemantra.manage.admin.entity.ProductHierarchyEntity;
import com.codemantra.manage.admin.entity.SequenceId;
import com.codemantra.manage.admin.entity.StagesEntity;
import com.codemantra.manage.admin.entity.StandardDashboardDetailsEntity;
import com.codemantra.manage.admin.entity.StandardDashboardSummaryEntity;
import com.codemantra.manage.admin.entity.StatusEntity;
import com.codemantra.manage.admin.entity.UserEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CodeDescription;
import com.codemantra.manage.admin.model.KeyValue;
import com.mongodb.WriteResult;


@Repository
@Qualifier("masterDao")
public class MasterDaoImpl implements MasterDao{
	
	private static final Logger logger = LoggerFactory.getLogger(MasterDaoImpl.class);

	@Value("${INPUT_DATE_FORMAT}")
	String INPUT_DATE_FORMAT;
	
	SimpleDateFormat sdf = "dd/MM/yyyy".equalsIgnoreCase(INPUT_DATE_FORMAT)? new SimpleDateFormat("dd/MM/yyyy"): new SimpleDateFormat("yyyy/MM/dd");
	
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	Jongo jongo;

	/**
	 * This Method is used to retrieve a document from a collection using single criteria for filtering out the document
	 * Parameter 'criteriaFieldName' is used to get the name of the criteria field
	 * Parameter 'criteriaFieldValue' is used to get the value of the criteria field
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntityObject(T masterObj, String criteriaFieldName, String criteriaFieldValue) {
		
		try {
			Query query = new Query();			
			
			query.addCriteria(Criteria.where(criteriaFieldName).is(criteriaFieldValue));  
			logger.info("Query ["+query+"]");
			masterObj = (T) mongoTemplate.findOne(query, masterObj.getClass());
			logger.info("Master Data ["+masterObj+"]");
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Master Data by Name :: "+e);
			masterObj = null;
		}
		
		return masterObj;
	}

	/**
	 * This Method is used to retrieve a document from a collection using multiple criteria for filtering out the document
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the document to be retrieved
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntityObject(T masterObj, Map<String, Object> criteriaMap) {

		try {
			Query query = new Query();

			if (criteriaMap != null && criteriaMap.size() > 0) {
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
				query.addCriteria(criteria);
			}

			logger.info("Query [" + query + "]");
			masterObj = (T) mongoTemplate.findOne(query, masterObj.getClass());
			logger.info("Master Data [" + masterObj + "]");
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Master Data by Name :: " + e);
			masterObj = null;
		}

		return masterObj;
	}
	
	/*
	 * This Method is used to retrieve a document from a collection using multiple criteria (both for Equal/IN & NOTEqual.NIN) for filtering out the document
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the document to be retrieved
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntityObject(T masterObj, Map<String, Object> inCriteriaMap, Map<String, Object> ninCriteriaMap) {

		try {
			Query query = new Query();
			Criteria inCriteria = new Criteria();
			Criteria ninCriteria = new Criteria();

			if (!isNull(inCriteriaMap)) {
				List<Criteria> criterias = new ArrayList<Criteria>(inCriteriaMap.size());
				inCriteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				inCriteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			}
			
			if (!isNull(ninCriteriaMap)) {
				List<Criteria> criterias = new ArrayList<Criteria>(ninCriteriaMap.size());
				ninCriteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).nin((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).ne(v));
				});
				ninCriteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			}
			
			query.addCriteria(new Criteria().andOperator(inCriteria, ninCriteria));

			logger.info("Query [" + query + "]");
			masterObj = (T) mongoTemplate.findOne(query, masterObj.getClass());
			logger.info("Master Data [" + masterObj + "]");
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Master Data by Name :: " + e);
			masterObj = null;
		}

		return masterObj;
	}
	
	/**
	 * This Method is used to retrieve the next sequence id from a particular sequence
	 * Parameter 'key' filters out the sequence whose next value is to be retrieved
	 */	
	
	@Override
	public String getNextSequenceId(String key){
		logger.info("Sequence Key ["+key+"]");
		SequenceId seqId = null;		
		
		try{
			Query query = new Query(Criteria.where("_id").is(key));	
			Update update = new Update();
		    update.inc("seq", 1);
			FindAndModifyOptions options = new FindAndModifyOptions();
			options.returnNew(true);
			
			seqId =  mongoOperations.findAndModify(query, update, options, SequenceId.class);
		}catch (Exception e) {
			logger.error("Exception occured while generating id for Master :: "+e);
			seqId = null;
		}
		
		if(null == seqId)
			return "-1";
		else
			return(String.format("%05d", seqId.getSeq()));
	}

	/**
	 * This Method is used to save a document inside a collection using Entity Object of the collection
	 * Parameter 'masterObj' is used to get the entity object(document) to be saved
	 */

	@Override
	public <T> boolean saveEntityObject(T masterObj) throws Exception {
		boolean result = false;
		try {
			
			mongoTemplate.insert(masterObj);			
			result = true;
			
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			throw e;		
		} 
		return result;
	}

	/**
	 * This Method is used to retrieve all the documents inside a collection, using Entity Class T to identify the collection
	 */
	
	@Override
	public <T> List<?> getAllEntityObjects(T masterObj) {
		List<?> masterObjects = null;
		
		try {				
			Query query = new Query();			
			/** Deleted Master Objects should not be retrieved **/
			query.addCriteria(Criteria.where("isDeleted").is(false));			
			logger.info("Query ["+query+"]");
			
			masterObjects = mongoOperations.find(query, masterObj.getClass());				
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
			throw e;
		}
		return masterObjects;
	}
	
	
	/**
	 * This Method is used to retrieve multiple documents from a collection, filtered using a set of criteria
	 * The class Type 'T' of parameter masterObj is used to identify the collection and the bucket class
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the documents to be retrieved
	 */
	
	@Override
	public <T> List<?> getAllEntityObjects(T masterObj, Map<String, Object> criteriaMap) {
		List<?> masterObjects = null;
		
		try {				
			Query query = new Query();
			
			if(criteriaMap != null && criteriaMap.size() > 0){
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}
			
			
			logger.info("Query ["+query+"]");
			
			masterObjects = mongoOperations.find(query, masterObj.getClass());			
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
			throw e;
		}
		return masterObjects;
	}
	
	/**
	 * This Method is used to retrieve key-value pairs across multiple documents/sub-documents in a collection filtered using single criteria
	 * Parameter 'keyField' is used to get the name of the key field
	 * Parameter 'valueField' is used to get the name of the value field
	 * Parameter 'criteriaFieldName' is used to get the name of the field to be used for filtering out the documents/sub-documents
	 * Parameter 'criteriaFieldValue' is used to get the value of the criteria field for filtering out the documents/sub-documents
	 * Parameter 'unwindField', is used to get the name of the unwind field, if unwinding is needed else null
	 */
	
	public <U> List<KeyValue> getKeyValueList(String collection, String keyField, String valueField, String criteriaFieldName, U criteriaFieldValue, String unwindField) {
		
		List<KeyValue> result = null;
		try {
			
			Aggregation aggregation;
			
			if(null == unwindField){
				aggregation = newAggregation(
						match(Criteria.where(criteriaFieldName).is(criteriaFieldValue)),
						project(Fields.fields().and("key", keyField).and("value",valueField))	        
				    );
			}else{
				aggregation = newAggregation(
						unwind(unwindField),
						match(Criteria.where(criteriaFieldName).is(criteriaFieldValue)),
						project(Fields.fields().and("key", keyField).and("value",valueField))
				    );
			}
			
			AggregationResults<KeyValue> groupResults =  mongoTemplate.aggregate(aggregation, collection, KeyValue.class);
		    result = groupResults.getMappedResults();			
		} catch (Exception e) {
			logger.info("Exception occured while updating Master :: "+e);
			result = null;
		}
		
		return result;
	}
	
	/**
	 * This Method is used to retrieve key-value pairs across multiple documents/sub-documents in a collection filtered using multiple criteria
	 * Parameter 'keyField' is used to get the name of the key field
	 * Parameter 'valueField' is used to get the name of the value field
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the documents/sub-documents
	 * Parameter 'unwindField', is used to get the name of the unwind field, if unwinding is needed else null
	 */
	
	public <U> List<KeyValue> getKeyValueList(String collection, String keyField, String valueField, Map<String, Object> criteriaMap, String unwindField) {
		
		List<KeyValue> result = null;
		Criteria criteria =  new Criteria();
		try {				
			if(criteriaMap != null && criteriaMap.size() > 0){
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));					
			}
			
			Aggregation aggregation;
			
			if(null == unwindField){
				aggregation = newAggregation(
						match(criteria),
						project(Fields.fields().and("key", keyField).and("value",valueField))	        
				    );
			}else{
				aggregation = newAggregation(
						unwind(unwindField),
						match(criteria),
						project(Fields.fields().and("key", keyField).and("value",valueField))		        
				    );
			}
			
			AggregationResults<KeyValue> groupResults =  mongoTemplate.aggregate(aggregation, collection, KeyValue.class);
		    result = groupResults.getMappedResults();			
		} catch (Exception e) {
			logger.info("Exception occured while Retrieving Data in getSingleFieldValueList :: "+e);
			result = null;
		}
		
		return result;
	}

	/**
	 * This Method is used to save/update (upsert) a document inside a collection using Entity Object of the collection
	 */
	@Override
	public <T> boolean updateEntityObject(T masterObj) {
		boolean result = false;
		try {

			mongoTemplate.save(masterObj);
			result = true;

		} catch (Exception e) {
			logger.info("Exception occured while updating Master :: "+e);
			result = false;			
		} 
		return result;
	}

	/**
	 * This Method is used to update a document/documents inside a collection without using Entity Object of the collection with single criteria and single field update
	 * Parameter 'criteriaFieldName' is used to get the name of the criteria field
	 * Parameter 'criteriaFieldValue' is used to get the value of the criteria field
	 * Parameter 'updateFieldName' is used to get the name of the field to be updated in the document
	 * Parameter 'updateFieldValue' is used to get the value to be updated for the 'updateFieldName'
	 */

	@Override
	public <T> boolean updateCollectionDocument(String collection, String criteriaFieldName, String criteriaFieldValue, String updateFieldName, T updateFieldValue){
		WriteResult wrResult;
		try{
			Query query = new Query();
			query.addCriteria(Criteria.where(criteriaFieldName).is(criteriaFieldValue));

			Update update = new Update();

			update.set(updateFieldName, updateFieldValue);
			
			wrResult = mongoTemplate.updateMulti(query, update, collection);
			
			logger.info("No. of documents updated "+wrResult.getN());
		}catch(Exception e){
			logger.error("Exception :: ", e);
			return false;
		}
		return (wrResult.getN() > 0);
		
	}
	
	
	/**
	 * This Method is used to update a document/documents inside a collection without using Entity Object of the collection with multiple criteria and multiple field update
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the documents to be updated
	 * Parameter 'updateMap' is used to get the names and new values of the fields to updated in the document
	 */
	@Override
	public <T> boolean updateCollectionDocument(String collection, Map<String, Object> criteriaMap, Map<String, Object> updateMap){
		WriteResult wrResult;
		try{
			Query query = new Query();			
			if(criteriaMap != null && criteriaMap.size() > 0){
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}

			Update update = new Update();			
			updateMap.forEach((k,v) -> update.set(k, v));
			
			wrResult = mongoTemplate.updateMulti(query, update, collection);
			
			logger.info("No. of documents updated "+wrResult.getN());
		}catch(Exception e){
			logger.error("Exception :: ", e);
			return false;
		}
		return (wrResult.getN() > 0);		
	}
	
	/**
	 * This Method is used to update a document/documents inside a collection without using Entity Object of the collection with multiple criteria and multiple field update
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the documents to be updated
	 * Parameter 'updateMap' is used to get the names and new values of the fields to updated in the document
	 */
	@Override
	public <T> boolean updateCollectionDocument(String collection, Map<String, Object> criteriaMap, Map<String, Object> updateMap, Map<String, Object> updatePushMap){
		WriteResult wrResult;
		try{
			Query query = new Query();			
			if(criteriaMap != null && criteriaMap.size() > 0){
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}

			Update update = new Update();			
			updateMap.forEach((k,v) -> update.set(k, v));
			updatePushMap.forEach((k,v) -> update.push(k, v));
			
			wrResult = mongoTemplate.updateMulti(query, update, collection);
			
			logger.info("No. of documents updated "+wrResult.getN());
		}catch(Exception e){
			logger.error("Exception :: ", e);
			return false;
		}
		return (wrResult.getN() > 0);		
	}
	
	/**
	 * This Method is used to get the number of documents inside a collection
	 * Parameter 'criteriaMap' is used to get the criteria for filtering out the documents
	 */

	@Override
	public Integer getCountOfDocuments(String collection, Map<String, Object> criteriaMap) {
		Integer docCount=0;
		
		try {				
			Query query = new Query();	
			
			if(criteriaMap != null && criteriaMap.size() > 0){
				Criteria criteria = null;
				List<Criteria> criterias = new ArrayList<>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}
			logger.info("Query ["+query+"]");
			docCount = (int) mongoTemplate.count(query, collection);
					
			logger.info("Document Count -"+docCount);
			
		} catch (Exception e) {
			logger.error("Exception :: ", e);
		}
		return docCount;
	}
	
	@Override
	public Integer getCountOfDocuments(String collection, List<Criteria> criteriaMap) {
		Integer docCount=0;
		
		try {				
			Query query = new Query();	
			Criteria criteria = new Criteria();
			if(criteriaMap != null && criteriaMap.size() > 0){
				criteria = new Criteria().andOperator(criteriaMap.toArray(new Criteria[criteriaMap.size()]));
			    query.addCriteria(criteria);
			}
			logger.info("Query ["+query+"]");
			docCount = (int) mongoTemplate.count(query, collection);
					
			logger.info("Document Count -"+docCount);
			
		} catch (Exception e) {
			logger.error("Exception :: ", e);
		}
		return docCount;
	}


	/**
	 * This Method is used to save a List of documents inside a collection Parameter
	 * 'entityObjList' is used to get the entity object(document) List to be saved
	 */

	@Override
	public <T> boolean saveEntityObjectList(Collection<T> entityObjList) throws Exception {
		boolean result = false;
		try {

			mongoTemplate.insertAll(entityObjList);
			result = true;

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			throw e;
		}
		return result;
	}
	
	/*<p>
	 * This Method is used to join two collections
	 * 'masterObj' is used to get bean which will store the joined result
	 * 'collection' is used to get source collection name - (from collection)
	 * 'criteriaMap' is used to filter out the result
	 * 'lookupOperation' is used get the information about the join - the other joining collection name, the linking field names ( local field & foreign field), & join result alias
	 * 'unwindField' is used to get the field name for unwinding, if unwinding is required, else null</p>
	 */

	@Override
	public <T> List<?> joinCollectionsWithSkipLimit(T masterObj, String collection, Map<String, Object> criteriaMap,
			LookupOperation lookupOperation, String unwindField, Integer skip , Integer limit) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			if (criteriaMap != null && criteriaMap.size() > 0) {
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			}

			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria) , skip(skip) , limit(limit));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField) , skip(skip) , limit(limit));
			}
			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	@Override
	public <T> List<?> joinCollectionsWithSkipLimit(T masterObj, String collection, List<Criteria> criteriaList,
			LookupOperation lookupOperation, String unwindField , Integer skip , Integer limit) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			
			criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			
			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria), skip(skip) , limit(limit));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField), skip(skip) , limit(limit));
			}
			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	
	@Override
	public <T> List<?> joinCollections(T masterObj, String collection, Map<String, Object> criteriaMap,
			LookupOperation lookupOperation, String unwindField) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			if (criteriaMap != null && criteriaMap.size() > 0) {
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			}

			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField));
			}

			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	@Override
	public <T> List<?> joinCollections(T masterObj, String collection, List<Criteria> criteriaList,
			LookupOperation lookupOperation, String unwindField) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			
			criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			
			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField));
			}

			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	/**
	 * This Method is used to run native mongodb queries
	 * 'masterObj' is used to get bean which will store the query result
	 * 'query' is used to get query to run
	 * 'parameters' is used to query parameter array
	 */
	
	public <T> List<?> runNativeMongoQuery(T masterObj, String query, Object... parameters) {
		List<?> results = new LinkedList<>();
		
		logger.info("Query ["+query+"]");
		logger.info("parameters ["+parameters+"]");

		try {
			if(parameters.length == 0)
				results = jongo.runCommand(query).field("result").as(masterObj.getClass());
			else
				results = jongo.runCommand(query, parameters).field("result").as(masterObj.getClass());
			
		} catch (Exception e) {
			logger.error("Exception in executing native mongo query :: ", e);
		}

		return results;
	}
	
	@SuppressWarnings("unchecked")
	public UserWithPermissionGroupEntity getUserEntity (String loggedUser) {
		UserWithPermissionGroupEntity userEntityObj = null;
		
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			
			criteriaMap.put("userId", loggedUser);
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			
			LookupOperation lookupOperation = LookupOperation.newLookup()
				    .from("mPermissionGroup")
				    .localField("permissionGroupId")
				    .foreignField("permissionGroupId")
				    .as("permissionGroup");
			
			List<UserWithPermissionGroupEntity> userEntityList = (List<UserWithPermissionGroupEntity>) joinCollections(new UserWithPermissionGroupEntity(), "mUser",  criteriaMap, lookupOperation, "permissionGroup" );
		
			if(userEntityList.size() == 1 && !userEntityList.get(0).getPermissionGroup().getIsDeleted())
				userEntityObj = userEntityList.get(0);

			
		}catch(Exception e){
			logger.error("Exception in getUserEntity :: " , e);
			userEntityObj = null;
		}
		
		return userEntityObj;
	}

	@Override
	public boolean checkPermission(String moduleId, String accessId, Object activityAccess, UserWithPermissionGroupEntity userEntityObj) {
		boolean returnFlg = false;
		List<KeyValue> result = new ArrayList<>();
		
		try {
			if(null != userEntityObj) {
				if(CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAdministrator()).stream().anyMatch(obj -> obj.equals(accessId))) {
					
					List<Criteria> criterias = new ArrayList<Criteria>();	
					criterias.add(Criteria.where("roleId").is(userEntityObj.getPermissionGroup().getRoleId()));
					criterias.add(Criteria.where("isDeleted").is(Boolean.FALSE));
					criterias.add(Criteria.where("module.moduleId").is(moduleId));
					criterias.add(Criteria.where("module.submodule.accessId").is(accessId));
					if(activityAccess instanceof Collection<?>)
						criterias.add(Criteria.where("module.submodule.activityAccess").in((Collection<?>) activityAccess));
					else
						criterias.add(Criteria.where("module.submodule.activityAccess").is(activityAccess));
					
					Criteria criteria =  new Criteria();					
				    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));					

				    Aggregation aggregation = newAggregation(
							unwind("module"),
							unwind("module.submodule"),
							match(criteria),
							project(Fields.fields().and("key", "roleId").and("value","roleName"))		        
					    );
				    
				    AggregationResults<KeyValue> groupResults =  mongoTemplate.aggregate(aggregation, "mRole", KeyValue.class);
				    result = groupResults.getMappedResults();
					
					if (!result.isEmpty())
						returnFlg = true;
				}
			}
		
		}catch(Exception e) {
			logger.error("Exception in checkPermission :: " , e);
			returnFlg = false;
		}
		
		return returnFlg;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getModuleAccessList(String moduleId, String accessId, UserWithPermissionGroupEntity userEntityObj) {
		List<String> accessList = new ArrayList<>();
		List<KeyValue> result = new ArrayList<>();
		
		try {
			if(null != userEntityObj) {				
				
				List<Criteria> criterias = new ArrayList<Criteria>();	
				criterias.add(Criteria.where("roleId").is(userEntityObj.getPermissionGroup().getRoleId()));
				criterias.add(Criteria.where("isDeleted").is(Boolean.FALSE));
				criterias.add(Criteria.where("module.moduleId").is(moduleId));
				criterias.add(Criteria.where("module.submodule.accessId").is(accessId));
				
				Criteria criteria =  new Criteria();					
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));					

			    Aggregation aggregation = newAggregation(
						unwind("module"),
						unwind("module.submodule"),
						match(criteria),
						project(Fields.fields().and("key", "module.submodule.accessId").and("value","module.submodule.activityAccess"))		        
				    );
			    
			    AggregationResults<KeyValue> groupResults =  mongoTemplate.aggregate(aggregation, "mRole", KeyValue.class);
			    
			    result = groupResults.getMappedResults();
				
				if (!result.isEmpty())
					accessList = (List<String>) result.get(0).getValue();
			}
		
		}catch(Exception e) {
			logger.error("Exception in checkPermission :: " , e);
		}
		
		return accessList;		
	}
	
	public <T> boolean  isNull(T obj){
		
		if(obj instanceof Collection<?>)
			return (obj == null || ((Collection<?>) obj).isEmpty());
		else if(obj instanceof String)
			return (obj == null || ((String) obj).trim().isEmpty());
		else
			return obj == null;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctFieldValues(String collectionName, Map<String, Object> criteriaMap, String fieldName) {
		List<String> valueList = new ArrayList<>();
		
		try {				
			Query query = new Query();
			
			if(criteriaMap != null && criteriaMap.size() > 0){
				Criteria criteria = new Criteria();
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}			
			
			query.fields().include(fieldName).exclude("_id");	
			
			valueList = mongoTemplate.getCollection(collectionName).distinct(fieldName,
					query.getQueryObject());	
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
		}
		return valueList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctFieldValues(String collectionName, List<Criteria> criteriaList, String fieldName) {
		List<String> valueList = new ArrayList<>();
		
		try {				
			Query query = new Query();
			
			if(criteriaList != null && criteriaList.size() > 0){
				Criteria criteria = new Criteria();
			    criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			    query.addCriteria(criteria);			    
			}			
			
			query.fields().include(fieldName).exclude("_id");			
			valueList = mongoTemplate.getCollection(collectionName).distinct(fieldName,
					query.getQueryObject());	
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
		}
		return valueList;
	}

	@Override
	public List<CodeDescription> getColumnFilterSuggestions(String collection, String codeField, String descField,
			List<Criteria> criteriaList, String filterText) {
		List<CodeDescription> resultList = new ArrayList<>();
		
		try {
			Criteria criteria = new Criteria();		
			Aggregation aggregation;
			AggregationResults<CodeDescription> groupResults = null;
			criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			aggregation = newAggregation(
					match(criteria), 
					project(Fields.fields().and("code", codeField).and("description",descField)), 
					limit(10));
			
			groupResults = mongoTemplate.aggregate(aggregation, collection,
					CodeDescription.class);
			
			resultList = groupResults.getMappedResults();
			
		}catch (Exception e) {
			logger.error("Exception occured in getColumnFilterSuggestions :: "+e.getMessage());
		}
		
		return resultList;
	}
	
	@Override
	public AwsAPIEntity getAwsCredentials() {
		AwsAPIEntity apiEntity = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("company").is("US"));
			apiEntity = (AwsAPIEntity) mongoOperations.findOne(query, AwsAPIEntity.class);

		} catch (Exception e) {
			logger.error("Exception occured while AWS credentials :: " + e.getMessage());
			throw e;
		}
		return apiEntity;
	}
	
	@Override
	public URL uploadFileToS3(AwsAPIEntity apiEntity, String inputFilePath, String S3FileName,
			Date urlExpiryDate, String s3Path) {

		URL url = null;

		try {
			BasicAWSCredentials awsCreds = null;
			awsCreds = new BasicAWSCredentials(apiEntity.getAccessKey(), apiEntity.getSecretKey());
			AmazonS3 s3client = new AmazonS3Client(awsCreds);

			String fileName = s3Path + File.separator + S3FileName;
			s3client.putObject(new PutObjectRequest(apiEntity.getBucketName(), fileName, new File(inputFilePath))
					.withCannedAcl(CannedAccessControlList.PublicRead));

			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
					apiEntity.getBucketName(), fileName);
			generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
			generatePresignedUrlRequest.setExpiration(urlExpiryDate);

			s3client.setRegion(Region.getRegion(Regions.fromName(apiEntity.getRegion())));
			s3client.setS3ClientOptions(S3ClientOptions.builder().setAccelerateModeEnabled(true).build());
			url = s3client.generatePresignedUrl(generatePresignedUrlRequest);

		} catch (Exception e) {
			e.printStackTrace();
			url = null;
		}

		return url;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctFieldValues(String collectionName, Map<String, Object> criteriaMap, String fieldName, String sortBy) {
		List<String> valueList = new ArrayList<>();
		
		try {				
			Query query = new Query();
			Criteria criteria = new Criteria();
			
			if(!isNull(criteriaMap)){				
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());				
				criteriaMap.forEach((k,v) -> {
					if(v instanceof Collection<?>){
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					}else
						criterias.add(Criteria.where(k).is(v));
				});
			    criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			    query.addCriteria(criteria);
			}			
			
			query.fields().include(fieldName).exclude("_id");	
			
			if(null != sortBy) {			
				Aggregation aggregation;
				AggregationResults<CodeDescription> groupResults = null;
				List<CodeDescription> resultList = new ArrayList<>();
				
				aggregation = newAggregation(
						match(criteria),
						group(fieldName, sortBy), 
						sort(new Sort(Sort.Direction.ASC, "_id."+sortBy)),
						project(Fields.fields().and("code", "_id."+fieldName))
						);				
				
				groupResults = mongoTemplate.aggregate(aggregation, collectionName, CodeDescription.class);				
				resultList = groupResults.getMappedResults();				
				valueList = resultList.stream().map(data -> data.getCode()).collect(Collectors.toList());
				
				
			}else {
				valueList = mongoTemplate.getCollection(collectionName).distinct(fieldName,
						query.getQueryObject());
			}	
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
		}
		return valueList;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctFieldValues(String collectionName, List<Criteria> criteriaList, String fieldName, String sortBy) {
		List<String> valueList = new ArrayList<>();
		
		try {				
			Query query = new Query();
			Criteria criteria = new Criteria();
			
			if(!isNull(criteriaList)){
			    criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			    query.addCriteria(criteria);			    
			}			
			
			query.fields().include(fieldName).exclude("_id");
			if(null != sortBy) {			
				Aggregation aggregation;
				AggregationResults<CodeDescription> groupResults = null;
				List<CodeDescription> resultList = new ArrayList<>();
				
				aggregation = newAggregation(
						match(criteria),
						group(fieldName, sortBy), 
						sort(new Sort(Sort.Direction.ASC, "_id."+sortBy)),
						project(Fields.fields().and("code", "_id."+fieldName))
						);				
				
				groupResults = mongoTemplate.aggregate(aggregation, collectionName, CodeDescription.class);				
				resultList = groupResults.getMappedResults();				
				valueList = resultList.stream().map(data -> data.getCode()).collect(Collectors.toList());
				
				
			}else {
				valueList = mongoTemplate.getCollection(collectionName).distinct(fieldName,
						query.getQueryObject());
			}	
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
		}
		return valueList;
	}
	
	@Override
	public StandardDashboardSummaryEntity checkStandardDashboardValidDate(Date dateVal){
		StandardDashboardSummaryEntity summaryEntityObj = null;		
		
		try{
			
			List<StandardDashboardSummaryEntity> objList = new ArrayList<>();
			
			List<Criteria> criteriaList = new ArrayList<>();
			criteriaList.add(Criteria.where("startDate").lte(dateVal));
			criteriaList.add(Criteria.where("endDate").gte(dateVal));
			criteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
			criteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
			
			Query query = new Query();
			query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));
						
			objList = mongoOperations.find(query, StandardDashboardSummaryEntity.class);
			
			if(objList.size() == 1)
				summaryEntityObj = objList.get(0);
			
		}catch (Exception e) {
			logger.error("Exception occured in checkStandardDashboardValidDate :: "+e);
			summaryEntityObj = null;
		}
		
		return summaryEntityObj;
	}
	
	
	public List<StandardDashboardDetailsEntity> getStandardDashboardDetails(StandardDashboardSummaryEntity entityObj){
		
		List<StandardDashboardDetailsEntity> objList = new ArrayList<>();
		
		try{
			
			List<Criteria> criteriaList = new ArrayList<>();
			criteriaList.add(Criteria.where("releasedOn").gte(entityObj.getStartDate()));
			criteriaList.add(Criteria.where("releasedOn").lte(entityObj.getEndDate()));
			criteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
			criteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
			
			Query query = new Query();
			
			
			query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));
			objList = mongoOperations.find(query, StandardDashboardDetailsEntity.class);			
			
		}catch (Exception e) {
			logger.error("Exception occured in checkStandardDashboardValidDate :: "+e);
			objList = null;
		}
		
		return objList;
		
	}

	@Override
	public List<UserEntity> getUserSuggestionsForStandardDashboard(String searchText) {
		List<UserEntity> users = null;
		
		try {				
			Query query = new Query();			
			/** Deleted Master Objects should not be retrieved **/
			
			Criteria[] searchTextCriteria = new Criteria[] {Criteria.where("firstName").regex(searchText,"i"), Criteria.where("emailId").regex(searchText,"i")};
						
			query.addCriteria(new Criteria().andOperator(new Criteria[] {Criteria.where("isDeleted").is(Boolean.FALSE),
					Criteria.where("isActive").is(Boolean.TRUE), new Criteria().orOperator(searchTextCriteria)}));
			
			users = mongoOperations.find(query, UserEntity.class);			
			
		} catch (Exception e) {
			logger.error("Exception occured while retrieving Account list :: "+e.getMessage());
			throw e;
		}
		return users;
	}

	/**
	 * we are retrieve the Stages from the database which are in active and delete has false 
	 *  retrieving all entities from the cMetadataStages.
	 */
	@Override
	public List<StagesEntity> getAllProductHierarchiesStages() {
		
	
	List<StagesEntity> stagesList = null;
	try {
		
		 Query query = new Query();				
			query.addCriteria(Criteria.where("isDeleted").is(false).and("isActive").is(true));	
			logger.info("Query ["+query+"]");
			stagesList = mongoOperations.find(query, StagesEntity.class);
		
	}catch(Exception e) {
		logger.error("Error while retrieving product hierarchy Stages in DAO : "+e.getMessage());
	}
	return stagesList;
}


	@Override
	public <T> List<?> joinCollectionsWithSkipLimitSkip(T masterObj, String collection, Map<String, Object> criteriaMap,
			LookupOperation lookupOperation, String unwindField, Integer skip , Integer limit) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			if (criteriaMap != null && criteriaMap.size() > 0) {
				List<Criteria> criterias = new ArrayList<Criteria>(criteriaMap.size());
				criteriaMap.forEach((k, v) -> {
					if (v instanceof Collection<?>) {
						criterias.add(Criteria.where(k).in((Collection<?>) v));
					} else
						criterias.add(Criteria.where(k).is(v));
				});
				criteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
			}

			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria) , skip(skip) , limit(limit));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField) , skip(skip) , limit(limit));
			}
			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	public static SkipOperation skip(int elementsToSkip) {
		return new SkipOperation(elementsToSkip);
	}

	@Override
	public <T> List<?> joinCollectionsWithSkipLimitSkip(T masterObj, String collection, List<Criteria> criteriaList,
			LookupOperation lookupOperation, String unwindField , Integer skip , Integer limit) {
		List<?> joinedObjects = null;
		Criteria criteria = new Criteria();

		try {
			
			criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
			
			Aggregation aggregation;

			if (null == unwindField) {
				aggregation = newAggregation(lookupOperation, match(criteria), skip(skip) , limit(limit));
			} else {
				aggregation = newAggregation(lookupOperation, match(criteria), unwind(unwindField), skip(skip) , limit(limit));
			}
			AggregationResults<?> groupResults = mongoTemplate.aggregate(aggregation, collection , masterObj.getClass());
			joinedObjects = groupResults.getMappedResults();

		} catch (Exception e) {
			logger.error("Exception occured while joining collections :: " , e);
			joinedObjects = null;
		}

		return joinedObjects;

	}
	
	@SuppressWarnings({ "unchecked"})
	public List<String> getAssetFormatIdList() {
		List<String> assetFormatIdList = new ArrayList<>();
		try {
			
			Criteria[] assetCriteria = new Criteria[] {Criteria.where("Product.isDeleted").is(Boolean.FALSE), Criteria.where("Product.isActive").is(Boolean.TRUE)}; 
			
			Query query = new Query();
			query.addCriteria(Criteria.where("asset").elemMatch(Criteria.where("isDeleted").is(Boolean.FALSE)));
			query.addCriteria(new Criteria().andOperator(assetCriteria));
						
			assetFormatIdList = mongoTemplate.getCollection("products").distinct("asset.formatId",query.getQueryObject());
			
		} catch (Exception e) {
			logger.error("Exception :: ", e);
		}
		
		
		return assetFormatIdList;
	}
	
	@Override
	public boolean addFormatPg(String pgId, String collectionName, String formatId, String permissionType) {
		boolean result = false;
		try {
			
			Query query = new Query();
			
			Criteria[] updateCriteria = new Criteria[] {Criteria.where("permissionGroupId").is(pgId), Criteria.where("asset").elemMatch(Criteria.where("permission").is(permissionType))};
			query.addCriteria(new Criteria().andOperator(updateCriteria));
						
			Update update = new Update();
			update.addToSet("asset.$.format", formatId);
			
			mongoTemplate.updateFirst(query, update, PermissionGroupEntity.class);

			result = true;

		} catch (Exception e) {
			logger.error("Exception occured while updating Master :: "+e);
			result = false;			
		} 

		return result;
	}
	
	@Override
	public Map<String,String> gethierarchyIdNameMap() {
		Map<String,String> id_Name = new HashMap<>();
		try {
			Query query = new Query().addCriteria(Criteria.where("isActive").is(Boolean.TRUE).and("isDeleted").is(Boolean.FALSE));
			query.fields().include("_id").include("productHierarchyName");
			List<ProductHierarchyEntity> hList = mongoTemplate.find(query, ProductHierarchyEntity.class);
			for(ProductHierarchyEntity hEntity : hList) {
				id_Name.put(hEntity.getId(), hEntity.getProductHierarchyName());
			}
		} catch (Exception e) {
			logger.error("In Dao: Unable to add download Data." + e.getMessage());
			throw e;
		}
		return id_Name;
	}

	@Override
	public MetadataFieldEntity getMetadataFieldEntity(String metaDataFieldName) {
		MetadataFieldEntity metadataField = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("metaDataFieldName").is(metaDataFieldName));
			query.fields().exclude("_id");
			metadataField = mongoTemplate.findOne(query, MetadataFieldEntity.class, "metaDataFieldConfig");
		} catch (Exception e) {
			logger.error("Exception " + e.getMessage());
			throw e;
		}
		return metadataField;
	}

	@Override
	public List<StatusEntity> getStatusbyType(String type) {
		List<StatusEntity> statusList = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("type").is(type).and("isActive").is(true));
			statusList = mongoTemplate.find(query, StatusEntity.class);
		} catch (Exception e) {
			logger.error("Exception " + e.getMessage());
		}
		return statusList;
	}

}

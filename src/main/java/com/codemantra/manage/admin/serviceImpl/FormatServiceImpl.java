/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.CApprovalEligibleFormatsEntity;
import com.codemantra.manage.admin.entity.FormatEntity;
import com.codemantra.manage.admin.entity.FormatTypeEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CApprovalEligibleFormats;
import com.codemantra.manage.admin.model.Format;
import com.codemantra.manage.admin.model.FormatType;
import com.codemantra.manage.admin.service.FormatService;

@Service("formatService")
public class FormatServiceImpl implements FormatService {

	private static final Logger logger = LoggerFactory.getLogger(FormatServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${SUCCESS_RETRIEVE_FT_LIST}")
	String SUCCESS_RETRIEVE_FT_LIST;

	@Value("${FAILURE_RETRIEVE_FT_LIST}")
	String FAILURE_RETRIEVE_FT_LIST;

	@Value("${SUCCESS_RETRIEVE_FORMAT_LIST}")
	String SUCCESS_RETRIEVE_FORMAT_LIST;

	@Value("${FAILURE_RETRIEVE_FORMAT_LIST}")
	String FAILURE_RETRIEVE_FORMAT_LIST;

	@Value("${FAILURE_ADD_FT_MAND_NULL}")
	String FAILURE_ADD_FT_MAND_NULL;
	
	@Value("${FAILURE_ADD_FT_ID_ERROR}")
	String FAILURE_ADD_FT_ID_ERROR;
	
	@Value("${FAILURE_ADD_FT_FORMAT_NOT_EXISTS}")
	String FAILURE_ADD_FT_FORMAT_NOT_EXISTS;
	
	@Value("${SUCCESS_ADD_FT}")
	String SUCCESS_ADD_FT;
	
	@Value("${FAILURE_ADD_FT}")
	String FAILURE_ADD_FT;
	
	@Value("${FT_ALREADY_EXISTS}")
	String FT_ALREADY_EXISTS;
	
	@Value("${FAILURE_EDIT_FT_MAND_NULL}")
	String FAILURE_EDIT_FT_MAND_NULL;
	
	@Value("${FAILURE_EDIT_FT_NOT_EXISTS}")
	String FAILURE_EDIT_FT_NOT_EXISTS;
	
	@Value("${SUCCESS_EDIT_FT}")
	String SUCCESS_EDIT_FT;

	@Value("${FAILURE_EDIT_FT}")
	String FAILURE_EDIT_FT;
	
	@Value("${FAILURE_EDIT_FT_EXISTS}")
	String FAILURE_EDIT_FT_EXISTS;
	
	@Value("${FAILURE_DELETE_FT_NOT_EXISTS}")
	String FAILURE_DELETE_FT_NOT_EXISTS;
	
	@Value("${SUCCESS_DELETE_FT}")
	String SUCCESS_DELETE_FT;
	
	@Value("${FAILURE_DELETE_FT}")
	String FAILURE_DELETE_FT;
	
	@Value("${FAILURE_RETRIEVE_FT_NOT_EXISTS}")
	String FAILURE_RETRIEVE_FT_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_FT}")
	String SUCCESS_RETRIEVE_FT;
	
	@Value("${FAILURE_RETRIEVE_FT}")
	String FAILURE_RETRIEVE_FT;
	
	@Value("${FAILURE_ADD_FORMAT_MAND_NULL}")
	String FAILURE_ADD_FORMAT_MAND_NULL;
	
	@Value("${SUCCESS_ADD_FORMAT}")
	String SUCCESS_ADD_FORMAT;
	
	@Value("${FAILURE_ADD_FORMAT}")
	String FAILURE_ADD_FORMAT;
	
	@Value("${FAILURE_ADD_FORMAT_ID_ERROR}")
	String FAILURE_ADD_FORMAT_ID_ERROR;
	
	@Value("${FORMAT_ALREADY_EXISTS}")
	String FORMAT_ALREADY_EXISTS;
	
	@Value("${FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR}")
	String FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR;
	
	@Value("${FAILURE_ADD_FORMAT_FT_NOT_EXISTS}")
	String FAILURE_ADD_FORMAT_FT_NOT_EXISTS;
	
	@Value("${FAILURE_ADD_FORMAT_PERMISSION_NOT_EXISTS}")
	String FAILURE_ADD_FORMAT_PERMISSION_NOT_EXISTS;
	
	@Value("${FAILURE_EDIT_FORMAT_MAND_NULL}")
	String FAILURE_EDIT_FORMAT_MAND_NULL;
	
	@Value("${FAILURE_EDIT_FORMAT_NOT_EXISTS}")
	String FAILURE_EDIT_FORMAT_NOT_EXISTS;
	
	@Value("${SUCCESS_EDIT_FORMAT}")
	String SUCCESS_EDIT_FORMAT;
	
	@Value("${FAILURE_EDIT_FORMAT_EXISTS}")
	String FAILURE_EDIT_FORMAT_EXISTS;
	
	@Value("${FAILURE_EDIT_FORMAT}")
	String FAILURE_EDIT_FORMAT;
	
	@Value("${FAILURE_DELETE_FORMAT_NOT_EXISTS}")
	String FAILURE_DELETE_FORMAT_NOT_EXISTS;
	
	@Value("${SUCCESS_DELETE_FORMAT}")
	String SUCCESS_DELETE_FORMAT;
	
	@Value("${SUCCESS_RETRIEVE_FORMAT}")
	String SUCCESS_RETRIEVE_FORMAT;
	
	@Value("${FAILURE_RETRIEVE_FORMAT_NOT_EXISTS}")
	String FAILURE_RETRIEVE_FORMAT_NOT_EXISTS;
	
	@Value("${FAILURE_RETRIEVE_FORMAT}")
	String FAILURE_RETRIEVE_FORMAT;
	
	@Value("${FAILURE_DELETE_FORMAT}")
	String FAILURE_DELETE_FORMAT;
	
	@Value("${FAILURE_ADD_CAEF_MAND_NULL}")
	String FAILURE_ADD_CAEF_MAND_NULL;
	
	@Value("${FAILURE_ADD_CAEF_ID_ERROR}")
	String FAILURE_ADD_CAEF_ID_ERROR;
	
	@Value("${SUCCESS_ADD_CAEF}")
	String SUCCESS_ADD_CAEF;
	
	@Value("${FAILURE_ADD_CAEF}")
	String FAILURE_ADD_CAEF;
	
	@Value("${CAEF_ALREADY_EXISTS}")
	String CAEF_ALREADY_EXISTS;
	
	@Value("#{${PERMISSIONS}}")
	List<String> permissionList;
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllFormatTypes(String loggedUser) {
		List<FormatTypeEntity> formatTypes = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);
			if (permissionFlg) {				
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				formatTypes = (List<FormatTypeEntity>) masterDao.getAllEntityObjects(new FormatTypeEntity(), criteriaMap);

				status.setCode("SUCCESS_RETRIEVE_FT_LIST");
				status.setMessage(SUCCESS_RETRIEVE_FT_LIST);
				status.setStatus(true);
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAllFormatTypes :: ", e);
			status.setCode("FAILURE_RETRIEVE_FT_LIST");
			status.setMessage(FAILURE_RETRIEVE_FT_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", formatTypes);
		}
		return finalData;
	}
	
	@Override
	public Map<String, Object> saveFormatType(FormatType modelObj, String loggedUser) {
		
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "CREATE", userEntityObj);
			if (permissionFlg) {
				if (masterDao.isNull(modelObj.getFormatTypeName()) ) {
					status.setCode("FAILURE_ADD_FT_MAND_NULL");
					status.setMessage(FAILURE_ADD_FT_MAND_NULL);
					status.setStatus(false);
					
					
				} else {
					/**
					 * Format Type Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("formatTypeName", modelObj.getFormatTypeName());
					criteriaMap.put("isDeleted", Boolean.FALSE);
					if (null == masterDao.getEntityObject(new FormatType(), criteriaMap)) {
						String id = masterDao.getNextSequenceId("formatTypeIdSeq");
						logger.info("Format Type Id generated is [FT" + id + "]");

						if (id.equals("-1")) {
							status.setCode("FAILURE_ADD_FT_ID_ERROR");
							status.setMessage(FAILURE_ADD_FT_ID_ERROR);
							status.setStatus(false);
						} else {

							criteriaMap.clear();
							criteriaMap.put("formatTypeId", modelObj.getFormatTypeId());
							criteriaMap.put("isDeleted", Boolean.FALSE);


								FormatTypeEntity entityObj = new FormatTypeEntity();
								Timestamp currentTs = new Timestamp(new Date().getTime());

								modelObj.setFormatTypeId("FT".concat(id));
								entityObj.setIsActive(Boolean.TRUE);
								entityObj.setIsDeleted(Boolean.FALSE);
								entityObj.setCreatedOn(currentTs);
								entityObj.setCreatedBy(loggedUser);
								entityObj.setModifiedOn(currentTs);
								entityObj.setModifiedBy(loggedUser);

								BeanUtils.copyProperties(modelObj, entityObj);
								result = masterDao.saveEntityObject(entityObj);

								if (result) {
									status.setCode("SUCCESS_ADD_FT");
									status.setMessage(SUCCESS_ADD_FT);
									status.setStatus(true);
								} else {
									status.setCode("FAILURE_ADD_FT");
									status.setMessage(FAILURE_ADD_FT);
									status.setStatus(false);
								}
						}
					} else {
						status.setCode("FT_ALREADY_EXISTS");
						status.setMessage(FT_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_FT");
			status.setMessage(FAILURE_ADD_FT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormatType(modelObj.getFormatTypeId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;
		
	}

	@Override
	public Map<String, Object> editFormatType(FormatType modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "EDIT", userEntityObj);

			if (permissionFlg) {
				if (masterDao.isNull(modelObj.getFormatTypeName())) {
					status.setCode("FAILURE_EDIT_FT_MAND_NULL");
					status.setMessage(FAILURE_EDIT_FT_MAND_NULL);
					status.setStatus(false);
				} else {


					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("formatTypeId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					FormatTypeEntity entityObj = masterDao.getEntityObject(new FormatTypeEntity(), criteriaMap);
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_FT_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_FT_NOT_EXISTS);
						status.setStatus(false);
					} else {

						FormatTypeEntity entityObjExists = null;
						if (!entityObj.getFormatTypeName().equals(modelObj.getFormatTypeName())) {
							criteriaMap.clear();
							criteriaMap.put("formatTypeName", modelObj.getFormatTypeName());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							entityObjExists = masterDao.getEntityObject(new FormatTypeEntity(), criteriaMap);
						}

						if (null == entityObjExists) {
							entityObj.setFormatTypeName(modelObj.getFormatTypeName());
							entityObj.setModifiedBy(loggedUser);
							entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

							result = masterDao.updateEntityObject(entityObj);

							if (result) {
								status.setCode("SUCCESS_EDIT_FT");
								status.setMessage(SUCCESS_EDIT_FT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_FT");
								status.setMessage(FAILURE_EDIT_FT);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_EDIT_FT_EXISTS");
							status.setMessage(FAILURE_EDIT_FT_EXISTS);
							status.setStatus(false);
						}
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_FT");
			status.setMessage(FAILURE_EDIT_FT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormatType(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}
		
		

	@Override
	public Map<String, Object> deleteFormatType(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "DELETE", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("formatTypeId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				FormatTypeEntity entityObj = masterDao.getEntityObject(new FormatTypeEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_FT_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_FT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(Boolean.TRUE);
					entityObj.setIsActive(Boolean.FALSE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_FT");
						status.setMessage(SUCCESS_DELETE_FT);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_FT");
						status.setMessage(FAILURE_DELETE_FT);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_FT");
			status.setMessage(FAILURE_DELETE_FT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormatType(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrieveFormatType(String id, String loggedUser) {
		FormatType modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("formatTypeId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				FormatTypeEntity entityObj = masterDao.getEntityObject(new FormatTypeEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_FT_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_FT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					modelObj = new FormatType();
					BeanUtils.copyProperties(entityObj, modelObj);
					status.setCode("SUCCESS_RETRIEVE_FT");
					status.setMessage(SUCCESS_RETRIEVE_FT);
					status.setStatus(true);
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_FT");
			status.setMessage(FAILURE_RETRIEVE_FT);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

	///////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllFormats(String loggedUser) {
		List<FormatEntity> formatEntityList = new LinkedList<>();
		List<Format> formats= new LinkedList<>();
		
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;

		List<String> assetFormat = masterDao.getAssetFormatIdList();

		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);

			if (permissionFlg) {

				List<Object> tempList = CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAsset())
						.stream().filter(k -> k.getPermission().equals("VIEW")).map(kv -> kv.getFormat())
						.collect(Collectors.toList());
				List<String> loggedUserFormatList = new ArrayList<>();
				if (tempList.size() == 1) {
					loggedUserFormatList = (List<String>) tempList.get(0);
				}

				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("formatId", loggedUserFormatList);
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				formatEntityList = (List<FormatEntity>) masterDao.getAllEntityObjects(new FormatEntity(), criteriaMap);
				for(FormatEntity i : formatEntityList) {
					Format format = new Format();
					BeanUtils.copyProperties(i, format);
					if(assetFormat.contains(i.getFormatId())) {
						format.setHasAsset(true);
					}
					
					formats.add(format);
				}
				status.setCode("SUCCESS_RETRIEVE_FORMAT_LIST");
				status.setMessage(SUCCESS_RETRIEVE_FORMAT_LIST);
				status.setStatus(true);

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAllFormats :: ", e);
			status.setCode("FAILURE_RETRIEVE_FORMAT_LIST");
			status.setMessage(FAILURE_RETRIEVE_FORMAT_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", formats);
		}

		return finalData;
	}


	@Override
	public Map<String, Object> saveFormat(Format modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "CREATE", userEntityObj);
			if (permissionFlg ) {
				if (masterDao.isNull(modelObj.getFormatTypeId()) || masterDao.isNull(modelObj.getFormatName())) {
					status.setCode("FAILURE_ADD_FORMAT_MAND_NULL");
					status.setMessage(FAILURE_ADD_FORMAT_MAND_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Format Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("formatTypeId", modelObj.getFormatTypeId());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					FormatTypeEntity formatTypeEntityObj = masterDao.getEntityObject(new FormatTypeEntity(),criteriaMap);
					
					if (null != formatTypeEntityObj) {

						criteriaMap.clear();
						criteriaMap.put("formatTypeId", formatTypeEntityObj.getFormatTypeId());
						criteriaMap.put("isDeleted", Boolean.FALSE);
						criteriaMap.put("isActive", Boolean.TRUE);


							criteriaMap.clear();
							criteriaMap.put("formatName", modelObj.getFormatName());
							criteriaMap.put("isDeleted", Boolean.FALSE);
							if (null == masterDao.getEntityObject(new FormatEntity(), criteriaMap)) {
								String id = masterDao.getNextSequenceId("formatIdSeq");
								logger.info("Format Id generated is [F" + id + "]");


								if (id.equals("-1")) {
									status.setCode("FAILURE_ADD_FORMAT_ID_ERROR");
									status.setMessage(FAILURE_ADD_FORMAT_ID_ERROR);
									status.setStatus(false);
								} else {
									FormatEntity entityObj = new FormatEntity();
									Timestamp currentTs = new Timestamp(new Date().getTime());
									
									modelObj.setFormatId("F".concat(id));

									BeanUtils.copyProperties(modelObj, entityObj);				
									entityObj.setIsActive(Boolean.TRUE);
									entityObj.setIsDeleted(Boolean.FALSE);
									entityObj.setCreatedOn(currentTs);
									entityObj.setCreatedBy(loggedUser);
									entityObj.setModifiedOn(currentTs);
									entityObj.setModifiedBy(loggedUser);
									
									result = masterDao.saveEntityObject(entityObj);

									if (result) {
										
										for (String p : permissionList) {
											masterDao.addFormatPg(userEntityObj.getPermissionGroupId(),
													"mPermissionGroup", modelObj.getFormatId(), p);
										}
										
										status.setCode("SUCCESS_ADD_FORMAT");
										status.setMessage(SUCCESS_ADD_FORMAT);
										status.setStatus(true);
									} else {
										status.setCode("FAILURE_ADD_FORMAT");
										status.setMessage(FAILURE_ADD_FORMAT);
										status.setStatus(false);
									}
								}
							} else {
								status.setCode("FORMAT_ALREADY_EXISTS");
								status.setMessage(FORMAT_ALREADY_EXISTS);
								status.setStatus(false);
							}
						
					} else {
						status.setCode("FAILURE_ADD_FORMAT_FT_NOT_EXISTS");
						status.setMessage(FAILURE_ADD_FORMAT_FT_NOT_EXISTS);
						status.setStatus(false);
					}

				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_FORMAT");
			status.setMessage(FAILURE_ADD_FORMAT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormat(modelObj.getFormatId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}
	
	@Override
	public Map<String, Object> editFormat(Format modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "EDIT", userEntityObj);
			if (permissionFlg) 
			{

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("formatId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					FormatEntity entityObj = masterDao.getEntityObject(new FormatEntity(), criteriaMap);
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_FORMAT_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_FORMAT_NOT_EXISTS);
						status.setStatus(false);
					}  else {

						FormatEntity entityObjExists = null;
						if (!entityObj.getFormatName().equals(modelObj.getFormatName())) {
							criteriaMap.clear();
							criteriaMap.put("formatName", modelObj.getFormatName());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							entityObjExists = masterDao.getEntityObject(new FormatEntity(), criteriaMap);
						}

						if (null == entityObjExists) {
							criteriaMap.clear();
							criteriaMap.put("formatId", modelObj.getFormatId());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							FormatEntity formatEntityObj = masterDao.getEntityObject(new FormatEntity(),
									criteriaMap);


								criteriaMap.clear();
								criteriaMap.put("formatId", formatEntityObj.getFormatId());
								criteriaMap.put("isDeleted", Boolean.FALSE);
								criteriaMap.put("isActive", Boolean.TRUE);

								
								List<String> formatIdList = masterDao.getDistinctFieldValues("mFormat", criteriaMap, "formatId");

								if (formatIdList.contains(modelObj.getFormatId())) {
																			
									
									entityObj.setFormatName(modelObj.getFormatName());
									entityObj.setFormatId(modelObj.getFormatId());
									entityObj.setPreference(modelObj.getPreference());
									entityObj.setFilePurpose(modelObj.getFilePurpose());
									entityObj.setModifiedBy(loggedUser);
									entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
									entityObj.setSingleFile(modelObj.getSingleFile());
									entityObj.setIsVirtualViewerFormat(modelObj.getIsVirtualViewerFormat());
									entityObj.setIsEpubFormat(modelObj.getIsEpubFormat());
									entityObj.setCreateWidget(modelObj.getCreateWidget());
									
									result = masterDao.updateEntityObject(entityObj);

									if (result) {
										status.setCode("SUCCESS_EDIT_FORMAT");
										status.setMessage(SUCCESS_EDIT_FORMAT);
										status.setStatus(true);
									} else {
										status.setCode("FAILURE_EDIT_FORMAT");
										status.setMessage(FAILURE_EDIT_FORMAT);
										status.setStatus(false);
									}
								} else {
									status.setCode("FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR");
									status.setMessage(FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR);
									status.setStatus(false);
								}
							

						} else {
							status.setCode("FAILURE_EDIT_FORMAT_EXISTS");
							status.setMessage(FAILURE_EDIT_FORMAT_EXISTS);
							status.setStatus(false);
						}
					}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_FORMAT");
			status.setMessage(FAILURE_EDIT_FORMAT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormat(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}
	
	
	@Override
	public Map<String, Object> deleteFormat(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "DELETE", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("formatId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				FormatEntity entityObj = masterDao.getEntityObject(new FormatEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_FORMAT_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_FORMAT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(Boolean.TRUE);
					entityObj.setIsActive(Boolean.FALSE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_FORMAT");
						status.setMessage(SUCCESS_DELETE_FORMAT);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_FORMAT");
						status.setMessage(FAILURE_DELETE_FORMAT);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_FORMAT");
			status.setMessage(FAILURE_DELETE_FORMAT);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormat(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	

	@Override
	public Map<String, Object> retrieveFormat(String id, String loggedUser) {
		Format modelObj = null;
		Status status = new Status();
		
		
		List<String> assetFormat = masterDao.getAssetFormatIdList();
		
		
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("formatId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				FormatEntity entityObj = masterDao.getEntityObject(new FormatEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_FORMAT_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_FORMAT_NOT_EXISTS);
					status.setStatus(false);
				}   else {
					modelObj = new Format();
					BeanUtils.copyProperties(entityObj, modelObj);
					
					for (String element : assetFormat) { 
			            if (element.equals(id)) { 
			                modelObj.setHasAsset(true);
			                break; 
			            }
			            else {
			            	modelObj.setHasAsset(false);	
			            }
			        }
					
					status.setCode("SUCCESS_RETRIEVE_FORMAT");
					status.setMessage(SUCCESS_RETRIEVE_FORMAT);
					status.setStatus(true);
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_FORMAT");
			status.setMessage(FAILURE_RETRIEVE_FORMAT);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);

		}
		return finalData;
	}

	
	////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllCApprovalEligibleFormats(String loggedUser) {
	  List<CApprovalEligibleFormatsEntity> cApprovalformats = new LinkedList<>();

	  Status status = new Status();
	  Map<String, Object> finalData = new HashMap<>();
	  boolean permissionFlg = false;

	  try {
	    UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
	    permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);

	    if (permissionFlg) {

	      List<Object> tempList = CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAsset())
	          .stream().filter(k -> k.getPermission().equals("VIEW")).map(kv -> kv.getFormat())
	          .collect(Collectors.toList());
	      List<String> loggedUserFormatList = new ArrayList<>();
	      if (tempList.size() == 1) {
	        loggedUserFormatList = (List<String>) tempList.get(0);
	      }

	      Map<String, Object> criteriaMap = new HashMap<>();
	      criteriaMap.put("formatId", loggedUserFormatList);
	      criteriaMap.put("isDeleted", Boolean.FALSE);
	      criteriaMap.put("isActive", Boolean.TRUE);

	      cApprovalformats = (List<CApprovalEligibleFormatsEntity>) masterDao.getAllEntityObjects(new CApprovalEligibleFormatsEntity(), criteriaMap);

	      status.setCode("SUCCESS_RETRIEVE_FORMAT_LIST");
	      status.setMessage(SUCCESS_RETRIEVE_FORMAT_LIST);
	      status.setStatus(true);

	    } else {
	      status.setCode("REQUEST_FORBIDDEN");
	      status.setMessage(REQUEST_FORBIDDEN);
	      status.setStatus(false);
	    }

	  } catch (Exception e) {
	    logger.error("Exception in retrieveAllFormats :: ", e);
	    status.setCode("FAILURE_RETRIEVE_FORMAT_LIST");
	    status.setMessage(FAILURE_RETRIEVE_FORMAT_LIST);
	    status.setStatus(false);
	  } finally {
	    finalData.put("status", status);
	    finalData.put("data", cApprovalformats);
	  }

	  return finalData;

	}
	
	@Override
	public Map<String, Object> saveCApprovalEligibleFormats(CApprovalEligibleFormats modelObj, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "CREATE", userEntityObj);
			if (permissionFlg) {
				if (masterDao.isNull(modelObj.getFormatName()) || masterDao.isNull(modelObj.getFormatId())) {
					status.setCode("FAILURE_ADD_CAEF_MAND_NULL");
					status.setMessage(FAILURE_ADD_CAEF_MAND_NULL);
					status.setStatus(false);
					
					
				} else {
					/**
					 * Format Name can't be duplicated
					 */
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("formatName", modelObj.getFormatName());
					criteriaMap.put("isDeleted", Boolean.FALSE);
					if (null == masterDao.getEntityObject(new CApprovalEligibleFormats(), criteriaMap)) {

						CApprovalEligibleFormatsEntity entityObj = new CApprovalEligibleFormatsEntity();
						Timestamp currentTs = new Timestamp(new Date().getTime());

						BeanUtils.copyProperties(modelObj, entityObj);
						entityObj.setIsActive(Boolean.TRUE);
						entityObj.setIsDeleted(Boolean.FALSE);
						entityObj.setCreatedOn(currentTs);
						entityObj.setCreatedBy(loggedUser);
						entityObj.setModifiedOn(currentTs);
						entityObj.setModifiedBy(loggedUser);
						result = masterDao.saveEntityObject(entityObj);

						if (result) {

							status.setCode("SUCCESS_ADD_CAEF");
							status.setMessage(SUCCESS_ADD_CAEF);
							status.setStatus(true);
						} else {
							status.setCode("FAILURE_ADD_CAEF");
							status.setMessage(FAILURE_ADD_CAEF);
							status.setStatus(false);
						}

					} else {
						status.setCode("CAEF_ALREADY_EXISTS");
						status.setMessage(CAEF_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_CAEF");
			status.setMessage(FAILURE_ADD_CAEF);
			status.setStatus(false);
		} finally {
			finalData = retrieveFormat(modelObj.getFormatId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> editCApprovalEligibleFormats(CApprovalEligibleFormats modelObj, String id,
	    String loggedUser) {

	  boolean result = false;
	  Map<String, Object> finalData = new HashMap<>();
	  Status status = new Status();
	  boolean permissionFlg = false;
	  try {
	    UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
	    permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "EDIT", userEntityObj);
	    if (permissionFlg)
	    {
	      if (masterDao.isNull(modelObj.getFormatId()) ||  masterDao.isNull(modelObj.getFormatName()))
	      {
	        status.setCode("FAILURE_EDIT_FORMAT_MAND_NULL");
	        status.setMessage(FAILURE_EDIT_FORMAT_MAND_NULL);
	        status.setStatus(false);
	      } else {
	        /**
	         * Name can't be duplicated
	         */

	        Map<String, Object> criteriaMap = new HashMap<>();
	        criteriaMap.put("formatId", id);
	        criteriaMap.put("isDeleted", Boolean.FALSE);

	        CApprovalEligibleFormatsEntity entityObj = masterDao.getEntityObject(new CApprovalEligibleFormatsEntity(), criteriaMap);
	        if (null == entityObj) {
	          status.setCode("FAILURE_EDIT_FORMAT_NOT_EXISTS");
	          status.setMessage(FAILURE_EDIT_FORMAT_NOT_EXISTS);
	          status.setStatus(false);
	        }  else {

	          CApprovalEligibleFormatsEntity entityObjExists = null;
	          if (!entityObj.getFormatName().equals(modelObj.getFormatName())) {
	            criteriaMap.clear();
	            criteriaMap.put("formatName", modelObj.getFormatName());
	            criteriaMap.put("isDeleted", Boolean.FALSE);

	            entityObjExists = masterDao.getEntityObject(new CApprovalEligibleFormatsEntity(), criteriaMap);
	          }

	          if (null == entityObjExists) {
	            criteriaMap.clear();
	            criteriaMap.put("formatId", modelObj.getFormatId());
	            criteriaMap.put("isDeleted", Boolean.FALSE);

	            CApprovalEligibleFormatsEntity CApprovalEligibleFormatsEntityObj = masterDao.getEntityObject(new CApprovalEligibleFormatsEntity(),
	                criteriaMap);

	            if (null != CApprovalEligibleFormatsEntityObj) {

	              criteriaMap.clear();
		          criteriaMap.put("formatId", CApprovalEligibleFormatsEntityObj.getFormatId());
	              criteriaMap.put("isDeleted", Boolean.FALSE);
	              criteriaMap.put("isActive", Boolean.TRUE);

	              List<String> formatIdList = masterDao.getDistinctFieldValues("cApprovalEligibleFormats", criteriaMap, "formatId");

	              if (formatIdList.contains(modelObj.getFormatId())) {
	                entityObj.setFormatName(modelObj.getFormatName());;
	                entityObj.setFormatId(modelObj.getFormatId());
	                entityObj.setModifiedBy(loggedUser);
	                entityObj.setModifiedOn(new Timestamp(new Date().getTime()));


	                result = masterDao.updateEntityObject(entityObj);

	                if (result) {
	                  status.setCode("SUCCESS_EDIT_FORMAT");
	                  status.setMessage(SUCCESS_EDIT_FORMAT);
	                  status.setStatus(true);
	                } else {
	                  status.setCode("FAILURE_EDIT_FORMAT");
	                  status.setMessage(FAILURE_EDIT_FORMAT);
	                  status.setStatus(false);
	                }
	              } else {
	                status.setCode("FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR");
	                status.setMessage(FAILURE_ADD_FORMAT_FORMAT_LIST_ERROR);
	                status.setStatus(false);
	              }
	            } else {
	              status.setCode("FAILURE_ADD_FORMAT_FT_NOT_EXISTS");
	              status.setMessage(FAILURE_ADD_FORMAT_FT_NOT_EXISTS);
	              status.setStatus(false);
	            }

	          } else {
	            status.setCode("FAILURE_EDIT_FORMAT_EXISTS");
	            status.setMessage(FAILURE_EDIT_FORMAT_EXISTS);
	            status.setStatus(false);
	          }
	        }
	      }
	    } else {
	      status.setCode("REQUEST_FORBIDDEN");
	      status.setMessage(REQUEST_FORBIDDEN);
	      status.setStatus(false);
	    }

	  } catch (Exception e) {
	    logger.error("Exception in edit :: ", e);
	    status.setCode("FAILURE_EDIT_FORMAT");
	    status.setMessage(FAILURE_EDIT_FORMAT);
	    status.setStatus(false);
	  } finally {
	    finalData = retrieveFormat(id, loggedUser);
	    finalData.put("status", status);
	  }
	  return finalData;

	}


	@Override
	public Map<String, Object> deleteCApprovalEligibleFormats(String id, String loggedUser) {
	  boolean result = false;
	  Map<String, Object> finalData = new HashMap<>();
	  Status status = new Status();
	  boolean permissionFlg = false;
	  try {
	    UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
	    permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "DELETE", userEntityObj);
	    if (permissionFlg) {
	      Map<String, Object> criteriaMap = new HashMap<>();
	      criteriaMap.put("formatId", id);
	      criteriaMap.put("isDeleted", Boolean.FALSE);

	      CApprovalEligibleFormatsEntity entityObj = masterDao.getEntityObject(new CApprovalEligibleFormatsEntity(), criteriaMap);

	      if (null == entityObj) {
	        status.setCode("FAILURE_DELETE_FORMAT_NOT_EXISTS");
	        status.setMessage(FAILURE_DELETE_FORMAT_NOT_EXISTS);
	        status.setStatus(false);
	      } else {
	        entityObj.setIsDeleted(Boolean.TRUE);
	        entityObj.setIsActive(Boolean.FALSE);
	        entityObj.setModifiedBy(loggedUser);
	        entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

	        result = masterDao.updateEntityObject(entityObj);

	        if (result) {
	          status.setCode("SUCCESS_DELETE_FORMAT");
	          status.setMessage(SUCCESS_DELETE_FORMAT);
	          status.setStatus(true);
	        } else {
	          status.setCode("FAILURE_DELETE_FORMAT");
	          status.setMessage(FAILURE_DELETE_FORMAT);
	          status.setStatus(false);
	        }
	      }
	    } else {
	      status.setCode("REQUEST_FORBIDDEN");
	      status.setMessage(REQUEST_FORBIDDEN);
	      status.setStatus(false);
	    }

	  } catch (Exception e) {
	    logger.error("Exception in delete :: ", e);
	    status.setCode("FAILURE_DELETE_FORMAT");
	    status.setMessage(FAILURE_DELETE_FORMAT);
	    status.setStatus(false);
	  } finally {
	    finalData = retrieveFormat(id, loggedUser);
	    finalData.put("status", status);
	  }
	  return finalData;
	}

	@Override
	public Map<String, Object> retrieveCApprovalEligibleFormats(String id, String loggedUser) {
	  CApprovalEligibleFormats modelObj = null;
	  Status status = new Status();
	  Map<String, Object> finalData = new HashMap<>();
	  boolean permissionFlg = false;
	  try {
	    UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
	    permissionFlg = masterDao.checkPermission("MOD00006", "FORMAT", "VIEW", userEntityObj);
	    if (permissionFlg) {
	      Map<String, Object> criteriaMap = new HashMap<>();
	      criteriaMap.put("formatId", id);
	      criteriaMap.put("isDeleted", Boolean.FALSE);

	      CApprovalEligibleFormatsEntity entityObj = masterDao.getEntityObject(new CApprovalEligibleFormatsEntity(), criteriaMap);

	      if (null == entityObj) {
	        status.setCode("FAILURE_RETRIEVE_FORMAT_NOT_EXISTS");
	        status.setMessage(FAILURE_RETRIEVE_FORMAT_NOT_EXISTS);
	        status.setStatus(false);
	      }
	      
	      else {
				modelObj = new CApprovalEligibleFormats();
				BeanUtils.copyProperties(entityObj, modelObj);
				status.setCode("SUCCESS_RETRIEVE_FORMAT");
				status.setMessage(SUCCESS_RETRIEVE_FORMAT);
				status.setStatus(true);
			}


	    } else {
	      status.setCode("REQUEST_FORBIDDEN");
	      status.setMessage(REQUEST_FORBIDDEN);
	      status.setStatus(false);
	    }
	  } catch (Exception e) {
	    logger.error("Exception in retrieve :: ", e);
	    status.setCode("FAILURE_RETRIEVE_FORMAT");
	    status.setMessage(FAILURE_RETRIEVE_FORMAT);
	    status.setStatus(false);
	  } finally {
	    finalData.put("status", status);
	    finalData.put("data", modelObj);
	  }
	  return finalData;
	}

}

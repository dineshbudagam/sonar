/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.AwsAPIEntity;
import com.codemantra.manage.admin.entity.CMetaRefreshEntity;
import com.codemantra.manage.admin.entity.CPartnerConfigEntity;
import com.codemantra.manage.admin.entity.DownloadEntity;
import com.codemantra.manage.admin.entity.FTPDetailsEntity;
import com.codemantra.manage.admin.entity.FormatDetailsEntity;
import com.codemantra.manage.admin.entity.FormatEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.PartnerEntity;
import com.codemantra.manage.admin.entity.TransformFieldDetails;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CLookupMappingPartnerConfig;
import com.codemantra.manage.admin.model.CPartnerConfig;
import com.codemantra.manage.admin.model.CodeListConditions;
import com.codemantra.manage.admin.model.CoverFormat;
import com.codemantra.manage.admin.model.ExcelFieldDetails;
import com.codemantra.manage.admin.model.FTPDetails;
import com.codemantra.manage.admin.model.FormatDetails;
import com.codemantra.manage.admin.model.ReadExcelData;
import com.codemantra.manage.admin.model.TransformCodeList;
import com.codemantra.manage.admin.model.TransformRules;
import com.codemantra.manage.admin.service.GdsService;
import com.codemantra.manage.admin.util.EncryptDecrypt;
import com.opencsv.CSVWriter;

@Service("gdsService")
public class GdsServiceImpl implements GdsService {

	private static final Logger logger = LoggerFactory.getLogger(GdsServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${FAILURE_EDIT_PT_NOT_EXISTS}")
	String FAILURE_EDIT_PT_NOT_EXISTS;

	@Value("${FAILURE_EDIT_PT}")
	String FAILURE_EDIT_PT;

	@Value("${FAILURE_DELETE_PT_NOT_EXISTS}")
	String FAILURE_DELETE_PT_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_PT}")
	String SUCCESS_DELETE_PT;

	@Value("${FAILURE_DELETE_PT}")
	String FAILURE_DELETE_PT;

	@Value("${SUCCESS_RETRIEVE_PARTNER}")
	String SUCCESS_RETRIEVE_PARTNER;

	@Value("${SUCCESS_RETRIEVE_PARTNER_LIST}")
	String SUCCESS_RETRIEVE_PARTNER_LIST;

	@Value("${FAILURE_RETRIEVE_PARTNER_LIST}")
	String FAILURE_RETRIEVE_PARTNER_LIST;

	@Value("${SUCCESS_DELETE_GDS_CONFIG}")
	String SUCCESS_DELETE_GDS_CONFIG;
 
	@Value("${FAILURE_DELETE_GDS_CONFIG}")
	String FAILURE_DELETE_GDS_CONFIG;
	
	@Value("${SUCCESS_SAVING_GDS_CONFIG}")
	String SUCCESS_SAVING_GDS_CONFIG;
	
	@Value("${FAILURE_SAVING_GDS_CONFIG}")
	String FAILURE_SAVING_GDS_CONFIG;
	
	@Value("${SUCCESS_EDIT_GDS_CONFIG}")
	String SUCCESS_EDIT_GDS_CONFIG;
	
	@Value("${FAILURE_EDIT_GDS_CONFIG}")
	String FAILURE_EDIT_GDS_CONFIG;
	
	@Value("${SUCCESS_RETRIEVE_PARTNER_CONFIG_LIST}")
	String SUCCESS_RETRIEVE_PARTNER_CONFIG_LIST;
	
	@Value("${FAILURE_RETRIEVE_PARTNER_CONFIG_LIST}")
	String FAILURE_RETRIEVE_PARTNER_CONFIG_LIST;
	
	@Value("${FAILURE_DELETE_GDS_NT_EXIST}")
	String FAILURE_DELETE_GDS_NT_EXIST;
	
	@Value("${FAILURE_EDIT_GDS_NOT_EXISTS}")
	String FAILURE_EDIT_GDS_NOT_EXISTS;
	
	@Value("${EXPORT_URL_EXPIRY_DAYS}")
	int EXPORT_URL_EXPIRY_DAYS;
	
	@Value("${TEMP_OUTPUT_FOLDER_GDS}")
	String TEMP_OUTPUT_FOLDER_GDS;
	
	@Value("${GDS_EXPORT_FORMAT}")
	String GDS_EXPORT_FORMAT;
	
	@Value("${EXPORT_GDS_FILE_S3_PATH}")
	String EXPORT_GDS_FILE_S3_PATH;

	@Value("${DOWNLOAD_STATUS_GDS}")
	String DOWNLOAD_STATUS_GDS;
	
	@Value("${DOWNLOAD_TYPE_GDS}")
	String DOWNLOAD_TYPE_GDS;
	
	@Value("${SUCCESS_EXPORT_GDS_LIST}")
	String SUCCESS_EXPORT_GDS_LIST;
	
	@Value("${FAILURE_EXPORT_GDS_LIST}")
	String FAILURE_EXPORT_GDS_LIST;
	

	@Value("${FORMAT.TEMPLATE.DATE}")
	String templateDateFormat;

	@Value("${CUSTOMMDATA.DOWNLOAD.FILEPATH}")
	String strCustomDownloadPath;
	

	@Value("${CUSTOM.TEMPLATE.TEMPLATENAME}")
	String templateName;
	
	@Value("${FORMAT.TEMPLATE.FILE}")
	String templateFileFormat;

	@Value("${RESPONSE.COMMON.SUCCESS}")
	String success;

	@Value("${RESPONSE.TEMPLATE.DOWNLOAD.SUCCESS}")
	String successDownload;
	
	@Value("${RESPONSE.COMMON.FAILURE}")
	String failure;
	
	@Value("${RESPONSE.TEMPLATE.DOWNLOAD.FAILURE}")
	String failureDownload;

	
	public Map<String, Object> savePartnerConfig(CPartnerConfigEntity partnerConfig,String loggedUser){
		logger.info("INSIDE savePartnerConfig FUNCTION");
		Map<String, Object> finalData = new HashMap<>();
		
		List<FTPDetailsEntity> ftpList= new ArrayList<>();
		List<FormatDetailsEntity> formatEntityList = new ArrayList<>();
		List<TransformFieldDetails> transformFieldList = new ArrayList<>();
		TransformFieldDetails nonExclusiveRightsCountry = new TransformFieldDetails();
		TransformFieldDetails nonForSaleCountry = new TransformFieldDetails();
		TransformFieldDetails discountCode = new TransformFieldDetails();
		TransformFieldDetails exclusiveRightsCountry = new TransformFieldDetails();
		Map<String,String> lookup = new HashMap<>();
		lookup.put("DEFAULT", "EBO2");
		
		CMetaRefreshEntity metaRefresh = new CMetaRefreshEntity();
		Status status = new Status();
		boolean result=false;
		boolean result1=false;
		try {
			String id = masterDao.getNextSequenceId("cPartnerIdSeq");
			partnerConfig.setPartnerConfigId("CP"+id);
			partnerConfig.setIsActive(Boolean.TRUE);
			partnerConfig.setIsDeleted(Boolean.FALSE);
			partnerConfig.setCreatedBy(loggedUser);
			partnerConfig.setCreatedOn(new Date());
			partnerConfig.setModifiedBy(loggedUser);
			partnerConfig.setModifiedOn(new Date());
			if(partnerConfig.getTransformFieldList()==null || partnerConfig.getTransformFieldList().isEmpty())
			{
				exclusiveRightsCountry.setMetaDataFieldName("ExclusiveRightsCountry");
				exclusiveRightsCountry.setOperation("JOIN");
				exclusiveRightsCountry.setJoinBy(" ");
				
				nonExclusiveRightsCountry.setMetaDataFieldName("NonExclusiveRightsCountry");
				nonExclusiveRightsCountry.setOperation("JOIN");
				nonExclusiveRightsCountry.setJoinBy(" ");
				
				nonForSaleCountry.setMetaDataFieldName("NonForSaleCountry");
				nonForSaleCountry.setOperation("JOIN");
				nonForSaleCountry.setJoinBy(" ");
				
				discountCode.setMetaDataFieldName("DISCOUNTCODE");
				discountCode.setOperation("REPLACE");
				discountCode.setLookUp(lookup);
				
				transformFieldList.add(exclusiveRightsCountry);
				transformFieldList.add(nonExclusiveRightsCountry);
				transformFieldList.add(nonForSaleCountry);
				transformFieldList.add(discountCode);
				partnerConfig.setTransformFieldList(transformFieldList);
			}
			else 
			{
				List<String> metaFieldNames = partnerConfig.getTransformFieldList().stream().map(TransformFieldDetails::getMetaDataFieldName).collect(Collectors.toList());
				if(!metaFieldNames.contains("ExclusiveRightsCountry"))
				{
					exclusiveRightsCountry.setMetaDataFieldName("ExclusiveRightsCountry");
					exclusiveRightsCountry.setOperation("JOIN");
					exclusiveRightsCountry.setJoinBy(" ");
					transformFieldList.add(exclusiveRightsCountry);
				}
			    if(!metaFieldNames.contains("NonExclusiveRightsCountry"))
				{
					nonExclusiveRightsCountry.setMetaDataFieldName("NonExclusiveRightsCountry");
					nonExclusiveRightsCountry.setOperation("JOIN");
					nonExclusiveRightsCountry.setJoinBy(" ");
					transformFieldList.add(nonExclusiveRightsCountry);
				}
			    if(!metaFieldNames.contains("NonForSaleCountry"))
				{
					nonForSaleCountry.setMetaDataFieldName("NonForSaleCountry");
					nonForSaleCountry.setOperation("JOIN");
					nonForSaleCountry.setJoinBy(" ");
					transformFieldList.add(nonForSaleCountry);
				}
				if(!metaFieldNames.contains("DISCOUNTCODE"))
				{
					discountCode.setMetaDataFieldName("DISCOUNTCODE");
					discountCode.setOperation("REPLACE");
					discountCode.setLookUp(lookup);
					transformFieldList.add(discountCode);
				}
				transformFieldList.addAll(partnerConfig.getTransformFieldList());
				partnerConfig.setTransformFieldList(transformFieldList);

			}
			//ADDING formatNaming FOR formatDetails START

			if(partnerConfig.getFormatDetails()!=null)
			{
				for(FormatDetailsEntity formatEntity : partnerConfig.getFormatDetails())
				{
					formatEntity.setFormatNaming("$.product.recordReference");
					formatEntityList.add(formatEntity);
				}
				partnerConfig.setFormatDetails(formatEntityList);
			}
		
			//ADDING formatNaming FOR formatDetails END
			
			// ADDING isActive FLAG FOR excelFieldDetails START
			if(partnerConfig.getExcelFieldDetails() ==null || partnerConfig.getExcelFieldDetails().isEmpty())
			{
				partnerConfig.setExcelTempate("empty");
				partnerConfig.setExcelTempateRowStartAt(null);
			}
			if(partnerConfig.getMetaRefreshInterval()!=null) {	
			metaRefresh.setPartnerId(partnerConfig.getPartnerId());
			metaRefresh.setInterval(partnerConfig.getMetaRefreshInterval());
			metaRefresh.setStartDate(new Date());
			metaRefresh.setLastRun(new Date());
			metaRefresh.setNextRun(new DateTime().plusDays(partnerConfig.getMetaRefreshInterval()).toDate());
			metaRefresh.setIsActive(Boolean.TRUE);
			metaRefresh.setIsDeleted(Boolean.FALSE);
			metaRefresh.setCreatedBy(loggedUser);
			metaRefresh.setCreatedOn(new Date());
			metaRefresh.setModifiedBy(loggedUser);
			metaRefresh.setModifiedOn(new Date());
			result1 = masterDao.saveEntityObject(metaRefresh);
		}		
			//ENCRYPTING PASSWORD FOR FTP START
			if(partnerConfig.getFtpDetails() != null && !partnerConfig.getFtpDetails().isEmpty())
			{
				for(FTPDetailsEntity ftpentity : partnerConfig.getFtpDetails()) 
				{
					EncryptDecrypt ed= new EncryptDecrypt();
					ftpentity.setPassword(ed.encrypt(ftpentity.getPassword()));
					ftpList.add(ftpentity);
				}
				partnerConfig.setFtpDetails(ftpList);
			}
			//ENCRYPTING PASSWORD FOR FTP END
			partnerConfig.setMetaRefreshInterval(null);
			result = masterDao.saveEntityObject(partnerConfig);
			
			if (result && result1) {
				status.setCode("SUCCESS_SAVING_GDS_CONFIG");
				status.setMessage(SUCCESS_SAVING_GDS_CONFIG);
				status.setStatus(true);
			} 
			else if(result && !result1){
				status.setCode("SUCCESS_SAVING_GDS_CONFIG");
				status.setMessage("GDS Configuration saved successfully");
				status.setStatus(true);
			}else {
				status.setCode("FAILURE_SAVING_GDS_CONFIG");
				status.setMessage(FAILURE_SAVING_GDS_CONFIG);
				status.setStatus(false);
			}		
		}
		catch(Exception e){
			logger.error("Exception :: ", e);
			status.setCode("FAILURE_SAVING_GDS_CONFIG");
			status.setMessage(FAILURE_SAVING_GDS_CONFIG);
			status.setStatus(false);
		}
		finalData.put("status", status);
		return finalData;
	}
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public Map<String, Object> getPartnerConfigById(String id) {
		Map<String, Object> finalData = new HashMap<>();
		CPartnerConfigEntity entityObj= null;
		Status status = new Status();
		
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("_id", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			entityObj = masterDao.getEntityObject(new CPartnerConfigEntity(), criteriaMap);
			
			if (entityObj!=null) {
				status.setCode("SUCCESS_PARTNER_CONFIG_RETRIEVED");
				status.setMessage("SUCCESS_PARTNER_CONFIG_RETRIEVED");
				status.setStatus(true);
			} else {
				status.setCode("FAILURE_SAVING_PARTNER_CONFIG");
				status.setMessage("FAILURE_SAVING_PARTNER_CONFIG");
				status.setStatus(false);
			}		
		}
		catch(Exception e){
			logger.error("Exception :: ", e);
			status.setCode("FAILURE_SAVING_PARTNER_CONFIG");
			status.setMessage("FAILURE_SAVING_PARTNER_CONFIG");
			status.setStatus(false);
		}
		finalData.put("status", status);
		finalData.put("data", entityObj);
		return finalData;
	}

	@Override
	public Map<String, Object> editPartnerConfig(CPartnerConfigEntity modelObj, String id, String loggedUser) {
		logger.info("INSIDE editPartnerConfig FUNCTION");
		boolean result = false;
		boolean result1 = false;
		Map<String, Object> finalData = new HashMap<>();
		CMetaRefreshEntity entityObjForMetaRefresh =null;
		Status status = new Status();
		try 
		{

			/**
			 * Partner Type Name can't be duplicated
			 */
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("_id", id);
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			CPartnerConfigEntity entityObj = masterDao.getEntityObject(new CPartnerConfigEntity(), criteriaMap);

			if (null != entityObj) {
				criteriaMap.clear();
				criteriaMap.put("partnerId", entityObj.getPartnerId());
				criteriaMap.put("isActive", Boolean.TRUE);
				criteriaMap.put("isDeleted", Boolean.FALSE);
				entityObjForMetaRefresh = masterDao.getEntityObject(new CMetaRefreshEntity(), criteriaMap);

			}
			if (null == entityObj) {
				status.setCode("FAILURE_EDIT_GDS_NOT_EXISTS");
				status.setMessage(FAILURE_EDIT_GDS_NOT_EXISTS);
				status.setStatus(false);
			} else {
				modelObj.setCreatedOn(entityObj.getCreatedOn());
				modelObj.setCreatedBy(entityObj.getCreatedBy());
				modelObj.setIsActive(entityObj.getIsActive());
				modelObj.setIsDeleted(entityObj.getIsDeleted());
				BeanUtils.copyProperties(modelObj, entityObj);
				entityObj.setMetaRefreshInterval(null);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

				result = masterDao.updateEntityObject(entityObj);

				if (modelObj.getMetaRefreshInterval() != null && result && entityObjForMetaRefresh != null
						&& entityObjForMetaRefresh.getInterval() != null) {
					if (!modelObj.getMetaRefreshInterval().equals(entityObjForMetaRefresh.getInterval())) {
						entityObjForMetaRefresh.setInterval(modelObj.getMetaRefreshInterval());
						entityObjForMetaRefresh.setStartDate(new Timestamp(new Date().getTime()));
						entityObjForMetaRefresh.setLastRun(new Timestamp(new Date().getTime()));
						entityObjForMetaRefresh
								.setNextRun((new DateTime().plusDays(modelObj.getMetaRefreshInterval()).toDate()));
						entityObjForMetaRefresh.setModifiedBy(loggedUser);
						entityObjForMetaRefresh.setModifiedOn(new Timestamp(new Date().getTime()));

						result1 = masterDao.updateEntityObject(entityObjForMetaRefresh);
					}
				}
				// IN CASE IF INTERVAL IS NOT KEPT MANDATORY DURING FIST TIME CREATION AND
				// CHOOSED DURING EDIT
				// THEN WE NEED TO CREATE A NEW ENTRY IN CMETAREFRESH

				else if (modelObj.getMetaRefreshInterval() != null && result && entityObjForMetaRefresh == null) {

					entityObjForMetaRefresh = new CMetaRefreshEntity();
					entityObjForMetaRefresh.setPartnerId(modelObj.getPartnerId());
					entityObjForMetaRefresh.setInterval(modelObj.getMetaRefreshInterval());
					entityObjForMetaRefresh.setStartDate(new Timestamp(new Date().getTime()));
					entityObjForMetaRefresh.setLastRun(new Timestamp(new Date().getTime()));
					entityObjForMetaRefresh
							.setNextRun((new DateTime().plusDays(modelObj.getMetaRefreshInterval()).toDate()));
					entityObjForMetaRefresh.setCreatedOn(new Timestamp(new Date().getTime()));
					entityObjForMetaRefresh.setCreatedBy(loggedUser);
					entityObjForMetaRefresh.setModifiedBy(loggedUser);
					entityObjForMetaRefresh.setModifiedOn(new Timestamp(new Date().getTime()));

					entityObjForMetaRefresh.setIsActive(Boolean.TRUE);
					entityObjForMetaRefresh.setIsDeleted(Boolean.FALSE);

					result1 = masterDao.updateEntityObject(entityObjForMetaRefresh);
				}
				if (result) {
					status.setCode("SUCCESS_EDIT_GDS_CONFIG");
					status.setMessage(SUCCESS_EDIT_GDS_CONFIG);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_EDIT_GDS_CONFIG");
					status.setMessage(FAILURE_EDIT_GDS_CONFIG);
					status.setStatus(false);
				}
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_GDS_CONFIG");
			status.setMessage(FAILURE_EDIT_GDS_CONFIG);
			status.setStatus(false);
		}
		finally {
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public APIResponse<Object> customTemplateMData(List<String> custMdata) throws Exception {
		APIResponse<Object> response = new APIResponse<>();
		SimpleDateFormat sm = new SimpleDateFormat(templateDateFormat);
		String strDate = sm.format(new Date());
		
		File tempExportDir = new File(strCustomDownloadPath);
		if (!tempExportDir.exists())
			tempExportDir.mkdirs();
		
		String fileName = strCustomDownloadPath + templateName + strDate + templateFileFormat;
		if(!custMdata.isEmpty())
			custMdata.add("Value");
		
		OutputStream fos = null;
		try (XSSFWorkbook workBook = new XSSFWorkbook()) {
			File file = new File(fileName);
			CellStyle style = null;
			DataFormat format = null;
			format = workBook.createDataFormat();
			style = workBook.createCellStyle();
			style.setDataFormat(format.getFormat("@"));

			XSSFSheet sheet = workBook.createSheet("Sheet1");
			int currentRowNumber = 0;
			Row rowHeader = sheet.createRow(currentRowNumber);
			for (int i = 0; i < custMdata.size(); i++) {
				Cell headerCell = rowHeader.createCell(i);
				headerCell.setCellValue(custMdata.get(i));
				sheet.setColumnWidth(i, 3000);
				sheet.setDefaultColumnStyle(i, style);
			}
			fos = new FileOutputStream(file);

			workBook.write(fos);
			fos.flush();
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus(success);
			response.setStatusMessage(successDownload);
			response.setData(strCustomDownloadPath + templateName + strDate + templateFileFormat);
		} catch (Exception e) {
			logger.error("Error occurred while retrieving custom template xlsx::", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus(failure);
			response.setStatusMessage(failureDownload);
			//response.setData(strCustomDownloadPath+templateName + strDate + templateFileFormat);
		} finally {
			if(null != fos)
				fos.close();
		}
		return response;
	}
	
	
	@Override
	public Map<String, Object> deletePartnerConfig(String id, String loggedUser) {
		logger.info("INSIDE deletePartnerConfig FUNCTION");
		boolean result = false;
		boolean result1 = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		CMetaRefreshEntity entityObjForRefresh=null;
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("_id", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			CPartnerConfigEntity entityObj = masterDao.getEntityObject(new CPartnerConfigEntity(), criteriaMap);
			
			if(null != entityObj) {
			criteriaMap.clear();	
			criteriaMap.put("partnerId", entityObj.getPartnerId());
			criteriaMap.put("isDeleted", Boolean.FALSE);
			entityObjForRefresh = masterDao.getEntityObject(new CMetaRefreshEntity(), criteriaMap);
			}
			
			if (null == entityObj) {
				status.setCode("FAILURE_DELETE_GDS_NT_EXIST");
				status.setMessage(FAILURE_DELETE_GDS_NT_EXIST);
				status.setStatus(false);
			}
			else {
				entityObj.setIsDeleted(Boolean.TRUE);
				entityObj.setIsActive(Boolean.FALSE);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

				result = masterDao.updateEntityObject(entityObj);
				
				if(result && entityObjForRefresh!=null) {
				entityObjForRefresh.setIsDeleted(Boolean.TRUE);
				entityObjForRefresh.setIsActive(Boolean.FALSE);
				entityObjForRefresh.setModifiedBy(loggedUser);
				entityObjForRefresh.setModifiedOn(new Timestamp(new Date().getTime()));
				
				result1 = masterDao.updateEntityObject(entityObjForRefresh);
				}
				//IN CASE IF INTERVAL IS NOT KEPT MANDATORY AND WE WANT TO DELETE GDS CONFIG
				//WE NEED TO SET result1 as true SO THAT IT does'nt show error message
				else
					result1=true;
				if (result && result1) {
					status.setCode("SUCCESS_DELETE_GDS_CONFIG");
					status.setMessage(SUCCESS_DELETE_GDS_CONFIG);
					status.setStatus(true);
				}
				else if(result && !result1) {
					status.setCode("SUCCESS_DELETE_GDS_CONFIG");
					status.setMessage("GDS configuration deleted : Error in deleting refresh interval");
					status.setStatus(true);
				}
				else {
					status.setCode("FAILURE_DELETE_GDS_CONFIG");
					status.setMessage("FAILURE_DELETE_GDS_CONFIG");
					status.setStatus(false);
				}
			}
		}
		catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_GDS_CONFIG");
			status.setMessage(FAILURE_DELETE_GDS_CONFIG);
			status.setStatus(false);
		} finally {
			finalData = getPartnerConfigById(id);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> getAllpartnerconfig(String loggedUser) {
		logger.info("INSIDE getAllpartnerconfig FUNCTION");
		List<CPartnerConfig> partnerss = new LinkedList<>();
		List<CPartnerConfig> toVo = new ArrayList<>();
		List<CPartnerConfig> finalList = new ArrayList<>();
		List<PartnerEntity> mPartnerList = new ArrayList<>();
		List<CMetaRefreshEntity> metaRefreshList = null;
		
		UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
		
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			toVo = (List<CPartnerConfig>) masterDao.getAllEntityObjects(new CPartnerConfig(), criteriaMap);
			
			criteriaMap.put("partnerTypeId", userEntityObj.getPermissionGroup().getPartnerType());
			criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
			mPartnerList = (List<PartnerEntity>) masterDao.getAllEntityObjects(new PartnerEntity(), criteriaMap);
			List<String> mPartnerId = mPartnerList.stream().map(p->p.getPartnerId()).collect(Collectors.toList());
			List<String> cPartnerId = toVo.stream().map(p->p.getPartnerId()).collect(Collectors.toList());
			
			finalList = toVo.stream().filter(p -> mPartnerId.contains(p.getPartnerId())).collect(Collectors.toList());
			// 28 NOV 2018
			
			criteriaMap.clear();
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("partnerId", cPartnerId);
			
			metaRefreshList = (List<CMetaRefreshEntity>) masterDao.getAllEntityObjects(new CMetaRefreshEntity(), criteriaMap);
			if(metaRefreshList != null) {
				for (CPartnerConfig partnerEntity : finalList) {
					CMetaRefreshEntity obj = metaRefreshList.stream()
		  					.filter(p -> p.getPartnerId().equalsIgnoreCase(partnerEntity.getPartnerId()))
		  					.findAny()
		  	                .orElse(null);
					if(obj != null)
						partnerEntity.setMetaRefreshInterval(obj.getInterval());
					partnerss.add(partnerEntity);
				}	
			}
			
			// 28 NOV 2018 END
			logger.info("Configured GDS List = "+finalList.size());
			for(String partnerId : mPartnerId) {
				if(!cPartnerId.contains(partnerId)) {
					CPartnerConfig addPartner = new CPartnerConfig();
					addPartner.setPartnerId(partnerId);
					addPartner.setIsActive(Boolean.TRUE);
					addPartner.setIsDeleted(Boolean.FALSE);
					finalList.add(addPartner);
				}
			}
			
			if(!finalList.isEmpty())
			{
				status.setCode("SUCCESS_RETRIEVE_PARTNER_CONFIG_LIST");
				status.setMessage("SUCCESS_RETRIEVE_PARTNER_CONFIG_LIST");
				status.setStatus(true);
			}
			
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_PARTNER_CONFIG_LIST");
			status.setMessage(FAILURE_RETRIEVE_PARTNER_CONFIG_LIST);
			status.setStatus(false);
		}			
		finally {
			finalData.put("status", status);
			finalData.put("data", finalList);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrieveAllGdsConfig(String loggedUser) {
		logger.info("INSIDE retrieveAllGdsConfig FUNCTION");
		Map<String, Object> finalData = new HashMap<>();
		boolean exportFlg = false;
		Status status = new Status();
		List<CPartnerConfig> finalList = null;

		try {
			finalData = getAllpartnerconfig(loggedUser);
			finalList = (List<CPartnerConfig>) finalData.get("data");
			logger.info("EXPORT LIST SIZE = " , finalList.size());

			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			Date today = Calendar.getInstance().getTime();
			String currentDate = df.format(today);

			File tempExportDir = new File(TEMP_OUTPUT_FOLDER_GDS);
			if (!tempExportDir.exists())
				tempExportDir.mkdirs();

			String fileName = "GDS_" + loggedUser + "_" + currentDate
					+ ("XLSX".equalsIgnoreCase(GDS_EXPORT_FORMAT) ? ".xlsx" : ".csv");
			String filePath = TEMP_OUTPUT_FOLDER_GDS + File.separator + fileName;

			URL url = null;
			AwsAPIEntity apiEntity = masterDao.getAwsCredentials();

			Date urlExpiryDate = new Date();
			long msec = urlExpiryDate.getTime();
			msec += Integer.valueOf(EXPORT_URL_EXPIRY_DAYS) * 24 * 60 * 60 * 1000;
			urlExpiryDate.setTime(msec);

			exportFlg = export(finalList, filePath);
			if (exportFlg) {

				url = masterDao.uploadFileToS3(apiEntity, filePath, fileName, urlExpiryDate, EXPORT_GDS_FILE_S3_PATH);

				if (null != url) {

					DownloadEntity entityObj = new DownloadEntity();
					entityObj.setUniqueId("GDS_" + loggedUser + "_" + currentDate);
					entityObj.setRequestedDate(today);
					entityObj.setUserId(loggedUser);
					entityObj.setDownloadStatus(DOWNLOAD_STATUS_GDS);
					entityObj.setExpiryDays(EXPORT_URL_EXPIRY_DAYS);
					entityObj.setDownloadType(DOWNLOAD_TYPE_GDS);
					entityObj.setCreatedBy(loggedUser);
					entityObj.setCreatedDate(today);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedDate(today);
					entityObj.setDownloadLink(url);
					entityObj.setExpiryDate(urlExpiryDate);

					if (masterDao.saveEntityObject(entityObj)) {
						boolean deleted = new File(filePath).delete();
						if (deleted) {
							logger.info("File deleted successfully");
						}
						status.setCode("SUCCESS_EXPORT_GDS_LIST");
						status.setMessage(SUCCESS_EXPORT_GDS_LIST);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_EXPORT_GDS_LIST");
						status.setMessage(FAILURE_EXPORT_GDS_LIST);
						status.setStatus(false);
					}
				} else {
					status.setCode("FAILURE_EXPORT_GDS_LIST");
					status.setMessage(FAILURE_EXPORT_GDS_LIST);
					status.setStatus(false);
				}
			}
		}
		catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			
				status.setCode("FAILURE_EXPORT_GDS_LIST");
				status.setMessage(FAILURE_EXPORT_GDS_LIST);
				status.setStatus(false);
						
		} finally {
			finalData.put("status", status);
			finalData.put("data", finalList);
		}
		return finalData;
				
		
	}
	
	public Boolean export(List<CPartnerConfig> partners, String fileName) {
		logger.info("INSIDE EXPORT FUNCTION");
		boolean exportFlg = true;
		List<FormatEntity> formats = new ArrayList<>();
		List<PartnerEntity> partnerList = new ArrayList<>();
		List<MetadataFieldEntity> metadataList = new ArrayList<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("isActive", Boolean.TRUE);
			
			formats = (List<FormatEntity>) masterDao.getAllEntityObjects(new FormatEntity(), criteriaMap);
			partnerList = (List<PartnerEntity>) masterDao.getAllEntityObjects(new PartnerEntity(), criteriaMap);
			metadataList = (List<MetadataFieldEntity>) masterDao.getAllEntityObjects(new MetadataFieldEntity(), criteriaMap);

			criteriaMap.clear();
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			Map<String, String> thierarchyIdNameMap = masterDao.gethierarchyIdNameMap();

			String[] headers = {"PARTNER NAME","HIERARCHY","FORMATS","COVER REQUIRED","COVER FORMAT","EXCEL FIELDS"
					,"EXCLUDE FIELDS","TRANSFORM FIELDS","METADATA REQUIRED","METADATA TYPE",
					"ONIX TYPE","TRANSFER FOLDER STRUCTURE","FTP DETAILS", "EMAIL","SIMULATION", 
					"CDATA REQUIRED","METADATA REFRESH INTERVAL","ACTIVE","PARTNER_STATUS"};
			
			if("XLSX".equalsIgnoreCase(GDS_EXPORT_FORMAT)) {
				
			}else {
				CSVWriter writer = null;
				try {
					File exportFile = new File(fileName);
					writer = new CSVWriter(new FileWriter(exportFile));
					writer.writeNext(headers);
					for(CPartnerConfig partner: partners) {
						try {
					    	int count = 0;
					    	StringBuilder formatSb = new StringBuilder();
					    	StringBuilder coverSb = new StringBuilder();
					    	String partnerName = partnerList.stream().filter(p -> p.getPartnerId().equalsIgnoreCase(partner.getPartnerId())).map(p -> p.getPartnerName()).collect(Collectors.joining());
					    	StringBuilder hierarchySb = new StringBuilder();
					    	if(partner.getProductHierarchyIds()!=null && partner.getProductHierarchyIds().size()>0) {
					    		for(String hId : partner.getProductHierarchyIds()) {
					    			count++;
					    			hierarchySb.append(thierarchyIdNameMap.get(hId));
					    			if(count<partner.getProductHierarchyIds().size())
					    				hierarchySb.append(" ,");
					    		}
					    	}
					    	Boolean partnerActiveStatus = partnerList.stream().filter(p -> p.getPartnerId().equalsIgnoreCase(partner.getPartnerId())).map(p -> p.getIsActive()).findFirst().get();
					    	
					    	if(partner.getFormatDetails() != null && !partner.getFormatDetails().isEmpty()) {
					    	String formatNames ="";
					       	String formatNaming ="";	
					       	count = 0;
					    	for(FormatDetails fd : partner.getFormatDetails()) {
					    		count ++;
					    		formatNames = "";
					    		formatNaming ="";
					        	if(fd.getFormatId()!=null && !fd.getFormatId().equalsIgnoreCase(""))
					        		formatNames = formats.stream().filter(p -> p.getFormatId().equalsIgnoreCase(fd.getFormatId()))
					        							.map(p -> p.getFormatName()).findFirst().orElse(" ");
					        	if(fd.getFieldDisplayName()==null && !fd.getFormatNaming().equalsIgnoreCase("*")) {
						        	if(fd.getFormatNaming()!=null && !fd.getFormatNaming().equalsIgnoreCase("*") && !fd.getFormatNaming().equalsIgnoreCase(""))
						        		formatNaming = metadataList.stream().filter(p -> p.getJsonPath().equalsIgnoreCase(fd.getFormatNaming()))
						        							.map(p -> p.getFieldDisplayName()).findFirst().orElse(" ");
					        	}
					        	else if(fd.getFormatNaming().equalsIgnoreCase("*"))
					        		formatNaming = "*";
					        	else
					        		formatNaming = fd.getFieldDisplayName();
					        	formatSb.append("Format Name = "+formatNames);
					        	formatSb.append(" , Format Naming = "+formatNaming+" , ");
					        	formatSb.append("Format naming prefix = "+(fd.getFormatNamingPrefix() != null?fd.getFormatNamingPrefix():"NULL")+" , ");
					        	formatSb.append("Format naming suffix = "+(fd.getFormatNamingSuffix() != null?fd.getFormatNamingSuffix():"NULL")+" , ");
					        	formatSb.append("Epub Version = "+(fd.getePubVersion()!=null?fd.getePubVersion():"NULL")+" , ");
					        	formatSb.append("Apple File Type = "+(fd.getAppleFileType()!=null?fd.getAppleFileType():"NULL")+" , ");
					        	formatSb.append("Epub Validator = "+((fd.getEpubValidator()!=null && fd.getEpubValidator())?"Yes":"No")+" , ");
					        	formatSb.append("Compressed if multiple = "+((fd.getCompressedIfMultiple()!=null && fd.getCompressedIfMultiple())?"Yes":"No"));
					        	if(count<partner.getFormatDetails().size())
					        		formatSb.append("\n");
					    		}
					  
					    	}
							if (partner.getCoverFormat() != null && !partner.getCoverFormat().isEmpty()) {
								count = 0;
								String coverformatNames = "";
								for (CoverFormat cf : partner.getCoverFormat()) {
									count++;
									coverformatNames = "";

									if (cf.getFormatId() != null && !cf.getFormatId().equalsIgnoreCase(""))
										coverformatNames = formats.stream()
												.filter(p -> p.getFormatId().equalsIgnoreCase(cf.getFormatId()))
												.map(p -> p.getFormatName()).findFirst().orElse(" ");
									coverSb.append("Format Name = " + coverformatNames);
									if (cf.getFormatOrder() != null)
										coverSb.append(" , Cover Order = " + cf.getFormatOrder().toString());
									if (count < partner.getCoverFormat().size())
										coverSb.append("\n");
								}
							}
					    	
					    	StringBuilder excelSb = new StringBuilder();
					    	StringBuilder transformSb = new StringBuilder();
					    	
					    	if(partner.getExcelFieldDetails() != null && !partner.getExcelFieldDetails().isEmpty()) {
					    		count = 0;
					    		for(ExcelFieldDetails efd : partner.getExcelFieldDetails()) {
					    		count++;
					    		String excelFieldNames ="";
					    		if(efd.getMetaDataFieldName()!=null && !efd.getMetaDataFieldName().equals("")) {
					    			excelFieldNames = metadataList.stream().filter(p -> p.getMetaDataFieldName().equalsIgnoreCase(efd.getMetaDataFieldName()))
					        									.map(p -> p.getFieldDisplayName()).findFirst().orElse(" ");
					    		}
					        	excelSb.append("Field Name = "+excelFieldNames);
					        	excelSb.append(", Sheet Name = "+(efd.getSheetName()!=null?efd.getSheetName():"NULL"));
					        	excelSb.append(", Display Order = "+(efd.getDisplayOrderNo()!=null?efd.getDisplayOrderNo():"NULL"));
					        	excelSb.append(", Prefix = "+(efd.getPrefix()!=null?efd.getPrefix():"NULL")+", ");
					        	excelSb.append("Suffix = "+(efd.getSuffix()!=null?efd.getSuffix():"NULL"));
					        	excelSb.append(", Default ="+(efd.getDefaultValue()!=null?efd.getDefaultValue():"NULL")+", Active = "+((efd.getIsActive() != null && efd.getIsActive())?"Yes":"No"));
					        	if(count<partner.getExcelFieldDetails().size())
					        		excelSb.append("\n");
					        	}
					    	}
					    	String excludeFieldNames ="";
					    	if(partner.getExcludeFieldList() != null && !partner.getExcludeFieldList().isEmpty()) {
					    	excludeFieldNames = metadataList.stream().filter(p -> partner.getExcludeFieldList().contains(p.getMetaDataFieldName()))
									.map(p -> p.getFieldDisplayName()).collect(Collectors.joining(",")); 
					    	}
					    	
					    	String transformFieldNames="";

					    	if(partner.getTransformFieldList() != null && !partner.getTransformFieldList().isEmpty()) {
					    		count = 0;
					    		
					    		for(com.codemantra.manage.admin.model.TransformFieldDetails tfd : partner.getTransformFieldList()) {
					    		count++;
					    		int innerCount = 0;
					    		if(tfd.getMetaDataFieldName()!=null && !tfd.getMetaDataFieldName().equalsIgnoreCase("")) 
					    			transformFieldNames = metadataList.stream().filter(p -> p.getMetaDataFieldName().equalsIgnoreCase(tfd.getMetaDataFieldName()))
					        									.map(p -> p.getFieldDisplayName()).findFirst().orElse(" ");
					    		transformSb.append("Field Name = "+transformFieldNames+", Operation = "+tfd.getOperation()+", ");
					        	if(tfd.getOperation()!=null && tfd.getOperation().equalsIgnoreCase("JOIN")){
					        		transformSb.append("Join By = '"+tfd.getJoinBy()+"'");
					        	}
					        	else if(tfd.getOperation()!=null && (tfd.getOperation().equalsIgnoreCase("REPLACE") 
					        			|| tfd.getOperation().equalsIgnoreCase("CONCAT_PREFIX") 
					        			|| tfd.getOperation().equalsIgnoreCase("CONCAT_SUFFIX"))) {
					        		transformSb.append("Look Up = ");
					        		for(Map.Entry<String,String> lookup : tfd.getLookUp().entrySet()) {
					        			transformSb.append(lookup.getKey()+":"+lookup.getValue());
					        			innerCount++;
					        			if(innerCount<tfd.getLookUp().size()) {
					        				transformSb.append(" , ");
					        			}
					        		}
					        	}
					        	else if(tfd.getOperation()!=null && (tfd.getOperation().equalsIgnoreCase("Arithmetic"))){
					        		transformSb.append("Arithmetic expression = " + tfd.getArithmeticExpression());
					        	}
					        	else if(tfd.getOperation()!=null && (tfd.getOperation().equalsIgnoreCase("Uppercase") ||
					        			tfd.getOperation().equalsIgnoreCase("Lowercase"))) {
					        		transformSb.append("Change case to = " + tfd.getOperation());
					        	}
					        	if(count<partner.getTransformFieldList().size())
					        		transformSb.append("\n");
					        	}
					    	}
					    	StringBuilder sb = new StringBuilder();
					    	
					    	if(partner.getFtpDetails() != null && !partner.getFtpDetails().isEmpty()) {
					    		count = 0;
					    		for(FTPDetails ftp : partner.getFtpDetails()) {
					    			count++;
					    			sb.append("FTP Type = "+(ftp.getFtpType()!=null?ftp.getFtpType():"NULL"));
					    			sb.append(", FTP Protocol = "+(ftp.getProtocol()!=null?ftp.getProtocol():"NULL"));
					    			sb.append(", FTP Host = "+(ftp.getHost()!=null?ftp.getHost():"NULL"));
					    			sb.append(", FTP Port = "+(ftp.getPort()!=null?ftp.getPort():"NULL"));
					    			sb.append(", FTP UserName = "+(ftp.getUserName()!=null?ftp.getUserName():"NULL"));
					    			sb.append(", FTP Password = "+(ftp.getPassword()!=null?ftp.getPassword():"NULL"));
					    			sb.append(", FTP Path = "+(ftp.getFtpPath()!=null?ftp.getFtpPath():"NULL"));
					    			sb.append(", FTP Connection Timeout = "+(ftp.getConnectionTimeout()!=null?ftp.getConnectionTimeout():"NULL"));
					    			sb.append(", FTP Data Timeout ="+(ftp.getDataTimeout()!=null?ftp.getDataTimeout():"NULL"));
					    			if(count<partner.getFtpDetails().size())
					    				sb.append("\n");
					    		}
					    		
					    	}
					    	
					    	writer.writeNext(new String[]{partnerName,hierarchySb.toString(), formatSb.toString(),
					    			(partner.getCoverRequired()!=null && partner.getCoverRequired())?"Yes":"No",coverSb.toString(),	
					    			excelSb.toString(),excludeFieldNames,transformSb.toString(),
					    			(partner.getMetadataRequired()!=null && partner.getMetadataRequired())?"Yes":"No",
					    			(partner.getMetadataType()!=null)?partner.getMetadataType():"NULL",
					    			(partner.getOnixType()!=null)?partner.getOnixType():"NULL",
					    			(partner.getTransferFolderStructure()!=null && partner.getTransferFolderStructure()?"Yes":"No"),		
					    			sb.toString(),partner.getEmailId(),
					    			(partner.getIsSimulation()!=null && partner.getIsSimulation())?"Yes":"No",
					    			(partner.getcDATARequired()!=null && partner.getcDATARequired())?"Yes":"No",
					    			partner.getMetaRefreshInterval()!=null?partner.getMetaRefreshInterval().toString():"NULL"
					    			,partner.getIsActive()?"Yes":"No",partnerActiveStatus?"Active":"InActive"
					    			
					    		 });
							}
						catch(Exception e) {
							logger.info("Error in retrieving partner's GDS details :"+partner.getPartnerId());
						}
					}		        
					writer.flush();
				} catch (Exception e) {
					logger.info("Exception occurred", e);
				} finally {
					if(null != writer)
						writer.close();
				}
			}
	        
		}catch(Exception e) {
			logger.info("Error in exporting gds");
			e.printStackTrace();
			exportFlg = false;
		}
		
		return exportFlg;
	}

	@Override
	public Boolean readLookupExcel(ReadExcelData modelObj) throws Exception {
		Boolean successFlag = false;
		FileInputStream inputStream = null;
		Workbook wb = null;
		try{
			
		File file = new File(modelObj.getFilePath());
		inputStream = new FileInputStream(file);
		wb = new XSSFWorkbook(inputStream);
		Sheet sh = wb.getSheet("Sheet1");
		List<String> lookupValues = new ArrayList<>();
		List<List<String>> lookupValuesFinalList = new ArrayList<>();
		List<String> metaDataFieldNames = null;
		List<MetadataFieldEntity> metadataFieldEntityObj = null;
		
		metadataFieldEntityObj = (List<MetadataFieldEntity>) masterDao
				.getAllEntityObjects(new MetadataFieldEntity());
		
		if(metadataFieldEntityObj != null) {
			metaDataFieldNames = metadataFieldEntityObj.stream().filter(transaction -> transaction.getMetaDataFieldName() != null)
					.map(transaction -> transaction.getMetaDataFieldName()).distinct().collect(Collectors.toList());
		}
		
		Iterator rowIterate = sh.rowIterator();
		int i = 0;
		Row r=sh.getRow(0);
	    Iterator c = r.cellIterator();
		while(c.hasNext()) {
	    	Cell cc = (Cell) c.next();
	    	if(cc != null && (metaDataFieldNames.contains(cc.getStringCellValue()) 
	    			|| cc.getStringCellValue().equalsIgnoreCase("Value"))){
	    	i = cc.getColumnIndex();
	    	for (Row row : sh) { // For each Row.
			      Cell cell = row.getCell(i);
			      DataFormatter formatter = new DataFormatter();
	  	    	String val = formatter.formatCellValue(cell);
	  	    	lookupValues.add(val);
			 }
	    	lookupValuesFinalList.add(lookupValues);
	    	lookupValues = new ArrayList<>();
	    	}
		}
		
		if(lookupValuesFinalList!=null && lookupValuesFinalList.size() > 1)
		{
			CLookupMappingPartnerConfig obj = new CLookupMappingPartnerConfig();
			CodeListConditions transformObj = new CodeListConditions();
			TransformCodeList transformCodeObj = new TransformCodeList();
			List<CodeListConditions> transformObjList = new ArrayList<>();
			List<TransformCodeList> transformCodeList = new ArrayList<>();
			List<TransformRules> transformRulesListFinal = new ArrayList<>();
			obj.setPartnerId(modelObj.getPartnerId());
			for(int k=0; k<lookupValuesFinalList.get(0).size()-1 ; k++) {
				for(int j = 0; j<lookupValuesFinalList.size() ; j++)
					if (j < lookupValuesFinalList.size() - 1) {
						transformObj.setMetaDataFieldName(lookupValuesFinalList.get(j).get(0));
						transformObj.setOperation("=");
						transformObj.setValue(lookupValuesFinalList.get(j).get(k + 1));
						transformObjList.add(transformObj);
						transformObj = new CodeListConditions();
					} else {
						transformCodeObj.setMetaDataFieldName(lookupValuesFinalList.get(j).get(0));
						transformCodeObj.setOperation("REPLACE");
						transformCodeObj.setReplaceValue(lookupValuesFinalList.get(j).get(k + 1));
						transformCodeList.add(transformCodeObj);
						transformCodeObj = new TransformCodeList();
					}
				
					TransformRules rules = new TransformRules();
					rules.setCodeListConditions(transformObjList);
					rules.setTransformCodeList(transformCodeList);
					transformRulesListFinal.add(rules);
					
					transformObjList = new ArrayList<>();
					transformCodeList = new ArrayList<>();
					transformObj = new CodeListConditions();
					transformCodeObj = new TransformCodeList();
					rules = new TransformRules(); 	
			}
			
			obj.setTransformRules(transformRulesListFinal);
			obj.setFieldsToMap(modelObj.getFieldsToMap());
			obj.setLookupName(modelObj.getLookupName());
			obj.setIsActive(Boolean.TRUE);
			obj.setIsDeleted(Boolean.FALSE);
			obj.setCreatedBy(modelObj.getUserId());
			obj.setModifiedBy(modelObj.getUserId());
			obj.setCreatedOn(new Date());
			obj.setModifiedOn(new Date());
			if(modelObj.getFilePath() != null)
				obj.setFilePath(modelObj.getFilePath());
			
			CLookupMappingPartnerConfig entityObj = new CLookupMappingPartnerConfig();
			
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("partnerId", modelObj.getPartnerId());
			criteriaMap.put("lookupName", modelObj.getLookupName());
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			entityObj = masterDao.getEntityObject(new CLookupMappingPartnerConfig(), criteriaMap);
			
			if(entityObj != null) {
				entityObj.setIsActive(Boolean.FALSE);
				entityObj.setIsDeleted(Boolean.TRUE);
				entityObj.setModifiedBy(modelObj.getUserId());
				entityObj.setModifiedOn(new Date());
				masterDao.updateEntityObject(entityObj);
			}
			
			successFlag = masterDao.saveEntityObject(obj);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			successFlag = false;
		} finally {
			if(null != inputStream)
				inputStream.close();
			if(null != wb)
				wb.close();
		}
		return successFlag;
	}

	@Override
	public Map<String, Object> getAllGdsLookupNames(String loggedUser) {
		logger.info("INSIDE getAlllookupName FUNCTION");
		List<CLookupMappingPartnerConfig> cLookupMappingPartnerConfig = new LinkedList<>();
		
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			cLookupMappingPartnerConfig = (List<CLookupMappingPartnerConfig>) masterDao.getAllEntityObjects(new CLookupMappingPartnerConfig(), criteriaMap);
			if (!cLookupMappingPartnerConfig.isEmpty()) {
				status.setCode("SUCCESS_RETRIEVE_LOOKUP_LIST");
				status.setMessage("SUCCESS_RETRIEVE_LOOKUP_LIST");
				status.setStatus(true);
			} else {
				status.setCode("LOOKUP LIST IS EMPTY");
				status.setMessage("LOOKUP LIST IS EMPTY");
				status.setStatus(true);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_LOOKUP_LIST");
			status.setMessage("FAILURE_RETRIEVE_LOOKUP_LIST");
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", cLookupMappingPartnerConfig);
		}
		return finalData;
	}
	
}

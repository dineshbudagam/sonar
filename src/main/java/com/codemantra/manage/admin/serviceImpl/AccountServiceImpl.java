/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.AccountEntity;
import com.codemantra.manage.admin.entity.AccountMappedField;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.Account;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.service.AccountService;

@Service("accountService")
public class AccountServiceImpl implements AccountService {

	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${FAILURE_ADD_ACCOUNT_NAME_NULL}")
	String FAILURE_ADD_ACCOUNT_NAME_NULL;

	@Value("${FAILURE_ADD_ACCOUNT_ACCOUNTCODE_ERROR}")
	String FAILURE_ADD_ACCOUNT_ACCOUNTCODE_ERROR;

	@Value("${SUCCESS_ADD_ACCOUNT}")
	String SUCCESS_ADD_ACCOUNT;

	@Value("${FAILURE_ADD_ACCOUNT}")
	String FAILURE_ADD_ACCOUNT;

	@Value("${ACCOUNT_ALREADY_EXISTS}")
	String ACCOUNT_ALREADY_EXISTS;

	@Value("${SUCCESS_RETRIEVE_ACCOUNT_LIST}")
	String SUCCESS_RETRIEVE_ACCOUNT_LIST;

	@Value("${FAILURE_RETRIEVE_ACCOUNT_LIST}")
	String FAILURE_RETRIEVE_ACCOUNT_LIST;

	@Value("${FAILURE_ADD_MAPPING_NULL_ERROR}")
	String FAILURE_ADD_MAPPING_NULL_ERROR;

	@Value("${FAILURE_ADD_MAPPING_ACCOUNTCODE_ERROR}")
	String FAILURE_ADD_MAPPING_ACCOUNTCODE_ERROR;

	@Value("${SUCCESS_ADD_MAPPING}")
	String SUCCESS_ADD_MAPPING;

	@Value("${FAILURE_ADD_MAPPING}")
	String FAILURE_ADD_MAPPING;

	@Value("${FAILURE_EDIT_ACCOUNT_NAME_NULL}")
	String FAILURE_EDIT_ACCOUNT_NAME_NULL;

	@Value("${FAILURE_EDIT_ACCOUNT_ACCOUNTCODE_ERROR}")
	String FAILURE_EDIT_ACCOUNT_ACCOUNTCODE_ERROR;

	@Value("${SUCCESS_EDIT_ACCOUNT}")
	String SUCCESS_EDIT_ACCOUNT;

	@Value("${FAILURE_EDIT_ACCOUNT}")
	String FAILURE_EDIT_ACCOUNT;

	@Value("${FAILURE_DELETE_ACCOUNT_ACCOUNTCODE_ERROR}")
	String FAILURE_DELETE_ACCOUNT_ACCOUNTCODE_ERROR;

	@Value("${SUCCESS_DELETE_ACCOUNT}")
	String SUCCESS_DELETE_ACCOUNT;

	@Value("${FAILURE_DELETE_ACCOUNT}")
	String FAILURE_DELETE_ACCOUNT;

	@Value("${FAILURE_DELETE_MAPPING_ACCOUNTCODE_ERROR}")
	String FAILURE_DELETE_MAPPING_ACCOUNTCODE_ERROR;

	@Value("${SUCCESS_DELETE_MAPPING}")
	String SUCCESS_DELETE_MAPPING;

	@Value("${FAILURE_DELETE_MAPPING}")
	String FAILURE_DELETE_MAPPING;

	@Value("${FAILURE_ADD_NEW_MAPPING_EXISTS}")
	String FAILURE_ADD_NEW_MAPPING_EXISTS;

	@Value("${SUCCESS_ADD_NEW_MAPPING}")
	String SUCCESS_ADD_NEW_MAPPING;

	@Value("${FAILURE_ADD_NEW_MAPPING}")
	String FAILURE_ADD_NEW_MAPPING;

	@Value("${FAILURE_RETRIEVE_ACCOUNT_NOT_EXISTS}")
	String FAILURE_RETRIEVE_ACCOUNT_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_ACCOUNT}")
	String SUCCESS_RETRIEVE_ACCOUNT;

	@Value("${FAILURE_RETRIEVE_ACCOUNT}")
	String FAILURE_RETRIEVE_ACCOUNT;

	@Value("${FAILURE_EDIT_ACCOUNT_EXISTS}")
	String FAILURE_EDIT_ACCOUNT_EXISTS;

	@Override
	public Map<String, Object> save(Account account, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "CREATE", userEntityObj);

			if (permissionFlg) {
				if (masterDao.isNull(account.getAccountName())) {
					status.setCode("FAILURE_ADD_ACCOUNT_NAME_NULL");
					status.setMessage(FAILURE_ADD_ACCOUNT_NAME_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Account Name can't be duplicated
					 */
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("accountName", account.getAccountName());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					if (null == masterDao.getEntityObject(new AccountEntity(), criteriaMap)) {
						String accountId = masterDao.getNextSequenceId("accountIdSeq");
						logger.info("Account Id generated is [AC" + accountId + "]");

						if (accountId.equals("-1")) {
							status.setCode("FAILURE_ADD_ACCOUNT_ACCOUNTCODE_ERROR");
							status.setMessage(FAILURE_ADD_ACCOUNT_ACCOUNTCODE_ERROR);
							status.setStatus(false);
						} else {
							AccountEntity accountEntity = new AccountEntity();
							Timestamp currentTs = new Timestamp(new Date().getTime());

							account.setAccountId("AC".concat(accountId));
							accountEntity.setAccountId(account.getAccountId());
							accountEntity.setAccountName(account.getAccountName());
							accountEntity.setIsActive(Boolean.TRUE);
							accountEntity.setIsDeleted(Boolean.FALSE);
							accountEntity.setCreatedOn(currentTs);
							accountEntity.setCreatedBy(loggedUser);
							accountEntity.setModifiedOn(currentTs);
							accountEntity.setModifiedBy(loggedUser);

							result = masterDao.saveEntityObject(accountEntity);

							if (result) {

								criteriaMap.clear();
								criteriaMap.put("permissionGroupId", userEntityObj.getPermissionGroupId());
								criteriaMap.put("isDeleted", Boolean.FALSE);

								Map<String, Object> updateMap = new HashMap<>();
								updateMap.put("modifiedOn", currentTs);
								updateMap.put("modifiedBy", loggedUser);

								Map<String, Object> updatePushMap = new HashMap<>();
								updatePushMap.put("account", account.getAccountId());

								masterDao.updateCollectionDocument("mPermissionGroup", criteriaMap, updateMap,
										updatePushMap);

								status.setCode("SUCCESS_ADD_ACCOUNT");
								status.setMessage(SUCCESS_ADD_ACCOUNT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_ADD_ACCOUNT");
								status.setMessage(FAILURE_ADD_ACCOUNT);
								status.setStatus(false);
							}
						}
					} else {
						status.setCode("ACCOUNT_ALREADY_EXISTS");
						status.setMessage(ACCOUNT_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in saveAccount :: ", e);
			status.setCode("FAILURE_ADD_ACCOUNT");
			status.setMessage(FAILURE_ADD_ACCOUNT);
			status.setStatus(false);
		} finally {
			if (permissionFlg)
				finalData = retrieve(account.getAccountId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;

	}

	@Override
	public Map<String, Object> retrieve(String accountId, String loggedUser) {
		Account acnt = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();

		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "VIEW", userEntityObj);

			if (permissionFlg && userEntityObj.getPermissionGroup().getAccount().contains(accountId)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("accountId", accountId);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				AccountEntity entityObj = masterDao.getEntityObject(new AccountEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_ACCOUNT_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_ACCOUNT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					acnt = new Account();

					entityObj.setMetadataMappedField(CollectionUtils.emptyIfNull(entityObj.getMetadataMappedField())
							.stream().filter(obj -> (!obj.getIsDeleted())).collect(Collectors.toList()));

					BeanUtils.copyProperties(entityObj, acnt);
					status.setCode("SUCCESS_RETRIEVE_ACCOUNT");
					status.setMessage(SUCCESS_RETRIEVE_ACCOUNT);
					status.setStatus(true);
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_ACCOUNT");
			status.setMessage(FAILURE_RETRIEVE_ACCOUNT);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", acnt);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> saveMappedFieldValues(Account account, String accountId, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "EDIT", userEntityObj);
			if (permissionFlg && userEntityObj.getPermissionGroup().getAccount().contains(accountId)) {
				if (masterDao.isNull(account.getMetadataMappedFieldValues())) {
					status.setCode("FAILURE_ADD_MAPPING_NULL_ERROR");
					status.setMessage(FAILURE_ADD_MAPPING_NULL_ERROR);
					status.setStatus(false);
				} else {
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("accountId", accountId);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					AccountEntity accountEntity = masterDao.getEntityObject(new AccountEntity(), criteriaMap);

					if (null == accountEntity) {
						status.setCode("FAILURE_ADD_MAPPING_ACCOUNTCODE_ERROR");
						status.setMessage(FAILURE_ADD_MAPPING_ACCOUNTCODE_ERROR);
						status.setStatus(false);
					} else {

						criteriaMap.clear();
						criteriaMap.put("metadataMappedField.value", account.getMetadataMappedFieldValues());
						criteriaMap.put("metadataMappedField.isDeleted", Boolean.FALSE);

						List<KeyValue> list = masterDao.getKeyValueList("mAccount", "accountId",
								"metadataMappedField.value", criteriaMap, "metadataMappedField");

						if (!list.isEmpty()) {
							status.setCode("FAILURE_ADD_NEW_MAPPING_EXISTS");
							status.setMessage(FAILURE_ADD_NEW_MAPPING_EXISTS);
							status.setStatus(false);
						} else {
							List<String> metadataMappedFieldValues = account.getMetadataMappedFieldValues();
							Timestamp currentTs = new Timestamp(new Date().getTime());

							if (accountEntity.getMetadataMappedField() == null)
								accountEntity.setMetadataMappedField(new LinkedList<>());

							for (AccountMappedField accountMappedField : CollectionUtils
									.emptyIfNull(accountEntity.getMetadataMappedField().stream()
											.filter(obj -> obj.getIsDeleted()).collect(Collectors.toList()))) {
								if (CollectionUtils.emptyIfNull(metadataMappedFieldValues)
										.contains(accountMappedField.getValue())) {
									accountMappedField.setIsDeleted(Boolean.FALSE);
									accountMappedField.setIsActive(Boolean.TRUE);
									accountMappedField.setModifiedBy(loggedUser);
									accountMappedField.setModifiedOn(currentTs);
									metadataMappedFieldValues.remove(accountMappedField.getValue());
								}
							}

							AccountMappedField accountMappingField = null;

							for (String mappingValue : CollectionUtils.emptyIfNull(metadataMappedFieldValues)) {
								accountMappingField = new AccountMappedField();

								accountMappingField.setValue(mappingValue);
								accountMappingField.setIsActive(Boolean.TRUE);
								accountMappingField.setIsDeleted(Boolean.FALSE);
								accountMappingField.setCreatedOn(currentTs);
								accountMappingField.setCreatedBy(loggedUser);
								accountMappingField.setModifiedOn(currentTs);
								accountMappingField.setModifiedBy(loggedUser);

								accountEntity.getMetadataMappedField().add(accountMappingField);
								accountMappingField = null;
							}

							accountEntity.setModifiedBy(loggedUser);
							accountEntity.setModifiedOn(currentTs);

							result = masterDao.updateEntityObject(accountEntity);

							if (result) {
								status.setCode("SUCCESS_ADD_MAPPING");
								status.setMessage(SUCCESS_ADD_MAPPING);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_ADD_MAPPING");
								status.setMessage(FAILURE_ADD_MAPPING);
								status.setStatus(false);
							}
						}
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in saveMappedFieldValues :: ", e);
			status.setCode("FAILURE_ADD_MAPPING");
			status.setMessage(FAILURE_ADD_MAPPING);
			status.setStatus(false);
		} finally {
			finalData = retrieve(accountId, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> edit(Account account, String accountId, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "EDIT", userEntityObj);
			if (permissionFlg && userEntityObj.getPermissionGroup().getAccount().contains(accountId)) {
				if (masterDao.isNull(account.getAccountName())) {
					status.setCode("FAILURE_EDIT_ACCOUNT_NAME_NULL");
					status.setMessage(FAILURE_EDIT_ACCOUNT_NAME_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Account Name can't be duplicated
					 */
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("accountId", accountId);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					AccountEntity accountEntity = masterDao.getEntityObject(new AccountEntity(), criteriaMap);
					if (null == accountEntity) {
						status.setCode("FAILURE_EDIT_ACCOUNT_ACCOUNTCODE_ERROR");
						status.setMessage(FAILURE_EDIT_ACCOUNT_ACCOUNTCODE_ERROR);
						status.setStatus(false);
					} else {

						AccountEntity accountEntityExists = null;
						if (!accountEntity.getAccountName().equals(account.getAccountName())) {
							criteriaMap.clear();
							criteriaMap.put("accountName", account.getAccountName());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							accountEntityExists = masterDao.getEntityObject(new AccountEntity(), criteriaMap);
						}

						if (null == accountEntityExists) {
							accountEntity.setAccountName(account.getAccountName());
							accountEntity.setModifiedBy(loggedUser);
							accountEntity.setModifiedOn(new Timestamp(new Date().getTime()));

							result = masterDao.updateEntityObject(accountEntity);

							if (result) {
								status.setCode("SUCCESS_EDIT_ACCOUNT");
								status.setMessage(SUCCESS_EDIT_ACCOUNT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_ACCOUNT");
								status.setMessage(FAILURE_EDIT_ACCOUNT);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_EDIT_ACCOUNT_EXISTS");
							status.setMessage(FAILURE_EDIT_ACCOUNT_EXISTS);
							status.setStatus(false);
						}
					}

				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in editAccount :: ", e);
			status.setCode("FAILURE_EDIT_ACCOUNT");
			status.setMessage(FAILURE_EDIT_ACCOUNT);
			status.setStatus(false);
		} finally {
			logger.info(status.getCode());
			finalData = retrieve(accountId, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> delete(String accountId, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "DELETE", userEntityObj);
			if (permissionFlg && userEntityObj.getPermissionGroup().getAccount().contains(accountId)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("accountId", accountId);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				AccountEntity accountEntity = masterDao.getEntityObject(new AccountEntity(), criteriaMap);

				if (null == accountEntity) {
					status.setCode("FAILURE_DELETE_ACCOUNT_ACCOUNTCODE_ERROR");
					status.setMessage(FAILURE_DELETE_ACCOUNT_ACCOUNTCODE_ERROR);
					status.setStatus(false);
				} else {
					Timestamp currentTs = new Timestamp(new Date().getTime());

					for (AccountMappedField accountMappedField : CollectionUtils
							.emptyIfNull(accountEntity.getMetadataMappedField())) {
						accountMappedField.setIsDeleted(Boolean.TRUE);
						accountMappedField.setIsActive(Boolean.FALSE);
						accountMappedField.setModifiedBy(loggedUser);
						accountMappedField.setModifiedOn(currentTs);
					}

					accountEntity.setIsDeleted(Boolean.TRUE);
					accountEntity.setIsActive(Boolean.FALSE);
					accountEntity.setModifiedBy(loggedUser);
					accountEntity.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(accountEntity);

					if (result) {
						status.setCode("SUCCESS_DELETE_ACCOUNT");
						status.setMessage(SUCCESS_DELETE_ACCOUNT);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_ACCOUNT");
						status.setMessage(FAILURE_DELETE_ACCOUNT);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in deleteAccount :: ", e);
			status.setCode("FAILURE_DELETE_ACCOUNT");
			status.setMessage(FAILURE_DELETE_ACCOUNT);
			status.setStatus(false);
		} finally {
			finalData = retrieve(accountId, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> deleteMappedFieldValue(String accountId, String mappedFieldValue, String loggedUser) {
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "EDIT", userEntityObj);
			if (permissionFlg && userEntityObj.getPermissionGroup().getAccount().contains(accountId)) {

				Map<String, Object> criteriaMap = new HashMap<>();

				criteriaMap.put("accountId", accountId);
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("metadataMappedField.value", mappedFieldValue);
				criteriaMap.put("metadataMappedField.isDeleted", Boolean.FALSE);

				AccountEntity accountEntity = masterDao.getEntityObject(new AccountEntity(), criteriaMap);
				if (null == accountEntity) {
					status.setCode("FAILURE_DELETE_MAPPING_ACCOUNTCODE_ERROR");
					status.setMessage(FAILURE_DELETE_MAPPING_ACCOUNTCODE_ERROR);
					status.setStatus(false);
				} else {

					Map<String, Object> updateMap = new HashMap<>();
					updateMap.put("metadataMappedField.$.isActive", Boolean.FALSE);
					updateMap.put("metadataMappedField.$.isDeleted", Boolean.TRUE);
					updateMap.put("metadataMappedField.$.modifiedOn", new Timestamp(new Date().getTime()));
					updateMap.put("metadataMappedField.$.modifiedBy", loggedUser);

					boolean updateFlg = masterDao.updateCollectionDocument("mAccount", criteriaMap, updateMap);

					if (updateFlg) {
						status.setCode("SUCCESS_DELETE_MAPPING");
						status.setMessage(SUCCESS_DELETE_MAPPING);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_MAPPING");
						status.setMessage(FAILURE_DELETE_MAPPING);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in saveMappedFieldValues :: ", e);
			status.setCode("FAILURE_DELETE_MAPPING");
			status.setMessage(FAILURE_DELETE_MAPPING);
			status.setStatus(false);
		} finally {
			finalData = retrieve(accountId, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAll(String loggedUser) {
		List<AccountEntity> accounts = new LinkedList<AccountEntity>();
		Map<String, Object> mappingFieldData = new HashMap<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "VIEW", userEntityObj);

			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				accounts = (List<AccountEntity>) masterDao.getAllEntityObjects(new AccountEntity(), criteriaMap);

				for (AccountEntity ac : CollectionUtils.emptyIfNull(accounts)) {
					ac.setMetadataMappedField(CollectionUtils.emptyIfNull(ac.getMetadataMappedField()).stream()
							.filter(obj -> (!obj.getIsDeleted())).collect(Collectors.toList()));
				}

				mappingFieldData = getMappedFieldValues();

				status.setCode("SUCCESS_RETRIEVE_ACCOUNT_LIST");
				status.setMessage(SUCCESS_RETRIEVE_ACCOUNT_LIST);
				status.setStatus(true);

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_ACCOUNT_LIST");
			status.setMessage(FAILURE_RETRIEVE_ACCOUNT_LIST);
			status.setStatus(false);
		} finally {
			finalData.putAll(mappingFieldData);
			finalData.put("accountListStatus", status);
			finalData.put("accountList", accounts);
		}
		return finalData;
	}

	public Map<String, Object> getMappedFieldValues() throws Exception {
		Map<String, Object> mappingFieldData = new HashMap<>();
		String metadataMappedFieldLabel = null;
		String metadataMappedFieldName = null;

		List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig", "config.metadataMappingFieldName",
				"config.metadataMappingFieldLabel", "_id", "account", null);

		if (mappingValues.size() > 0) {
			metadataMappedFieldName = mappingValues.get(0).getKey();
			metadataMappedFieldLabel = (String) mappingValues.get(0).getValue();
		}
		Map<String, Object> criteriaMap = new HashMap<>();
		criteriaMap.put("Product.isDeleted", Boolean.FALSE);
		List<KeyValue> metadataMappingFieldValues = masterDao.getKeyValueList("products", metadataMappedFieldName, null,
				criteriaMap, null);
		Set<String> metadataMappingFieldValueList = CollectionUtils.emptyIfNull(metadataMappingFieldValues).stream()
				.filter(kv -> "".equals(kv.getKey()) || kv.getKey() == null).map(kv -> kv.getKey())
				.collect(Collectors.toSet());

		LinkedHashMap<String, Object> accountCriteriaMap = new LinkedHashMap<>();
		accountCriteriaMap.put("isDeleted", Boolean.FALSE);
		accountCriteriaMap.put("metadataMappedField.isDeleted", Boolean.FALSE);

		List<KeyValue> mappedValues = masterDao.getKeyValueList("mAccount", "metadataMappedField.value", null,
				accountCriteriaMap, "metadataMappedField");
		Set<String> mappedValueList = CollectionUtils.emptyIfNull(mappedValues).stream().map(kv -> kv.getKey())
				.collect(Collectors.toSet());

		metadataMappingFieldValueList.remove("");
		Set<String> metadataMappingFieldValueAvailableList = new HashSet<>(metadataMappingFieldValueList);

		metadataMappingFieldValueList.addAll(mappedValueList);
		metadataMappingFieldValueAvailableList.removeAll(mappedValueList);

		mappingFieldData.put("metadataMappedFieldName", metadataMappedFieldName);
		mappingFieldData.put("metadataMappedFieldLabel", metadataMappedFieldLabel);
		mappingFieldData.put("metadataMappingFieldValueList", metadataMappingFieldValueList);

		mappingFieldData.put("metadataMappingFieldValueAvailableList", metadataMappingFieldValueAvailableList);

		return mappingFieldData;
	}

	/**
	 * Method Not Used
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> addNewMappingValue(String mappingValue, String loggedUser) {

		List<String> metadataMappedFieldList = new LinkedList<>();
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean addFlg = false;
		try {

			List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig", "config.metadataMappingField",
					"config.values", "_id", "account", null);

			metadataMappedFieldList = (List<String>) mappingValues.get(0).getValue();

			if (metadataMappedFieldList.contains(mappingValue)) {
				status.setCode("FAILURE_ADD_NEW_MAPPING_EXISTS");
				status.setMessage(FAILURE_ADD_NEW_MAPPING_EXISTS);
				status.setStatus(false);
			} else {
				logger.info(
						"Adding new Mapping Value [" + mappingValue + "] in manageConfig for accountMapping Values");
				metadataMappedFieldList.add(mappingValue);
				addFlg = masterDao.updateCollectionDocument("manageConfig", "_id", "account", "config.values",
						metadataMappedFieldList);
				if (addFlg) {
					status.setCode("SUCCESS_ADD_NEW_MAPPING");
					status.setMessage(SUCCESS_ADD_NEW_MAPPING);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_ADD_NEW_MAPPING");
					status.setMessage(FAILURE_ADD_NEW_MAPPING);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in addNewMappingValue :: ", e);
			status.setCode("FAILURE_ADD_NEW_MAPPING");
			status.setMessage(FAILURE_ADD_NEW_MAPPING);
			status.setStatus(false);
		} finally {
			Map<String, Object> accountsMap = retrieveAll(loggedUser);
			finalData.put("status", status);
			finalData.put("data", accountsMap);
		}
		return finalData;
	}


}

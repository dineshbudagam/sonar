/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.ManageConfigEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.ProductCategoryEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.ProductCategory;
import com.codemantra.manage.admin.service.ProductCategoryService;

@Service("productCategoryService")
public class ProductCategoryServiceImpl implements ProductCategoryService {

	private static final Logger logger = LoggerFactory.getLogger(ProductCategoryServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${SUCCESS_ADD_METADATA_MAPPING}")
	String SUCCESS_ADD_METADATA_MAPPING;

	@Value("${FAILURE_ADD_METADATA_MAPPING}")
	String FAILURE_ADD_METADATA_MAPPING;

	@Value("${FAILURE_ADD_METADATA_MAPPING_EXISTS}")
	String FAILURE_ADD_METADATA_MAPPING_EXISTS;

	@Value("${FAILURE_ADD_PRODCAT_PRODCATCODE_ERROR}")
	String FAILURE_ADD_PRODCAT_PRODCATCODE_ERROR;

	@Value("${SUCCESS_ADD_PRODCAT}")
	String SUCCESS_ADD_PRODCAT;

	@Value("${FAILURE_ADD_PRODCAT}")
	String FAILURE_ADD_PRODCAT;

	@Value("${PRODCAT_ALREADY_EXISTS}")
	String PRODCAT_ALREADY_EXISTS;

	@Value("${SUCCESS_EDIT_PRODCAT}")
	String SUCCESS_EDIT_PRODCAT;

	@Value("${FAILURE_EDIT_PRODCAT}")
	String FAILURE_EDIT_PRODCAT;

	@Value("${FAILURE_EDIT_PRODCAT_ALREADY_EXISTS}")
	String FAILURE_EDIT_PRODCAT_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_PRODCAT_NOTEXIST_ERROR}")
	String FAILURE_EDIT_PRODCAT_NOTEXIST_ERROR;

	@Value("${SUCCESS_DELETE_PRODCAT}")
	String SUCCESS_DELETE_PRODCAT;

	@Value("${FAILURE_DELETE_PRODCAT}")
	String FAILURE_DELETE_PRODCAT;

	@Value("${FAILURE_DELETE_PRODCAT_NOTEXIST_ERROR}")
	String FAILURE_DELETE_PRODCAT_NOTEXIST_ERROR;

	@Value("${SUCCESS_RETRIEVE_PRODCAT_LIST}")
	String SUCCESS_RETRIEVE_PRODCAT_LIST;

	@Value("${FAILURE_RETRIEVE_PRODCAT_LIST}")
	String FAILURE_RETRIEVE_PRODCAT_LIST;

	@Value("${FAILURE_DELETE_PRODCAT_CHILDREN_ERROR}")
	String FAILURE_DELETE_PRODCAT_CHILDREN_ERROR;

	@Value("${FAILURE_RETRIEVE_PRODCAT_NOT_EXISTS}")
	String FAILURE_RETRIEVE_PRODCAT_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_PRODCAT}")
	String SUCCESS_RETRIEVE_PRODCAT;

	@Value("${FAILURE_RETRIEVE_PRODCAT}")
	String FAILURE_RETRIEVE_PRODCAT;

	@Value("${FAILURE_ADD_METADATA_MAPPING_MAND_NULL}")
	String FAILURE_ADD_METADATA_MAPPING_MAND_NULL;

	@Value("${FAILURE_ADD_METADATA_MAPPING_INVALID_VALUE}")
	String FAILURE_ADD_METADATA_MAPPING_INVALID_VALUE;

	@Override
	public Map<String, Object> saveMetadataMapping(String metadataFieldName, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "CREATE", userEntityObj);

			if (permissionFlg) {
				if (masterDao.isNull(metadataFieldName)) {
					status.setCode("FAILURE_ADD_METADATA_MAPPING_MAND_NULL");
					status.setMessage(FAILURE_ADD_METADATA_MAPPING_MAND_NULL);
					status.setStatus(false);
				} else {
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("groupId", userEntityObj.getPermissionGroup().getMetadataGroup());
					criteriaMap.put("metaDataFieldName", metadataFieldName);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					MetadataFieldEntity metadataFieldEntityObj = masterDao.getEntityObject(new MetadataFieldEntity(),
							criteriaMap);

					if (null == metadataFieldEntityObj) {
						status.setCode("FAILURE_ADD_METADATA_MAPPING_INVALID_VALUE");
						status.setMessage(FAILURE_ADD_METADATA_MAPPING_INVALID_VALUE);
						status.setStatus(false);
					} else {
						ManageConfigEntity manageConfigEntity = new ManageConfigEntity();
						LinkedHashMap<String, Object> configMap = new LinkedHashMap<>();

						configMap.put("metadataMappingField", metadataFieldEntityObj.getMetaDataFieldName());
						configMap.put("referencePath", metadataFieldEntityObj.getReferencePath());
						manageConfigEntity.setId("productCategory");
						manageConfigEntity.setConfig(configMap);

						result = masterDao.saveEntityObject(manageConfigEntity);

						if (result) {
							status.setCode("SUCCESS_ADD_METADATA_MAPPING");
							status.setMessage(SUCCESS_ADD_METADATA_MAPPING);
							status.setStatus(true);
						} else {
							status.setCode("FAILURE_ADD_METADATA_MAPPING");
							status.setMessage(FAILURE_ADD_METADATA_MAPPING);
							status.setStatus(false);
						}
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (DuplicateKeyException | com.mongodb.DuplicateKeyException e) {
			status.setCode("FAILURE_ADD_METADATA_MAPPING_EXISTS");
			status.setMessage(FAILURE_ADD_METADATA_MAPPING_EXISTS);
			status.setStatus(false);
		} catch (Exception e) {
			logger.error("Exception in saveMetadataMapping :: ", e);
			status.setCode("FAILURE_ADD_METADATA_MAPPING");
			status.setMessage(FAILURE_ADD_METADATA_MAPPING);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", null);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> save(ProductCategory modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "CREATE", userEntityObj);

			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
							.contains(modelObj.getAccountId())
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getMetadataType())
							.contains(modelObj.getMetadataTypeId())
					&& (masterDao.isNull(modelObj.getParentId()) || CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getProductCategory())
							.contains(modelObj.getParentId()))) {
				if (masterDao.isNull(modelObj.getAccountId()) || masterDao.isNull(modelObj.getMetadataTypeId())
						|| masterDao.isNull(modelObj.getProductCategoryName())
						|| masterDao.isNull(modelObj.getProductCategoryCode())) {
					status.setCode("FAILURE_ADD_METADATA_MAPPING_MAND_NULL");
					status.setMessage(FAILURE_ADD_METADATA_MAPPING_MAND_NULL);
					status.setStatus(false);
				} else {

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("productCategoryCode", modelObj.getProductCategoryCode());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					if (null == masterDao.getEntityObject(new ProductCategoryEntity(), criteriaMap)) {
						String productCategoryId = masterDao.getNextSequenceId("productCategoryIdSeq");
						logger.info("Product Category Id Code generated is [PC" + productCategoryId + "]");

						if (productCategoryId.equals("-1")) {
							status.setCode("FAILURE_ADD_PRODCAT_PRODCATCODE_ERROR");
							status.setMessage(FAILURE_ADD_PRODCAT_PRODCATCODE_ERROR);
							status.setStatus(false);
						} else {
							ProductCategoryEntity entityObj = new ProductCategoryEntity();
							Timestamp currentTs = new Timestamp(new Date().getTime());

							modelObj.setProductCategoryId("PC".concat(productCategoryId));
							modelObj.setIsActive(Boolean.TRUE);
							modelObj.setIsDeleted(Boolean.FALSE);
							modelObj.setCreatedOn(currentTs);
							modelObj.setCreatedBy(loggedUser);
							modelObj.setModifiedOn(currentTs);
							modelObj.setModifiedBy(loggedUser);

							BeanUtils.copyProperties(modelObj, entityObj);
							result = masterDao.saveEntityObject(entityObj);

							if (result) {
								criteriaMap.clear();
								criteriaMap.put("permissionGroupId", userEntityObj.getPermissionGroupId());
								criteriaMap.put("isDeleted", Boolean.FALSE);

								Map<String, Object> updateMap = new HashMap<>();
								updateMap.put("modifiedOn", currentTs);
								updateMap.put("modifiedBy", loggedUser);

								Map<String, Object> updatePushMap = new HashMap<>();
								updatePushMap.put("productCategory", entityObj.getProductCategoryId());

								masterDao.updateCollectionDocument("mPermissionGroup", criteriaMap, updateMap,
										updatePushMap);

								status.setCode("SUCCESS_ADD_PRODCAT");
								status.setMessage(SUCCESS_ADD_PRODCAT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_ADD_PRODCAT");
								status.setMessage(FAILURE_ADD_PRODCAT);
								status.setStatus(false);
							}
						}
					} else {
						status.setCode("PRODCAT_ALREADY_EXISTS");
						status.setMessage(PRODCAT_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_PRODCAT");
			status.setMessage(FAILURE_ADD_PRODCAT);
			status.setStatus(false);
		} finally {
			finalData = retrieve(modelObj.getProductCategoryId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> edit(ProductCategory modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "EDIT", userEntityObj);

			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
							.contains(modelObj.getAccountId())
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getMetadataType())
							.contains(modelObj.getMetadataTypeId())
					&& (masterDao.isNull(modelObj.getParentId()) || CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getProductCategory())
							.contains(modelObj.getParentId()))) {
				if (masterDao.isNull(modelObj.getAccountId()) || masterDao.isNull(modelObj.getMetadataTypeId())
						|| masterDao.isNull(modelObj.getProductCategoryName())
						|| masterDao.isNull(modelObj.getProductCategoryCode())) {
					status.setCode("FAILURE_E_METADATA_MAPPING_MAND_NULL");
					status.setMessage(FAILURE_ADD_METADATA_MAPPING_MAND_NULL);
					status.setStatus(false);
				} else {
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("productCategoryId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					ProductCategoryEntity entityObj = masterDao.getEntityObject(new ProductCategoryEntity(), criteriaMap);
					
					if (null != entityObj) {
						criteriaMap.remove("productCategoryId");
						criteriaMap.put("productCategoryCode", modelObj.getProductCategoryCode());

						if (entityObj.getProductCategoryCode().equals(modelObj.getProductCategoryCode())
								|| null == masterDao.getEntityObject(new ProductCategoryEntity(), criteriaMap)) {
							Timestamp currentTs = new Timestamp(new Date().getTime());

							LinkedHashMap<String, Object> criteriaMapForChildCount = new LinkedHashMap<>();
							criteriaMapForChildCount.put("parentId", id);
							criteriaMapForChildCount.put("isDeleted", Boolean.FALSE);

							Integer childCount = masterDao.getCountOfDocuments("mProductCategory", criteriaMapForChildCount);

							if (null == entityObj.getParentId() && childCount == 0) {
								entityObj.setAccountId(modelObj.getAccountId());
								entityObj.setMetadataTypeId(modelObj.getMetadataTypeId());
							}
							entityObj.setProductCategoryName(modelObj.getProductCategoryName());
							entityObj.setProductCategoryCode(modelObj.getProductCategoryCode());
							entityObj.setModifiedOn(currentTs);
							entityObj.setModifiedBy(loggedUser);

							result = masterDao.updateEntityObject(entityObj);

							if (result) {
								status.setCode("SUCCESS_EDIT_PRODCAT");
								status.setMessage(SUCCESS_EDIT_PRODCAT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_PRODCAT");
								status.setMessage(SUCCESS_EDIT_PRODCAT);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_EDIT_PRODCAT_ALREADY_EXISTS");
							status.setMessage(FAILURE_EDIT_PRODCAT_ALREADY_EXISTS);
							status.setStatus(false);
						}
					} else {
						status.setCode("FAILURE_EDIT_PRODCAT_NOTEXIST_ERROR");
						status.setMessage(FAILURE_EDIT_PRODCAT_NOTEXIST_ERROR);
						status.setStatus(false);
					}
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_PRODCAT");
			status.setMessage(FAILURE_EDIT_PRODCAT);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> delete(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "DELETE", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getProductCategory()).contains(id)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("productCategoryId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				ProductCategoryEntity entityObj = masterDao.getEntityObject(new ProductCategoryEntity(), criteriaMap);

				if (null != entityObj) {

					LinkedHashMap<String, Object> criteriaMapForChildCount = new LinkedHashMap<>();
					criteriaMapForChildCount.put("parentId", id);
					criteriaMapForChildCount.put("isDeleted", Boolean.FALSE);

					Integer childCount = masterDao.getCountOfDocuments("mProductCategory", criteriaMapForChildCount);

					if (childCount == 0) {
						Timestamp currentTs = new Timestamp(new Date().getTime());
						entityObj.setIsDeleted(Boolean.TRUE);
						entityObj.setIsActive(Boolean.FALSE);
						entityObj.setModifiedOn(currentTs);
						entityObj.setModifiedBy(loggedUser);

						result = masterDao.updateEntityObject(entityObj);

						if (result) {
							status.setCode("SUCCESS_DELETE_PRODCAT");
							status.setMessage(SUCCESS_DELETE_PRODCAT);
							status.setStatus(true);
						} else {
							status.setCode("FAILURE_DELETE_PRODCAT");
							status.setMessage(FAILURE_DELETE_PRODCAT);
							status.setStatus(false);
						}
					} else {
						status.setCode("FAILURE_DELETE_PRODCAT_CHILDREN_ERROR");
						status.setMessage(FAILURE_DELETE_PRODCAT_CHILDREN_ERROR);
						status.setStatus(false);
					}

				} else {
					status.setCode("FAILURE_DELETE_PRODCAT_NOTEXIST_ERROR");
					status.setMessage(FAILURE_DELETE_PRODCAT_NOTEXIST_ERROR);
					status.setStatus(false);
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}			
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_PRODCAT");
			status.setMessage(FAILURE_DELETE_PRODCAT);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAll(List<String> accounts, List<String> metadatatype, String loggedUser) {
		List<ProductCategoryEntity> productCategoryList = new LinkedList<>();
		String mappedMetadataFieldName = null;

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "VIEW", userEntityObj);

			if (permissionFlg) {
				if (masterDao.isNull(accounts))
					accounts = userEntityObj.getPermissionGroup().getAccount();
				else {
					accounts.retainAll(userEntityObj.getPermissionGroup().getAccount());
				}
				if (masterDao.isNull(metadatatype))
					metadatatype = userEntityObj.getPermissionGroup().getMetadataType();
				else {
					metadatatype.retainAll(userEntityObj.getPermissionGroup().getMetadataType());
				}

				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("accountId", accounts);
				criteriaMap.put("metadataTypeId", userEntityObj.getPermissionGroup().getMetadataType());
				criteriaMap.put("productCategoryId", userEntityObj.getPermissionGroup().getProductCategory());
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				productCategoryList = (List<ProductCategoryEntity>) masterDao
						.getAllEntityObjects(new ProductCategoryEntity(), criteriaMap);

				LinkedHashMap<String, Object> manageConfigCriteriaMap = new LinkedHashMap<>();
				manageConfigCriteriaMap.put("_id", "productCategory");
				List<KeyValue> mappedValues = masterDao.getKeyValueList("manageConfig", "config.metadataMappingField",
						null, manageConfigCriteriaMap, null);

				if (mappedValues.size() > 0)
					mappedMetadataFieldName = mappedValues.get(0).getKey();

				logger.info("Mapped Metadata Field Name for Product Category [" + mappedMetadataFieldName + "]");

				status.setCode("SUCCESS_RETRIEVE_PRODCAT_LIST");
				status.setMessage(SUCCESS_RETRIEVE_PRODCAT_LIST);
				status.setStatus(true);
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_PRODCAT_LIST");
			status.setMessage(FAILURE_RETRIEVE_PRODCAT_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("productCategoryList", productCategoryList);
			finalData.put("mappedMetadataFieldName", mappedMetadataFieldName);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrieve(String id, String loggedUser) {
		ProductCategory modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "VIEW", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getProductCategory()).contains(id)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("productCategoryId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				ProductCategoryEntity entityObj = masterDao.getEntityObject(new ProductCategoryEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_PRODCAT_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_PRODCAT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					modelObj = new ProductCategory();
					BeanUtils.copyProperties(entityObj, modelObj);
					status.setCode("SUCCESS_RETRIEVE_PRODCAT");
					status.setMessage(SUCCESS_RETRIEVE_PRODCAT);
					status.setStatus(true);
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_PRODCAT");
			status.setMessage(FAILURE_RETRIEVE_PRODCAT);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

}

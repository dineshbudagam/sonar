/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.AwsAPIEntity;
import com.codemantra.manage.admin.entity.DownloadEntity;
import com.codemantra.manage.admin.entity.FormatEntity;
import com.codemantra.manage.admin.entity.FormatTypeEntity;
import com.codemantra.manage.admin.entity.PartnerEntity;
import com.codemantra.manage.admin.entity.PartnerGroupEntity;
import com.codemantra.manage.admin.entity.PartnerTypeEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.Partner;
import com.codemantra.manage.admin.model.PartnerGroup;
import com.codemantra.manage.admin.model.PartnerType;
import com.codemantra.manage.admin.service.PartnerService;
import com.codemantra.manage.admin.util.EncryptDecrypt;
import com.opencsv.CSVWriter;

@Service("partnerService")
public class PartnerServiceImpl implements PartnerService {

	private static final Logger logger = LoggerFactory.getLogger(PartnerServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${FAILURE_ADD_PT_ID_ERROR}")
	String FAILURE_ADD_PT_ID_ERROR;

	@Value("${SUCCESS_ADD_PT}")
	String SUCCESS_ADD_PT;

	@Value("${FAILURE_ADD_PT}")
	String FAILURE_ADD_PT;

	@Value("${PT_ALREADY_EXISTS}")
	String PT_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_PT_NOT_EXISTS}")
	String FAILURE_EDIT_PT_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_PT}")
	String SUCCESS_EDIT_PT;

	@Value("${FAILURE_EDIT_PT}")
	String FAILURE_EDIT_PT;

	@Value("${FAILURE_EDIT_PT_EXISTS}")
	String FAILURE_EDIT_PT_EXISTS;

	@Value("${FAILURE_DELETE_PT_NOT_EXISTS}")
	String FAILURE_DELETE_PT_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_PT}")
	String SUCCESS_DELETE_PT;

	@Value("${FAILURE_DELETE_PT}")
	String FAILURE_DELETE_PT;

	@Value("${FAILURE_RETRIEVE_PT_NOT_EXISTS}")
	String FAILURE_RETRIEVE_PT_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_PT}")
	String SUCCESS_RETRIEVE_PT;

	@Value("${FAILURE_RETRIEVE_PT}")
	String FAILURE_RETRIEVE_PT;

	@Value("${SUCCESS_RETRIEVE_PT_LIST}")
	String SUCCESS_RETRIEVE_PT_LIST;

	@Value("${FAILURE_RETRIEVE_PT_LIST}")
	String FAILURE_RETRIEVE_PT_LIST;

	@Value("${FAILURE_ADD_PARTNER_ID_ERROR}")
	String FAILURE_ADD_PARTNER_ID_ERROR;

	@Value("${SUCCESS_ADD_PARTNER}")
	String SUCCESS_ADD_PARTNER;

	@Value("${FAILURE_ADD_PARTNER}")
	String FAILURE_ADD_PARTNER;

	@Value("${PARTNER_ALREADY_EXISTS}")
	String PARTNER_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_PARTNER_NOT_EXISTS}")
	String FAILURE_EDIT_PARTNER_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_PARTNER}")
	String SUCCESS_EDIT_PARTNER;

	@Value("${FAILURE_EDIT_PARTNER}")
	String FAILURE_EDIT_PARTNER;

	@Value("${FAILURE_EDIT_PARTNER_EXISTS}")
	String FAILURE_EDIT_PARTNER_EXISTS;

	@Value("${FAILURE_DELETE_PARTNER_NOT_EXISTS}")
	String FAILURE_DELETE_PARTNER_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_PARTNER}")
	String SUCCESS_DELETE_PARTNER;

	@Value("${FAILURE_DELETE_PARTNER}")
	String FAILURE_DELETE_PARTNER;

	@Value("${FAILURE_RETRIEVE_PARTNER_NOT_EXISTS}")
	String FAILURE_RETRIEVE_PARTNER_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_PARTNER}")
	String SUCCESS_RETRIEVE_PARTNER;

	@Value("${FAILURE_RETRIEVE_PARTNER}")
	String FAILURE_RETRIEVE_PARTNER;

	@Value("${SUCCESS_RETRIEVE_PARTNER_LIST}")
	String SUCCESS_RETRIEVE_PARTNER_LIST;

	@Value("${FAILURE_RETRIEVE_PARTNER_LIST}")
	String FAILURE_RETRIEVE_PARTNER_LIST;

	@Value("${FAILURE_DEACTIVATE_PARTNER_NOT_EXISTS}")
	String FAILURE_DEACTIVATE_PARTNER_NOT_EXISTS;

	@Value("${FAILURE_ACTIVATE_PARTNER_NOT_EXISTS}")
	String FAILURE_ACTIVATE_PARTNER_NOT_EXISTS;

	@Value("${SUCCESS_ACTIVATE_PARTNER}")
	String SUCCESS_ACTIVATE_PARTNER;

	@Value("${SUCCESS_DEACTIVATE_PARTNER}")
	String SUCCESS_DEACTIVATE_PARTNER;

	@Value("${FAILURE_ACTIVATE_PARTNER}")
	String FAILURE_ACTIVATE_PARTNER;

	@Value("${FAILURE_DEACTIVATE_PARTNER}")
	String FAILURE_DEACTIVATE_PARTNER;

	@Value("${FAILURE_ADD_PARTNER_GROUP_ID_ERROR}")
	String FAILURE_ADD_PARTNER_GROUP_ID_ERROR;

	@Value("${SUCCESS_ADD_PARTNER_GROUP}")
	String SUCCESS_ADD_PARTNER_GROUP;

	@Value("${FAILURE_ADD_PARTNER_GROUP}")
	String FAILURE_ADD_PARTNER_GROUP;

	@Value("${PARTNER_GROUP_ALREADY_EXISTS}")
	String PARTNER_GROUP_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_PARTNER_GROUP_NOT_EXISTS}")
	String FAILURE_EDIT_PARTNER_GROUP_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_PARTNER_GROUP}")
	String SUCCESS_EDIT_PARTNER_GROUP;

	@Value("${FAILURE_EDIT_PARTNER_GROUP}")
	String FAILURE_EDIT_PARTNER_GROUP;

	@Value("${FAILURE_EDIT_PARTNER_GROUP_EXISTS}")
	String FAILURE_EDIT_PARTNER_GROUP_EXISTS;

	@Value("${FAILURE_DELETE_PARTNER_GROUP_NOT_EXISTS}")
	String FAILURE_DELETE_PARTNER_GROUP_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_PARTNER_GROUP}")
	String SUCCESS_DELETE_PARTNER_GROUP;

	@Value("${FAILURE_DELETE_PARTNER_GROUP}")
	String FAILURE_DELETE_PARTNER_GROUP;

	@Value("${FAILURE_RETRIEVE_PARTNER_GROUP_NOT_EXISTS}")
	String FAILURE_RETRIEVE_PARTNER_GROUP_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_PARTNER_GROUP}")
	String SUCCESS_RETRIEVE_PARTNER_GROUP;

	@Value("${FAILURE_RETRIEVE_PARTNER_GROUP}")
	String FAILURE_RETRIEVE_PARTNER_GROUP;

	@Value("${SUCCESS_RETRIEVE_PARTNER_GROUP_LIST}")
	String SUCCESS_RETRIEVE_PARTNER_GROUP_LIST;

	@Value("${FAILURE_RETRIEVE_PARTNER_GROUP_LIST}")
	String FAILURE_RETRIEVE_PARTNER_GROUP_LIST;

	@Value("${FAILURE_ADD_PT_MAND_NULL}")
	String FAILURE_ADD_PT_MAND_NULL;

	@Value("${FAILURE_EDIT_PT_MAND_NULL}")
	String FAILURE_EDIT_PT_MAND_NULL;

	@Value("${FAILURE_ADD_PARTNER_MAND_NULL}")
	String FAILURE_ADD_PARTNER_MAND_NULL;

	@Value("${FAILURE_EDIT_PARTNER_MAND_NULL}")
	String FAILURE_EDIT_PARTNER_MAND_NULL;

	@Value("${FAILURE_ADD_PARTNER_GROUP_MAND_NULL}")
	String FAILURE_ADD_PARTNER_GROUP_MAND_NULL;

	@Value("${FAILURE_EDIT_PARTNER_GROUP_MAND_NULL}")
	String FAILURE_EDIT_PARTNER_GROUP_MAND_NULL;

	@Value("${FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR}")
	String FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR;

	@Value("${FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR}")
	String FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR;

	@Value("${FAILURE_ADD_PARTNER_PT_NOT_EXISTS}")
	String FAILURE_ADD_PARTNER_PT_NOT_EXISTS;

	@Value("${FAILURE_ADD_PT_FORMAT_NOT_EXISTS}")
	String FAILURE_ADD_PT_FORMAT_NOT_EXISTS;
	

	
	@Value("${EXPORT_URL_EXPIRY_DAYS}")
	int EXPORT_URL_EXPIRY_DAYS;
	
	@Value("${TEMP_OUTPUT_FOLDER_PARTNER}")
	String TEMP_OUTPUT_FOLDER_PARTNER;
	
	@Value("${EXPORT_FORMAT}")
	String EXPORT_FORMAT;
	
	@Value("${EXPORT_PARTNER_FILE_S3_PATH}")
	String EXPORT_PARTNER_FILE_S3_PATH;
	
	@Value("${DOWNLOAD_STATUS}")
	String DOWNLOAD_STATUS;
	
	@Value("${DOWNLOAD_TYPE_PARTNER}")
	String DOWNLOAD_TYPE_PARTNER;
	
	@Value("${SUCCESS_EXPORT_PARTNER_LIST}")
	String SUCCESS_EXPORT_PARTNER_LIST;
	
	@Value("${FAILURE_EXPORT_PARTNER_LIST}")
	String FAILURE_EXPORT_PARTNER_LIST;
	
	@Value("${FAILURE_CHANGE_PASSWORD_PARTNER_NOT_EXISTS}")
	String FAILURE_CHANGE_PASSWORD_PARTNER_NOT_EXISTS;
	
	@Value("${SUCCESS_CHANGE_PASSWORD_PARTNER}")
	String SUCCESS_CHANGE_PASSWORD_PARTNER;
	
	@Value("${FAILURE_CHANGE_PASSWORD_PARTNER}")
	String FAILURE_CHANGE_PASSWORD_PARTNER;
	
	@Value("${FAILURE_CHANGE_PASSWORD_WRONG_PASSWORD}")
	String FAILURE_CHANGE_PASSWORD_WRONG_PASSWORD;
	

	@Override
	public Map<String, Object> savePartnerType(PartnerType modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "CREATE", userEntityObj);
			if (permissionFlg) {
				if (masterDao.isNull(modelObj.getPartnerTypeName()) || masterDao.isNull(modelObj.getFormatTypeId())) {
					status.setCode("FAILURE_ADD_PT_MAND_NULL");
					status.setMessage(FAILURE_ADD_PT_MAND_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Partner Type Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerTypeName", modelObj.getPartnerTypeName());
					criteriaMap.put("isDeleted", Boolean.FALSE);
					if (null == masterDao.getEntityObject(new PartnerTypeEntity(), criteriaMap)) {
						String id = masterDao.getNextSequenceId("partnerTypeIdSeq");
						logger.info("Partner Type Id generated is [PT" + id + "]");

						if (id.equals("-1")) {
							status.setCode("FAILURE_ADD_PT_ID_ERROR");
							status.setMessage(FAILURE_ADD_PT_ID_ERROR);
							status.setStatus(false);
						} else {

							criteriaMap.clear();
							criteriaMap.put("formatTypeId", modelObj.getFormatTypeId());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							if (null == masterDao.getEntityObject(new FormatTypeEntity(), criteriaMap)) {
								status.setCode("FAILURE_ADD_PT_FORMAT_NOT_EXISTS");
								status.setMessage(FAILURE_ADD_PT_FORMAT_NOT_EXISTS);
								status.setStatus(false);
							} else {
								PartnerTypeEntity entityObj = new PartnerTypeEntity();
								Timestamp currentTs = new Timestamp(new Date().getTime());

								BeanUtils.copyProperties(modelObj, entityObj);
								entityObj.setPartnerTypeId("PT".concat(id));
								entityObj.setIsActive(Boolean.TRUE);
								entityObj.setIsDeleted(Boolean.FALSE);
								entityObj.setCreatedOn(currentTs);
								entityObj.setCreatedBy(loggedUser);
								entityObj.setModifiedOn(currentTs);
								entityObj.setModifiedBy(loggedUser);

								result = masterDao.saveEntityObject(entityObj);

								if (result) {
									criteriaMap.clear();
									criteriaMap.put("permissionGroupId", userEntityObj.getPermissionGroupId());
									criteriaMap.put("isDeleted", Boolean.FALSE);

									Map<String, Object> updateMap = new HashMap<>();
									updateMap.put("modifiedOn", currentTs);
									updateMap.put("modifiedBy", loggedUser);

									Map<String, Object> updatePushMap = new HashMap<>();
									updatePushMap.put("partnerType", entityObj.getPartnerTypeId());

									masterDao.updateCollectionDocument("mPermissionGroup", criteriaMap, updateMap,
											updatePushMap);

									status.setCode("SUCCESS_ADD_PT");
									status.setMessage(SUCCESS_ADD_PT);
									status.setStatus(true);
								} else {
									status.setCode("FAILURE_ADD_PT");
									status.setMessage(FAILURE_ADD_PT);
									status.setStatus(false);
								}
							}
						}
					} else {
						status.setCode("PT_ALREADY_EXISTS");
						status.setMessage(PT_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save() :: ", e);
			status.setCode("FAILURE_ADD_PT");
			status.setMessage(FAILURE_ADD_PT);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerType(modelObj.getPartnerTypeId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;

	}

	@Override
	public Map<String, Object> editPartnerType(PartnerType modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "EDIT", userEntityObj);

			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType()).contains(id)) {
				if (masterDao.isNull(modelObj.getPartnerTypeName())) {
					status.setCode("FAILURE_EDIT_PT_MAND_NULL");
					status.setMessage(FAILURE_EDIT_PT_MAND_NULL);
					status.setStatus(false);
				} else {

					/**
					 * Partner Type Name can't be duplicated
					 */
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerTypeId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					PartnerTypeEntity entityObj = masterDao.getEntityObject(new PartnerTypeEntity(), criteriaMap);
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_PT_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_PT_NOT_EXISTS);
						status.setStatus(false);
					} else {

						PartnerTypeEntity entityObjExists = null;
						if (!entityObj.getPartnerTypeName().equals(modelObj.getPartnerTypeName())) {
							criteriaMap.clear();
							criteriaMap.put("partnerTypeName", modelObj.getPartnerTypeName());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							entityObjExists = masterDao.getEntityObject(new PartnerTypeEntity(), criteriaMap);
						}

						if (null == entityObjExists) {
							entityObj.setPartnerTypeName(modelObj.getPartnerTypeName());
							entityObj.setModifiedBy(loggedUser);
							entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

							result = masterDao.updateEntityObject(entityObj);

							if (result) {
								status.setCode("SUCCESS_EDIT_PT");
								status.setMessage(SUCCESS_EDIT_PT);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_PT");
								status.setMessage(FAILURE_EDIT_PT);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_EDIT_PT_EXISTS");
							status.setMessage(FAILURE_EDIT_PT_EXISTS);
							status.setStatus(false);
						}
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_PT");
			status.setMessage(FAILURE_EDIT_PT);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerType(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> deletePartnerType(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "DELETE", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType()).contains(id)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerTypeId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerTypeEntity entityObj = masterDao.getEntityObject(new PartnerTypeEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_PT_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_PT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(Boolean.TRUE);
					entityObj.setIsActive(Boolean.FALSE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_PT");
						status.setMessage(SUCCESS_DELETE_PT);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_PT");
						status.setMessage(FAILURE_DELETE_PT);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_PT");
			status.setMessage(FAILURE_DELETE_PT);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerType(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrievePartnerType(String id, String loggedUser) {
		PartnerType modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType()).contains(id)) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerTypeId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerTypeEntity entityObj = masterDao.getEntityObject(new PartnerTypeEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_PT_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_PT_NOT_EXISTS);
					status.setStatus(false);
				} else {
					modelObj = new PartnerType();
					BeanUtils.copyProperties(entityObj, modelObj);
					status.setCode("SUCCESS_RETRIEVE_PT");
					status.setMessage(SUCCESS_RETRIEVE_PT);
					status.setStatus(true);
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_PT");
			status.setMessage(FAILURE_RETRIEVE_PT);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllPartnerTypes(String loggedUser) {
		List<PartnerTypeEntity> partnerTypes = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerTypeId", userEntityObj.getPermissionGroup().getPartnerType());
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				partnerTypes = (List<PartnerTypeEntity>) masterDao.getAllEntityObjects(new PartnerTypeEntity(),
						criteriaMap);

				status.setCode("SUCCESS_RETRIEVE_PT_LIST");
				status.setMessage(SUCCESS_RETRIEVE_PT_LIST);
				status.setStatus(true);
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_PT_LIST");
			status.setMessage(FAILURE_RETRIEVE_PT_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", partnerTypes);
		}
		return finalData;
	}

	/////////////////////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> savePartner(Partner modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "CREATE", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
							.contains(modelObj.getPartnerTypeId())
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
							.contains(modelObj.getAccountId())) {
				if (masterDao.isNull(modelObj.getAccountId()) || masterDao.isNull(modelObj.getPartnerTypeId())
						|| masterDao.isNull(modelObj.getChannelName()) || masterDao.isNull(modelObj.getPartnerName())
						|| masterDao.isNull(modelObj.getFormatId())) {
					status.setCode("FAILURE_ADD_PARTNER_MAND_NULL");
					status.setMessage(FAILURE_ADD_PARTNER_MAND_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Partner Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerTypeId", modelObj.getPartnerTypeId());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					PartnerTypeEntity partnerTypeEntityObj = masterDao.getEntityObject(new PartnerTypeEntity(),
							criteriaMap);

					if (null != partnerTypeEntityObj) {
						List<Object> tempList = CollectionUtils
								.emptyIfNull(userEntityObj.getPermissionGroup().getAsset()).stream()
								.filter(k -> k.getPermission().equals("VIEW")).map(kv -> kv.getFormat())
								.collect(Collectors.toList());
						List<String> loggedUserFormatList = new ArrayList<>();
						if (tempList.size() == 1) {
							loggedUserFormatList = (List<String>) tempList.get(0);
						}

						criteriaMap.clear();
						criteriaMap.put("formatTypeId", partnerTypeEntityObj.getFormatTypeId());
						criteriaMap.put("formatId", loggedUserFormatList);
						criteriaMap.put("isDeleted", Boolean.FALSE);
						criteriaMap.put("isActive", Boolean.TRUE);

						List<FormatEntity> formats = (List<FormatEntity>) masterDao
								.getAllEntityObjects(new FormatEntity(), criteriaMap);
						List<String> formatIdList = CollectionUtils.emptyIfNull(formats).stream().map(obj -> obj.getFormatId())
								.collect(Collectors.toList());

						if (formatIdList.containsAll(modelObj.getFormatId())) {
							criteriaMap.clear();
							criteriaMap.put("partnerName", modelObj.getPartnerName());
							criteriaMap.put("isDeleted", Boolean.FALSE);
							if (null == masterDao.getEntityObject(new PartnerEntity(), criteriaMap)) {
								String id = masterDao.getNextSequenceId("partnerIdSeq");
								logger.info("Partner Id generated is [P" + id + "]");

								if (id.equals("-1")) {
									status.setCode("FAILURE_ADD_PARTNER_ID_ERROR");
									status.setMessage(FAILURE_ADD_PARTNER_ID_ERROR);
									status.setStatus(false);
								} else {
									PartnerEntity entityObj = new PartnerEntity();
									Timestamp currentTs = new Timestamp(new Date().getTime());

									entityObj.setPartnerId("P".concat(id));
									entityObj.setIsActive(Boolean.TRUE);
									entityObj.setIsDeleted(Boolean.FALSE);
									entityObj.setCreatedOn(currentTs);
									entityObj.setCreatedBy(loggedUser);
									entityObj.setModifiedOn(currentTs);
									entityObj.setModifiedBy(loggedUser);

									BeanUtils.copyProperties(modelObj, entityObj);									
									
									if(!masterDao.isNull(modelObj.getFtpPassword())) {
										EncryptDecrypt ed= new EncryptDecrypt();
										entityObj.setFtpPassword(ed.encrypt(modelObj.getFtpPassword()));
									}
									result = masterDao.saveEntityObject(entityObj);

									if (result) {
										status.setCode("SUCCESS_ADD_PARTNER");
										status.setMessage(SUCCESS_ADD_PARTNER);
										status.setStatus(true);
									} else {
										status.setCode("FAILURE_ADD_PARTNER");
										status.setMessage(FAILURE_ADD_PARTNER);
										status.setStatus(false);
									}
								}
							} else {
								status.setCode("PARTNER_ALREADY_EXISTS");
								status.setMessage(PARTNER_ALREADY_EXISTS);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR");
							status.setMessage(FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR);
							status.setStatus(false);
						}
					} else {
						status.setCode("FAILURE_ADD_PARTNER_PT_NOT_EXISTS");
						status.setMessage(FAILURE_ADD_PARTNER_PT_NOT_EXISTS);
						status.setStatus(false);
					}

				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_PARTNER");
			status.setMessage(FAILURE_ADD_PARTNER);
			status.setStatus(false);
		} finally {
			finalData = retrievePartner(modelObj.getPartnerId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> editPartner(Partner modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "EDIT", userEntityObj);
			if (permissionFlg
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
							.contains(modelObj.getPartnerTypeId())
					&& CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
							.contains(modelObj.getAccountId())) {
				if (masterDao.isNull(modelObj.getAccountId()) || masterDao.isNull(modelObj.getPartnerTypeId())
						|| masterDao.isNull(modelObj.getChannelName()) || masterDao.isNull(modelObj.getPartnerName())
						|| masterDao.isNull(modelObj.getFormatId())) {
					status.setCode("FAILURE_EDIT_PARTNER_MAND_NULL");
					status.setMessage(FAILURE_EDIT_PARTNER_MAND_NULL);
					status.setStatus(false);
				} else {
					/**
					 * Partner Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					PartnerEntity entityObj = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_PARTNER_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_PARTNER_NOT_EXISTS);
						status.setStatus(false);
					} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
							.contains(entityObj.getPartnerTypeId())) {
						status.setCode("REQUEST_FORBIDDEN");
						status.setMessage(REQUEST_FORBIDDEN);
						status.setStatus(false);
					} else {

						PartnerEntity entityObjExists = null;
						if (!entityObj.getPartnerName().equals(modelObj.getPartnerName())) {
							criteriaMap.clear();
							criteriaMap.put("partnerName", modelObj.getPartnerName());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							entityObjExists = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);
						}

						if (null == entityObjExists) {
							criteriaMap.clear();
							criteriaMap.put("partnerTypeId", modelObj.getPartnerTypeId());
							criteriaMap.put("isDeleted", Boolean.FALSE);

							PartnerTypeEntity partnerTypeEntityObj = masterDao.getEntityObject(new PartnerTypeEntity(),
									criteriaMap);

							if (null != partnerTypeEntityObj) {
								List<Object> tempList = CollectionUtils
										.emptyIfNull(userEntityObj.getPermissionGroup().getAsset()).stream()
										.filter(k -> k.getPermission().equals("VIEW")).map(kv -> kv.getFormat())
										.collect(Collectors.toList());
								List<String> loggedUserFormatList = new ArrayList<>();
								if (tempList.size() == 1) {
									loggedUserFormatList = (List<String>) tempList.get(0);
								}

								criteriaMap.clear();
								criteriaMap.put("formatTypeId", partnerTypeEntityObj.getFormatTypeId());
								criteriaMap.put("formatId", loggedUserFormatList);
								criteriaMap.put("isDeleted", Boolean.FALSE);
								criteriaMap.put("isActive", Boolean.TRUE);

								List<String> formatIdList = masterDao.getDistinctFieldValues("mFormat", criteriaMap, "formatId");

								if (formatIdList.containsAll(modelObj.getFormatId())) {
									entityObj.setPartnerName(modelObj.getPartnerName());
									entityObj.setAccountId(modelObj.getAccountId());
									entityObj.setPartnerTypeId(modelObj.getPartnerTypeId());
									entityObj.setChannelName(modelObj.getChannelName());
									entityObj.setFormatId(modelObj.getFormatId());
									entityObj.setEmailId(modelObj.getEmailId());
									entityObj.setModifiedBy(loggedUser);
									entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
									
									entityObj.setFtpUrl(modelObj.getFtpUrl());
									entityObj.setFtpUserName(modelObj.getFtpUserName());
									entityObj.setFtpPort(modelObj.getFtpPort());
									entityObj.setFtpFolderName(modelObj.getFtpFolderName());
									entityObj.setFtpInstructions(modelObj.getFtpInstructions());

									result = masterDao.updateEntityObject(entityObj);

									if (result) {
										status.setCode("SUCCESS_EDIT_PARTNER");
										status.setMessage(SUCCESS_EDIT_PARTNER);
										status.setStatus(true);
									} else {
										status.setCode("FAILURE_EDIT_PARTNER");
										status.setMessage(FAILURE_EDIT_PARTNER);
										status.setStatus(false);
									}
								} else {
									status.setCode("FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR");
									status.setMessage(FAILURE_ADD_PARTNER_FORMAT_LIST_ERROR);
									status.setStatus(false);
								}
							} else {
								status.setCode("FAILURE_ADD_PARTNER_PT_NOT_EXISTS");
								status.setMessage(FAILURE_ADD_PARTNER_PT_NOT_EXISTS);
								status.setStatus(false);
							}

						} else {
							status.setCode("FAILURE_EDIT_PARTNER_EXISTS");
							status.setMessage(FAILURE_EDIT_PARTNER_EXISTS);
							status.setStatus(false);
						}
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_PARTNER");
			status.setMessage(FAILURE_EDIT_PARTNER);
			status.setStatus(false);
		} finally {
			finalData = retrievePartner(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> deletePartner(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "DELETE", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerEntity entityObj = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_PARTNER_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_PARTNER_NOT_EXISTS);
					status.setStatus(false);
				} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
						.contains(entityObj.getPartnerTypeId())
						|| !CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
								.contains(entityObj.getAccountId())) {
					status.setCode("REQUEST_FORBIDDEN");
					status.setMessage(REQUEST_FORBIDDEN);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(Boolean.TRUE);
					entityObj.setIsActive(Boolean.FALSE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_PARTNER");
						status.setMessage(SUCCESS_DELETE_PARTNER);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_PARTNER");
						status.setMessage(FAILURE_DELETE_PARTNER);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_PARTNER");
			status.setMessage(FAILURE_DELETE_PARTNER);
			status.setStatus(false);
		} finally {
			finalData = retrievePartner(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrievePartner(String id, String loggedUser) {
		Partner modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerEntity entityObj = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_PARTNER_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_PARTNER_NOT_EXISTS);
					status.setStatus(false);
				} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
						.contains(entityObj.getPartnerTypeId())
						|| !CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
								.contains(entityObj.getAccountId())) {
					status.setCode("REQUEST_FORBIDDEN");
					status.setMessage(REQUEST_FORBIDDEN);
					status.setStatus(false);
				} else {
					modelObj = new Partner();
					BeanUtils.copyProperties(entityObj, modelObj);
					EncryptDecrypt ed= new EncryptDecrypt();
					if(!masterDao.isNull(modelObj.getFtpPassword()))
						modelObj.setErpepswr(ed.decrypt(entityObj.getFtpPassword()));
					status.setCode("SUCCESS_RETRIEVE_PARTNER");
					status.setMessage(SUCCESS_RETRIEVE_PARTNER);
					status.setStatus(true);
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_PARTNER");
			status.setMessage(FAILURE_RETRIEVE_PARTNER);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllPartners(Boolean isExport, String loggedUser) {
		List<PartnerEntity> partners = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerTypeId", userEntityObj.getPermissionGroup().getPartnerType());
				criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
				criteriaMap.put("isDeleted", Boolean.FALSE);

				partners = (List<PartnerEntity>) masterDao.getAllEntityObjects(new PartnerEntity(), criteriaMap);
				
				criteriaMap.clear();
				criteriaMap.put("isDeleted", Boolean.FALSE);			
				
				List<KeyValue> accountList = masterDao.getKeyValueList("mAccount", "accountId", "accountName",
						criteriaMap, null);
				
				Map<String, Object> accountMap = CollectionUtils.emptyIfNull(accountList).stream().collect(
		                Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
				
				EncryptDecrypt ed= new EncryptDecrypt();
				for (PartnerEntity partnerEntity : partners) {
					partnerEntity.setAccountName((String) accountMap.get(partnerEntity.getAccountId()));
					if(!masterDao.isNull(partnerEntity.getFtpPassword()))
						partnerEntity.setErpepswr(ed.decrypt(partnerEntity.getFtpPassword()));
				}
				
				if(isExport) {
					SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					Date today = Calendar.getInstance().getTime();    
					String currentDate = df.format(today);
					
					File tempExportDir = new File(TEMP_OUTPUT_FOLDER_PARTNER);
					if (!tempExportDir.exists())
						tempExportDir.mkdirs();
					
					String fileName = "PARTNERS_"+currentDate+("XLSX".equalsIgnoreCase(EXPORT_FORMAT)?".xlsx" : ".csv");
					String filePath = TEMP_OUTPUT_FOLDER_PARTNER+File.separator + fileName;
					
					URL url = null;
					AwsAPIEntity apiEntity = masterDao.getAwsCredentials();
					
					Date urlExpiryDate = new Date();
					long msec = urlExpiryDate.getTime();
					msec += Integer.valueOf(EXPORT_URL_EXPIRY_DAYS) * 24 * 60 * 60 * 1000;
					urlExpiryDate.setTime(msec);
					
					boolean exportFlg = export(partners, filePath);
					if(exportFlg) {
						
						url = masterDao.uploadFileToS3(apiEntity, filePath, fileName, urlExpiryDate, EXPORT_PARTNER_FILE_S3_PATH);
						
						if(null != url) {	
							
							DownloadEntity entityObj = new DownloadEntity();
							entityObj.setUniqueId("PARTNERS_"+currentDate);
							entityObj.setRequestedDate(today);
							entityObj.setUserId(loggedUser);
							entityObj.setDownloadStatus(DOWNLOAD_STATUS);
							entityObj.setExpiryDays(EXPORT_URL_EXPIRY_DAYS);
							entityObj.setDownloadType(DOWNLOAD_TYPE_PARTNER);
							entityObj.setCreatedBy(loggedUser);
							entityObj.setCreatedDate(today);
							entityObj.setModifiedBy(loggedUser);
							entityObj.setModifiedDate(today);
							entityObj.setDownloadLink(url);
							entityObj.setExpiryDate(urlExpiryDate);
							
							masterDao.saveEntityObject(entityObj);
							
							status.setCode("SUCCESS_EXPORT_PARTNER_LIST");
							status.setMessage(SUCCESS_EXPORT_PARTNER_LIST);
							status.setStatus(true);
						}else {

							status.setCode("FAILURE_EXPORT_PARTNER_LIST");
							status.setMessage(FAILURE_EXPORT_PARTNER_LIST);
							status.setStatus(false);
						}					
					}else {
						status.setCode("FAILURE_EXPORT_PARTNER_LIST");
						status.setMessage(FAILURE_EXPORT_PARTNER_LIST);
						status.setStatus(false);
					}
					
					new File(filePath).delete();
				}else {
					status.setCode("SUCCESS_RETRIEVE_PARTNER_LIST");
					status.setMessage(SUCCESS_RETRIEVE_PARTNER_LIST);
					status.setStatus(true);
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			
			if(isExport) {
				status.setCode("FAILURE_EXPORT_PARTNER_LIST");
				status.setMessage(FAILURE_EXPORT_PARTNER_LIST);
				status.setStatus(false);
			}else {
				status.setCode("FAILURE_RETRIEVE_PARTNER_LIST");
				status.setMessage(FAILURE_RETRIEVE_PARTNER_LIST);
				status.setStatus(false);
			}			
		} finally {
			finalData.put("status", status);
			finalData.put("data", partners);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> activateDeactivatePartner(Partner modelObj, String id, Integer activeStatus,
			String loggedUser) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			List<String> accessList = new ArrayList<>();
			accessList.add("ACTIVATE");
			accessList.add("DEACTIVATE");
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", accessList, userEntityObj);

			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerEntity entityObj = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);
				if (null == entityObj) {
					status.setCode(activeStatus == 0 ? "FAILURE_DEACTIVATE_PARTNER_NOT_EXISTS"
							: "FAILURE_ACTIVATE_PARTNER_NOT_EXISTS");
					status.setMessage(activeStatus == 0 ? FAILURE_DEACTIVATE_PARTNER_NOT_EXISTS
							: FAILURE_ACTIVATE_PARTNER_NOT_EXISTS);
					status.setStatus(false);
				} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
						.contains(entityObj.getPartnerTypeId())
						|| !CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getAccount())
								.contains(entityObj.getAccountId())) {
					status.setCode("REQUEST_FORBIDDEN");
					status.setMessage(REQUEST_FORBIDDEN);
					status.setStatus(false);
				} else {
					entityObj.setIsActive(activeStatus == 0 ? Boolean.FALSE : Boolean.TRUE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
					result = masterDao.updateEntityObject(entityObj);
					if (result) {
						if (activeStatus == 1) {
							status.setCode("SUCCESS_ACTIVATE_PARTNER");
							status.setMessage(SUCCESS_ACTIVATE_PARTNER);
						} else {
							status.setCode("SUCCESS_DEACTIVATE_PARTNER");
							status.setMessage(SUCCESS_DEACTIVATE_PARTNER);
						}
						status.setStatus(true);
					} else {
						if (activeStatus == 1) {
							status.setCode("FAILURE_ACTIVATE_PARTNER");
							status.setMessage(FAILURE_ACTIVATE_PARTNER);
						} else {
							status.setCode("FAILURE_DEACTIVATE_PARTNER");
							status.setMessage(FAILURE_DEACTIVATE_PARTNER);
						}
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in activateDeactivate :: ", e);
			if (activeStatus == 1) {
				status.setCode("FAILURE_ACTIVATE_PARTNER");
				status.setMessage(FAILURE_ACTIVATE_PARTNER);
			} else {
				status.setCode("FAILURE_DEACTIVATE_PARTNER");
				status.setMessage(FAILURE_DEACTIVATE_PARTNER);
			}
			status.setStatus(false);
		} finally {
			finalData = retrieveAllPartners(false, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	///////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> savePartnerGroup(PartnerGroup modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "CREATE", userEntityObj);
			if (permissionFlg && CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
					.contains(modelObj.getPartnerTypeId())) {
				if (masterDao.isNull(modelObj.getPartnerTypeId()) || masterDao.isNull(modelObj.getPartnerId())
						|| masterDao.isNull(modelObj.getPartnerGroupName())) {
					status.setCode("FAILURE_ADD_PARTNER_GROUP_MAND_NULL");
					status.setMessage(FAILURE_ADD_PARTNER_GROUP_MAND_NULL);
					status.setStatus(false);
				} else {
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerTypeId", modelObj.getPartnerTypeId());
					criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					List<PartnerEntity> partners = (List<PartnerEntity>) masterDao
							.getAllEntityObjects(new PartnerEntity(), criteriaMap);

					List<String> partnerIdList = CollectionUtils.emptyIfNull(partners).stream().map(pt -> pt.getPartnerId())
							.collect(Collectors.toList());

					if (partnerIdList.containsAll(modelObj.getPartnerId())) {
						/**
						 * Partner Group Name can't be duplicated
						 */
						criteriaMap.clear();
						criteriaMap.put("partnerGroupName", modelObj.getPartnerGroupName());
						criteriaMap.put("isDeleted", Boolean.FALSE);
						if (null == masterDao.getEntityObject(new PartnerGroupEntity(), criteriaMap)) {
							String id = masterDao.getNextSequenceId("partnerGroupIdSeq");
							logger.info("Partner Group Id generated is [PG" + id + "]");

							if (id.equals("-1")) {
								status.setCode("FAILURE_ADD_PARTNER_GROUP_ID_ERROR");
								status.setMessage(FAILURE_ADD_PARTNER_GROUP_ID_ERROR);
								status.setStatus(false);
							} else {
								PartnerGroupEntity entityObj = new PartnerGroupEntity();
								Timestamp currentTs = new Timestamp(new Date().getTime());

								modelObj.setPartnerGroupId("PG".concat(id));
								modelObj.setIsActive(Boolean.TRUE);
								modelObj.setIsDeleted(Boolean.FALSE);
								modelObj.setCreatedOn(currentTs);
								modelObj.setCreatedBy(loggedUser);
								modelObj.setModifiedOn(currentTs);
								modelObj.setModifiedBy(loggedUser);

								BeanUtils.copyProperties(modelObj, entityObj);
								result = masterDao.saveEntityObject(entityObj);

								if (result) {
									status.setCode("SUCCESS_ADD_PARTNER_GROUP");
									status.setMessage(SUCCESS_ADD_PARTNER_GROUP);
									status.setStatus(true);
								} else {
									status.setCode("FAILURE_ADD_PARTNER_GROUP");
									status.setMessage(FAILURE_ADD_PARTNER_GROUP);
									status.setStatus(false);
								}
							}
						} else {
							status.setCode("FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR");
							status.setMessage(FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR);
							status.setStatus(false);
						}
					} else {
						status.setCode("PARTNER_GROUP_ALREADY_EXISTS");
						status.setMessage(PARTNER_GROUP_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_PARTNER_GROUP");
			status.setMessage(FAILURE_ADD_PARTNER_GROUP);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerGroup(modelObj.getPartnerGroupId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> editPartnerGroup(PartnerGroup modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "EDIT", userEntityObj);
			if (permissionFlg && CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
					.contains(modelObj.getPartnerTypeId())) {
				if (masterDao.isNull(modelObj.getPartnerTypeId()) || masterDao.isNull(modelObj.getPartnerId())
						|| masterDao.isNull(modelObj.getPartnerGroupName())) {
					status.setCode("FAILURE_EDIT_PARTNER_GROUP_MAND_NULL");
					status.setMessage(FAILURE_EDIT_PARTNER_GROUP_MAND_NULL);
					status.setStatus(false);
				} else {

					/**
					 * Partner Group Name can't be duplicated
					 */

					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("partnerGroupId", id);
					criteriaMap.put("isDeleted", Boolean.FALSE);

					PartnerGroupEntity entityObj = masterDao.getEntityObject(new PartnerGroupEntity(), criteriaMap);
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_PARTNER_GROUP_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_PARTNER_GROUP_NOT_EXISTS);
						status.setStatus(false);
					} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
							.contains(entityObj.getPartnerTypeId())) {
						status.setCode("REQUEST_FORBIDDEN");
						status.setMessage(REQUEST_FORBIDDEN);
						status.setStatus(false);
					} else {
						criteriaMap.clear();
						criteriaMap.put("partnerTypeId", modelObj.getPartnerTypeId());
						criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
						criteriaMap.put("isDeleted", Boolean.FALSE);

						List<PartnerEntity> partners = (List<PartnerEntity>) masterDao
								.getAllEntityObjects(new PartnerEntity(), criteriaMap);

						List<String> partnerIdList = CollectionUtils.emptyIfNull(partners).stream().map(pt -> pt.getPartnerId())
								.collect(Collectors.toList());

						if (partnerIdList.containsAll(modelObj.getPartnerId())) {
							PartnerGroupEntity entityObjExists = null;
							if (!entityObj.getPartnerGroupName().equals(modelObj.getPartnerGroupName())) {
								criteriaMap.clear();
								criteriaMap.put("partnerGroupName", modelObj.getPartnerGroupName());
								criteriaMap.put("isDeleted", Boolean.FALSE);

								entityObjExists = masterDao.getEntityObject(new PartnerGroupEntity(), criteriaMap);
							}

							if (null == entityObjExists) {
								entityObj.setPartnerGroupName(modelObj.getPartnerGroupName());
								entityObj.setPartnerTypeId(modelObj.getPartnerTypeId());
								entityObj.setPartnerId(modelObj.getPartnerId());
								entityObj.setModifiedBy(loggedUser);
								entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

								result = masterDao.updateEntityObject(entityObj);

								if (result) {
									status.setCode("SUCCESS_EDIT_PARTNER_GROUP");
									status.setMessage(SUCCESS_EDIT_PARTNER_GROUP);
									status.setStatus(true);
								} else {
									status.setCode("FAILURE_EDIT_PARTNER_GROUP");
									status.setMessage(FAILURE_EDIT_PARTNER_GROUP);
									status.setStatus(false);
								}
							} else {
								status.setCode("FAILURE_EDIT_PARTNER_GROUP_EXISTS");
								status.setMessage(FAILURE_EDIT_PARTNER_GROUP_EXISTS);
								status.setStatus(false);
							}
						} else {
							status.setCode("FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR");
							status.setMessage(FAILURE_ADD_PARTNER_GROUP_PARTNER_LIST_ERROR);
							status.setStatus(false);
						}

					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_PARTNER_GROUP");
			status.setMessage(FAILURE_EDIT_PARTNER_GROUP);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerGroup(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> deletePartnerGroup(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "DELETE", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerGroupId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerGroupEntity entityObj = masterDao.getEntityObject(new PartnerGroupEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_PARTNER_GROUP_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_PARTNER_GROUP_NOT_EXISTS);
					status.setStatus(false);
				} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
						.contains(entityObj.getPartnerTypeId())) {
					status.setCode("REQUEST_FORBIDDEN");
					status.setMessage(REQUEST_FORBIDDEN);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(Boolean.TRUE);
					entityObj.setIsActive(Boolean.FALSE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_PARTNER_GROUP");
						status.setMessage(SUCCESS_DELETE_PARTNER_GROUP);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_PARTNER_GROUP");
						status.setMessage(FAILURE_DELETE_PARTNER_GROUP);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_PARTNER_GROUP");
			status.setMessage(FAILURE_DELETE_PARTNER_GROUP);
			status.setStatus(false);
		} finally {
			finalData = retrievePartnerGroup(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrievePartnerGroup(String id, String loggedUser) {
		PartnerGroup modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerGroupId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerGroupEntity entityObj = masterDao.getEntityObject(new PartnerGroupEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_PARTNER_GROUP_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_PARTNER_GROUP_NOT_EXISTS);
					status.setStatus(false);
				} else if (!CollectionUtils.emptyIfNull(userEntityObj.getPermissionGroup().getPartnerType())
						.contains(entityObj.getPartnerTypeId())) {
					status.setCode("REQUEST_FORBIDDEN");
					status.setMessage(REQUEST_FORBIDDEN);
					status.setStatus(false);
				} else {
					modelObj = new PartnerGroup();
					BeanUtils.copyProperties(entityObj, modelObj);
					status.setCode("SUCCESS_RETRIEVE_PARTNER_GROUP");
					status.setMessage(SUCCESS_RETRIEVE_PARTNER_GROUP);
					status.setStatus(true);
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_PARTNER_GROUP");
			status.setMessage(FAILURE_RETRIEVE_PARTNER_GROUP);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllPartnerGroups(String loggedUser) {
		List<PartnerGroupEntity> partnerGroups = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "VIEW", userEntityObj);
			if (permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerTypeId", userEntityObj.getPermissionGroup().getPartnerType());
				criteriaMap.put("isDeleted", Boolean.FALSE);

				partnerGroups = (List<PartnerGroupEntity>) masterDao.getAllEntityObjects(new PartnerGroupEntity(),
						criteriaMap);

				status.setCode("SUCCESS_RETRIEVE_PARTNER_GROUP_LIST");
				status.setMessage(SUCCESS_RETRIEVE_PARTNER_GROUP_LIST);
				status.setStatus(true);

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_PARTNER_GROUP_LIST");
			status.setMessage(FAILURE_RETRIEVE_PARTNER_GROUP_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", partnerGroups);
		}
		return finalData;
	}
	
	public Boolean export(List<PartnerEntity> partners, String fileName) {
		boolean exportFlg = true;
		
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isDeleted", Boolean.FALSE);			
			
			List<KeyValue> partnerTypeList = masterDao.getKeyValueList("mPartnerType", "partnerTypeId", "partnerTypeName",
					criteriaMap, null);
			
			Map<String, Object> partnerTypeMap = CollectionUtils.emptyIfNull(partnerTypeList).stream().collect(
	                Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
			
			criteriaMap.clear();
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			List<KeyValue> formatList = masterDao.getKeyValueList("mFormat", "formatId", "formatName",
					criteriaMap, null);
			
			Map<String, Object> formatMap = CollectionUtils.emptyIfNull(formatList).stream().collect(
	                Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
						
			String[] headers = {"PARTNER NAME", "CHANNEL NAME", "ACCOUNT", "PARTNER TYPE", "PARTNER EMAIL", "FORMATS", "ACTIVE"};
			
			if("XLSX".equalsIgnoreCase(EXPORT_FORMAT)) {
				XSSFWorkbook workbook = new XSSFWorkbook(); 
				
		        XSSFSheet sheet = workbook.createSheet("Partners");// creating a blank sheet
		        int rownum = 0;
		        
		        XSSFCellStyle style = workbook.createCellStyle();
		        XSSFFont font = workbook.createFont();
		        font.setFontHeightInPoints((short) 10);
		        font.setBold(true);
		        style.setFont(font);
		        
		        Row header = sheet.createRow(rownum++);
		        for(int i=0; i<headers.length; i++) {
		        	header.createCell(i).setCellValue(headers[i]);
			        header.getCell(i).setCellStyle(style);
		        }	        
		        
		        for(PartnerEntity partner: partners) {
		        	
		        	List<String> formats = CollectionUtils.emptyIfNull(partner.getFormatId()).stream().map(data -> (String)formatMap.get(data)).collect(Collectors.toList());
		        	
		        	Row row = sheet.createRow(rownum++);	        	
		        	row.createCell(0).setCellValue(partner.getPartnerName());
		        	row.createCell(1).setCellValue(partner.getChannelName());
		        	row.createCell(2).setCellValue(partner.getAccountName());
		        	row.createCell(3).setCellValue((String)partnerTypeMap.get(partner.getPartnerTypeId()));
		        	row.createCell(4).setCellValue(CollectionUtils.emptyIfNull(partner.getEmailId()).stream().collect(Collectors.joining(",")));
		        	row.createCell(5).setCellValue(CollectionUtils.emptyIfNull(formats).stream().collect(Collectors.joining(",")));
		        	row.createCell(6).setCellValue(partner.getIsActive()?"Yes" : "No");
		        }
		        
	            FileOutputStream out = new FileOutputStream(new File(fileName)); // file name with path
	            workbook.write(out);
	            out.close();
	            workbook.close();
			}else {
				File exportFile = new File(fileName);
				
				CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
		        writer.writeNext(headers);
		        
		        for(PartnerEntity partner: partners) {
		        	List<String> formats = CollectionUtils.emptyIfNull(partner.getFormatId()).stream().map(data -> (String)formatMap.get(data)).collect(Collectors.toList());
		        	
		        	writer.writeNext(new String[]{partner.getPartnerName(), partner.getChannelName(), partner.getAccountName(),
		        			(String)partnerTypeMap.get(partner.getPartnerTypeId()), CollectionUtils.emptyIfNull(partner.getEmailId()).stream().collect(Collectors.joining(",")),
		        			CollectionUtils.emptyIfNull(formats).stream().collect(Collectors.joining(",")), partner.getIsActive()?"Yes":"No"});
		        }		        
		        writer.flush();
		        writer.close();
			}
	        
		}catch(Exception e) {
			e.printStackTrace();
			exportFlg = false;
		}
		
		return exportFlg;
	}

	@Override
	public Map<String, Object> changeFtpPassword(Partner modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			
			permissionFlg = masterDao.checkPermission("MOD00006", "PARTNER", "UPDATE_FTP_PASSWORD", userEntityObj);
			if (permissionFlg) {
				/**
				 * Partner Name can't be duplicated
				 */

				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("partnerId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				PartnerEntity entityObj = masterDao.getEntityObject(new PartnerEntity(), criteriaMap);
				EncryptDecrypt ed= new EncryptDecrypt();
				if (null == entityObj) {
					status.setCode("FAILURE_CHANGE_PASSWORD_PARTNER_NOT_EXISTS");
					status.setMessage(FAILURE_CHANGE_PASSWORD_PARTNER_NOT_EXISTS);
					status.setStatus(false);
				} else {
					if(!masterDao.isNull(modelObj.getFtpPassword()))
						entityObj.setFtpPassword(ed.encrypt(modelObj.getFtpPassword()));
					else
						entityObj.setFtpPassword(null);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);					

					if (result) {
						status.setCode("SUCCESS_CHANGE_PASSWORD_PARTNER");
						status.setMessage(SUCCESS_CHANGE_PASSWORD_PARTNER);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_CHANGE_PASSWORD_PARTNER");
						status.setMessage(FAILURE_CHANGE_PASSWORD_PARTNER);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_CHANGE_PASSWORD_PARTNER");
			status.setMessage(FAILURE_CHANGE_PASSWORD_PARTNER);
			status.setStatus(false);
		} finally {
			finalData = retrievePartner(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}



}

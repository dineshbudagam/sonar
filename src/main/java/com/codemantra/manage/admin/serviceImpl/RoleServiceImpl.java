/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.RoleEntity;
import com.codemantra.manage.admin.entity.StagesEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.Role;
import com.codemantra.manage.admin.service.RoleService;



@Service("roleService")
public class RoleServiceImpl implements RoleService {

	private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${FAILURE_ADD_ROLE_ID_ERROR}")
	String FAILURE_ADD_ROLE_ID_ERROR;

	@Value("${SUCCESS_ADD_ROLE}")
	String SUCCESS_ADD_ROLE;

	@Value("${FAILURE_ADD_ROLE}")
	String FAILURE_ADD_ROLE;

	@Value("${ROLE_ALREADY_EXISTS}")
	String ROLE_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_ROLE_NOT_EXISTS}")
	String FAILURE_EDIT_ROLE_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_ROLE}")
	String SUCCESS_EDIT_ROLE;

	@Value("${FAILURE_EDIT_ROLE}")
	String FAILURE_EDIT_ROLE;

	@Value("${FAILURE_DELETE_ROLE_NOT_EXISTS}")
	String FAILURE_DELETE_ROLE_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_ROLE}")
	String SUCCESS_DELETE_ROLE;

	@Value("${FAILURE_DELETE_ROLE}")
	String FAILURE_DELETE_ROLE;

	@Value("${SUCCESS_RETRIEVE_ROLE_LIST}")
	String SUCCESS_RETRIEVE_ROLE_LIST;

	@Value("${FAILURE_RETRIEVE_ROLE_LIST}")
	String FAILURE_RETRIEVE_ROLE_LIST;

	@Value("${FAILURE_RETRIEVE_ROLE_NOT_EXISTS}")
	String FAILURE_RETRIEVE_ROLE_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_ROLE}")
	String SUCCESS_RETRIEVE_ROLE;

	@Value("${FAILURE_RETRIEVE_ROLE}")
	String FAILURE_RETRIEVE_ROLE;

	@Value("${FAILURE_DEACTIVATE_ROLE_NOT_EXISTS}")
	String FAILURE_DEACTIVATE_ROLE_NOT_EXISTS;

	@Value("${FAILURE_ACTIVATE_ROLE_NOT_EXISTS}")
	String FAILURE_ACTIVATE_ROLE_NOT_EXISTS;

	@Value("${SUCCESS_ACTIVATE_ROLE}")
	String SUCCESS_ACTIVATE_ROLE;

	@Value("${SUCCESS_DEACTIVATE_ROLE}")
	String SUCCESS_DEACTIVATE_ROLE;

	@Value("${FAILURE_ACTIVATE_ROLE}")
	String FAILURE_ACTIVATE_ROLE;

	@Value("${FAILURE_DEACTIVATE_ROLE}")
	String FAILURE_DEACTIVATE_ROLE;

	@Value("${ROLENAME_ALREADY_EXISTS}")
	String ROLENAME_ALREADY_EXISTS;

	@Value("${ROLENAME_NOT_EXISTS}")
	String ROLENAME_NOT_EXISTS;

	@Value("${FAILURE_EDIT_ROLE_EXISTS}")
	String FAILURE_EDIT_ROLE_EXISTS;
	
	@Value("${RESPONSE.COMMON.FAILURE}")
	String failure;
	
	@Override
	public Map<String, Object> save(Role modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "CREATE", userEntityObj);			
			logger.info("The Permission Status..=>",permissionFlg);
			if(permissionFlg) {
				/**
				 * Role Name can't be duplicated, even in case of deleted ones
				 */
				if(StringUtils.isEmpty(modelObj.getRoleName())) {
					status.setCode("FAILURE_ADD_ROLE_ID_ERROR");
					status.setMessage("Role Id Cannot be empty");
					status.setStatus(false);
				} else {
					
					if (null == masterDao.getEntityObject(new RoleEntity(), "roleName", modelObj.getRoleName())) {
						String roleId = masterDao.getNextSequenceId("roleIdSeq");
						logger.info("Id generated is [R" + roleId + "]");
						
						if (roleId.equals("-1")) {
							status.setCode("FAILURE_ADD_ROLE_ID_ERROR");
							status.setMessage(FAILURE_ADD_ROLE_ID_ERROR);
							status.setStatus(false);
						} else {
							RoleEntity entityObj = new RoleEntity();
							Timestamp currentTs = new Timestamp(new Date().getTime());
							
							modelObj.setRoleId("R".concat(roleId));
							modelObj.setIsActive(true);
							modelObj.setIsDeleted(false);
							modelObj.setCreatedOn(currentTs);
							modelObj.setCreatedBy(loggedUser);
							modelObj.setModifiedOn(currentTs);
							modelObj.setModifiedBy(loggedUser);
							
							BeanUtils.copyProperties(modelObj, entityObj); 
							result = masterDao.saveEntityObject(entityObj);
							
							if (result) {
								status.setCode("SUCCESS_ADD_ROLE");
								status.setMessage(SUCCESS_ADD_ROLE);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_ADD_ROLE");
								status.setMessage(FAILURE_ADD_ROLE);
								status.setStatus(false);
							}
						}
					} else {
						status.setCode("ROLE_ALREADY_EXISTS");
						status.setMessage(ROLE_ALREADY_EXISTS);
						status.setStatus(false);
					}
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
			
			
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_ROLE");
			status.setMessage(FAILURE_ADD_ROLE);
			status.setStatus(false);
		} finally {
			finalData = retrieve(modelObj.getRoleId(), loggedUser);
			finalData.put("status", status);
		}
		return finalData;

	}

	@Override
	public Map<String, Object> edit(Role modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "EDIT", userEntityObj);			
			
			if(permissionFlg) {
				
				if(StringUtils.isEmpty(modelObj.getRoleName())) {
					status.setCode("FAILURE_ADD_ROLE_ERROR");
					status.setMessage("Role Id Cannot be empty");
					status.setStatus(false);
				} else {
					
					Map<String, Object> criteriaMap = new HashMap<>();
					criteriaMap.put("roleId", id);
					criteriaMap.put("isDeleted", false);
					
					RoleEntity entityObj = masterDao.getEntityObject(new RoleEntity(), criteriaMap);
					
					if (null == entityObj) {
						status.setCode("FAILURE_EDIT_ROLE_NOT_EXISTS");
						status.setMessage(FAILURE_EDIT_ROLE_NOT_EXISTS);
						status.setStatus(false);
					} else {
						
						boolean dupeFlg = false;
						
						if(modelObj.getRoleName().equals(entityObj.getRoleName())) {
							criteriaMap.clear();
							criteriaMap.put("roleName", modelObj.getRoleName());
							criteriaMap.put("isDeleted", Boolean.FALSE);
							
							Map<String, Object> ninCriteriaMap = new HashMap<>();
							ninCriteriaMap.put("roleId", modelObj.getRoleId());
							
							if (null != masterDao.getEntityObject(new RoleEntity(), criteriaMap, ninCriteriaMap)) {
								dupeFlg = true;
								status.setCode("FAILURE_EDIT_ROLE_EXISTS");
								status.setMessage(FAILURE_EDIT_ROLE_EXISTS);
								status.setStatus(false);
							}
						}
						
						if(!dupeFlg) {
							entityObj.setModifiedBy(loggedUser);
							entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
							entityObj.setRoleName(modelObj.getRoleName());
							entityObj.setModule(modelObj.getModule());
							
							result = masterDao.updateEntityObject(entityObj); 
							
							if (result) {
								status.setCode("SUCCESS_EDIT_ROLE");
								status.setMessage(SUCCESS_EDIT_ROLE);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_ROLE");
								status.setMessage(FAILURE_EDIT_ROLE);
								status.setStatus(false);
							}
						}
					}
				}
				
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
			
		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_ROLE");
			status.setMessage(FAILURE_EDIT_ROLE);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> delete(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "DELETE", userEntityObj);
			
			if(permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("roleId", id);
				criteriaMap.put("isDeleted", false);

				RoleEntity entityObj = masterDao.getEntityObject(new RoleEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_DELETE_ROLE_NOT_EXISTS");
					status.setMessage(FAILURE_DELETE_ROLE_NOT_EXISTS);
					status.setStatus(false);
				} else {
					entityObj.setIsDeleted(true);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_DELETE_ROLE");
						status.setMessage(SUCCESS_DELETE_ROLE);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_DELETE_ROLE");
						status.setMessage(FAILURE_DELETE_ROLE);
						status.setStatus(false);
					}
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}			
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_ROLE");
			status.setMessage(FAILURE_DELETE_ROLE);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id, loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAll(String loggedUser) {
		List<RoleEntity> roleList = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "VIEW", userEntityObj);
			
			if(permissionFlg) {
				roleList = (List<RoleEntity>) masterDao.getAllEntityObjects(new RoleEntity());

				status.setCode("SUCCESS_RETRIEVE_ROLE_LIST");
				status.setMessage(SUCCESS_RETRIEVE_ROLE_LIST);
				status.setStatus(true);
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);				
			}
			
			
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_ROLE_LIST");
			status.setMessage(FAILURE_RETRIEVE_ROLE_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", roleList);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrieve(String id, String loggedUser) {
		Role modelObj = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "VIEW", userEntityObj);
			
			if(permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("roleId", id);
				criteriaMap.put("isDeleted", false);

				RoleEntity entityObj = masterDao.getEntityObject(new RoleEntity(), criteriaMap);

				if (null == entityObj) {
					status.setCode("FAILURE_RETRIEVE_ROLE_NOT_EXISTS");
					status.setMessage(FAILURE_RETRIEVE_ROLE_NOT_EXISTS);
					status.setStatus(false);
				} else {
					modelObj = new Role();
					BeanUtils.copyProperties(entityObj, modelObj);
					status.setCode("SUCCESS_RETRIEVE_ROLE");
					status.setMessage(SUCCESS_RETRIEVE_ROLE);
					status.setStatus(true);
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);				
			}

		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_ROLE");
			status.setMessage(FAILURE_RETRIEVE_ROLE);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", modelObj);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> activateDeactivateRole(Role modelObj, String id, Integer activeStatus,
			String loggedUser) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean permissionFlg = false;
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			permissionFlg = masterDao.checkPermission("MOD00006", "ROLE", "ACTIVATE", userEntityObj);
			
			if(permissionFlg) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("roleId", id);

				RoleEntity entityObj = masterDao.getEntityObject(new RoleEntity(), criteriaMap);
				if (null == entityObj) {
					status.setCode(
							activeStatus == 0 ? "FAILURE_DEACTIVATE_ROLE_NOT_EXISTS" : "FAILURE_ACTIVATE_ROLE_NOT_EXISTS");
					status.setMessage(
							activeStatus == 0 ? FAILURE_DEACTIVATE_ROLE_NOT_EXISTS : FAILURE_ACTIVATE_ROLE_NOT_EXISTS);
					status.setStatus(false);
				} else {
					entityObj.setIsActive(activeStatus == 0 ? Boolean.FALSE : Boolean.TRUE);
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
					result = masterDao.updateEntityObject(entityObj);
					if (result) {
						if (activeStatus == 1) {
							status.setCode("SUCCESS_ACTIVATE_ROLE");
							status.setMessage(SUCCESS_ACTIVATE_ROLE);
						} else {
							status.setCode("SUCCESS_DEACTIVATE_ROLE");
							status.setMessage(SUCCESS_DEACTIVATE_ROLE);
						}
						status.setStatus(true);
					} else {
						if (activeStatus == 1) {
							status.setCode("FAILURE_ACTIVATE_ROLE");
							status.setMessage(FAILURE_ACTIVATE_ROLE);
						} else {
							status.setCode("FAILURE_DEACTIVATE_ROLE");
							status.setMessage(FAILURE_DEACTIVATE_ROLE);
						}
						status.setStatus(false);
					}
				}
			}else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);				
			}
			
			
		} catch (Exception e) {
			logger.error("Exception in activateDeactivate :: ", e);
			if (activeStatus == 1) {
				status.setCode("FAILURE_ACTIVATE_ROLE");
				status.setMessage(FAILURE_ACTIVATE_ROLE);
			} else {
				status.setCode("FAILURE_DEACTIVATE_ROLE");
				status.setMessage(FAILURE_DEACTIVATE_ROLE);
			}
			status.setStatus(false);
		} finally {
			finalData = retrieveAll(loggedUser);
			finalData.put("status", status);
		}
		return finalData;
	}
	

	
	@Override
	public Map<String, Object> roleExists(Role role, String loggedUser) {

		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			/**
			 * User Email Id can't be duplicated, even in case of deleted Users
			 */
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("roleName", role.getRoleName());
			criteriaMap.put("isDeleted", Boolean.FALSE);
			
			Map<String, Object> ninCriteriaMap = new HashMap<>();
			ninCriteriaMap.put("roleId", role.getRoleId());
			
			
			if (null == masterDao.getEntityObject(new RoleEntity(), criteriaMap, ninCriteriaMap)) {
				status.setCode("ROLENAME_NOT_EXISTS");
				status.setMessage(ROLENAME_NOT_EXISTS);
				status.setStatus(false);
			}else {
				status.setCode("ROLENAME_ALREADY_EXISTS");
				status.setMessage(ROLENAME_ALREADY_EXISTS);
				status.setStatus(true);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("ROLENAME_NOT_EXISTS");
			status.setMessage(ROLENAME_NOT_EXISTS);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;

	}

	/*
	 * Method for retrieve the all hierarchy Stages on all levels in the product. 
	 */
	@Override
	public APIResponse<Object> getAllProductHierarchiesStages() {
		APIResponse<Object> response = new APIResponse<>();

		List<StagesEntity> productHierarchyStages = masterDao.getAllProductHierarchiesStages();
		if (null != productHierarchyStages && !productHierarchyStages.isEmpty()) {
			response.setCode(ResponseCode.SUCCESS.toString());
			response.setStatus("success");
			response.setStatusMessage("Successfully retrieved product hierarchy Stages");
			response.setData(productHierarchyStages);

		} else {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("OOPS No Records Found");
		}
		return response;
	}

	
}

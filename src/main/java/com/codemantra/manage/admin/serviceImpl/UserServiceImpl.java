/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
05-08-2017			v1.0       	   Shahid ul Islam  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.admin.serviceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.AwsAPIEntity;
import com.codemantra.manage.admin.entity.DownloadEntity;
import com.codemantra.manage.admin.entity.MailDetailsEntity;
import com.codemantra.manage.admin.entity.PasswordEntity;
import com.codemantra.manage.admin.entity.PermissionGroupEntity;
import com.codemantra.manage.admin.entity.TenantEntity;
import com.codemantra.manage.admin.entity.UserEntity;
import com.codemantra.manage.admin.entity.UserTenantMappingEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CodeDescription;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.User;
import com.codemantra.manage.admin.service.UserService;
import com.codemantra.manage.admin.util.AccessTokenGenerator;
import com.opencsv.CSVWriter;

@Service("userService")
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@Value("${INPUT_DATE_FORMAT}")
	String INPUT_DATE_FORMAT;

	SimpleDateFormat sdf2 = "dd/MM/yyyy".equalsIgnoreCase(INPUT_DATE_FORMAT)
			? new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
			: new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Autowired
	MasterDao masterDao;

	@Value("${FAILURE_ADD_USER_ID_ERROR}")
	String FAILURE_ADD_USER_ID_ERROR;

	@Value("${SUCCESS_ADD_USER_FAILURE_MAIL}")
	String SUCCESS_ADD_USER_FAILURE_MAIL;

	@Value("${FAILURE_ADD_USER}")
	String FAILURE_ADD_USER;

	@Value("${USER_ALREADY_EXISTS}")
	String USER_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_USER_NOT_EXISTS}")
	String FAILURE_EDIT_USER_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_USER}")
	String SUCCESS_EDIT_USER;

	@Value("${SUCCESS_EDIT_USER_FAILURE_MAIL}")
	String SUCCESS_EDIT_USER_FAILURE_MAIL;

	@Value("${FAILURE_EDIT_USER}")
	String FAILURE_EDIT_USER;

	@Value("${FAILURE_EDIT_USER_ALREADY_EXISTS}")
	String FAILURE_EDIT_USER_ALREADY_EXISTS;

	@Value("${FAILURE_DELETE_USER_NOT_EXISTS}")
	String FAILURE_DELETE_USER_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_USER}")
	String SUCCESS_DELETE_USER;

	@Value("${FAILURE_DELETE_USER}")
	String FAILURE_DELETE_USER;

	@Value("${SUCCESS_RETRIEVE_USER_LIST}")
	String SUCCESS_RETRIEVE_USER_LIST;

	@Value("${FAILURE_RETRIEVE_USER_LIST}")
	String FAILURE_RETRIEVE_USER_LIST;

	@Value("${FAILURE_RETRIEVE_USER_NOT_EXISTS}")
	String FAILURE_RETRIEVE_USER_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_USER}")
	String SUCCESS_RETRIEVE_USER;

	@Value("${FAILURE_RETRIEVE_USER}")
	String FAILURE_RETRIEVE_USER;

	@Value("${MANAGE_VERIFY_ACCESS_SERVICE_URL}")
	String MANAGE_VERIFY_ACCESS_SERVICE_URL;

	@Value("${USER_INVITATION_TEMPLATE}")
	String USER_INVITATION_TEMPLATE;

	@Value("${MANAGE_EMAIL_SERVICE_URL}")
	String MANAGE_EMAIL_SERVICE_URL;

	@Value("${SUCCESS_ADD_USER_SUCCESS_MAIL}")
	String SUCCESS_ADD_USER_SUCCESS_MAIL;

	@Value("${FAILURE_DEACTIVATE_USER_NOT_EXISTS}")
	String FAILURE_DEACTIVATE_USER_NOT_EXISTS;

	@Value("${FAILURE_ACTIVATE_USER_NOT_EXISTS}")
	String FAILURE_ACTIVATE_USER_NOT_EXISTS;

	@Value("${SUCCESS_ACTIVATE_USER}")
	String SUCCESS_ACTIVATE_USER;

	@Value("${SUCCESS_DEACTIVATE_USER}")
	String SUCCESS_DEACTIVATE_USER;

	@Value("${FAILURE_ACTIVATE_USER}")
	String FAILURE_ACTIVATE_USER;

	@Value("${FAILURE_DEACTIVATE_USER}")
	String FAILURE_DEACTIVATE_USER;

	@Value("${FAILURE_UPDATE_USER_INFO_USER_NOT_EXISTS}")
	String FAILURE_UPDATE_USER_INFO_USER_NOT_EXISTS;

	@Value("${SUCCESS_UPDATE_USER_INFO}")
	String SUCCESS_UPDATE_USER_INFO;

	@Value("${FAILURE_UPDATE_USER_INFO}")
	String FAILURE_UPDATE_USER_INFO;

	@Value("${FAILURE_UPDATE_PROFILE_PIC_USER_NOT_EXISTS}")
	String FAILURE_UPDATE_PROFILE_PIC_USER_NOT_EXISTS;

	@Value("${SUCCESS_UPDATE_PROFILE_PIC}")
	String SUCCESS_UPDATE_PROFILE_PIC;

	@Value("${FAILURE_UPDATE_PROFILE_PIC}")
	String FAILURE_UPDATE_PROFILE_PIC;

	@Value("${FAILURE_RESEND_INV_MAIL_USER_NOT_EXISTS}")
	String FAILURE_RESEND_INV_MAIL_USER_NOT_EXISTS;

	@Value("${SUCCESS_RESEND_INV_MAIL}")
	String SUCCESS_RESEND_INV_MAIL;

	@Value("${FAILURE_RESEND_INV_MAIL}")
	String FAILURE_RESEND_INV_MAIL;

	@Value("${SUCCESS_ADD_USER}")
	String SUCCESS_ADD_USER;

	@Value("${USER_NOT_EXISTS}")
	String USER_NOT_EXISTS;

	@Value("${PASSWORD_EXPIRY_DAYS}")
	String PASSWORD_EXPIRY_DAYS;

	@Value("${STATUS_ONLINE}")
	String STATUS_ONLINE;

	@Value("${STATUS_OFFLINE}")
	String STATUS_OFFLINE;

	@Value("${PG_NAME_ALREADY_EXISTS}")
	String PG_NAME_ALREADY_EXISTS;

	@Value("${PG_NAME_NOT_EXISTS}")
	String PG_NAME_NOT_EXISTS;

	@Value("${SUCCESS_EXPORT_USER_LIST}")
	String SUCCESS_EXPORT_USER_LIST;

	@Value("${FAILURE_EXPORT_USER_LIST}")
	String FAILURE_EXPORT_USER_LIST;

	@Value("${EXPORT_FILE_S3_PATH}")
	String EXPORT_FILE_S3_PATH;

	@Value("${EXPORT_URL_EXPIRY_DAYS}")
	int EXPORT_URL_EXPIRY_DAYS;

	@Value("${TEMP_OUTPUT_FOLDER_USER}")
	String TEMP_OUTPUT_FOLDER_USER;

	@Value("${EXPORT_FORMAT}")
	String EXPORT_FORMAT;

	@Value("${DOWNLOAD_STATUS}")
	String DOWNLOAD_STATUS;

	@Value("${DOWNLOAD_TYPE_USER}")
	String DOWNLOAD_TYPE_USER;

	@Value("${FAILURE_UNLOCK_USER_NOT_EXISTS}")
	String FAILURE_UNLOCK_USER_NOT_EXISTS;

	@Value("${SUCCESS_UNLOCK_USER}")
	String SUCCESS_UNLOCK_USER;

	@Value("${FAILURE_UNLOCK_USER}")
	String FAILURE_UNLOCK_USER;
	
	@Value("${keycloak.auth-server-url}")
	private String keycloakServerSurl;
	@Value("${keycloak.realm}")
	private String keycloakRealm;
	@Value("${master.keycloak.realm}")
	private String masterRealm;
	@Value("${master.keycloak.user}")
	private String masterUser;
	@Value("${master.keycloak.password}")
	private String masterPassword;
	@Value("${master.keycloak.client}")
	private String masterClientId;
	@Value("${master.keycloak.clientsecret}")
	private String masterClientSecret;
	
	@Value("${mongodb.defaultDatabaseName}")
	private String defaultDatabase;
	
	@Value("${feature.enable-skiplimit}")
	private boolean enableSkipLimit;
	
	@Override
	public Map<String, Object> save(User user, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			/**
			 * User Email Id can't be duplicated, even in case of deleted Users
			 */
			TenantContext.setTenant(defaultDatabase);
			user.setEmailId(user.getEmailId().toLowerCase());
			if (null == masterDao.getEntityObject(new UserEntity(), "emailId", user.getEmailId())) {
				String userId = masterDao.getNextSequenceId("userIdSeq");
				logger.info("User Id generated is [USR" + userId + "]");

				if (userId.equals("-1")) {
					status.setCode("FAILURE_ADD_USER_ID_ERROR");
					status.setMessage(FAILURE_ADD_USER_ID_ERROR);
					status.setStatus(false);
				} else {
					UserEntity entityObj = new UserEntity();
					Timestamp currentTs = new Timestamp(new Date().getTime());
					UserTenantMappingEntity userTenant = new UserTenantMappingEntity();
					
					TenantEntity tenant = masterDao.getEntityObject(new TenantEntity(), "tenantName",user.getAccountName());
					
					userTenant.setEmailId(user.getEmailId());
					userTenant.setTenantId(tenant.getTenantId());
					userTenant.setTenantCode(tenant.getTenantCode());
					userTenant.setTenantName(tenant.getTenantName());
					userTenant.setUserId("USR".concat(userId));
					masterDao.saveEntityObject(userTenant);
					
					user.setAccessKey(AccessTokenGenerator.generateAccessKey());
					user.setUserId("USR".concat(userId));
					user.setIsRegistered(Boolean.FALSE);
					user.setIsActive(Boolean.TRUE);
					user.setIsDeleted(Boolean.FALSE);
					user.setCreatedOn(currentTs);
					user.setCreatedBy(loggedUser);
					user.setModifiedOn(currentTs);
					user.setModifiedBy(loggedUser);
					user.setIsPasswordExpired(Boolean.FALSE);
					user.setPasswordExpiryDate(DateUtils.addDays(new Date(), Integer.valueOf(PASSWORD_EXPIRY_DAYS)));
					user.setOnlineStatus(STATUS_OFFLINE);

					BeanUtils.copyProperties(user, entityObj);

					if (!masterDao.isNull(user.getUserExpiryDate()))
						entityObj.setUserExpiryDate(
								new Timestamp(sdf.parse(user.getUserExpiryDate() + " 23:59:59").getTime()));

					if (!masterDao.isNull(user.getUserType()))
						entityObj.setFirstName(user.getEmailId());
					boolean isKeycloakUserCreated = false;

					/** Keycloak user creation start */
					if(user.getIsKeycloakUser()) {
						
						Response response;
						try {
							Keycloak keycloak = Keycloak.getInstance(keycloakServerSurl, masterRealm, masterUser,
									masterPassword, masterClientId, masterClientSecret);
							
							UserRepresentation keyCloakuser = new UserRepresentation();
							String[] email = user.getEmailId().toLowerCase().split("@");
							keyCloakuser.setUsername(email[0]);
							keyCloakuser.setEmail(user.getEmailId().toLowerCase());
							keyCloakuser.setEmailVerified(true);
							keyCloakuser.setEnabled(true);
							
							RealmResource realmResource = keycloak.realm(keycloakRealm);
							UsersResource userRessource = realmResource.users();
							
							response = userRessource.create(keyCloakuser);
							System.out.println("Keycloak user created : " + response.getStatus());
							logger.info("Keycloak user created : " + response.getStatus());
							isKeycloakUserCreated = true;
						} catch (Exception e) {
							isKeycloakUserCreated = false;
							logger.error("Error while creating Keycloak User ", e.getMessage());
							e.printStackTrace();
						}
					}
					/** Keycloak user creation end */
					if((user.getIsKeycloakUser() && isKeycloakUserCreated) || !user.getIsKeycloakUser()) {
						result = masterDao.saveEntityObject(entityObj);
						
						if (result) {
							
							boolean pwdTblInsertFlg = insertRecordIntoPasswordCollection(user, loggedUser);
							if (pwdTblInsertFlg) {
								/** User Invitation Email Sending **/
								status = sendInvitationMail(user, loggedUser);
							} else {
								logger.error("Error in sending mail due to non-insertion in Password table");
							}
							status.setCode("SUCCESS_ADD_USER_SUCCESS_MAIL");
							status.setMessage(SUCCESS_ADD_USER_SUCCESS_MAIL);
							status.setStatus(true);
						} else {
							status.setCode("FAILURE_ADD_USER");
							status.setMessage(FAILURE_ADD_USER);
							status.setStatus(false);
						}
					} else {
						status.setCode("FAILURE_ADD_USER");
						status.setMessage(FAILURE_ADD_USER);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("USER_ALREADY_EXISTS");
				status.setMessage(USER_ALREADY_EXISTS);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_USER");
			status.setMessage(FAILURE_ADD_USER);
			status.setStatus(false);
		} finally {
			finalData = retrieve(user.getUserId());
			finalData.put("status", status);
		}
		return finalData;

	}

	/**
	 * This method is used to update single user data by Admin.
	 * 
	 **/
	@Override
	public Map<String, Object> edit(User user, String id, String loggedUser) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		boolean sendMailFlg = false;
		boolean modifyFlg = false;
		user.setUserId(id);
		user.setCreatedBy(loggedUser);
		try {
			user.setEmailId(user.getEmailId().toLowerCase());
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode("FAILURE_EDIT_USER_NOT_EXISTS");
				status.setMessage(FAILURE_EDIT_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				/**
				 * If the Email of the unregistered user is updated, then Invitation Mail needs
				 * to be resent to the new updated Mail Id.
				 */
				entityObj.setEmailId(entityObj.getEmailId().toLowerCase());
				if (user.getEmailId().equals(entityObj.getEmailId()) || entityObj.getIsRegistered()) {
					modifyFlg = true;
				} else {
					if (null == masterDao.getEntityObject(new UserEntity(), "emailId", user.getEmailId())) {
						modifyFlg = true;
						sendMailFlg = true;
						user.setAccessKey(AccessTokenGenerator.generateAccessKey());
					} else {
						modifyFlg = false;
					}
				}

				if (modifyFlg) {
					entityObj.setPermissionGroupId(user.getPermissionGroupId());
					if (!entityObj.getIsRegistered()) {
						logger.info("Not Registered User");
						entityObj.setEmailId(user.getEmailId());
					}
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
					result = masterDao.updateEntityObject(entityObj);

					if (result) {
						if (sendMailFlg) {
							logger.info("Email has been updated. Sending mail for the updated emailId");
							boolean pwdTblInsertFlg = insertRecordIntoPasswordCollection(user, loggedUser);
							if (pwdTblInsertFlg) {
								/** User Invitation Email Sending in case of Updation of EMail Id **/
								status = sendInvitationMail(user, loggedUser);

							} else {
								logger.error("Error in sending mail due to non-insertion in Password table");
							}
						}
						status.setCode("SUCCESS_EDIT_USER");
						status.setMessage(SUCCESS_EDIT_USER);
						status.setStatus(true);

					} else {
						status.setCode("FAILURE_EDIT_USER");
						status.setMessage(FAILURE_EDIT_USER);
						status.setStatus(false);
					}
				} else {
					status.setCode("FAILURE_EDIT_USER_ALREADY_EXISTS");
					status.setMessage(FAILURE_EDIT_USER_ALREADY_EXISTS);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in editAccount :: ", e);
			status.setCode("FAILURE_EDIT_USER");
			status.setMessage(FAILURE_EDIT_USER);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> delete(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);

			if (null == entityObj) {
				status.setCode("FAILURE_DELETE_USER_NOT_EXISTS");
				status.setMessage(FAILURE_DELETE_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				entityObj.setIsDeleted(Boolean.TRUE);
				entityObj.setOnlineStatus(STATUS_OFFLINE);
				entityObj.setIsActive(Boolean.FALSE);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

				result = masterDao.updateEntityObject(entityObj);

				if (result) {
					status.setCode("SUCCESS_DELETE_USER");
					status.setMessage(SUCCESS_DELETE_USER);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_DELETE_USER");
					status.setMessage(FAILURE_DELETE_USER);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in deleteAccount :: ", e);
			status.setCode("FAILURE_DELETE_USER");
			status.setMessage(FAILURE_DELETE_USER);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAll(String loggedUser) {
		List<UserWithPermissionGroupEntity> users = new LinkedList<UserWithPermissionGroupEntity>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			Map<String, Object> criteriaMap = new HashMap<>();

			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("permissionGroup.account", userEntityObj.getPermissionGroup().getAccount());

			LookupOperation lookupOperation = LookupOperation.newLookup().from("mPermissionGroup")
					.localField("permissionGroupId").foreignField("permissionGroupId").as("permissionGroup");

			users = (List<UserWithPermissionGroupEntity>) masterDao.joinCollections(new UserWithPermissionGroupEntity(),
					"mUser", criteriaMap, lookupOperation, "permissionGroup");

			status.setCode("SUCCESS_RETRIEVE_USER_LIST");
			status.setMessage(SUCCESS_RETRIEVE_USER_LIST);
			status.setStatus(true);
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_USER_LIST");
			status.setMessage(FAILURE_RETRIEVE_USER_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", users);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieve(String id) {
		User user = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {

			Map<String, Object> criteriaMap = new HashMap<>();

			criteriaMap.put("userId", id);
			 criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			LookupOperation lookupOperation = LookupOperation.newLookup().from("mPermissionGroup")
					.localField("permissionGroupId").foreignField("permissionGroupId").as("permissionGroup");

			List<UserWithPermissionGroupEntity> userEntityList = (List<UserWithPermissionGroupEntity>) masterDao
					.joinCollections(new UserWithPermissionGroupEntity(), "mUser", criteriaMap, lookupOperation,
							"permissionGroup");

			if (userEntityList.size() != 1) {
				status.setCode("FAILURE_RETRIEVE_USER_NOT_EXISTS");
				status.setMessage(FAILURE_RETRIEVE_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				UserWithPermissionGroupEntity userEntityObj = userEntityList.get(0);

				user = new User();
				BeanUtils.copyProperties(userEntityObj, user);
				status.setCode("SUCCESS_RETRIEVE_USER");
				status.setMessage(SUCCESS_RETRIEVE_USER);
				status.setStatus(true);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_USER");
			status.setMessage(FAILURE_RETRIEVE_USER);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", user);
		}
		return finalData;
	}

	/**
	 * This method is used to activate or deactivate single user data.
	 * 
	 **/
	@Override
	public Map<String, Object> activateDeactivateUser(User user, String id, Integer activeStatus, String loggedUser) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode(
						activeStatus == 0 ? "FAILURE_DEACTIVATE_USER_NOT_EXISTS" : "FAILURE_ACTIVATE_USER_NOT_EXISTS");
				status.setMessage(
						activeStatus == 0 ? FAILURE_DEACTIVATE_USER_NOT_EXISTS : FAILURE_ACTIVATE_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				entityObj.setIsActive(activeStatus == 0 ? Boolean.FALSE : Boolean.TRUE);
				entityObj.setOnlineStatus(STATUS_OFFLINE);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
				result = masterDao.updateEntityObject(entityObj);
				if (result) {
					if (activeStatus == 1) {
						status.setCode("SUCCESS_ACTIVATE_USER");
						status.setMessage(SUCCESS_ACTIVATE_USER);
					} else {
						status.setCode("SUCCESS_DEACTIVATE_USER");
						status.setMessage(SUCCESS_DEACTIVATE_USER);
					}
					status.setStatus(true);
				} else {
					if (activeStatus == 1) {
						status.setCode("FAILURE_ACTIVATE_USER");
						status.setMessage(FAILURE_ACTIVATE_USER);
					} else {
						status.setCode("FAILURE_DEACTIVATE_USER");
						status.setMessage(FAILURE_DEACTIVATE_USER);
					}
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in activateDeactivateUser :: ", e);
			if (activeStatus == 1) {
				status.setCode("FAILURE_ACTIVATE_USER");
				status.setMessage(FAILURE_ACTIVATE_USER);
			} else {
				status.setCode("FAILURE_DEACTIVATE_USER");
				status.setMessage(FAILURE_DEACTIVATE_USER);
			}
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;
	}

	/**
	 * This method is used to update user information by the user itself
	 * 
	 **/
	@Override
	public Map<String, Object> updateUserInfo(User user, String id) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode("FAILURE_UPDATE_USER_INFO_USER_NOT_EXISTS");
				status.setMessage(FAILURE_UPDATE_USER_INFO_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				user.setModifiedBy(id);
				user.setModifiedOn(new Timestamp(new Date().getTime()));
				result = masterDao.updateEntityObject(setUserEntityFieldsForUserInfoUpdate(user, entityObj));
				if (result) {
					status.setCode("SUCCESS_UPDATE_USER_INFO");
					status.setMessage(SUCCESS_UPDATE_USER_INFO);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_UPDATE_USER_INFO");
					status.setMessage(FAILURE_UPDATE_USER_INFO);
					status.setStatus(false);
				}

			}
		} catch (Exception e) {
			logger.error("Exception in updateUserInfo :: ", e);
			status.setCode("FAILURE_UPDATE_USER_INFO");
			status.setMessage(FAILURE_UPDATE_USER_INFO);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override

	public Map<String, Object> updateProfilePic(User user, String loggedUser) {
		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();

		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", loggedUser);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode("FAILURE_UPDATE_PROFILE_PIC_USER_NOT_EXISTS");
				status.setMessage(FAILURE_UPDATE_PROFILE_PIC_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				entityObj.setProfilePicName(user.getProfilePicName());
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));
				result = masterDao.updateEntityObject(entityObj);
				if (result) {
					status.setCode("SUCCESS_UPDATE_PROFILE_PIC");
					status.setMessage(SUCCESS_UPDATE_PROFILE_PIC);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_UPDATE_PROFILE_PIC");
					status.setMessage(FAILURE_UPDATE_PROFILE_PIC);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in updateProfilePic :: ", e);
			status.setCode("FAILURE_UPDATE_PROFILE_PIC");
			status.setMessage(FAILURE_UPDATE_PROFILE_PIC);
			status.setStatus(false);
		} finally {
			finalData = retrieve(loggedUser);
			finalData.put("status", status);
		}

		return finalData;
	}

	/**
	 * This method is used to resend the Invitation Mail to a particular user
	 **/
	@Override
	public Map<String, Object> resendInvitationMail(User user, String id, String loggedUser) {

		boolean result = false;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);
			criteriaMap.put("isDeleted", false);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode("FAILURE_RESEND_INV_MAIL_USER_NOT_EXISTS");
				status.setMessage(FAILURE_RESEND_INV_MAIL_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				BeanUtils.copyProperties(entityObj, user);

				user.setAccessKey(AccessTokenGenerator.generateAccessKey());
				user.setCreatedBy(loggedUser);
				user.setCreatedOn(new Timestamp(new Date().getTime()));
				user.setModifiedBy(loggedUser);
				user.setModifiedOn(new Timestamp(new Date().getTime()));

				boolean pwdTblInsertFlg = insertRecordIntoPasswordCollection(user, loggedUser);
				if (pwdTblInsertFlg) {
					/** User Invitation Email Sending **/
					status = sendInvitationMail(user, loggedUser);

					if (status.isStatus()) {
						result = true;
					} else {
						result = false;
					}
				} else {
					logger.error("Error in sending mail due to non-insertion in Password table");
					result = false;
				}

				if (result) {
					status.setCode("SUCCESS_RESEND_INV_MAIL");
					status.setMessage(SUCCESS_RESEND_INV_MAIL);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_RESEND_INV_MAIL");
					status.setMessage(FAILURE_RESEND_INV_MAIL);
					status.setStatus(false);
				}

			}
		} catch (Exception e) {
			logger.error("Exception in removeUser :: ", e);
			status.setCode("FAILURE_RESEND_INV_MAIL");
			status.setMessage(FAILURE_RESEND_INV_MAIL);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;
	}

	
	/*
	 * This method is used to insert a record into Password Collection, once the
	 * Mail for Setting Password is sent
	 */
	public boolean insertRecordIntoPasswordCollection(User user, String loggedUser) {

		boolean insertFlg = false;
		String verifyAccessUrl = MANAGE_VERIFY_ACCESS_SERVICE_URL + user.getAccessKey();

		try {
			PasswordEntity pwdEntity = new PasswordEntity();
			Timestamp currentTs = new Timestamp(new Date().getTime());

			pwdEntity.setUserId(user.getUserId());
			pwdEntity.setEmailId(user.getEmailId().toLowerCase());
			pwdEntity.setPasswordLink(verifyAccessUrl);
			pwdEntity.setAccessKey(user.getAccessKey());
			pwdEntity.setIsActive(Boolean.TRUE);
			pwdEntity.setIsDeleted(Boolean.FALSE);
			pwdEntity.setCreatedBy(loggedUser);
			pwdEntity.setCreatedOn(currentTs);
			pwdEntity.setModifiedBy(loggedUser);
			pwdEntity.setModifiedOn(currentTs);
			pwdEntity.setPassword(null);
			pwdEntity.setExpiryOn(user.getPasswordExpiryDate());

			insertFlg = masterDao.saveEntityObject(pwdEntity);

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			insertFlg = false;
		}

		return insertFlg;
	}

	/*
	 * This method is used to send Invitation Mail to newly created User for Setting
	 * Password
	 */

	public Status sendInvitationMail(User user, String loggedUser) {
		Status status = new Status();
		try {
			if(null!=user.getEmailId())
				user.setEmailId(user.getEmailId().toLowerCase());
			UserEntity loggedUserData = masterDao.getEntityObject(new UserEntity(), "userId", loggedUser);
			user.setLoginName(loggedUserData.getFirstName()
					+ (loggedUserData.getLastName() == null ? "" : " " + loggedUserData.getLastName()));

			logger.info("Access Key " + user.getAccessKey());

			String subject = "User Invitation";
			String verifyAccessUrl = MANAGE_VERIFY_ACCESS_SERVICE_URL + user.getAccessKey();
			Map<String, Object> templateMap = new HashMap<>();
			templateMap.put("to", user.getFirstName() + (user.getLastName() == null ? "" : " " + user.getLastName()));
			templateMap.put("url", verifyAccessUrl);
			templateMap.put("from", "Manage Support Team");
			templateMap.put("createdBy", user.getLoginName());

			Map<String, Object> request = new HashMap<>();
			request.put("to_email", user.getEmailId());
			request.put("email_from", "donotreply@codemantra.com");
			request.put("subject", subject);
			request.put("templateData", templateMap);
			request.put("templateName", USER_INVITATION_TEMPLATE);

			RestTemplate restTemplate = new RestTemplate();

			APIResponse<?> response = restTemplate.postForObject(MANAGE_EMAIL_SERVICE_URL, request, APIResponse.class);

			logger.info("Response Code for Sending Mail [" + response.getCode() + "] - ["
					+ ResponseCode.SUCCESS.toString().equals(response.getCode()) + "]");

			status.setCode("SUCCESS_ADD_USER_SUCCESS_MAIL");
			status.setMessage(SUCCESS_ADD_USER_SUCCESS_MAIL);
			status.setStatus(true);

			if (ResponseCode.SUCCESS.toString().equals(response.getCode())) {
				
				logger.info("Mail sent Successfully for User Invitation");
				
			} else {
				logger.info("Error happened while sending User Invitation mail");
				
			}
		} catch (Exception e) {
			logger.error("Exception :: ", e);

			logger.info("Access Keyyyyyyy " + user.getAccessKey());
			insertRecordIntoMailingCollection(user);
			status.setCode("SUCCESS_ADD_USER_SUCCESS_MAIL");
			status.setMessage(SUCCESS_ADD_USER_SUCCESS_MAIL);
			status.setStatus(false);
		} finally {
			masterDao.updateCollectionDocument("mUser", "userId", user.getUserId(), "invitationMailSentOn",
					new Timestamp(new Date().getTime()));
		}

		return status;
	}

	/*
	 * This method is used to insert a record into Mailing Collection, if Email
	 * sending for setting Password Fails
	 */

	public boolean insertRecordIntoMailingCollection(User user) {

		boolean insertFlg = false;
		logger.info("Accesssssss Key " + user.getAccessKey());
		String verifyAccessUrl = MANAGE_VERIFY_ACCESS_SERVICE_URL + user.getAccessKey();

		String contents = null;
		String ccEmail = null;
		String subject = "User Invitation";
		String emailFrom = "donotreply@codemantra.com";
		String header = null;
		String headerValue = null;
		String companyEmail = null;
		int updateFlg = 1;

		Map<String, Object> templateMap = new HashMap<>();
		templateMap.put("to", user.getFirstName() + " " + user.getLastName());
		templateMap.put("url", verifyAccessUrl);
		templateMap.put("from", "Manage Support Team");
		templateMap.put("createdBy", user.getLoginName());

		try {

			MailDetailsEntity mailingEntity = new MailDetailsEntity();
			mailingEntity.setUserId(user.getUserId());
			mailingEntity.setToEmail(user.getEmailId().toLowerCase());
			mailingEntity.setCcEmail(ccEmail); // To be verified
			mailingEntity.setContents(contents); // To be verified
			mailingEntity.setSubject(subject);
			mailingEntity.setEmailFrom(emailFrom);
			mailingEntity.setHeader(header);
			mailingEntity.setHeaderValue(headerValue);
			mailingEntity.setCompanyEmail(companyEmail);
			mailingEntity.setTemplateData(templateMap);
			mailingEntity.setTemplateName(USER_INVITATION_TEMPLATE);
			mailingEntity.setUpdateFlag(updateFlg);
			mailingEntity.setCreatedBy(user.getCreatedBy());

			insertFlg = masterDao.saveEntityObject(mailingEntity);

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			insertFlg = false;
		}

		return insertFlg;
	}

	@Override
	public Map<String, Object> userExists(User user, String loggedUser) {

		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			/**
			 * User Email Id can't be duplicated, even in case of deleted Users
			 */
			user.setEmailId(user.getEmailId().toLowerCase());
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("emailId", user.getEmailId());

			Map<String, Object> ninCriteriaMap = new HashMap<>();
			ninCriteriaMap.put("userId", user.getUserId());

			if (null == masterDao.getEntityObject(new UserEntity(), criteriaMap, ninCriteriaMap)) {
				status.setCode("USER_NOT_EXISTS");
				status.setMessage(USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				status.setCode("USER_ALREADY_EXISTS");
				status.setMessage(USER_ALREADY_EXISTS);
				status.setStatus(true);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("USER_NOT_EXISTS");
			status.setMessage(USER_NOT_EXISTS);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;

	}

	@Override
	public Map<String, Object> pgExists(User user, String loggedUser) {

		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			/**
			 * User Email Id can't be duplicated, even in case of deleted Users
			 */

			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("permissionGroupName", user.getPgName());
			criteriaMap.put("isDeleted", Boolean.FALSE);

			if (null == masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap)) {
				status.setCode("PG_NAME_NOT_EXISTS");
				status.setMessage(PG_NAME_NOT_EXISTS);
				status.setStatus(false);
			} else {
				status.setCode("PG_NAME_ALREADY_EXISTS");
				status.setMessage(PG_NAME_ALREADY_EXISTS);
				status.setStatus(true);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("PG_NAME_NOT_EXISTS");
			status.setMessage(PG_NAME_NOT_EXISTS);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;

	}

	private UserEntity setUserEntityFieldsForUserInfoUpdate(User user, UserEntity userEntity) {
		userEntity.setFirstName(user.getFirstName());
		userEntity.setLastName(user.getLastName());
		userEntity.setAddress(user.getAddress());
		userEntity.setCity(user.getCity());
		userEntity.setZipCode(user.getZipCode());
		userEntity.setContactNo(user.getContactNo());
		userEntity.setModifiedBy(user.getModifiedBy());
		userEntity.setModifiedOn(user.getModifiedOn());
		userEntity.setFirstNameFirst(user.getFirstNameFirst());
		userEntity.setFirstNameLast(user.getFirstNameLast());

		return userEntity;
	}

	@Override
	public Map<String, Object> getSuggestions(User user, String loggedUser) {
		List<CodeDescription> filterList = null;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			List<Criteria> finalCriteriaList = new ArrayList<>();
			Map<String, Object> criteriaMap = new HashMap<>();

			List<String> pgList = new ArrayList<>();

			String codeField = "";
			String descField = "";
			String collection = "";

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("isActive", Boolean.TRUE);
			criteriaMap.put("account", userEntityObj.getPermissionGroup().getAccount());

			List<String> userAcntPgList = masterDao.getDistinctFieldValues("mPermissionGroup", criteriaMap,
					"permissionGroupId");

			if ("NAME".equals(user.getFilterFieldName()) || "EMAIL".equals(user.getFilterFieldName())) {
				if (!masterDao.isNull(user.getUserRoleFilterList()) || !masterDao.isNull(user.getUserPgFilterList())) {

					if (!masterDao.isNull(user.getUserRoleFilterList())) {
						criteriaMap.clear();
						criteriaMap.put("roleId", user.getUserRoleFilterList());
						criteriaMap.put("isActive", Boolean.TRUE);
						criteriaMap.put("isDeleted", Boolean.FALSE);

						pgList = masterDao.getDistinctFieldValues("mPermissionGroup", criteriaMap, "permissionGroupId");

						if (!masterDao.isNull(user.getUserPgFilterList()))
							pgList.retainAll(user.getUserPgFilterList());

					} else
						pgList = user.getUserPgFilterList();
				}

				if (!masterDao.isNull(pgList))
					userAcntPgList.retainAll(pgList);

				finalCriteriaList.add(Criteria.where("permissionGroupId").in(userAcntPgList));

				if (!masterDao.isNull(user.getUserStatusFilterList()) && user.getUserStatusFilterList().size() == 1) {
					if (user.getUserStatusFilterList().contains("21"))
						finalCriteriaList.add(Criteria.where("isRegistered").is(Boolean.TRUE));
					else
						finalCriteriaList.add(Criteria.where("isRegistered").is(Boolean.FALSE));
				}

				if (!masterDao.isNull(user.getUserOnlineStatusFilterList())) {
					finalCriteriaList.add(Criteria.where("onlineStatus").in(user.getUserOnlineStatusFilterList()));
				}

				if (!masterDao.isNull(user.getCreatedFromDate())) {
					finalCriteriaList.add(Criteria.where("createdOn")
							.gte(new Date(sdf2.parse(user.getCreatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getCreatedToDate())) {
					finalCriteriaList.add(Criteria.where("createdOn")
							.lte(new Date(sdf2.parse(user.getCreatedToDate() + " 23:59:59").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedFromDate())) {
					finalCriteriaList.add(Criteria.where("registeredOn")
							.gte(new Date(sdf2.parse(user.getActivatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedToDate())) {
					finalCriteriaList.add(Criteria.where("registeredOn")
							.lte(new Date(sdf2.parse(user.getActivatedToDate() + " 23:59:59").getTime())));
				}

				if ("NAME".equals(user.getFilterFieldName()) && !masterDao.isNull(user.getUserEmailFilterList())) {
					finalCriteriaList.add(Criteria.where("emailId").in(user.getUserEmailFilterList()));
				}

				if ("EMAIL".equals(user.getFilterFieldName()) && !masterDao.isNull(user.getUserNameFilterList())) {
					finalCriteriaList.add(Criteria.where("firstName").in(user.getUserNameFilterList()));
				}

				if ("NAME".equals(user.getFilterFieldName())) {
					finalCriteriaList
							.add(Criteria.where("firstName").regex(Pattern.quote(user.getFilterFieldText()), "i"));
					codeField = "firstName";
					descField = "firstName";
				} else {
					finalCriteriaList
							.add(Criteria.where("emailId").regex(Pattern.quote(user.getFilterFieldText()), "i"));
					codeField = "emailId";
					descField = "emailId";
				}

				collection = "mUser";
				finalCriteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
			} else if ("PG".equals(user.getFilterFieldName())) {

				List<Criteria> criteriaList = new ArrayList<>();

				if (!masterDao.isNull(user.getUserNameFilterList())) {
					criteriaList.add(Criteria.where("firstName").in(user.getUserNameFilterList()));
				}

				if (!masterDao.isNull(user.getUserEmailFilterList())) {
					criteriaList.add(Criteria.where("emailId").in(user.getUserEmailFilterList()));
				}

				if (!masterDao.isNull(user.getUserStatusFilterList())
						&& user.getUserStatusFilterList().size() == 1) {
					if (user.getUserStatusFilterList().contains("21"))
						criteriaList.add(Criteria.where("isRegistered").is(Boolean.TRUE));
					else
						criteriaList.add(Criteria.where("isRegistered").is(Boolean.FALSE));
				}

				if (!masterDao.isNull(user.getUserOnlineStatusFilterList())) {
					criteriaList.add(Criteria.where("onlineStatus").in(user.getUserOnlineStatusFilterList()));
				}

				if (!masterDao.isNull(user.getCreatedFromDate())) {
					criteriaList.add(Criteria.where("createdOn")
							.gte(new Date(sdf2.parse(user.getCreatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getCreatedToDate())) {
					criteriaList.add(Criteria.where("createdOn")
							.lte(new Date(sdf2.parse(user.getCreatedToDate() + " 23:59:59").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedFromDate())) {
					criteriaList.add(Criteria.where("registeredOn")
							.gte(new Date(sdf2.parse(user.getActivatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedToDate())) {
					criteriaList.add(Criteria.where("registeredOn")
							.lte(new Date(sdf2.parse(user.getActivatedToDate() + " 23:59:59").getTime())));
				}

				criteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
				criteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));

				pgList = masterDao.getDistinctFieldValues("mUser", criteriaList, "permissionGroupId");

				if (!masterDao.isNull(pgList))
					userAcntPgList.retainAll(pgList);

				finalCriteriaList.add(Criteria.where("permissionGroupId").in(userAcntPgList));

				if (!masterDao.isNull(user.getUserRoleFilterList())) {
					finalCriteriaList.add(Criteria.where("roleId").in(user.getUserRoleFilterList()));
				}

				finalCriteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
				finalCriteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
				finalCriteriaList.add(
						Criteria.where("permissionGroupName").regex(Pattern.quote(user.getFilterFieldText()), "i"));
				collection = "mPermissionGroup";
				codeField = "permissionGroupId";
				descField = "permissionGroupName";
			} else if ("ROLE".equals(user.getFilterFieldName())) {

				List<Criteria> criteriaList = new ArrayList<>();

				if (!masterDao.isNull(user.getUserNameFilterList())) {
					criteriaList.add(Criteria.where("firstName").in(user.getUserNameFilterList()));
				}

				if (!masterDao.isNull(user.getUserEmailFilterList())) {
					criteriaList.add(Criteria.where("emailId").in(user.getUserEmailFilterList()));
				}

				if (!masterDao.isNull(user.getUserStatusFilterList())
						&& user.getUserStatusFilterList().size() == 1) {
					if (user.getUserStatusFilterList().contains("21"))
						criteriaList.add(Criteria.where("isRegistered").is(Boolean.TRUE));
					else
						criteriaList.add(Criteria.where("isRegistered").is(Boolean.FALSE));

				}

				if (!masterDao.isNull(user.getUserOnlineStatusFilterList())) {
					criteriaList.add(Criteria.where("onlineStatus").in(user.getUserOnlineStatusFilterList()));
				}

				if (!masterDao.isNull(user.getCreatedFromDate())) {
					criteriaList.add(Criteria.where("createdOn")
							.gte(new Date(sdf2.parse(user.getCreatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getCreatedToDate())) {
					criteriaList.add(Criteria.where("createdOn")
							.lte(new Date(sdf2.parse(user.getCreatedToDate() + " 23:59:59").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedFromDate())) {
					criteriaList.add(Criteria.where("registeredOn")
							.gte(new Date(sdf2.parse(user.getActivatedFromDate() + " 00:00:00").getTime())));
				}
				if (!masterDao.isNull(user.getActivatedToDate())) {
					criteriaList.add(Criteria.where("registeredOn")
							.lte(new Date(sdf2.parse(user.getActivatedToDate() + " 23:59:59").getTime())));
				}

				criteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
				criteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));

				pgList = masterDao.getDistinctFieldValues("mUser", criteriaList, "permissionGroupId");

				userAcntPgList.retainAll(pgList);
				if (!masterDao.isNull(user.getUserPgFilterList()))
					userAcntPgList.retainAll(user.getUserPgFilterList());

				List<Criteria> criteriaList2 = new ArrayList<>();

				criteriaList2.add(Criteria.where("permissionGroupId").in(userAcntPgList));
				criteriaList2.add(Criteria.where("isDeleted").is(Boolean.FALSE));
				criteriaList2.add(Criteria.where("isActive").is(Boolean.TRUE));

				List<String> roleList = masterDao.getDistinctFieldValues("mPermissionGroup", criteriaList2, "roleId");

				finalCriteriaList.add(Criteria.where("roleId").in(roleList));
				finalCriteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
				finalCriteriaList.add(Criteria.where("isActive").is(Boolean.TRUE));
				finalCriteriaList.add(Criteria.where("roleName").regex(Pattern.quote(user.getFilterFieldText()), "i"));
				collection = "mRole";
				codeField = "roleId";
				descField = "roleName";
			}

			filterList = masterDao.getColumnFilterSuggestions(collection, codeField, descField, finalCriteriaList,
					user.getFilterFieldText());
			status.setCode(ResponseCode.SUCCESS.toString());
			status.setMessage("Success");
			status.setStatus(true);

		} catch (Exception e) {
			logger.error("Exception in getColumnFilterSuggestions :: ", e);
			filterList = null;
			status.setCode(ResponseCode.ERROR.toString());
			status.setMessage("Error");
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", filterList);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllWithFilters(User user, String loggedUser) {
		List<UserWithPermissionGroupEntity> users = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			
			Integer limit = 10;
			if(enableSkipLimit) {
				List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig", null, "config.viewLimit",
						"_id", "user_master_search", null);
				if (!masterDao.isNull(mappingValues)) {
					if(mappingValues.get(0).getValue() instanceof Double)
						limit = ((Double) mappingValues.get(0).getValue()).intValue();
					else
						limit = (Integer) mappingValues.get(0).getValue();
				}
				
			}
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			List<Criteria> criteriaList = new ArrayList<>();

			criteriaList.add(Criteria.where("isDeleted").is(Boolean.FALSE));
			criteriaList
					.add(Criteria.where("permissionGroup.account").in(userEntityObj.getPermissionGroup().getAccount()));
			if (!masterDao.isNull(user.getUserNameFilterList()))
				criteriaList.add(Criteria.where("firstName").in(user.getUserNameFilterList()));
			if (!masterDao.isNull(user.getUserRoleFilterList()))
				criteriaList.add(Criteria.where("permissionGroup.roleId").in(user.getUserRoleFilterList()));
			if(enableSkipLimit) {
				if (!masterDao.isNull(user.getUserPgFilterList()))
					criteriaList.add(Criteria.where("permissionGroupId").in(user.getUserPgFilterList()));
			}
			if (!masterDao.isNull(user.getUserEmailFilterList()))
				criteriaList.add(Criteria.where("emailId").in(user.getUserEmailFilterList()));
			if (!masterDao.isNull(user.getUserStatusFilterList()) && user.getUserStatusFilterList().size() == 1) {
				if (user.getUserStatusFilterList().contains("21"))
					criteriaList.add(Criteria.where("isRegistered").is(Boolean.TRUE));
				else
					criteriaList.add(Criteria.where("isRegistered").is(Boolean.FALSE));
			}

			if (!masterDao.isNull(user.getUserOnlineStatusFilterList())) {
				criteriaList.add(Criteria.where("onlineStatus").in(user.getUserOnlineStatusFilterList()));
			}

			if (!masterDao.isNull(user.getCreatedFromDate())) {
				criteriaList.add(Criteria.where("createdOn")
						.gte(new Date(sdf2.parse(user.getCreatedFromDate() + " 00:00:00").getTime())));
			}
			if (!masterDao.isNull(user.getCreatedToDate())) {
				criteriaList.add(Criteria.where("createdOn")
						.lte(new Date(sdf2.parse(user.getCreatedToDate() + " 23:59:59").getTime())));
			}
			if (!masterDao.isNull(user.getActivatedFromDate())) {
				criteriaList.add(Criteria.where("registeredOn")
						.gte(new Date(sdf2.parse(user.getActivatedFromDate() + " 00:00:00").getTime())));
			}
			if (!masterDao.isNull(user.getActivatedToDate())) {
				criteriaList.add(Criteria.where("registeredOn")
						.lte(new Date(sdf2.parse(user.getActivatedToDate() + " 23:59:59").getTime())));
			}

			LookupOperation lookupOperation = LookupOperation.newLookup().from("mPermissionGroup")
					.localField("permissionGroupId").foreignField("permissionGroupId").as("permissionGroup");
			
			if(enableSkipLimit) {
				// FINDING FILTERED LIST COUNT LOGIC STARTS 

				Map<String, Object> criteriaMap = new HashMap<>();

				criteriaMap.put("account", userEntityObj.getPermissionGroup().getAccount());
				if (!masterDao.isNull(user.getUserRoleFilterList()))
					criteriaMap.put("roleId", user.getUserRoleFilterList());
				List<String> distinctPermissionGroupIds = masterDao.getDistinctFieldValues("mPermissionGroup",criteriaMap,"permissionGroupId");
				
				LinkedHashMap<String, Object> userCountcriteriaMap = new LinkedHashMap<>();
				userCountcriteriaMap.put("isDeleted", false);
				userCountcriteriaMap.put("permissionGroupId", distinctPermissionGroupIds);
				if (masterDao.isNull(user.getUserPgFilterList()))
					criteriaList.add(Criteria.where("permissionGroupId").in(distinctPermissionGroupIds));
				if (!masterDao.isNull(user.getUserPgFilterList())) {
					List<String> removeIds = new ArrayList<>();
					for(String pgId : user.getUserPgFilterList()) {
						if(!distinctPermissionGroupIds.contains(pgId))
							removeIds.add(pgId);
					}
					if(!removeIds.isEmpty())
						user.getUserPgFilterList().removeAll(removeIds);
					criteriaList.add(Criteria.where("permissionGroupId").in(user.getUserPgFilterList()));
				}
				
				// FINDING FILTERED LIST COUNT LOGIC End */ 
				
				criteriaList
					.add(Criteria.where("permissionGroup.account").in(userEntityObj.getPermissionGroup().getAccount()));
				if (!masterDao.isNull(user.getUserRoleFilterList()))
					criteriaList.add(Criteria.where("permissionGroup.roleId").in(user.getUserRoleFilterList()));
				
				if(user.isExport()) {
					user.setSkip(0);
					limit = 100000;
				}
		
				users = (List<UserWithPermissionGroupEntity>) masterDao.joinCollectionsWithSkipLimit(new UserWithPermissionGroupEntity(),
						"mUser", criteriaList, lookupOperation, "permissionGroup",user.getSkip(),limit);
			} else {
				
				users = (List<UserWithPermissionGroupEntity>) masterDao.joinCollections(new UserWithPermissionGroupEntity(),
						"mUser", criteriaList, lookupOperation, "permissionGroup");
			}

			if (user.isExport()) {
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date today = Calendar.getInstance().getTime();
				String currentDate = df.format(today);

				File tempExportDir = new File(TEMP_OUTPUT_FOLDER_USER);
				if (!tempExportDir.exists())
					tempExportDir.mkdirs();

				String fileName = "USERS_" + currentDate + ("XLSX".equalsIgnoreCase(EXPORT_FORMAT) ? ".xlsx" : ".csv");
				String filePath = TEMP_OUTPUT_FOLDER_USER + File.separator + fileName;

				URL url = null;
				AwsAPIEntity apiEntity = masterDao.getAwsCredentials();

				Date urlExpiryDate = new Date();
				long msec = urlExpiryDate.getTime();
				msec += EXPORT_URL_EXPIRY_DAYS * 24 * 60 * 60 * 1000;
				urlExpiryDate.setTime(msec);

				boolean exportFlg = exportUsers(users, filePath);
				if (exportFlg) {

					url = masterDao.uploadFileToS3(apiEntity, filePath, fileName, urlExpiryDate, EXPORT_FILE_S3_PATH);

					if (null != url) {

						DownloadEntity entityObj = new DownloadEntity();
						entityObj.setUniqueId("USERS_" + currentDate);
						entityObj.setRequestedDate(today);
						entityObj.setUserId(loggedUser);
						entityObj.setDownloadStatus(DOWNLOAD_STATUS);
						entityObj.setExpiryDays(EXPORT_URL_EXPIRY_DAYS);
						entityObj.setDownloadType(DOWNLOAD_TYPE_USER);
						entityObj.setCreatedBy(loggedUser);
						entityObj.setCreatedDate(today);
						entityObj.setModifiedBy(loggedUser);
						entityObj.setModifiedDate(today);
						entityObj.setDownloadLink(url);
						entityObj.setExpiryDate(urlExpiryDate);

						masterDao.saveEntityObject(entityObj);

						status.setCode("SUCCESS_EXPORT_USER_LIST");
						status.setMessage(SUCCESS_EXPORT_USER_LIST);
						status.setStatus(true);
					} else {

						status.setCode("FAILURE_EXPORT_USER_LIST");
						status.setMessage(FAILURE_EXPORT_USER_LIST);
						status.setStatus(false);
					}
				} else {
					status.setCode("FAILURE_EXPORT_USER_LIST");
					status.setMessage(FAILURE_EXPORT_USER_LIST);
					status.setStatus(false);
				}

				boolean deleted = new File(filePath).delete();
				if(deleted) {
					logger.info("File deleted successfully "+filePath);
				}
			} else {
				status.setCode("SUCCESS_RETRIEVE_USER_LIST");
				status.setMessage(SUCCESS_RETRIEVE_USER_LIST);
				status.setStatus(true);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			if (user.isExport()) {
				status.setCode("FAILURE_EXPORT_USER_LIST");
				status.setMessage(FAILURE_EXPORT_USER_LIST);
				status.setStatus(false);
			} else {
				status.setCode("FAILURE_RETRIEVE_USER_LIST");
				status.setMessage(FAILURE_RETRIEVE_USER_LIST);
				status.setStatus(false);
			}
		} finally {
			finalData.put("status", status);
			finalData.put("data", users);
		}
		return finalData;
	}

	public Boolean exportUsers(List<UserWithPermissionGroupEntity> users, String fileName) throws Exception {
		boolean exportFlg = true;
		XSSFWorkbook workbook = null;
		FileOutputStream out = null;
		CSVWriter writer = null;
		try {
			String dateFormat = INPUT_DATE_FORMAT;
			SimpleDateFormat sdf3 = new SimpleDateFormat(dateFormat);
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isDeleted", Boolean.FALSE);
			List<KeyValue> roleList = masterDao.getKeyValueList("mRole", "roleId", "roleName", criteriaMap, null);

			Map<String, Object> roleMap = CollectionUtils.emptyIfNull(roleList).stream()
					.collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));

			String[] headers = { "NAME", "USER ROLE", "PERMISSION GROUP", "EMAIL ID", "CREATED ON", "ACTIVATED ON",
					"ACTIVE" };

			if ("XLSX".equalsIgnoreCase(EXPORT_FORMAT)) {
				workbook = new XSSFWorkbook();

				XSSFSheet sheet = workbook.createSheet("Users");// creating a blank sheet
				int rownum = 0;

				XSSFCellStyle style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontHeightInPoints((short) 10);
				font.setBold(true);
				style.setFont(font);

				Row header = sheet.createRow(rownum++);
				for (int i = 0; i < headers.length; i++) {
					header.createCell(i).setCellValue(headers[i]);
					header.getCell(i).setCellStyle(style);
				}

				for (UserWithPermissionGroupEntity user : users) {
					Row row = sheet.createRow(rownum++);
					row.createCell(0).setCellValue(user.getFirstName());
					row.createCell(1).setCellValue((String) roleMap.get(user.getPermissionGroup().getRoleId()));
					row.createCell(2).setCellValue(user.getPermissionGroup().getPermissionGroupName());
					row.createCell(3).setCellValue(user.getEmailId());
					row.createCell(4).setCellValue(sdf3.format(user.getCreatedOn()));
					row.createCell(5)
							.setCellValue(user.getIsRegistered() ? sdf3.format(user.getRegisteredOn()) : "Pending");
					row.createCell(6).setCellValue(user.getIsActive() ? "Yes" : "No");

				}

				out = new FileOutputStream(new File(fileName)); // file name with path
				workbook.write(out);
			} else {
				File exportFile = new File(fileName);

				writer = new CSVWriter(new FileWriter(exportFile));
				writer.writeNext(headers);

				for (UserWithPermissionGroupEntity user : users) {
					writer.writeNext(new String[] { user.getFirstName(),
							(String) roleMap.get(user.getPermissionGroup().getRoleId()),
							user.getPermissionGroup().getPermissionGroupName(), user.getEmailId(),
							sdf3.format(user.getCreatedOn()),
							user.getIsRegistered() ? sdf3.format(user.getRegisteredOn()) : "Pending",
							user.getIsActive() ? "Yes" : "No" });
				}
				writer.flush();
			}

		} catch (Exception e) {
			e.printStackTrace();
			exportFlg = false;
		} finally {
			if(null != workbook)
				workbook.close();
			if(null != out) 
				out.close();
			if(null != writer) 
				writer.close();
		}

		return exportFlg;
	}

	@Override
	public Map<String, Object> unlockUser(String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("userId", id);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			UserEntity entityObj = masterDao.getEntityObject(new UserEntity(), criteriaMap);

			if (null == entityObj) {
				status.setCode("FAILURE_UNLOCK_USER_NOT_EXISTS");
				status.setMessage(FAILURE_UNLOCK_USER_NOT_EXISTS);
				status.setStatus(false);
			} else {
				entityObj.setIsLocked(Boolean.FALSE);
				entityObj.setLockCount(0);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

				result = masterDao.updateEntityObject(entityObj);

				if (result) {
					status.setCode("SUCCESS_UNLOCK_USER");
					status.setMessage(SUCCESS_UNLOCK_USER);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_UNLOCK_USER");
					status.setMessage(FAILURE_UNLOCK_USER);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in deleteAccount :: ", e);
			status.setCode("FAILURE_UNLOCK_USER");
			status.setMessage(FAILURE_UNLOCK_USER);
			status.setStatus(false);
		} finally {
			finalData = retrieve(id);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	public boolean sendInvitaiontoAllActiveUsers(String userId) {
		boolean result = false;
		try {
			UserEntity userEntity = new UserEntity();
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isActive", true);
			criteriaMap.put("isDeleted", false);
			List<UserEntity> users = (List<UserEntity>) masterDao.getAllEntityObjects(userEntity, criteriaMap);
			for (UserEntity userEnt : users) {
				User userObj = new User();
				BeanUtils.copyProperties(userEnt, userObj);
				sendInvitationMail(userObj, userId);
			}
			result = true;
		} catch (BeansException e) {
			result = false;
			logger.error("Excepion !!",e);
		}
		return result;
	}

	@Override
	public Map<String, Object> retrieveAllSkip(String loggedUser, Integer skip) {
		List<UserWithPermissionGroupEntity> users = new LinkedList<UserWithPermissionGroupEntity>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Integer limit = 10;
			List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig", null, "config.viewLimit",
					"_id", "user_master_search", null);
			if (!masterDao.isNull(mappingValues)) {
				if(mappingValues.get(0).getValue() instanceof Double)
					limit = ((Double) mappingValues.get(0).getValue()).intValue();
				else
					limit = (Integer) mappingValues.get(0).getValue();
			}
			
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			Map<String, Object> criteriaMap = new HashMap<>();

			criteriaMap.put("account", userEntityObj.getPermissionGroup().getAccount());
			List<String> distinctPermissionGroupIds = masterDao.getDistinctFieldValues("mPermissionGroup",criteriaMap,"permissionGroupId");
			
			LinkedHashMap<String, Object> userCountcriteriaMap = new LinkedHashMap<>();
			userCountcriteriaMap.put("isDeleted", false);
			userCountcriteriaMap.put("permissionGroupId", distinctPermissionGroupIds);
			
			Integer userListCount = masterDao.getCountOfDocuments("mUser", userCountcriteriaMap);
			
			criteriaMap.clear();
			
			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("permissionGroup.account", userEntityObj.getPermissionGroup().getAccount());

			
			
			LookupOperation lookupOperation = LookupOperation.newLookup().from("mPermissionGroup")
					.localField("permissionGroupId").foreignField("permissionGroupId").as("permissionGroup");

			users = (List<UserWithPermissionGroupEntity>) masterDao.joinCollectionsWithSkipLimitSkip(new UserWithPermissionGroupEntity(),
					"mUser", criteriaMap, lookupOperation,"permissionGroup",skip ,limit );

			status.setCode("SUCCESS_RETRIEVE_USER_LIST");
			status.setMessage(SUCCESS_RETRIEVE_USER_LIST);
			status.setCount(userListCount);
			status.setStatus(true);
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_USER_LIST");
			status.setMessage(FAILURE_RETRIEVE_USER_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", users);
		}
		return finalData;
	}

}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.JsonDbPathConditionEntity;
import com.codemantra.manage.admin.entity.ManageConfigEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.PermissionGroupEntity;
import com.codemantra.manage.admin.entity.ProductCategoryEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.PermissionGroup;
import com.codemantra.manage.admin.service.PermissionGroupService;

@Service("permissionGroupService")
public class PermissionGroupServiceImpl implements PermissionGroupService {

	private static final Logger logger = LoggerFactory.getLogger(PermissionGroupServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${FAILURE_ADD_PG_ID_ERROR}")
	String FAILURE_ADD_PG_ID_ERROR;

	@Value("${SUCCESS_ADD_PG}")
	String SUCCESS_ADD_PG;

	@Value("${FAILURE_ADD_PG}")
	String FAILURE_ADD_PG;

	@Value("${PG_ALREADY_EXISTS}")
	String PG_ALREADY_EXISTS;

	@Value("${FAILURE_EDIT_PG_NOT_EXISTS}")
	String FAILURE_EDIT_PG_NOT_EXISTS;

	@Value("${SUCCESS_EDIT_PG}")
	String SUCCESS_EDIT_PG;

	@Value("${FAILURE_EDIT_PG}")
	String FAILURE_EDIT_PG;

	@Value("${FAILURE_DELETE_PG_NOT_EXISTS}")
	String FAILURE_DELETE_PG_NOT_EXISTS;

	@Value("${SUCCESS_DELETE_PG}")
	String SUCCESS_DELETE_PG;

	@Value("${FAILURE_DELETE_PG}")
	String FAILURE_DELETE_PG;

	@Value("${SUCCESS_RETRIEVE_PG_LIST}")
	String SUCCESS_RETRIEVE_PG_LIST;

	@Value("${FAILURE_RETRIEVE_PG_LIST}")
	String FAILURE_RETRIEVE_PG_LIST;

	@Value("${FAILURE_RETRIEVE_PG_NOT_EXISTS}")
	String FAILURE_RETRIEVE_PG_NOT_EXISTS;

	@Value("${SUCCESS_RETRIEVE_PG}")
	String SUCCESS_RETRIEVE_PG;

	@Value("${FAILURE_RETRIEVE_PG}")
	String FAILURE_RETRIEVE_PG;

	@Value("${FAILURE_EDIT_PG_EXISTS}")
	String FAILURE_EDIT_PG_EXISTS;

	@Override
	public Map<String, Object> save(PermissionGroup pg, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			/**
			 * Permission Group Name can't be duplicated
			 */
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("permissionGroupName", pg.getPermissionGroupName());
			criteriaMap.put("isDeleted", Boolean.FALSE);
			if (null == masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap)) {
				String pgId = masterDao.getNextSequenceId("permissionGroupIdSeq");
				logger.info("Permission Group Id generated is [PG" + pgId + "]");

				if (pgId.equals("-1")) {
					status.setCode("FAILURE_ADD_PG_ID_ERROR");
					status.setMessage(FAILURE_ADD_PG_ID_ERROR);
					status.setStatus(false);
				} else {
					PermissionGroupEntity entityObj = new PermissionGroupEntity();
					Timestamp currentTs = new Timestamp(new Date().getTime());

					pg.setPermissionGroupId("PG".concat(pgId));
					if (pg.getPermissionGroupName() == null || pg.getPermissionGroupName() == "")
						pg.setPermissionGroupName(pg.getPermissionGroupId());
					
					BeanUtils.copyProperties(pg, entityObj);
					entityObj.setIsActive(Boolean.TRUE);
					entityObj.setIsDeleted(Boolean.FALSE);
					entityObj.setCreatedOn(currentTs);
					entityObj.setCreatedBy(loggedUser);
					entityObj.setModifiedOn(currentTs);
					entityObj.setModifiedBy(loggedUser);
					
					result = masterDao.saveEntityObject(entityObj);

					if (result) {
						status.setCode("SUCCESS_ADD_PG");
						status.setMessage(SUCCESS_ADD_PG);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_ADD_PG");
						status.setMessage(FAILURE_ADD_PG);
						status.setStatus(false);
					}
				}
			} else {
				status.setCode("PG_ALREADY_EXISTS");
				status.setMessage(PG_ALREADY_EXISTS);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_PG");
			status.setMessage(FAILURE_ADD_PG);
			status.setStatus(false);
		} finally {
			finalData = retrieve(pg.getPermissionGroupId());
			finalData.put("status", status);
		}
		return finalData;

	}

	@Override
	public Map<String, Object> edit(PermissionGroup pg, String pgId, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {

			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("permissionGroupId", pgId);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			PermissionGroupEntity entityObj = masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap);
			if (null == entityObj) {
				status.setCode("FAILURE_EDIT_PG_NOT_EXISTS");
				status.setMessage(FAILURE_EDIT_PG_NOT_EXISTS);
				status.setStatus(false);
			} else {

				PermissionGroupEntity entityObjExists = null;
				if (!entityObj.getPermissionGroupName().equals(pg.getPermissionGroupName())) {
					criteriaMap.clear();
					criteriaMap.put("permissionGroupName", pg.getPermissionGroupName());
					criteriaMap.put("isDeleted", Boolean.FALSE);

					entityObjExists = masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap);
				}

				if (null == entityObjExists) {
					entityObj.setModifiedBy(loggedUser);
					entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

					result = masterDao.updateEntityObject(setEntityFieldsForModify(pg, entityObj));

					if (result) {
						status.setCode("SUCCESS_EDIT_PG");
						status.setMessage(SUCCESS_EDIT_PG);
						status.setStatus(true);
					} else {
						status.setCode("FAILURE_EDIT_PG");
						status.setMessage(FAILURE_EDIT_PG);
						status.setStatus(false);
					}
				} else {
					status.setCode("FAILURE_EDIT_PG_EXISTS");
					status.setMessage(FAILURE_EDIT_PG_EXISTS);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in edit :: ", e);
			status.setCode("FAILURE_EDIT_PG");
			status.setMessage(FAILURE_EDIT_PG);
			status.setStatus(false);
		} finally {
			finalData = retrieve(pgId);
			finalData.put("status", status);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> delete(String pgId, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("permissionGroupId", pgId);
			criteriaMap.put("isDeleted", Boolean.FALSE);

			PermissionGroupEntity entityObj = masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap);

			if (null == entityObj) {
				status.setCode("FAILURE_DELETE_PG_NOT_EXISTS");
				status.setMessage(FAILURE_DELETE_PG_NOT_EXISTS);
				status.setStatus(false);
			} else {
				entityObj.setIsDeleted(Boolean.TRUE);
				entityObj.setIsActive(Boolean.FALSE);
				entityObj.setModifiedBy(loggedUser);
				entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

				result = masterDao.updateEntityObject(entityObj);

				if (result) {
					status.setCode("SUCCESS_DELETE_PG");
					status.setMessage(SUCCESS_DELETE_PG);
					status.setStatus(true);
				} else {
					status.setCode("FAILURE_DELETE_PG");
					status.setMessage(FAILURE_DELETE_PG);
					status.setStatus(false);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in delete :: ", e);
			status.setCode("FAILURE_DELETE_PG");
			status.setMessage(FAILURE_DELETE_PG);
			status.setStatus(false);
		} finally {
			finalData = retrieve(pgId);
			finalData.put("status", status);
		}
		return finalData;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAll() {
		List<PermissionGroupEntity> permissionGroups = new LinkedList<>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("isActive", Boolean.TRUE);
			permissionGroups = (List<PermissionGroupEntity>) masterDao.getAllEntityObjects(new PermissionGroupEntity(), criteriaMap);

			status.setCode("SUCCESS_RETRIEVE_PG_LIST");
			status.setMessage(SUCCESS_RETRIEVE_PG_LIST);
			status.setStatus(true);
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_PG_LIST");
			status.setMessage(FAILURE_RETRIEVE_PG_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", permissionGroups);
		}
		return finalData;
	}

	@Override
	public Map<String, Object> retrieve(String pgId) {
		PermissionGroup pg = null;
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("permissionGroupId", pgId);
			criteriaMap.put("isDeleted", Boolean.FALSE);
			criteriaMap.put("isActive", Boolean.TRUE);

			PermissionGroupEntity entityObj = masterDao.getEntityObject(new PermissionGroupEntity(), criteriaMap);

			if (null == entityObj) {
				status.setCode("FAILURE_RETRIEVE_PG_NOT_EXISTS");
				status.setMessage(FAILURE_RETRIEVE_PG_NOT_EXISTS);
				status.setStatus(false);
			} else {
				pg = new PermissionGroup();
				BeanUtils.copyProperties(entityObj, pg);
				status.setCode("SUCCESS_RETRIEVE_PG");
				status.setMessage(SUCCESS_RETRIEVE_PG);
				status.setStatus(true);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieve :: ", e);
			status.setCode("FAILURE_RETRIEVE_PG");
			status.setMessage(FAILURE_RETRIEVE_PG);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", pg);
		}
		return finalData;
	}

	private PermissionGroupEntity setEntityFieldsForModify(PermissionGroup modelObj, PermissionGroupEntity entityObj) {

		entityObj.setPermissionGroupName(modelObj.getPermissionGroupName());
		entityObj.setRoleId(modelObj.getRoleId());
		entityObj.setAccount(modelObj.getAccount());
		entityObj.setProductHierarchyId(modelObj.getProductHierarchyId());
		entityObj.setImprint(modelObj.getImprint());
		entityObj.setProductCategory(modelObj.getProductCategory());
		entityObj.setMetadataGroup(modelObj.getMetadataGroup());
		entityObj.setPartnerType(modelObj.getPartnerType());
		entityObj.setApplication(modelObj.getApplication());
		entityObj.setAsset(modelObj.getAsset());
		entityObj.setReport(modelObj.getReport());
		entityObj.setAdministrator(modelObj.getAdministrator());

		return entityObj;
	}

	@Override
	public Map<String, Object> getAppPermission(String configId, String loggedUser) {
		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		Map<String, Object> criteriaMap = new HashMap<>();
		Criteria criteria = new Criteria();
		List<Criteria> permissionCriteriaList = new LinkedList<>();
		try {
			ManageConfigEntity configEntity = masterDao.getEntityObject(new ManageConfigEntity(), "id", configId);
			if (null != configEntity) {
				LinkedHashMap<String, Object> config = configEntity.getConfig();
				List<String> fields = (List<String>) config.get("fields");
				if (CollectionUtils.isNotEmpty(fields)) {
					UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

					Set<String> mappedValueList = new HashSet<>();
					if (fields.stream().anyMatch("accounts"::equalsIgnoreCase)) {
						boolean acntPermissionFlg = masterDao.checkPermission("MOD00006", "ACCOUNT", "VIEW", userEntityObj);
						if (acntPermissionFlg && null != userEntityObj.getPermissionGroup() && CollectionUtils.isNotEmpty(userEntityObj.getPermissionGroup().getAccount())) {
							criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
							criteriaMap.put("isActive", Boolean.TRUE);
							criteriaMap.put("isDeleted", Boolean.FALSE);
							criteriaMap.put("metadataMappedField.isActive", Boolean.TRUE);
							criteriaMap.put("metadataMappedField.isDeleted", Boolean.FALSE);

							List<KeyValue> mappedValues = masterDao.getKeyValueList("mAccount", "metadataMappedField.value", null, criteriaMap, "metadataMappedField");
							mappedValueList = CollectionUtils.emptyIfNull(mappedValues).stream().map(kv -> kv.getKey())
									.collect(Collectors.toSet());
							List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig","config.metadataMappingFieldName", null, "_id", "account", null);
							String metadataMappedFieldName = null;
							if (!isNull(mappingValues)) {
								metadataMappedFieldName = mappingValues.get(0).getKey();
							}
							permissionCriteriaList.add(Criteria.where(metadataMappedFieldName).in(mappedValueList));
						}
					}
					if (fields.stream().anyMatch("imprints"::equalsIgnoreCase)) {
						if(null != userEntityObj.getPermissionGroup() && CollectionUtils.isNotEmpty(userEntityObj.getPermissionGroup().getImprint()))
							permissionCriteriaList.add(Criteria.where("Product.Imprint.ImprintName").in(userEntityObj.getPermissionGroup().getImprint()));
					}

					if (fields.stream().anyMatch("productCategory"::equalsIgnoreCase)) {
						List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig","config.metadataMappingField", null, "_id", "productCategory", null);

						if (!isNull(mappingValues)) {
							
							String metaDataFieldName = mappingValues.get(0).getKey();
							MetadataFieldEntity metadataFieldentity = masterDao.getMetadataFieldEntity(metaDataFieldName);

							if (!isNull(metadataFieldentity)) {

								boolean pcPermissionFlg = masterDao.checkPermission("MOD00006", "PRODUCT_CATEGORY", "VIEW", userEntityObj);
								List<String> prodCategoriesCodeList = new ArrayList<>();

								if (pcPermissionFlg) {
									criteriaMap.clear();
									criteriaMap.put("accountId", userEntityObj.getPermissionGroup().getAccount());
									criteriaMap.put("metadataTypeId", userEntityObj.getPermissionGroup().getMetadataType());
									criteriaMap.put("productCategoryId", userEntityObj.getPermissionGroup().getProductCategory());
									criteriaMap.put("isDeleted", Boolean.FALSE);

									List<ProductCategoryEntity> prodCategories = (List<ProductCategoryEntity>) masterDao
											.getAllEntityObjects(new ProductCategoryEntity(), criteriaMap);
									prodCategoriesCodeList = CollectionUtils.emptyIfNull(prodCategories).stream()
											.map(obj -> obj.getProductCategoryCode()).collect(Collectors.toList());

									prodCategoriesCodeList.add(null);
								}

								String referencePathValue = Boolean.FALSE.equals(metadataFieldentity.getUnwind())
										? metadataFieldentity.getExactReferencePath()
										: metadataFieldentity.getReferencePath();
								if (!isNull(metadataFieldentity.getJsonDbPathCondition())) {
									List<Criteria> subDocCriteria = getSubDocCriteria(metadataFieldentity.getJsonDbPathCondition());

									if (metadataFieldentity.getFieldType().equalsIgnoreCase("boolean")) {
										subDocCriteria.add(Criteria.where(metadataFieldentity.getJsonDbPathElement())
												.in(prodCategoriesCodeList.stream().map(Boolean::parseBoolean)
														.collect(Collectors.toList())));
									} else if (metadataFieldentity.getFieldType().equalsIgnoreCase("number")) {
										subDocCriteria.add(Criteria.where(metadataFieldentity.getJsonDbPathElement())
												.in(prodCategoriesCodeList.stream().map(Double::parseDouble)
														.collect(Collectors.toList())));
									} else
										subDocCriteria.add(Criteria.where(metadataFieldentity.getJsonDbPathElement())
												.in(prodCategoriesCodeList));

									permissionCriteriaList.add(Criteria.where(metadataFieldentity.getJsonDbPathList())
											.elemMatch(new Criteria().andOperator(
													subDocCriteria.toArray(new Criteria[subDocCriteria.size()]))));

								} else {
									if (metadataFieldentity.getFieldType().equalsIgnoreCase("boolean")) {
										permissionCriteriaList.add(
												Criteria.where(referencePathValue).in(prodCategoriesCodeList.stream()
														.map(Boolean::parseBoolean).collect(Collectors.toList())));
									} else if (metadataFieldentity.getFieldType().equalsIgnoreCase("number")) {
										permissionCriteriaList.add(
												Criteria.where(referencePathValue).in(prodCategoriesCodeList.stream()
														.map(Double::parseDouble).collect(Collectors.toList())));
									} else
										permissionCriteriaList
												.add(Criteria.where(referencePathValue).in(prodCategoriesCodeList));
								}

							}
						}
					}

				}
				if (!permissionCriteriaList.isEmpty()) {
					criteria = new Criteria()
							.andOperator(permissionCriteriaList.toArray(new Criteria[permissionCriteriaList.size()]));
				}
			}

			status.setCode("Success");
			status.setMessage("Success");
			status.setStatus(true);
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			status.setCode("Error");
			status.setMessage("Error");
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", criteria);
		}

		return finalData;
	}
	
	public <T> boolean isNull(T obj) {
		if (obj instanceof Collection<?>)
			return (obj == null || ((Collection<?>) obj).isEmpty());
		else if (obj instanceof String)
			return (obj == null || ((String) obj).trim().isEmpty());
		else
			return obj == null;
	}

	private List<Criteria> getSubDocCriteria(List<JsonDbPathConditionEntity> subDocs) {
		List<Criteria> subCriterias = new ArrayList<>();
		for (JsonDbPathConditionEntity subDoc : subDocs) {
			switch (subDoc.getCondition().toLowerCase()) {
			case "=":
			case "==":
				subCriterias.add(Criteria.where(subDoc.getField()).is(subDoc.getValue()));
				break;
			case "!=":
				subCriterias.add(Criteria.where(subDoc.getField()).ne(subDoc.getValue()));
				break;
			case "in":
				subCriterias.add(Criteria.where(subDoc.getField()).in((List<String>) subDoc.getValue()));
				break;
			case "notin":
				subCriterias.add(Criteria.where(subDoc.getField()).nin((List<String>) subDoc.getValue()));
				break;
			}
		}
		return subCriterias;
	}
	

}

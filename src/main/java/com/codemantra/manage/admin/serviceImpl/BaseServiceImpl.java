/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.entity.ApplicationEntity;
import com.codemantra.manage.admin.entity.CMetadataGroupEntity;
import com.codemantra.manage.admin.entity.MetadataTypeEntity;
import com.codemantra.manage.admin.entity.PrimaryWidgetEntity;
import com.codemantra.manage.admin.entity.ReportEntity;
import com.codemantra.manage.admin.entity.StatusEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.entity.ViewerFormatsEntity;
import com.codemantra.manage.admin.entity.WatermarkConfigEntity;
import com.codemantra.manage.admin.model.DistributionStatusUpdate;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.StatusModel;
import com.codemantra.manage.admin.service.BaseService;

@Service("baseService")
public class BaseServiceImpl implements BaseService {

	private static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${SUCCESS_RETRIEVE_LIST}")
	String SUCCESS_RETRIEVE_LIST;

	@Value("${FAILURE_RETRIEVE_LIST}")
	String FAILURE_RETRIEVE_LIST;

	@Value("${LOGGED_USER_NOT_EXISTS}")
	String LOGGED_USER_NOT_EXISTS;
	
	@Value("${SUCCESS_RETRIEVE_ELIGIBLE_FORMAT_LIST}")
	String SUCCESS_RETRIEVE_ELIGIBLE_FORMAT_LIST;
	
	@Value("${FAILURE_RETRIEVE_ELIGIBLE_FORMAT_LIST}")
	String FAILURE_RETRIEVE_ELIGIBLE_FORMAT_LIST;
	
	@Value("${SUCCESS_UPDATE_DISTRIBUTION_STATUS}")
	String SUCCESS_UPDATE_DISTRIBUTION_STATUS;
	
	@Value("${FAILURE_UPDATE_DISTRIBUTION_STATUS}")
	String FAILURE_UPDATE_DISTRIBUTION_STATUS;

	@SuppressWarnings("unchecked")
	@Override
	public <T> Map<String, Object> retrieveAll(T entityObj, String loggedUser) {
		List<T> list = new LinkedList<T>();

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {

			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			if (null != userEntityObj) {
				Map<String, Object> criteriaMap = new HashMap<>();

				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("isActive", Boolean.TRUE);

				if (entityObj.getClass().isInstance(new MetadataTypeEntity()))
					criteriaMap.put("metadataTypeId", userEntityObj.getPermissionGroup().getMetadataType());
				else if (entityObj.getClass().isInstance(new ApplicationEntity()))
					criteriaMap.put("applicationId", userEntityObj.getPermissionGroup().getApplication());
				
				else if (entityObj.getClass().isInstance(new ReportEntity())) {
					List<String> roleReportList = masterDao.getModuleAccessList("MOD00004", "REPORT_LIST", userEntityObj);					
					roleReportList.retainAll(userEntityObj.getPermissionGroup().getReport());					
					criteriaMap.put("reportId", roleReportList);
				}
				
				else if (entityObj.getClass().isInstance(new PrimaryWidgetEntity())) {
					List<String> roleWidgetList = masterDao.getModuleAccessList("MOD00001", "WIDGET_LIST", userEntityObj);	
					criteriaMap.put("widgetId", roleWidgetList);
				}
				else if (entityObj.getClass().isInstance(new CMetadataGroupEntity()))
					criteriaMap.put("groupId", userEntityObj.getPermissionGroup().getMetadataGroup());

				list = (List<T>) masterDao.getAllEntityObjects(entityObj, criteriaMap);

				status.setCode("SUCCESS_RETRIEVE_LIST");
				status.setMessage(SUCCESS_RETRIEVE_LIST);
				status.setStatus(true);
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_LIST");
			status.setMessage(FAILURE_RETRIEVE_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", list);
		}
		return finalData;
	}

	@Override
	public <T> Map<String, Object> retrieveImprints(List<String> accounts, String loggedUser) {

		Status status = new Status();
		List<String> imprintList = null;
		Map<String, Object> finalData = new HashMap<>();
		Map<String, Object> criteriaMap = new HashMap<>();

		String metadataMappedFieldName = null;

		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			if (null != userEntityObj) {
				List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig",
						"config.metadataMappingFieldName", "config.metadataMappingFieldLabel", "_id", "account", null);

				if (mappingValues.size() > 0) {
					metadataMappedFieldName = mappingValues.get(0).getKey();
				}

				if (masterDao.isNull(accounts))
					accounts = userEntityObj.getPermissionGroup().getAccount();
				else {
					accounts.retainAll(userEntityObj.getPermissionGroup().getAccount());
				}

				criteriaMap.put("accountId", accounts);
				criteriaMap.put("isActive", Boolean.TRUE);
				criteriaMap.put("isDeleted", Boolean.FALSE);
				criteriaMap.put("metadataMappedField.isActive", Boolean.TRUE);
				criteriaMap.put("metadataMappedField.isDeleted", Boolean.FALSE);
				List<KeyValue> mappedValues = masterDao.getKeyValueList("mAccount", null, "metadataMappedField.value",
						criteriaMap, "metadataMappedField");
				Set<Object> metadataMappedList = CollectionUtils.emptyIfNull(mappedValues).stream()
						.map(kv -> kv.getValue()).collect(Collectors.toSet());

				criteriaMap.clear();
				criteriaMap.put("Product.isDeleted", Boolean.FALSE);
				criteriaMap.put(metadataMappedFieldName, metadataMappedList);

				imprintList = masterDao.getDistinctFieldValues("products", criteriaMap, "Product.Imprint.ImprintName", null);

				imprintList.retainAll(userEntityObj.getPermissionGroup().getImprint());
				
				status.setCode("SUCCESS_RETRIEVE_LIST");
				status.setMessage(SUCCESS_RETRIEVE_LIST);
				status.setStatus(true);

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_LIST");
			status.setMessage(FAILURE_RETRIEVE_LIST);
		} finally {
			finalData.put("status", status);
			finalData.put("data", imprintList);
		}

		return finalData;
	}
	
	@Override
	public <T> Map<String, Object> retrieveImprintsFromPG(List<String> accounts, String loggedUser) {

		Status status = new Status();
		List<String> imprintList = null;
		Map<String, Object> finalData = new HashMap<>();

		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			if (null != userEntityObj) {
				imprintList = userEntityObj.getPermissionGroup().getImprint();
				
				status.setCode("SUCCESS_RETRIEVE_LIST");
				status.setMessage(SUCCESS_RETRIEVE_LIST);
				status.setStatus(true);

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_LIST");
			status.setMessage(FAILURE_RETRIEVE_LIST);
		} finally {
			finalData.put("status", status);
			finalData.put("data", imprintList);
		}

		return finalData;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveViewerEligibleFormats(String loggedUser) {

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();

		List<ViewerFormatsEntity> viewerEligibleFormats = new ArrayList<>();

		try {
			
			List<KeyValue> mappingValues = masterDao.getKeyValueList("manageConfig",
					null, "config.eligibleFormats", "_id", "VIEWER", null);
			
			if (mappingValues.size() > 0) {
				viewerEligibleFormats = (List<ViewerFormatsEntity>) mappingValues.get(0).getValue();
			}else
				viewerEligibleFormats = null;
			
			status.setCode("SUCCESS_RETRIEVE_LIST");
			status.setMessage(SUCCESS_RETRIEVE_LIST);
			status.setStatus(true);
			

		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_LIST");
			status.setMessage(FAILURE_RETRIEVE_LIST);
		} finally {
			finalData.put("status", status);
			finalData.put("data", viewerEligibleFormats);
		}

		return finalData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getEligibleFormats(String loggedUser) {
		List<WatermarkConfigEntity> eligibleFormats = new LinkedList<WatermarkConfigEntity>();
		List<String> eligibleFormatNames = new LinkedList<>();
		

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {

			HashMap<String, Object> criteriaMap = new LinkedHashMap<>();
			criteriaMap.put("isActive", Boolean.TRUE);
		    
			eligibleFormats = (List<WatermarkConfigEntity>) masterDao.getAllEntityObjects(new WatermarkConfigEntity(), criteriaMap);
			
			if(eligibleFormats.size()>0) {	
			
				List<String> formatIds = CollectionUtils.emptyIfNull(eligibleFormats).stream().map(data -> data.getDisplayFormatId()).collect(Collectors.toList());
				
				criteriaMap.clear();
				criteriaMap.put("formatId", formatIds);
				
				eligibleFormatNames = (List<String>) masterDao.getDistinctFieldValues("mFormat", criteriaMap,
						"formatName");
			}
			status.setCode("SUCCESS_RETRIEVE_ELIGIBLE_FORMAT_LIST");
			status.setMessage(SUCCESS_RETRIEVE_ELIGIBLE_FORMAT_LIST);
			status.setStatus(true);	
		} catch (Exception e) {
			logger.error("Exception in retrieveAll :: ", e);
			status.setCode("FAILURE_RETRIEVE_ELIGIBLE_FORMAT_LIST");
			status.setMessage(FAILURE_RETRIEVE_ELIGIBLE_FORMAT_LIST);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("eligibleFormats", eligibleFormats);
			finalData.put("eligibleFormatNames", eligibleFormatNames);
		}
		return finalData;
	}
	
	
	
	@Override
	public Map<String, Object> updateDistributionStatus(DistributionStatusUpdate modelObj, String loggedUser) {
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			Map<String, Object> criteriaMap = new HashMap<>();
			criteriaMap.put("partnerId", modelObj.getPartnerId());
			criteriaMap.put("isbn", modelObj.getIsbn());
			if(!(CollectionUtils.emptyIfNull(modelObj.getFormatId()).isEmpty()))
				criteriaMap.put("format.formatId", modelObj.getFormatId());
			criteriaMap.put("transactionType", modelObj.getTransactionType());
			
			
			Map<String, Object> updateMap = new HashMap<>();
			updateMap.put("transactionStatus", modelObj.getStatus());
			updateMap.put("remarks", modelObj.getRemarks());			
			updateMap.put("modifiedOn", new Timestamp(new Date().getTime()));
			updateMap.put("modifiedBy", loggedUser);

			masterDao.updateCollectionDocument("transactionEntity", criteriaMap, updateMap);

			status.setCode("SUCCESS_UPDATE_DISTRIBUTION_STATUS");
			status.setMessage(SUCCESS_UPDATE_DISTRIBUTION_STATUS);
			status.setStatus(true);

		} catch (Exception e) {
			logger.error("Exception in Update Distribution Status :: ", e);
			status.setCode("FAILURE_UPDATE_DISTRIBUTION_STATUS");
			status.setMessage(FAILURE_UPDATE_DISTRIBUTION_STATUS);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		
		return finalData;
	}

	@Override
	public APIResponse<Object> getStatusbyType(String type) {
		APIResponse<Object> response = new APIResponse<>();
		List<StatusModel> statusList = new ArrayList<>();
		try {
			List<StatusEntity> status = masterDao.getStatusbyType(type);
			if (null != status && !status.isEmpty()) {
				for (StatusEntity statusEntity : status) {
					StatusModel statusModel = new StatusModel();
					BeanUtils.copyProperties(statusModel, statusEntity);
					statusList.add(statusModel);
				}
				response.setData(statusList);
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus("Success");
				response.setStatusMessage("Status retrieved successfully");
			} else {
				response.setCode(ResponseCode.ERROR.toString());
				response.setStatus("Error");
				response.setStatusMessage("No Data");
			}
		} catch (Exception e) {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

}

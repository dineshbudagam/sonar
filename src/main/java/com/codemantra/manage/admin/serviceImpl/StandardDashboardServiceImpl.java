/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.serviceImpl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codemantra.manage.admin.dao.MasterDao;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.StandardDashboardDetailsEntity;
import com.codemantra.manage.admin.entity.StandardDashboardSummaryEntity;
import com.codemantra.manage.admin.entity.UserEntity;
import com.codemantra.manage.admin.entity.UserWithPermissionGroupEntity;
import com.codemantra.manage.admin.model.CodeDescription;
import com.codemantra.manage.admin.model.KeyValue;
import com.codemantra.manage.admin.model.StandardDashboardDetails;
import com.codemantra.manage.admin.model.StandardDashboardSummary;
import com.codemantra.manage.admin.service.StandardDashboardService;

@Service("standardDashboardService")
public class StandardDashboardServiceImpl implements StandardDashboardService {

	private static final Logger logger = LoggerFactory.getLogger(StandardDashboardServiceImpl.class);

	@Autowired
	MasterDao masterDao;

	@Value("${REQUEST_FORBIDDEN}")
	String REQUEST_FORBIDDEN;

	@Value("${INPUT_DATE_FORMAT}")
	String INPUT_DATE_FORMAT;

	@Value("${SUCCESS_ADD_STD_DB}")
	String SUCCESS_ADD_STD_DB;

	@Value("${FAILURE_ADD_STD_DB}")
	String FAILURE_ADD_STD_DB;
	
	@Value("${FAILURE_ADD_STD_DB_ID_ERROR}")
	String FAILURE_ADD_STD_DB_ID_ERROR;	

	@Value("${FAILURE_EDIT_STD_DB_ID_ERROR}")
	String FAILURE_EDIT_STD_DB_ID_ERROR;
	
	@Value("${SUCCESS_EDIT_STD_DB}")
	String SUCCESS_EDIT_STD_DB;
	
	@Value("${FAILURE_EDIT_STD_DB}")
	String FAILURE_EDIT_STD_DB;
	
	@Value("${FAILURE_ADD_STD_DB_DATE_ERROR}")
	String FAILURE_ADD_STD_DB_DATE_ERROR;
	
	@Value("${FAILURE_ADD_STD_DB_FREE_HRS_USED_ERROR}")
	String FAILURE_ADD_STD_DB_FREE_HRS_USED_ERROR;
	
	@Value("${FAILURE_EDIT_STD_DB_DATE_ERROR}")
	String FAILURE_EDIT_STD_DB_DATE_ERROR;
	
	@Value("${FAILURE_EDIT_STD_DB_FREE_HRS_USED_ERROR}")
	String FAILURE_EDIT_STD_DB_FREE_HRS_USED_ERROR;
	
	@Value("${FAILURE_RETRIEVE_STD_NO_DATA_FOUND}")
	String FAILURE_RETRIEVE_STD_NO_DATA_FOUND;
	
	@Value("${SUCCESS_RETRIEVE_STD_DATA}")
	String SUCCESS_RETRIEVE_STD_DATA;
	
	@Value("${FAILURE_RETRIEVE_STD_DATA}")
	String FAILURE_RETRIEVE_STD_DATA;
	
	@Value("${SUCCESS_RETRIEVE_STD_USER_SUGGESTIONS}")
	String SUCCESS_RETRIEVE_STD_USER_SUGGESTIONS;
	
	@Value("${FAILURE_RETRIEVE_STD_USER_SUGGESTIONS}")
	String FAILURE_RETRIEVE_STD_USER_SUGGESTIONS;
	
	SimpleDateFormat sdf = "dd/MM/yyyy".equalsIgnoreCase(INPUT_DATE_FORMAT)? new SimpleDateFormat("dd/MM/yyyy"): new SimpleDateFormat("yyyy/MM/dd");

	@Override
	public Map<String, Object> save(StandardDashboardDetails modelObj, String loggedUser) {

		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			 UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			if (null != userEntityObj) {
				
				StandardDashboardSummaryEntity sumEntityObj =  masterDao.checkStandardDashboardValidDate(new Timestamp(sdf.parse(modelObj.getReleasedOn()).getTime()));
				
				if(null == sumEntityObj) {
					status.setCode("FAILURE_ADD_STD_DB_DATE_ERROR");
					status.setMessage(FAILURE_ADD_STD_DB_DATE_ERROR);
					status.setStatus(false);
				}else {
					
					List<StandardDashboardDetailsEntity> objList = masterDao.getStandardDashboardDetails(sumEntityObj);
					int compareVal = -1;
					if(!masterDao.isNull(objList)){
						Double freeHrsUsed = objList.stream().mapToDouble(o -> o.getFreeHrsUsed()).sum();
						compareVal = Double.compare(Double.sum(freeHrsUsed, modelObj.getFreeHrsUsed()), sumEntityObj.getTotalFreeHrs());
					}
					
					if(compareVal == 1) {
						status.setCode("FAILURE_ADD_STD_DB_FREE_HRS_USED_ERROR");
						status.setMessage(FAILURE_ADD_STD_DB_FREE_HRS_USED_ERROR);
						status.setStatus(false);
					}else {
						String reqId = masterDao.getNextSequenceId("standardDashbaordSeq");
						logger.info("reqId generated is [STDB" + reqId + "]");

						if (reqId.equals("-1")) {
							status.setCode("FAILURE_ADD_STD_DB_ID_ERROR");
							status.setMessage(FAILURE_ADD_STD_DB_ID_ERROR);
							status.setStatus(false);
						} else {
							StandardDashboardDetailsEntity entityObj = new StandardDashboardDetailsEntity();
							Timestamp currentTs = new Timestamp(new Date().getTime());

							modelObj.setRequestId("STDB".concat(reqId));
							BeanUtils.copyProperties(modelObj, entityObj);					
							
							entityObj.setReleasedOn(new Timestamp(sdf.parse(modelObj.getReleasedOn()).getTime()));
							entityObj.setIsActive(Boolean.TRUE);
							entityObj.setIsDeleted(Boolean.FALSE);
							entityObj.setCreatedOn(currentTs);
							entityObj.setCreatedBy(loggedUser);
							entityObj.setModifiedOn(currentTs);
							entityObj.setModifiedBy(loggedUser);

							result = masterDao.saveEntityObject(entityObj);

							if (result) {
								status.setCode("SUCCESS_ADD_STD_DB");
								status.setMessage(SUCCESS_ADD_STD_DB);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_ADD_STD_DB");
								status.setMessage(FAILURE_ADD_STD_DB);
								status.setStatus(false);
							}
						}
					}						
				}				
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in save :: ", e);
			status.setCode("FAILURE_ADD_STD_DB");
			status.setMessage(FAILURE_ADD_STD_DB);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
		}
		return finalData;

	}
	
	
	@Override
	public Map<String, Object> edit(StandardDashboardDetails modelObj, String id, String loggedUser) {
		boolean result = false;
		Map<String, Object> finalData = new HashMap<>();
		Status status = new Status();
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);
			if (null != userEntityObj) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("requestId", id);
				criteriaMap.put("isDeleted", Boolean.FALSE);

				StandardDashboardDetailsEntity entityObj = masterDao.getEntityObject(new StandardDashboardDetailsEntity(), criteriaMap);
				if (null == entityObj) {
					status.setCode("FAILURE_EDIT_STD_DB_ID_ERROR");
					status.setMessage(FAILURE_EDIT_STD_DB_ID_ERROR);
					status.setStatus(false);
				} else {
					
					StandardDashboardSummaryEntity sumEntityObj =  masterDao.checkStandardDashboardValidDate(new Timestamp(sdf.parse(modelObj.getReleasedOn()).getTime()));
					
					if(null == sumEntityObj) {
						status.setCode("FAILURE_EDIT_STD_DB_DATE_ERROR");
						status.setMessage(FAILURE_EDIT_STD_DB_DATE_ERROR);
						status.setStatus(false);
					}else {
						List<StandardDashboardDetailsEntity> objList = masterDao.getStandardDashboardDetails(sumEntityObj);
						int compareVal = -1;
						if(!masterDao.isNull(objList)){
							Double freeHrsUsed = objList.stream().filter(o -> !o.getRequestId().equals(id)).mapToDouble(o -> o.getFreeHrsUsed()).sum();
							
							compareVal = Double.compare(Double.sum(freeHrsUsed, modelObj.getFreeHrsUsed()), sumEntityObj.getTotalFreeHrs());
						}
						
						if(compareVal == 1) {
							status.setCode("FAILURE_EDIT_STD_DB_FREE_HRS_USED_ERROR");
							status.setMessage(FAILURE_EDIT_STD_DB_FREE_HRS_USED_ERROR);
							status.setStatus(false);
						}else {
							entityObj.setProjectName(modelObj.getProjectName());
							entityObj.setProjectDescription(modelObj.getProjectDescription());
							entityObj.setFreeHrsUsed(modelObj.getFreeHrsUsed());
							entityObj.setPaidHrsUsed(modelObj.getPaidHrsUsed());
							entityObj.setRequestedBy(modelObj.getRequestedBy());
							entityObj.setReleasedOn(new Timestamp(sdf.parse(modelObj.getReleasedOn()).getTime()));							
							entityObj.setModifiedBy(loggedUser);
							entityObj.setModifiedOn(new Timestamp(new Date().getTime()));

							result = masterDao.updateEntityObject(entityObj);

							if (result) {						
								status.setCode("SUCCESS_EDIT_STD_DB");
								status.setMessage(SUCCESS_EDIT_STD_DB);
								status.setStatus(true);
							} else {
								status.setCode("FAILURE_EDIT_STD_DB");
								status.setMessage(FAILURE_EDIT_STD_DB);
								status.setStatus(false);
							}
						}
					}
				}

			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in editAccount :: ", e);
			status.setCode("FAILURE_EDIT_STD_DB");
			status.setMessage(FAILURE_EDIT_STD_DB);
			status.setStatus(false);
		} finally {
			logger.info(status.getCode());
			finalData.put("status", status);
		}
		return finalData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> retrieveAllDetailsForYear(Integer year, String loggedUser) {
		List<StandardDashboardSummaryEntity> summaryList = null;
		List<StandardDashboardDetailsEntity> details = null;
		StandardDashboardSummary summary = null;
		

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			if (null != userEntityObj) {
				Map<String, Object> criteriaMap = new HashMap<>();
				criteriaMap.put("isActive", Boolean.TRUE);
				criteriaMap.put("isDeleted", Boolean.FALSE);
				summaryList = (List<StandardDashboardSummaryEntity>) masterDao.getAllEntityObjects(new StandardDashboardSummaryEntity(), criteriaMap);
				
				
				StandardDashboardSummaryEntity sumEntityObj = null;
				if(masterDao.isNull(year))
					sumEntityObj =  masterDao.checkStandardDashboardValidDate(new Date());
				else {
					criteriaMap.clear();
					criteriaMap.put("year", year);
					criteriaMap.put("isDeleted", Boolean.FALSE);
					criteriaMap.put("isActive", Boolean.TRUE);
					
					sumEntityObj = masterDao.getEntityObject(new StandardDashboardSummaryEntity(), criteriaMap);
				}
				
				if(null == sumEntityObj) {
					status.setCode("FAILURE_RETRIEVE_STD_NO_DATA_FOUND");
					status.setMessage(FAILURE_RETRIEVE_STD_NO_DATA_FOUND);
					status.setStatus(true);
				}else {
					
					Double freeHrsUsed = 0.0;
					Double paidHrsUsed = 0.0;
					
					details = masterDao.getStandardDashboardDetails(sumEntityObj);
					
					
					if(!masterDao.isNull(details)){
						freeHrsUsed = details.stream().mapToDouble(o -> o.getFreeHrsUsed()).sum();
						paidHrsUsed = details.stream().mapToDouble(o -> o.getPaidHrsUsed()).sum();
						
						List<KeyValue> userList = masterDao.getKeyValueList("mUser", "userId", "firstName",
								null, null);
						
						Map<String, Object> userMap = CollectionUtils.emptyIfNull(userList).stream().collect(
				                Collectors.toMap(x -> x.getKey(), x -> x.getValue() == null ? "" : x.getValue()));
												
						for(StandardDashboardDetailsEntity data : details) {
							data.setRequestedByName((String) userMap.get(data.getRequestedBy()));
						}
					}
					
					Double availableFreeHrs = Double.sum(sumEntityObj.getTotalFreeHrs(), -freeHrsUsed);
					
					summary = new StandardDashboardSummary();
					BeanUtils.copyProperties(sumEntityObj, summary);
					
					summary.setFreeHrsUsed(freeHrsUsed);
					summary.setPaidHrsUsed(paidHrsUsed);
					summary.setAvailableFreeHrs(availableFreeHrs);
					
					status.setCode("SUCCESS_RETRIEVE_STD_DATA");
					status.setMessage(SUCCESS_RETRIEVE_STD_DATA);
					status.setStatus(true);
				}
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in retrieveAllDetailsForYear :: ", e);
			status.setCode("FAILURE_RETRIEVE_STD_DATA");
			status.setMessage(FAILURE_RETRIEVE_STD_DATA);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("summaryList", summaryList);
			finalData.put("summary", summary);
			finalData.put("details", details);
		}
		return finalData;
	}


	@Override
	public Map<String, Object> getUserSuggestions(String searchText, String loggedUser) {		

		Status status = new Status();
		Map<String, Object> finalData = new HashMap<>();
		List<UserEntity> users = null;
		List<CodeDescription> suggestionList = null;
		try {
			UserWithPermissionGroupEntity userEntityObj = masterDao.getUserEntity(loggedUser);

			if (null != userEntityObj) {				
				
				users = masterDao.getUserSuggestionsForStandardDashboard(searchText);
				
				suggestionList = CollectionUtils.emptyIfNull(users).stream().map(temp -> {
					CodeDescription obj = new CodeDescription(temp.getUserId(), temp.getFirstName() + " ("+temp.getEmailId()+")", false);
					return obj;
				}).collect(Collectors.toList());
					
				status.setCode("SUCCESS_RETRIEVE_STD_USER_SUGGESTIONS");
				status.setMessage(SUCCESS_RETRIEVE_STD_USER_SUGGESTIONS);
				status.setStatus(true);
			} else {
				status.setCode("REQUEST_FORBIDDEN");
				status.setMessage(REQUEST_FORBIDDEN);
				status.setStatus(false);
			}
		} catch (Exception e) {
			logger.error("Exception in getUserSuggestions :: ", e);
			status.setCode("FAILURE_RETRIEVE_STD_USER_SUGGESTIONS");
			status.setMessage(FAILURE_RETRIEVE_STD_USER_SUGGESTIONS);
			status.setStatus(false);
		} finally {
			finalData.put("status", status);
			finalData.put("data", suggestionList);
		}
		return finalData;
	
	}	

}

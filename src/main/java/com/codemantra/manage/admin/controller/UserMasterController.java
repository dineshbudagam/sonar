
/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.model.User;
import com.codemantra.manage.admin.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class UserMasterController {
	private static final Logger logger = LoggerFactory.getLogger(UserMasterController.class);

	@Autowired
	UserService service;
	
	@Autowired
	private HttpServletResponse request;
	
	@RequestMapping(value = "user", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> save(@RequestBody User user,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
    		user.setTenantCode(TenantContext.getTenant());
			finalData = service.save(user, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> edit(@RequestBody User user, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Permission Group Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.edit(user, id, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> delete(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: User Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.delete(id, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.DELETED.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAll(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.retrieveAll(loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setStatus(status.getCode());
				response.setCode(ResponseCode.SUCCESS.toString());
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatusMessage(status.getMessage());
			response.setData(finalData.get("data"));

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "user/{id}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<User> retrieve(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: ID [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<User> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.retrieve(id);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData((User) finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/{id}/activate/{activeStatus}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> activateDeactivateUser(@RequestBody User user, @PathVariable String id,
			@PathVariable Integer activeStatus,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: ID [" + id + "] - activeStatus [" + activeStatus + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.activateDeactivateUser(user, id, activeStatus, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/saveprofile/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<User> updateUserInfo(@RequestBody User user, @PathVariable String id) {
		logger.info("URI Parameters :: ID [" + id + "]");
		APIResponse<User> response = new APIResponse<User>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.updateUserInfo(user, id);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData((User) finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/{id}/updateprofilepic", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<User> updateProfilePic(@RequestBody User user, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		/** Send Profile Picture Name along with full directory in the Request Body **/
		logger.info("URI Parameters :: ID [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<User> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.updateProfilePic(user, id);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData((User) finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/{id}/resendmail", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> resend(@RequestBody User user, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: ID [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.resendInvitationMail(user, id, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/checkforduplicateuser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<User> checkForDuplicateUser(@RequestBody User user,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<User> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.userExists(user, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus())
				response.setCode(ResponseCode.SUCCESS.toString());
			else
				response.setCode(ResponseCode.NOT_FOUND.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/unlock/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> unlockUser(@RequestBody User user, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.unlockUser(id, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.NOT_FOUND.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "user/checkForDuplicatePgName", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<User> checkForDuplicatePgName(@RequestBody User user,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<User> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.pgExists(user, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus())
				response.setCode(ResponseCode.SUCCESS.toString());
			else
				response.setCode(ResponseCode.NOT_FOUND.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "getUserColumnFilterSuggestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> getUserColumnFilterSuggestions(@RequestBody User user,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.getSuggestions(user, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));
			}
			else
				response.setCode(ResponseCode.NOT_FOUND.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "filteredusers", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> retrieveAllWithFilters(@RequestBody User user,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			System.out.println(user.getUserNameFilterList());

			finalData = service.retrieveAllWithFilters(user, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setCount(status.getCount());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

	@RequestMapping(value = "sendInvitationToAll/{id}", method = RequestMethod.POST)
	@ResponseBody
	public APIResponse<Object> sendInvitationToAllActiveUsers(@PathVariable String id) {
		logger.info("URI Parameters :: Sender Id [" + id + "]");
		APIResponse<Object> response = new APIResponse<>();
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			boolean result = service.sendInvitaiontoAllActiveUsers(id);
			if (result)
				response.setCode(ResponseCode.SUCCESS.toString());
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus("Success.");
			response.setStatusMessage("Invitation send successfully to all active users.");

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	@RequestMapping(value = "users/{skip}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAll(@PathVariable Integer skip,@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    
			finalData = service.retrieveAllSkip(loggedUser, skip);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));
				response.setCount(status.getCount());
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

}

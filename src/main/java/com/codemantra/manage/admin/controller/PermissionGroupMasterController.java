
/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.model.PermissionGroup;
import com.codemantra.manage.admin.service.PermissionGroupService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class PermissionGroupMasterController {
	private static final Logger logger = LoggerFactory.getLogger(PermissionGroupMasterController.class);

	@Autowired
	PermissionGroupService service;
	
	@Autowired
	private HttpServletResponse request;
	
	@RequestMapping(value = "permissiongroup", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> save(@RequestBody PermissionGroup pg,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.save(pg, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "permissiongroup/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> edit(@RequestBody PermissionGroup pg, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Permission Group Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.edit(pg, id, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "permissiongroup/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> delete(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Permission Group Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.delete(id, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.DELETED.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	

	
	@RequestMapping(value = "permissiongroup", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAll(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAll();
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));
			}
			else
				response.setCode(ResponseCode.ERROR.toString());
			
			response.setStatusMessage(status.getMessage());		
			response.setStatus(status.getCode());	
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
    @RequestMapping(value = "permissiongroup/{id}", method = RequestMethod.GET)
    @ResponseBody
    public APIResponse<PermissionGroup> retrieve(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<PermissionGroup> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    		
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrieve(id);  
    			status = (Status) finalData.get("status");
    			if(status.isStatus()) {
    				response.setCode(ResponseCode.SUCCESS.toString());
    				response.setData((PermissionGroup) finalData.get("data"));
    			}
    			else
    				response.setCode(ResponseCode.ERROR.toString());
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
    
    @GetMapping(value= "getApplicationPermission")
	public APIResponse<Object> getAppPermission(@RequestParam(defaultValue = "applicationPermissions") String configId,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.getAppPermission(configId, loggedUser);
			status = (Status) finalData.get("status");
			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData.get("data"));				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
   

}



/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.AdministratorAccessEntity;
import com.codemantra.manage.admin.entity.ApplicationEntity;
import com.codemantra.manage.admin.entity.CMetadataGroupEntity;
import com.codemantra.manage.admin.entity.FormatEntity;
import com.codemantra.manage.admin.entity.MenuEntity;
import com.codemantra.manage.admin.entity.MetadataFieldEntity;
import com.codemantra.manage.admin.entity.MetadataGroupEntity;
import com.codemantra.manage.admin.entity.MetadataTypeEntity;
import com.codemantra.manage.admin.entity.ModuleEntity;
import com.codemantra.manage.admin.entity.PrimaryWidgetEntity;
import com.codemantra.manage.admin.entity.ReportEntity;
import com.codemantra.manage.admin.entity.RoleEntity;
import com.codemantra.manage.admin.model.DistributionStatusUpdate;
import com.codemantra.manage.admin.service.BaseService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class BaseController {
	private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

	@Autowired	
	BaseService service;
	
	@Autowired
	private HttpServletResponse request;
	
	
//	@RequestMapping(value= "/**", method=RequestMethod.OPTIONS)
//	public void corsHeaders(HttpServletResponse response) {
//	    response.addHeader("Access-Control-Allow-Origin", "*");
//	    response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//	    response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
//	    response.addHeader("Access-Control-Max-Age", "3600");
//	    
//	    response.setStatus(HttpServletResponse.SC_OK);
//	    
//	    
//	}
	
	
	@RequestMapping(value = "imprint", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveImprints(@RequestParam(required = false, value = "accounts") List<String> accounts, @RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "] - accounts ["+ accounts +"]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveImprints(accounts, loggedUser);
			
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	@RequestMapping(value = "imprintpg", method = RequestMethod.GET)
	@ResponseBody

	public APIResponse<Object> retrieveImprintsFromPG(@RequestParam(required = false, value = "accounts") List<String> accounts, @RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "] - accounts ["+ accounts +"]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveImprintsFromPG(accounts, loggedUser);
			
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	@RequestMapping(value = "viewerformats", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveViewerFormats(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser +"]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveViewerEligibleFormats(loggedUser);
			
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	@RequestMapping(value = "watermark/eligibleFormats", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getEligibleFormats(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    		
			finalData = service.getEligibleFormats(loggedUser);
			status = (Status) finalData.get("status");
			finalData.remove("status");
			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData);					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		
			

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	@RequestMapping(value = "distributionstatusupdate", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> editPartnerType(@RequestBody DistributionStatusUpdate modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.updateDistributionStatus(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus())
					response.setCode(ResponseCode.SUCCESS.toString());
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "{type}", method = RequestMethod.GET)
	@ResponseBody

	public APIResponse<Object> retrieveAll(@PathVariable String type, @RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);

			if ("metadatatype".equals(type))
				finalData = service.retrieveAll(new MetadataTypeEntity(), loggedUser);
			else if("application".equals(type))
				finalData = service.retrieveAll(new ApplicationEntity(), loggedUser);
			else if("module".equals(type))
				finalData = service.retrieveAll(new ModuleEntity(), loggedUser);
			else if("access".equals(type))
				finalData = service.retrieveAll(new AdministratorAccessEntity(), loggedUser);
			else if("format".equals(type))
				finalData = service.retrieveAll(new FormatEntity(), loggedUser);
			else if("report".equals(type))
				finalData = service.retrieveAll(new ReportEntity(), loggedUser);
			else if("metadatagroup".equals(type))
				finalData = service.retrieveAll(new MetadataGroupEntity(), loggedUser);
			else if("metadatafield".equals(type))
				finalData = service.retrieveAll(new MetadataFieldEntity(), loggedUser);
			else if("defaultwidget".equals(type))
				finalData = service.retrieveAll(new PrimaryWidgetEntity(), loggedUser);
			else if("cmetadatagroup".equals(type))
				finalData = service.retrieveAll(new CMetadataGroupEntity(), loggedUser);
			else if("role".equals(type))
				finalData = service.retrieveAll(new RoleEntity(), loggedUser);
			else if ("menu".equals(type))
				finalData = service.retrieveAll(new MenuEntity(), loggedUser);

			else 
				return new APIResponse(ResponseCode.BAD_REQUEST.toString(), "BAD_REQUEST", "Bad Request", null);
			
			
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setStatus(status.getCode());					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatusMessage(status.getMessage());		
			response.setData(finalData.get("data"));

		} catch (Exception e) {
			logger.error("Exception :: retrieveAll()", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}
	
	@GetMapping("/status/{type}")
	@ResponseBody
	public APIResponse<Object> getStatusbyType(@PathVariable String type) {
		APIResponse<Object> response = new APIResponse<>();
		try {
			String tenantCode = request.getHeader("X-Tenant");
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
			
    		response = service.getStatusbyType(type);
			
		} catch (Exception e) {
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}


}


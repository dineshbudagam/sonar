
/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.entity.CPartnerConfigEntity;
import com.codemantra.manage.admin.model.FieldsToMap;
import com.codemantra.manage.admin.model.ReadExcelData;
import com.codemantra.manage.admin.service.GdsService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class GdsMasterController {
	private static final Logger logger = LoggerFactory.getLogger(GdsMasterController.class);

	@Autowired
	GdsService service;
	
	@Autowired
	private HttpServletResponse request;

	@RequestMapping(value = "partnerconfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> savePartnerConfig(@RequestBody CPartnerConfigEntity modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.savePartnerConfig(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData);				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception  in controller partnerconfig :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	@RequestMapping(value = "getAllpartnerConfig", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getAllpartnerconfig(
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.getAllpartnerconfig(loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setData(finalData);				
				response.setCode(ResponseCode.SUCCESS.toString());
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception in controller getAllpartnerConfig :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "exportGds", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAllPartners(
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.retrieveAllGdsConfig(loggedUser);
			status = (Status) finalData.get("status");

			if (status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception in controller exportGds :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "getPartnerConfigById/{id}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getPartnerConfigById(@PathVariable String id) {
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.getPartnerConfigById(id);
			status = (Status) finalData.get("status");

			if (status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData);					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception in controller getPartnerConfigById:: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "/customTemplateMData", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> customTemplateMData(@RequestBody FieldsToMap fieldsToMap) {
		APIResponse<Object> response = new APIResponse<>();

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			response = service.customTemplateMData(fieldsToMap.getFieldsToMap());
		} catch (Exception e) {
			logger.error("Error occurred retrieving response customTemplateMData::" + e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "editPartnerConfig/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> editPartnerConfig(@RequestBody CPartnerConfigEntity modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.editPartnerConfig(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if (status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
				}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception in controller editPartnerConfig:: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
		@RequestMapping(value = "deletePartnerConfig/{id}", method = RequestMethod.DELETE)
		@ResponseBody
		public APIResponse<Object> deletePartnerConfig(@PathVariable String id,
				@RequestParam(required = true, value = "loggedUser") String loggedUser) {
			logger.info("URI Parameters :: Id [" + id + "]");
			logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
			APIResponse<Object> response = new APIResponse<>();
			Map<String, Object> finalData = null;
			Status status = null;
			try {
				// added the below two lines for multitenancy --KK- 03-07-2020
				String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
				if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
					TenantContext.setTenant(tenantCode);

				finalData = service.deletePartnerConfig(id, loggedUser);
				status = (Status) finalData.get("status");

				if (status.getCode().equals("REQUEST_FORBIDDEN"))
					response.setCode(ResponseCode.FORBIDDEN.toString());
				else {
					if (status.isStatus()) {
						response.setCode(ResponseCode.DELETED.toString());
						response.setData(finalData.get("data"));						
					}
					else
						response.setCode(ResponseCode.ERROR.toString());
					}

				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());

			} catch (Exception e) {
				logger.error("Exception in controller deletePartnerConfig:: ", e);
				response.setCode(ResponseCode.ERROR.toString());
				response.setStatus("Error");
				response.setStatusMessage("Error occurred");
			}
			return response;
		}
	
	@RequestMapping(value = "readLookupExcel", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> readLookupExcel(@RequestBody ReadExcelData modelObj) {
		APIResponse<Object> response = new APIResponse<>();
		boolean flg = false;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			flg = service.readLookupExcel(modelObj);

			if (flg) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setStatus("Lookup conditions stored successfully");
				response.setStatusMessage("Lookup conditions stored successfully");
			} else {
				response.setCode(ResponseCode.ERROR.toString());
				response.setStatus("Error in storing lookup conditions");
				response.setStatusMessage("Error in storing lookup conditions");
			}

		} catch (Exception e) {
			logger.error("Exception in controller deletePartnerConfig:: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error in storing lookup conditions");
			response.setStatusMessage("Error in storing lookup conditions");
		}
		return response;
	}


	@RequestMapping(value = "getAllGdsLookupNames", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> getAllGdsLookupNames(
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
			String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
			if (null != tenantCode && !tenantCode.equalsIgnoreCase(""))
				TenantContext.setTenant(tenantCode);

			finalData = service.getAllGdsLookupNames(loggedUser);
			status = (Status) finalData.get("status");

			if (status.isStatus()) {
				response.setCode(ResponseCode.SUCCESS.toString());
				response.setData(finalData);				
			}
			else
				response.setCode(ResponseCode.ERROR.toString());

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception in controller getAllGdsLookupNames :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

}


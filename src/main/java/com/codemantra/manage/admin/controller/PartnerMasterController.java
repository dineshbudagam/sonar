
/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.model.Partner;
import com.codemantra.manage.admin.model.PartnerGroup;
import com.codemantra.manage.admin.model.PartnerType;
import com.codemantra.manage.admin.service.PartnerService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class PartnerMasterController {
	private static final Logger logger = LoggerFactory.getLogger(PartnerMasterController.class);

	@Autowired
	PartnerService service;
	
	@Autowired
	private HttpServletResponse request;
	
	@RequestMapping(value = "partnertype", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> savePartnerType(@RequestBody PartnerType modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.savePartnerType(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partnertype/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> editPartnerType(@RequestBody PartnerType modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editPartnerType(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: savePartnerType() ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partnertype/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> deletePartnerType(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deletePartnerType(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
    @RequestMapping(value = "partnertype/{id}", method = RequestMethod.GET)
    @ResponseBody
    public APIResponse<PartnerType> retrievePartnerType(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<PartnerType> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    		
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrievePartnerType(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((PartnerType) finalData.get("data"));
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	
	@RequestMapping(value = "partnertype", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAllPartnerTypes(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAllPartnerTypes(loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	///////////////////////////////////
	
	@RequestMapping(value = "partner", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> savePartner(@RequestBody Partner modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.savePartner(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partner/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> editPartner(@RequestBody Partner modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editPartner(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partner/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> deletePartner(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deletePartner(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
    @RequestMapping(value = "partner/{id}", method = RequestMethod.GET)
    @ResponseBody
    public APIResponse<Partner> retrievePartner(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<Partner> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    		
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrievePartner(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((Partner) finalData.get("data"));    					
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	
	@RequestMapping(value = "partner", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAllPartners(@RequestParam(required = false, value = "export") Boolean isExport
			,@RequestParam(required = true, value = "loggedUser") String loggedUser,@RequestParam(required = true, value = "exportFormat") String exportFormat) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			if(null == isExport) {
				isExport = false;
			}
			finalData = service.retrieveAllPartners(isExport, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
    @RequestMapping(value = "partner/{id}/activate/{activeStatus}", method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    public APIResponse<Object> activateDeactivatePartner(@RequestBody Partner modelObj, @PathVariable String id, @PathVariable Integer activeStatus,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"] - activeStatus ["+activeStatus+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<Object> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {   
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.activateDeactivatePartner(modelObj, id, activeStatus, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData(finalData.get("data"));	    					
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
    
    
    
	///////////////////////////////////
	
	@RequestMapping(value = "partnergroup", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> savePartnerGroup(@RequestBody PartnerGroup modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.savePartnerGroup(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partnergroup/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> edit(@RequestBody PartnerGroup modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editPartnerGroup(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partnergroup/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> delete(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deletePartnerGroup(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
    @RequestMapping(value = "partnergroup/{id}", method = RequestMethod.GET)
    @ResponseBody
    public APIResponse<PartnerGroup> retrieve(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<PartnerGroup> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    		
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrievePartnerGroup(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((PartnerGroup) finalData.get("data"));    					
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	
	@RequestMapping(value = "partnergroup", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAll(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAllPartnerGroups(loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());	
			

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "partner/{id}/changeftppassword", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> changeFtpPassword(@RequestBody Partner modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.changeFtpPassword(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}

}


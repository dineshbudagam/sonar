
/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
25-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.model.CApprovalEligibleFormats;
import com.codemantra.manage.admin.model.Format;
import com.codemantra.manage.admin.model.FormatType;
import com.codemantra.manage.admin.service.FormatService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class FormatMasterController {
	private static final Logger logger = LoggerFactory.getLogger(FormatMasterController.class);
	
	@Autowired
	private HttpServletResponse request;
	
	@Autowired
	FormatService service;
	
	@RequestMapping(value = "formattype", method = RequestMethod.GET)
	@ResponseBody

	public APIResponse<Object> retrieveAllFormatTypes(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAllFormatTypes(loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "formattype", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> saveFormatType(@RequestBody FormatType modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.saveFormatType(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	@RequestMapping(value = "formattype/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> editFormatType(@RequestBody FormatType modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editFormatType(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "formattype/{id}", method = RequestMethod.DELETE)
	@ResponseBody

	public APIResponse<Object> deleteFormatType(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deleteFormatType(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "formattype/{id}", method = RequestMethod.GET)
    @ResponseBody
   
    public APIResponse<FormatType> retrieveFormatType(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<FormatType> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrieveFormatType(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((FormatType) finalData.get("data"));    					
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	//////////////////////////////////////
	
	@RequestMapping(value = "format", method = RequestMethod.GET)
	@ResponseBody

	public APIResponse<Object> retrieveAllFormats(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAllFormats(loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "format", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> saveFormat(@RequestBody Format modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.saveFormat(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	@RequestMapping(value = "format/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> editFormat(@RequestBody Format modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editFormat(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	
	////////////////////////////////

	@RequestMapping(value = "format/{id}", method = RequestMethod.DELETE)
	@ResponseBody

	public APIResponse<Object> deleteFormat(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deleteFormat(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "format/{id}", method = RequestMethod.GET)
    @ResponseBody
   
    public APIResponse<Format> retrieveFormat(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<Format> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {    
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrieveFormat(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((Format) finalData.get("data"));    					
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	
	////////////////////////////////
	
	
	@RequestMapping(value = "capprovaleligibleformats", method = RequestMethod.GET)
	@ResponseBody

	public APIResponse<Object> retrieveAllCApprovalEligibleFormats(@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAllCApprovalEligibleFormats(loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}
			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());		

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "capprovaleligibleformats", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> saveCApprovalEligibleFormats(@RequestBody CApprovalEligibleFormats modelObj,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.saveCApprovalEligibleFormats(modelObj, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			
			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	@RequestMapping(value = "capprovaleligibleformats/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody

	public APIResponse<Object> editCApprovalEligibleFormats(@RequestBody CApprovalEligibleFormats modelObj, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.editCApprovalEligibleFormats(modelObj, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	

	@RequestMapping(value = "capprovaleligibleformats/{id}", method = RequestMethod.DELETE)
	@ResponseBody

	public APIResponse<Object> deleteCApprovalEligibleFormats(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.deleteCApprovalEligibleFormats(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error occurred");
		}
		return response;
	}
	
	

    @RequestMapping(value = "capprovaleligibleformats/{id}", method = RequestMethod.GET)
    @ResponseBody
   
    public APIResponse<CApprovalEligibleFormats> retrieveCApprovalEligibleFormats(@PathVariable String id,
    		@RequestParam(required = true, value = "loggedUser") String loggedUser) {
    	logger.info("URI Parameters :: ID ["+id+"]");
    	logger.info("Request Parameters :: loggeduser ["+loggedUser+"]");
    	APIResponse<CApprovalEligibleFormats> response = new APIResponse<>();	
    	Map<String, Object> finalData = null;
    	Status status = null;
    	
    	try {   
    		// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
    			finalData = service.retrieveCApprovalEligibleFormats(id, loggedUser);  
    			status = (Status) finalData.get("status");
    			
    			if(status.getCode().equals("REQUEST_FORBIDDEN"))
    				response.setCode(ResponseCode.FORBIDDEN.toString());
    			else {
    				if (status.isStatus()) {
    					response.setCode(ResponseCode.SUCCESS.toString());
    					response.setData((CApprovalEligibleFormats) finalData.get("data"));
    				}
    				else
    					response.setCode(ResponseCode.ERROR.toString());
    			}
    			
				response.setStatus(status.getCode());
				response.setStatusMessage(status.getMessage());
    	} catch(Exception e) {
    		logger.error("Exception :: ", e);
    		response.setCode(ResponseCode.ERROR.toString());
    		response.setStatus("Error");
    		response.setStatusMessage("Error occurred");
    	}      
        return response;    
    }
	
}

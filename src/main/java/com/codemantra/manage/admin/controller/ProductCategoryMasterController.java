/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codemantra.manage.admin.config.TenantContext;
import com.codemantra.manage.admin.dto.APIResponse;
import com.codemantra.manage.admin.dto.APIResponse.ResponseCode;
import com.codemantra.manage.admin.dto.Status;
import com.codemantra.manage.admin.model.ProductCategory;
import com.codemantra.manage.admin.service.ProductCategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/manage-admin-service/")
public class ProductCategoryMasterController {
	private static final Logger logger = LoggerFactory.getLogger(ProductCategoryMasterController.class);
	
	@Autowired
	private HttpServletResponse request;
	@Autowired
	ProductCategoryService service;

	@RequestMapping(value = "productcategory/mapping/{metadataFieldName}", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
	@ResponseBody
	public APIResponse<Object> saveMetadataMapping(@RequestBody ProductCategory prodCat,
			@PathVariable String metadataFieldName,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: metadataFieldName [" + metadataFieldName + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.saveMetadataMapping(metadataFieldName, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");

		}
		return response;
	}

	@RequestMapping(value = "productcategory", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> save(@RequestBody ProductCategory prodCat,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.save(prodCat, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

	@RequestMapping(value = "productcategory/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public APIResponse<Object> edit(@RequestBody ProductCategory prodCat, @PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Product Category Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.edit(prodCat, id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

	@RequestMapping(value = "productcategory/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public APIResponse<Object> delete(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: Product Category Id [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.delete(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

	@RequestMapping(value = "productcategory", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<Object> retrieveAll(@RequestParam(required = false, value = "accounts") List<String> accounts,
			@RequestParam(required = false, value = "metadatatype") List<String> metadatatype,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "] - accounts ["+ accounts + "] - metadatatypes ["+ metadatatype +"]");
		APIResponse<Object> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;
		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieveAll(accounts, metadatatype, loggedUser);
			status = (Status) finalData.get("status");
			finalData.remove("status");
			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData(finalData);
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());

		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

	@RequestMapping(value = "productcategory/{id}", method = RequestMethod.GET)
	@ResponseBody
	public APIResponse<ProductCategory> retrieve(@PathVariable String id,
			@RequestParam(required = true, value = "loggedUser") String loggedUser) {
		logger.info("URI Parameters :: ID [" + id + "]");
		logger.info("Request Parameters :: loggeduser [" + loggedUser + "]");
		APIResponse<ProductCategory> response = new APIResponse<>();
		Map<String, Object> finalData = null;
		Status status = null;

		try {
			// added the below two lines for multitenancy --KK- 03-07-2020
    		String tenantCode = request.getHeader("X-Tenant"); /* This will be passed from UI */
    		if(null != tenantCode &&  !tenantCode.equalsIgnoreCase(""))
    			TenantContext.setTenant(tenantCode);
    	    	
			finalData = service.retrieve(id, loggedUser);
			status = (Status) finalData.get("status");

			if(status.getCode().equals("REQUEST_FORBIDDEN"))
				response.setCode(ResponseCode.FORBIDDEN.toString());
			else {
				if (status.isStatus()) {
					response.setCode(ResponseCode.SUCCESS.toString());
					response.setData((ProductCategory) finalData.get("data"));					
				}
				else
					response.setCode(ResponseCode.ERROR.toString());
			}

			response.setStatus(status.getCode());
			response.setStatusMessage(status.getMessage());
		} catch (Exception e) {
			logger.error("Exception :: ", e);
			response.setCode(ResponseCode.ERROR.toString());
			response.setStatus("Error");
			response.setStatusMessage("Error");
		}
		return response;
	}

}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
05-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.util;

import java.math.BigInteger;
import java.security.SecureRandom;

public class AccessTokenGenerator {

	public static String generateAccessKey() {
		SecureRandom secureRandom = new SecureRandom();
	    byte[] token = new byte[16];
	    secureRandom.nextBytes(token);
	    return new BigInteger(token).toString(16);
		
	}
	
}

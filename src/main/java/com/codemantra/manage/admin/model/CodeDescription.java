package com.codemantra.manage.admin.model;

public class CodeDescription {
	private String code;
	private String description;
	private Boolean selected = false;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	
	public CodeDescription(String code, String description, Boolean selected) {
		super();
		this.code = code;
		this.description = description;
		this.selected = selected;
	}
	
}

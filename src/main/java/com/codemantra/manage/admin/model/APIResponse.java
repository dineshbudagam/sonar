/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
19-05-2017			v1.0       	   	 Senthil J	  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.admin.model;

public class APIResponse<T> {

	public enum ResponseCode {
		SUCCESS {
		    public String toString() {
		        return "200";
		    }
		},
		FAILURE {
		    public String toString() {
		        return "400";
		    }
		},
 
		CREATED {
		    public String toString() {
		        return "201";
		    }
		},
		IS_EMPTY {
		    public String toString() {
		        return "300";
		    }
		},
		EXISTS {
		    public String toString() {
		        return "301";
		    }
		},
		NOT_MODIFIED {
		    public String toString() {
		        return "304";
		    }
		},
		ERROR {
		    public String toString() {
		        return "500";
		    }
		},
		UNAUTHORIZED {
		    public String toString() {
		        return "401";
		    }
		},
		RECORDS_FOUND {
		    public String toString() {
		        return "200";
		    }
		},
		NO_RECORDS_FOUND {
		    public String toString() {
		        return "404";
		    }
		},
		
		}
	
	private String code;
	private String status;
	private String statusMessage;
	private T data;
	private long searchCount;
	
	public APIResponse(){}
	
	public APIResponse(String code, String status, T data) {
		super();
		this.code = code;
		this.status = status;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public long getSearchCount() {
		return searchCount;
	}

	public void setSearchCount(long searchCount) {
		this.searchCount = searchCount;
	}

	
}

package com.codemantra.manage.admin.model;

import org.springframework.data.mongodb.core.mapping.Field;

public class Attributes {
	
	@Field("attributeName")
	public String attributeName;
	
	@Field("operation")
	public String operation;
	
	@Field("defaultValue")
	public String defaultValue;
	
	@Field("metadataFieldName")
	public String metadataFieldName;

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getMetadataFieldName() {
		return metadataFieldName;
	}

	public void setMetadataFieldName(String metadataFieldName) {
		this.metadataFieldName = metadataFieldName;
	}
	
	

}

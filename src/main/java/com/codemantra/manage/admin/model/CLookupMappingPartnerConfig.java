package com.codemantra.manage.admin.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class CLookupMappingPartnerConfig {
	
	@Id
	private String id;
	private String partnerId;
	@Field("filePath")
	private String filePath;
	@Field("lookupName")
	private String lookupName;
	 @Field("transformRules")
	 private List<TransformRules> transformRules;
	private Boolean isActive;
	@Field("fieldsToMap")
	private List<String> fieldsToMap;
	private Boolean isDeleted;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	
	
	
	public List<String> getFieldsToMap() {
		return fieldsToMap;
	}
	public void setFieldsToMap(List<String> fieldsToMap) {
		this.fieldsToMap = fieldsToMap;
	}
	public String getLookupName() {
		return lookupName;
	}
	public void setLookupName(String lookupName) {
		this.lookupName = lookupName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getId() {
		return id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String get_id() {
		return id;
	}
	public void set_id(String _id) {
		this.id = _id;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public List<TransformRules> getTransformRules() {
		return transformRules;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public void setTransformRules(List<TransformRules> transformRules) {
		this.transformRules = transformRules;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
}

package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class DistributePopulateElements {
	@Field("operation")
	public String operation;
	
	@Field("distributeFieldName")
	public String distributeFieldName;
	
	@Field("defaultVal")
	public String defaultVal;
	
	@Field("intialVal")
	public String intialVal;
	
	@Field("SupportFields")
	public List<String> SupportFields;
	
	@Field("metadataFieldName")
   public String metadataFieldName;
	
	@Field("attributes")
    public List<Attributes> attributes;
	
	@Field("isAttribute")
    public boolean isAttribute;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDistributeFieldName() {
		return distributeFieldName;
	}

	public void setDistributeFieldName(String distributeFieldName) {
		this.distributeFieldName = distributeFieldName;
	}

	public String getDefaultVal() {
		return defaultVal;
	}

	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}

	public String getIntialVal() {
		return intialVal;
	}

	public String getMetadataFieldName() {
		return metadataFieldName;
	}

	public void setMetadataFieldName(String metadataFieldName) {
		this.metadataFieldName = metadataFieldName;
	}

	public boolean isAttribute() {
		return isAttribute;
	}

	public void setAttribute(boolean isAttribute) {
		this.isAttribute = isAttribute;
	}

	public void setIntialVal(String intialVal) {
		this.intialVal = intialVal;
	}

	public List<String> getSupportFields() {
		return SupportFields;
	}

	public void setSupportFields(List<String> supportFields) {
		SupportFields = supportFields;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}
	
	

}

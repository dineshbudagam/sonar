package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class TransformRules {
    
	@Field("codeListConditions")
    public List<CodeListConditions> CodeListConditions;
    
	@Field("transformCodeList")
    public List<TransformCodeList> TransformCodeList;

    public List<CodeListConditions> getCodeListConditions() {
        return CodeListConditions;
    }

    public void setCodeListConditions(List<CodeListConditions> codeListConditions) {
        CodeListConditions = codeListConditions;
    }

    public List<TransformCodeList> getTransformCodeList() {
        return TransformCodeList;
    }

    public void setTransformCodeList(List<TransformCodeList> transformCodeList) {
        TransformCodeList = transformCodeList;
    }
}

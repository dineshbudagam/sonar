package com.codemantra.manage.admin.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

import com.codemantra.manage.admin.entity.CustomFieldsStringEntity;

public class TransformFieldDetails {

	@Field("metaDataFieldName")
	String metaDataFieldName;
	
	@Field("operation")
	String operation;
	
	@Field("joinBy")
	String joinBy;
	
	@Field("lookUp")
	Map<String, String> lookUp;

	@Field("fieldsToConcat")
	List<String> fieldsToConcat;
	
	@Field("fieldsToConcatJsonPath")
	List<String> fieldsToConcatJsonPath;

	@Field("fieldsToMap")
	List<String> fieldsToMap;
	
	@Field("fieldsToMapJsonPath")
	List<String> fieldsToMapJsonPath;
	
	@Field("arithmeticExpression")
	String arithmeticExpression;
	
	@Field("dateFormat")
	String dateFormat;
	
	@Field("withSeperator")
	String withSeperator;
	
	@Field("convertTo")
	String convertTo;
	
	@Field("sheetName")
	String sheetName;
	
	@Field("sheetNames")
	List<String> sheeNames;
	
	@Field("mappingFilePath")
	String mappingFilePath;
	
	@Field("lookupName")
	String lookupName;
	

	@Field("startIndex")
	Integer startIndex;
	
	
	@Field("endIndex")
	Integer endIndex;
	
	@Field("substituteText")
	String substituteText;
	
	@Field("customFieldString")
	String customFieldString;
	

	@Field("customFieldsString")
	List<CustomFieldsStringEntity> customFieldsString;
	
	
	
	public List<CustomFieldsStringEntity> getCustomFieldsString() {
		return customFieldsString;
	}

	public void setCustomFieldsString(List<CustomFieldsStringEntity> customFieldsString) {
		this.customFieldsString = customFieldsString;
	}

	
	
	public Integer getStartIndex() {
		return startIndex;
	}

	public Integer getEndIndex() {
		return endIndex;
	}

	public String getSubstituteText() {
		return substituteText;
	}

	public String getCustomFieldString() {
		return customFieldString;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public void setEndIndex(Integer endIndex) {
		this.endIndex = endIndex;
	}

	public void setSubstituteText(String substituteText) {
		this.substituteText = substituteText;
	}

	public void setCustomFieldString(String customFieldString) {
		this.customFieldString = customFieldString;
	}

	public String getLookupName() {
		return lookupName;
	}

	public void setLookupName(String lookupName) {
		this.lookupName = lookupName;
	}

	public String getSheetName() {
		return sheetName;
	}

	public List<String> getSheeNames() {
		return sheeNames;
	}

	public String getMappingFilePath() {
		return mappingFilePath;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public void setSheeNames(List<String> sheeNames) {
		this.sheeNames = sheeNames;
	}

	public void setMappingFilePath(String mappingFilePath) {
		this.mappingFilePath = mappingFilePath;
	}

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getJoinBy() {
		return joinBy;
	}

	public void setJoinBy(String joinBy) {
		this.joinBy = joinBy;
	}

	public Map<String, String> getLookUp() {
		return lookUp;
	}

	public void setLookUp(Map<String, String> lookUp) {
		this.lookUp = lookUp;
	}

	public List<String> getFieldsToConcat() {
		return fieldsToConcat;
	}

	public List<String> getFieldsToConcatJsonPath() {
		return fieldsToConcatJsonPath;
	}

	public String getArithmeticExpression() {
		return arithmeticExpression;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getWithSeperator() {
		return withSeperator;
	}

	public String getConvertTo() {
		return convertTo;
	}

	public void setFieldsToConcat(List<String> fieldsToConcat) {
		this.fieldsToConcat = fieldsToConcat;
	}

	public void setFieldsToConcatJsonPath(List<String> fieldsToConcatJsonPath) {
		this.fieldsToConcatJsonPath = fieldsToConcatJsonPath;
	}

	public void setArithmeticExpression(String arithmeticExpression) {
		this.arithmeticExpression = arithmeticExpression;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setWithSeperator(String withSeperator) {
		this.withSeperator = withSeperator;
	}

	public void setConvertTo(String convertTo) {
		this.convertTo = convertTo;
	}

	public List<String> getFieldsToMap() {
		return fieldsToMap;
	}

	public List<String> getFieldsToMapJsonPath() {
		return fieldsToMapJsonPath;
	}

	public void setFieldsToMap(List<String> fieldsToMap) {
		this.fieldsToMap = fieldsToMap;
	}

	public void setFieldsToMapJsonPath(List<String> fieldsToMapJsonPath) {
		this.fieldsToMapJsonPath = fieldsToMapJsonPath;
	}
	
	
	
}

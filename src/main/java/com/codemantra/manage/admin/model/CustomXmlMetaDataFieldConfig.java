package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "customXMLMetaDataFieldConfig")
public class CustomXmlMetaDataFieldConfig {
	
	@Field("metaDataFieldName")
	public String metaDataFieldName;
	
	@Field("jsonPath")
	public String jsonPath;

	
	@Field("tagName")
	public String tagName;
	
	
	
	@Field("transformations")
	public Transformation transformation;
	
	@Field("supportLookupName")
	public String supportLookupName;
	
	@Field("attributes")
    public List<Attributes> attributes;
	
	
	
	

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public String getJsonPath() {
		return jsonPath;
	}

	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}

	

	public String getSupportLookupName() {
		return supportLookupName;
	}

	public void setSupportLookupName(String supportLookupName) {
		this.supportLookupName = supportLookupName;
	}

	

	public Transformation getTransformation() {
		return transformation;
	}

	public void setTransformation(Transformation transformation) {
		this.transformation = transformation;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	
	
}

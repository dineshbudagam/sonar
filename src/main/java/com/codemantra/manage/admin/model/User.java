/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
05-08-2017			v1.0       	   Shahid ul Islam  		Initial Version.
***********************************************************************************************************************/

package com.codemantra.manage.admin.model;

import java.util.Date;
import java.util.List;

import com.codemantra.manage.admin.entity.MBaseEntity;
import com.codemantra.manage.admin.entity.PermissionGroupEntity;

public class User extends MBaseEntity{
	private String userId;
	private String userName;
	private String loginName;
	private String firstName;
	private String lastName;
	private String emailId;
	private String contactNo;
	private String address;
	private String city;
	private String profilePicName;	
	private Boolean isRegistered;
	private Date registeredOn;
	private Date invitationMailSentOn;
	private String accessKey;
	
	private String permissionGroupId;

	private String password;
	private Date passwordExpiryDate;
	private Boolean isPasswordExpired;
	private String zipCode;
	private String oautAaccessToken;
	private String clientIp;
	
	private PermissionGroupEntity permissionGroup;
	
	private String userType = null;
	private String userExpiryDate;
	
	private List<String> userNameFilterList;
	private List<String> userEmailFilterList;
	private List<String> userRoleFilterList;
	private List<String> userPgFilterList;
	private List<String> userStatusFilterList;
	private List<String> userOnlineStatusFilterList;
	private String filterFieldName;
	private String filterFieldText;
	
	private String createdFromDate;
	private String createdToDate;
	private String activatedFromDate;
	private String activatedToDate;
	
	private String onlineStatus;
	private String pgName;
	
	private boolean export;
	
	private Boolean isLocked;
	private Integer lockCount;
	private Date lockedTime;
	private String firstNameFirst;
	private String firstNameLast;
	private String accountName;
	private String tenantCode;
	private Integer skip;
	private boolean isKeycloakUser;

	//private long logCount;
	//private String hashPassword;
	//private String saltKey;
	//private int modificationCounter;
	//private long departmentId;
	//private long companyId;
	//private boolean status;
	//private String statusMessage;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAddress(){
		return address;
	}
	public void setAddress(String address){
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProfilePicName() {
		return profilePicName;
	}
	public void setProfilePicName(String profilePicName) {
		this.profilePicName = profilePicName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}
	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Date getRegisteredOn() {
		return registeredOn;
	}
	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}
	public Date getInvitationMailSentOn() {
		return invitationMailSentOn;
	}
	public void setInvitationMailSentOn(Date invitationMailSentOn) {
		this.invitationMailSentOn = invitationMailSentOn;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getOautAaccessToken() {
		return oautAaccessToken;
	}
	public void setOautAaccessToken(String oautAaccessToken) {
		this.oautAaccessToken = oautAaccessToken;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getPermissionGroupId() {
		return permissionGroupId;
	}
	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}
	public Boolean getIsRegistered() {
		return isRegistered;
	}
	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
	public Boolean getIsPasswordExpired() {
		return isPasswordExpired;
	}
	public void setIsPasswordExpired(Boolean isPasswordExpired) {
		this.isPasswordExpired = isPasswordExpired;
	}
	public PermissionGroupEntity getPermissionGroup() {
		return permissionGroup;
	}
	public void setPermissionGroup(PermissionGroupEntity permissionGroup) {
		this.permissionGroup = permissionGroup;
	}
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserExpiryDate() {
		return userExpiryDate;
	}
	public void setUserExpiryDate(String userExpiryDate) {
		this.userExpiryDate = userExpiryDate;
	}
	public List<String> getUserNameFilterList() {
		return userNameFilterList;
	}
	public void setUserNameFilterList(List<String> userNameFilterList) {
		this.userNameFilterList = userNameFilterList;
	}
	public List<String> getUserEmailFilterList() {
		return userEmailFilterList;
	}
	public void setUserEmailFilterList(List<String> userEmailFilterList) {
		this.userEmailFilterList = userEmailFilterList;
	}
	public List<String> getUserRoleFilterList() {
		return userRoleFilterList;
	}
	public void setUserRoleFilterList(List<String> userRoleFilterList) {
		this.userRoleFilterList = userRoleFilterList;
	}
	public List<String> getUserPgFilterList() {
		return userPgFilterList;
	}
	public void setUserPgFilterList(List<String> userPgFilterList) {
		this.userPgFilterList = userPgFilterList;
	}
	public List<String> getUserStatusFilterList() {
		return userStatusFilterList;
	}
	public void setUserStatusFilterList(List<String> userStatusFilterList) {
		this.userStatusFilterList = userStatusFilterList;
	}
	public List<String> getUserOnlineStatusFilterList() {
		return userOnlineStatusFilterList;
	}
	public void setUserOnlineStatusFilterList(List<String> userOnlineStatusFilterList) {
		this.userOnlineStatusFilterList = userOnlineStatusFilterList;
	}
	public String getFilterFieldName() {
		return filterFieldName;
	}
	public void setFilterFieldName(String filterFieldName) {
		this.filterFieldName = filterFieldName;
	}
	public String getFilterFieldText() {
		return filterFieldText;
	}
	public void setFilterFieldText(String filterFieldText) {
		this.filterFieldText = filterFieldText;
	}
	public String getCreatedFromDate() {
		return createdFromDate;
	}
	public void setCreatedFromDate(String createdFromDate) {
		this.createdFromDate = createdFromDate;
	}
	public String getCreatedToDate() {
		return createdToDate;
	}
	public void setCreatedToDate(String createdToDate) {
		this.createdToDate = createdToDate;
	}
	public String getActivatedFromDate() {
		return activatedFromDate;
	}
	public void setActivatedFromDate(String activatedFromDate) {
		this.activatedFromDate = activatedFromDate;
	}
	public String getActivatedToDate() {
		return activatedToDate;
	}
	public void setActivatedToDate(String activatedToDate) {
		this.activatedToDate = activatedToDate;
	}
	public String getOnlineStatus() {
		return onlineStatus;
	}
	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}
	public String getPgName() {
		return pgName;
	}
	public void setPgName(String pgName) {
		this.pgName = pgName;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	public Boolean getIsLocked() {
		return isLocked;
	}
	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}
	public Integer getLockCount() {
		return lockCount;
	}
	public void setLockCount(Integer lockCount) {
		this.lockCount = lockCount;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getFirstNameFirst() {
		return firstNameFirst;
	}
	public void setFirstNameFirst(String firstNameFirst) {
		this.firstNameFirst = firstNameFirst;
	}
	public String getFirstNameLast() {
		return firstNameLast;
	}
	public void setFirstNameLast(String firstNameLast) {
		this.firstNameLast = firstNameLast;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public Integer getSkip() {
		return skip;
	}
	public void setSkip(Integer skip) {
		this.skip = skip;
	}
	public boolean getIsKeycloakUser() {
		return isKeycloakUser;
	}
	public void setKeycloakUser(boolean isKeycloakUser) {
		this.isKeycloakUser = isKeycloakUser;
	}
	

/*	public long getLogCount() {
		return logCount;
	}
	public void setLogCount(long logCount) {
		this.logCount = logCount;
	}
	public String getHashPassword() {
		return hashPassword;
	}
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}
	public String getSaltKey() {
		return saltKey;
	}
	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	public int getModificationCounter() {
		return modificationCounter;
	}
	public void setModificationCounter(int modificationCounter) {
		this.modificationCounter = modificationCounter;
	}
	public long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isStatus() {
		return status;
	}*/
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

public class PartnerType{

	private String id;
	
	private String partnerTypeId;
	private String partnerTypeName;
	private String formatTypeId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPartnerTypeId() {
		return partnerTypeId;
	}
	public void setPartnerTypeId(String partnerTypeId) {
		this.partnerTypeId = partnerTypeId;
	}
	public String getPartnerTypeName() {
		return partnerTypeName;
	}
	public void setPartnerTypeName(String partnerTypeName) {
		this.partnerTypeName = partnerTypeName;
	}
	public String getFormatTypeId() {
		return formatTypeId;
	}
	public void setFormatTypeId(String formatTypeId) {
		this.formatTypeId = formatTypeId;
	}
	
	
}

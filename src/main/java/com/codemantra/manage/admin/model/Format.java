package com.codemantra.manage.admin.model;

import java.util.List;

public class Format {
	
	private String id;
	private String formatId;
	private String formatName;
	private String filePurpose;
	private List<String> extension;
	private String namingConvention;
	private String formatTypeId;
	private Integer preference;
	private Boolean singleFile;
	private Boolean isEpubFormat;
	private Boolean isVirtualViewerFormat;
	private Boolean createWidget;
	private Boolean hasAsset;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public String getFilePurpose() {
		return filePurpose;
	}
	public void setFilePurpose(String filePurpose) {
		this.filePurpose = filePurpose;
	}
	public List<String> getExtension() {
		return extension;
	}
	public void setExtension(List<String> extension) {
		this.extension = extension;
	}
	public String getNamingConvention() {
		return namingConvention;
	}
	public void setNamingConvention(String namingConvention) {
		this.namingConvention = namingConvention;
	}
	public String getFormatTypeId() {
		return formatTypeId;
	}
	public void setFormatTypeId(String formatTypeId) {
		this.formatTypeId = formatTypeId;
	}
	public Integer getPreference() {
		return preference;
	}
	public void setPreference(Integer preference) {
		this.preference = preference;
	}
	public Boolean getSingleFile() {
		return singleFile;
	}
	public void setSingleFile(Boolean singleFile) {
		this.singleFile = singleFile;
	}
	public Boolean getIsEpubFormat() {
		return isEpubFormat;
	}
	public void setIsEpubFormat(Boolean isEpubFormat) {
		this.isEpubFormat = isEpubFormat;
	}
	public Boolean getIsVirtualViewerFormat() {
		return isVirtualViewerFormat;
	}
	public void setIsVirtualViewerFormat(Boolean isVirtualViewerFormat) {
		this.isVirtualViewerFormat = isVirtualViewerFormat;
	}
	public Boolean getCreateWidget() {
		return createWidget;
	}
	public void setCreateWidget(Boolean createWidget) {
		this.createWidget = createWidget;
	}
	public Boolean getHasAsset() {
		return hasAsset;
	}
	public void setHasAsset(Boolean hasAsset) {
		this.hasAsset = hasAsset;
	}

}

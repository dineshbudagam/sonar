package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class CustomXMLDetails {

	@Field("tagName")
	public String tagName;

	@Field("level")
	public int level;

	@Field("orderNo")
	public int orderNo;
	
	
	@Field("isList")
	public boolean isList;

	@Field("parentTagName")
	public String parentTag;

	@Field("metadataFieldName")
	public String metaDataFieldName;
	
	@Field("transformations")
     public Transformation transformations;
	
	@Field("attributes")
    public List<Attributes> attributes;
	
	
	public String elementPath;//isElement
	
	@Field("isElement")
	public boolean isElement;
	
	@Field("isAttribute")
	public boolean isAttribute;

	
	@Field("siblings")
	public List<CustomXMLDetails> siblings;
	
	@Field("elements")
	public List<CustomXMLDetails> elements;
	
	@Field("subElements")
	public List<DistributePopulateElements> subElements;
	
	

	@Field("value")
	public String value;
	
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getLevel() {
		return level;
	}
	

	public Transformation getTransformations() {
		return transformations;
	}

	public void setTransformations(Transformation transformations) {
		this.transformations = transformations;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}

	public boolean isElement() {
		return isElement;
	}

	public void setElement(boolean isElement) {
		this.isElement = isElement;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public String getParentTag() {
		return parentTag;
	}

	public void setParentTag(String parentTag) {
		this.parentTag = parentTag;
	}

	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}

	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}

	public List<CustomXMLDetails> getSiblings() {
		return siblings;
	}

	public void setSiblings(List<CustomXMLDetails> siblings) {
		this.siblings = siblings;
	}
	

	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}

	public List<DistributePopulateElements> getSubElements() {
		return subElements;
	}

	public void setSubElements(List<DistributePopulateElements> subElements) {
		this.subElements = subElements;
	}

	public List<CustomXMLDetails> getElements() {
		return elements;
	}

	public void setElements(List<CustomXMLDetails> elements) {
		this.elements = elements;
	}

	public boolean isAttribute() {
		return isAttribute;
	}

	public void setAttribute(boolean isAttribute) {
		this.isAttribute = isAttribute;
	}

	public String getElementPath() {
		return elementPath;
	}

	public void setElementPath(String elementPath) {
		this.elementPath = elementPath;
	}

}

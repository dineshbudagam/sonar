/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

import java.util.List;

import com.codemantra.manage.admin.entity.MBaseEntity;

public class PermissionGroup extends MBaseEntity{
	
	private String permissionGroupId;
	private String permissionGroupName;
	private String roleId;
	
	private List<String> account;
	private List<String> productHierarchyId;
	public List<String> getProductHierarchyId() {
		return productHierarchyId;
	}
	public void setProductHierarchyId(List<String> productHierarchyId) {
		this.productHierarchyId = productHierarchyId;
	}
	private List<String> imprint;
	private List<String> productCategory;
	private List<Long> metadataGroup;
	private List<String> partnerType;
	private List<String> vendor;
	private List<String> application;
	
	private List<Asset> asset;
	private List<String> report;
	private List<String> administrator;
	
	
	public String getPermissionGroupId() {
		return permissionGroupId;
	}
	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}
	public String getPermissionGroupName() {
		return permissionGroupName;
	}
	public void setPermissionGroupName(String permissionGroupName) {
		this.permissionGroupName = permissionGroupName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public List<String> getAccount() {
		return account;
	}
	public void setAccount(List<String> account) {
		this.account = account;
	}
	public List<String> getImprint() {
		return imprint;
	}
	public void setImprint(List<String> imprint) {
		this.imprint = imprint;
	}
	public List<String> getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(List<String> productCategory) {
		this.productCategory = productCategory;
	}
	public List<Long> getMetadataGroup() {
		return metadataGroup;
	}
	public void setMetadataGroup(List<Long> metadataGroup) {
		this.metadataGroup = metadataGroup;
	}
	public List<String> getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(List<String> partnerType) {
		this.partnerType = partnerType;
	}
	public List<String> getVendor() {
		return vendor;
	}
	public void setVendor(List<String> vendor) {
		this.vendor = vendor;
	}
	public List<String> getApplication() {
		return application;
	}
	public void setApplication(List<String> application) {
		this.application = application;
	}
	public List<Asset> getAsset() {
		return asset;
	}
	public void setAsset(List<Asset> asset) {
		this.asset = asset;
	}
	public List<String> getReport() {
		return report;
	}
	public void setReport(List<String> report) {
		this.report = report;
	}
	public List<String> getAdministrator() {
		return administrator;
	}
	public void setAdministrator(List<String> administrator) {
		this.administrator = administrator;
	}
	
	
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
12-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

import java.util.List;

public class DistributionStatusUpdate{
	
	private String partnerId;
	private List<String> isbn;
	private List<String> formatId;
	private String status;
	private String transactionType;
	private String remarks;
	
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public List<String> getIsbn() {
		return isbn;
	}
	public void setIsbn(List<String> isbn) {
		this.isbn = isbn;
	}
	public List<String> getFormatId() {
		return formatId;
	}
	public void setFormatId(List<String> formatId) {
		this.formatId = formatId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}

package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;


public class Transformation {
	
	@Field("operation")
	public String operation;//splitter

	@Field("splitter")//dateFormat
	public String splitter;
	
	@Field("resDateFormat")
	public String resDateFormat;
	
	@Field("sourceDataFormat")//subElements
	public String sourceDataFormat;
	
	@Field("SparatorBoundaries")
	public List<SparatorBoundaries> sparatorBoundaries;
	
	@Field("subElements")
	public List<DistributePopulateElements> subElements;
	
	@Field("metadataFields")
	public List<String> metadataFields;
	
	
	@Field("supportFieldName")
	public String supportFieldName;
	
	@Field("defaultValue")
	public String defaultValue;
	
	@Field("separator")
	public String separator;
	
	@Field("suffix")
	public String suffix;
	
	
	@Field("supportLookupName")
	public String supportLookupName;
	
	@Field("prefix")
	public String prefix;


	public List<String> getMetadataFields() {
		return metadataFields;
	}


	public void setMetadataFields(List<String> metadataFields) {
		this.metadataFields = metadataFields;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getDefaultValue() {
		return defaultValue;
	}


	public List<SparatorBoundaries> getSparatorBoundaries() {
		return sparatorBoundaries;
	}


	public void setSparatorBoundaries(List<SparatorBoundaries> sparatorBoundaries) {
		this.sparatorBoundaries = sparatorBoundaries;
	}


	public String getSupportFieldName() {
		return supportFieldName;
	}


	public void setSupportFieldName(String supportFieldName) {
		this.supportFieldName = supportFieldName;
	}


	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public List<DistributePopulateElements> getSubElements() {
		return subElements;
	}


	public void setSubElements(List<DistributePopulateElements> subElements) {
		this.subElements = subElements;
	}


	public String getSeparator() {
		return separator;
	}


	public void setSeparator(String separator) {
		this.separator = separator;
	}


	public String getSuffix() {
		return suffix;
	}


	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}


	public String getPrefix() {
		return prefix;
	}


	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}


	

	public String getResDateFormat() {
		return resDateFormat;
	}


	public void setResDateFormat(String resDateFormat) {
		this.resDateFormat = resDateFormat;
	}


	public String getSourceDataFormat() {
		return sourceDataFormat;
	}


	public void setSourceDataFormat(String sourceDataFormat) {
		this.sourceDataFormat = sourceDataFormat;
	}


	public String getSplitter() {
		return splitter;
	}


	public void setSplitter(String splitter) {
		this.splitter = splitter;
	}


	public String getSupportLookupName() {
		return supportLookupName;
	}


	public void setSupportLookupName(String supportLookupName) {
		this.supportLookupName = supportLookupName;
	}
	
	
	

}

package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

public class FieldsToMap {
	
	@Field("userId")
	String userId;
	
	@Field("fieldsToMap")
	List<String> fieldsToMap;

	public String getUserId() {
		return userId;
	}

	public List<String> getFieldsToMap() {
		return fieldsToMap;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setFieldsToMap(List<String> fieldsToMap) {
		this.fieldsToMap = fieldsToMap;
	}

	
	
}

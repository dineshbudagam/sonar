package com.codemantra.manage.admin.model;

public class CodeListConditions {
	private String metaDataFieldName;
	private String operation;
	private String value;
	private String replaceValue;
	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}
	public String getOperation() {
		return operation;
	}
	public String getValue() {
		return value;
	}
	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getReplaceValue() {
		return replaceValue;
	}
	public void setReplaceValue(String replaceValue) {
		this.replaceValue = replaceValue;
	}
	

}

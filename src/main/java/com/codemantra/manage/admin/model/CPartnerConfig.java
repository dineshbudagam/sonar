package com.codemantra.manage.admin.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="cPartnerConfig")
public class CPartnerConfig{
	
	@Field
	private String _id;
	private String partnerConfigId;
	private String partnerId;
	private List<FormatDetails> formatDetails;
	
	private Boolean cancelRequestOnEpubFailure;
	
	
	private Boolean coverRequired;
	private Boolean metadataRequired;
	private List<CoverFormat> coverFormat;
	private boolean coverResizeRequired;
	private String coverMaxHeight;
	private String coverMaxWidth;
	private String metadataNaming;
	private String metadataType;
	private String onixType;
	private String excelTempate;
	private String excelTempateRowStartAt;
	private Boolean isSimulation;
	@Field("TransferFolderStructure")
	private Boolean transferFolderStructure;
	@Field("ShortTag")
	private Boolean shortTag;
	private Boolean cDATARequired;
	private String erpepswr;
	private String emailId;
	private String simulationEmail;
	private String erpepswr_default;
	private String erpepswr_meta;
	private List<FTPDetails> ftpDetails;


	private List<ExcelFieldDetails> excelFieldDetails;
	private List<CSVFieldDetails> csvFieldDetails;
	private List<String> sheetNames;
	List<String> excludeFieldList;
	List<TransformFieldDetails> transformFieldList;
	private Integer metaRefreshInterval;
	private String metadataTransferMedium;
	private Boolean isActive;
	private Boolean isDeleted;
	private List<String> productHierarchyIds;
	Boolean isMerge;
	String dtdLine;
	CustomXMLDetails customXMLDetails;
	List<TextFieldDetails> textFieldDetails;

	public Boolean getcDATARequired() {
		return cDATARequired;
	}

	public void setcDATARequired(Boolean cDATARequired) {
		this.cDATARequired = cDATARequired;
	}

	public String getMetadataTransferMedium() {
		return metadataTransferMedium;
	}

	public void setMetadataTransferMedium(String metadataTransferMedium) {
		this.metadataTransferMedium = metadataTransferMedium;
	}

	public Integer getMetaRefreshInterval() {
		return metaRefreshInterval;
	}

	public void setMetaRefreshInterval(Integer metaRefreshInterval) {
		this.metaRefreshInterval = metaRefreshInterval;
	}

	public List<CSVFieldDetails> getCsvFieldDetails() {
		return csvFieldDetails;
	}

	public List<String> getSheetNames() {
		return sheetNames;
	}

	public void setCsvFieldDetails(List<CSVFieldDetails> csvFieldDetails) {
		this.csvFieldDetails = csvFieldDetails;
	}

	public void setSheetNames(List<String> sheetNames) {
		this.sheetNames = sheetNames;
	}

	public Boolean getTransferFolderStructure() {
		return transferFolderStructure;
	}

	public Boolean getShortTag() {
		return shortTag;
	}

	public void setTransferFolderStructure(Boolean transferFolderStructure) {
		this.transferFolderStructure = transferFolderStructure;
	}

	public void setShortTag(Boolean shortTag) {
		this.shortTag = shortTag;
	}

	public String getErpepswr_default() {
		return erpepswr_default;
	}

	public String getErpepswr_meta() {
		return erpepswr_meta;
	}

	public void setErpepswr_default(String erpepswr_default) {
		this.erpepswr_default = erpepswr_default;
	}

	public void setErpepswr_meta(String erpepswr_meta) {
		this.erpepswr_meta = erpepswr_meta;
	}

	public Boolean getCancelRequestOnEpubFailure() {
		return cancelRequestOnEpubFailure;
	}

	public void setCancelRequestOnEpubFailure(Boolean cancelRequestOnEpubFailure) {
		this.cancelRequestOnEpubFailure = cancelRequestOnEpubFailure;
	}

	
	public String getPartnerConfigId() {
		return partnerConfigId;
	}

	public void setPartnerConfigId(String partnerConfigId) {
		this.partnerConfigId = partnerConfigId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public List<FormatDetails> getFormatDetails() {
		return formatDetails;
	}

	public void setFormatDetails(List<FormatDetails> formatDetails) {
		this.formatDetails = formatDetails;
	}

	public Boolean getCoverRequired() {
		return coverRequired;
	}

	public void setCoverRequired(Boolean coverRequired) {
		this.coverRequired = coverRequired;
	}

	public List<CoverFormat> getCoverFormat() {
		return coverFormat;
	}

	public void setCoverFormat(List<CoverFormat> coverFormat) {
		this.coverFormat = coverFormat;
	}

	
	public String getCoverMaxHeight() {
		return coverMaxHeight;
	}

	public void setCoverMaxHeight(String coverMaxHeight) {
		this.coverMaxHeight = coverMaxHeight;
	}

	public String getCoverMaxWidth() {
		return coverMaxWidth;
	}

	public void setCoverMaxWidth(String coverMaxWidth) {
		this.coverMaxWidth = coverMaxWidth;
	}

	public String getMetadataNaming() {
		return metadataNaming;
	}

	public void setMetadataNaming(String metadataNaming) {
		this.metadataNaming = metadataNaming;
	}

	public String getMetadataType() {
		return metadataType;
	}

	public void setMetadataType(String metadataType) {
		this.metadataType = metadataType;
	}

	public String getOnixType() {
		return onixType;
	}

	public void setOnixType(String onixType) {
		this.onixType = onixType;
	}
	public boolean isCoverResizeRequired() {
		return coverResizeRequired;
	}

	public void setCoverResizeRequired(boolean coverResizeRequired) {
		this.coverResizeRequired = coverResizeRequired;
	}

	public String getExcelTempate() {
		return excelTempate;
	}

	public void setExcelTempate(String excelTempate) {
		this.excelTempate = excelTempate;
	}

	public List<FTPDetails> getFtpDetails() {
		return ftpDetails;
	}

	public void setFtpDetails(List<FTPDetails> ftpDetails) {
		this.ftpDetails = ftpDetails;
	}

	public List<ExcelFieldDetails> getExcelFieldDetails() {
		return excelFieldDetails;
	}

	public void setExcelFieldDetails(List<ExcelFieldDetails> excelFieldDetails) {
		this.excelFieldDetails = excelFieldDetails;
	}

	public String getExcelTempateRowStartAt() {
		return excelTempateRowStartAt;
	}

	public void setExcelTempateRowStartAt(String excelTempateRowStartAt) {
		this.excelTempateRowStartAt = excelTempateRowStartAt;
	}

	public List<String> getExcludeFieldList() {
		return excludeFieldList;
	}

	public void setExcludeFieldList(List<String> excludeFieldList) {
		this.excludeFieldList = excludeFieldList;
	}

	public List<TransformFieldDetails> getTransformFieldList() {
		return transformFieldList;
	}

	public void setTransformFieldList(List<TransformFieldDetails> transformFieldList) {
		this.transformFieldList = transformFieldList;
	}

	public Boolean getMetadataRequired() {
		return metadataRequired;
	}

	public void setMetadataRequired(Boolean metadataRequired) {
		this.metadataRequired = metadataRequired;
	}

	public Boolean getIsSimulation() {
		return isSimulation;
	}

	public void setIsSimulation(Boolean isSimulation) {
		this.isSimulation = isSimulation;
	}

 
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSimulationEmail() {
		return simulationEmail;
	}

	public void setSimulationEmail(String simulationEmail) {
		this.simulationEmail = simulationEmail;
	}

	public String get_id() {
		return _id;
	}

	public String getErpepswr() {
		return erpepswr;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public void setErpepswr(String erpepswr) {
		this.erpepswr = erpepswr;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<String> getProductHierarchyIds() {
		return productHierarchyIds;
	}

	public void setProductHierarchyIds(List<String> productHierarchyIds) {
		this.productHierarchyIds = productHierarchyIds;
	}

	public Boolean getIsMerge() {
		return isMerge;
	}

	public void setIsMerge(Boolean isMerge) {
		this.isMerge = isMerge;
	}

	public String getDtdLine() {
		return dtdLine;
	}

	public void setDtdLine(String dtdLine) {
		this.dtdLine = dtdLine;
	}

	public CustomXMLDetails getCustomXMLDetails() {
		return customXMLDetails;
	}

	public void setCustomXMLDetails(CustomXMLDetails customXMLDetails) {
		this.customXMLDetails = customXMLDetails;
	}

	public List<TextFieldDetails> getTextFieldDetails() {
		return textFieldDetails;
	}

	public void setTextFieldDetails(List<TextFieldDetails> textFieldDetails) {
		this.textFieldDetails = textFieldDetails;
	}
	
	
	
	
} 

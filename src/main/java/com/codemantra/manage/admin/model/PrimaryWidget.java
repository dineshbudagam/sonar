/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
20-11-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

public class PrimaryWidget {
	
	private String widgetId;
	private String widgetName;
	//private List<String> roleId;
	
	public String getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(String widgetId) {
		this.widgetId = widgetId;
	}
	public String getWidgetName() {
		return widgetName;
	}
	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
	/*public List<String> getRoleId() {
		return roleId;
	}
	public void setRoleId(List<String> roleId) {
		this.roleId = roleId;
	}*/
	
	

}

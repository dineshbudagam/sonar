/**********************************************************************************************************************
Date          		Version       	Modified By      		Description  
***********			********     	*************			*************	
25-07-2017			v1.0       	   	 Saravanan K	  		Initial Version.
***********************************************************************************************************************/


package com.codemantra.manage.admin.model;

import java.util.Date;

public class MetaDataFields {
	
	public String metaDataFieldId;
	
	public String metaDataFieldName;
	
	public String displayOrderNo;
	
	public String groupId;
	
	public String fieldDisplayName;
	
	public String fieldType;
	
	public String jsonPath;
	
	public String jsonPathIndex;
	
	public String referenceTable;
	
	public String referencePath;
	
	public String referenceValue;
	
	public String isDisplay;
	
	public String isMandatory;
	
	public String isActive;
	
	public String isDeleted;
	
	public String isEdit;
	
	public String[] pageName;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String modifiedBy;
	
	private Date modifiedDate;
	
	
	public String getMetaDataFieldId() {
		return metaDataFieldId;
	}
	public void setMetaDataFieldId(String metaDataFieldId) {
		this.metaDataFieldId = metaDataFieldId;
	}
	public String getMetaDataFieldName() {
		return metaDataFieldName;
	}
	public void setMetaDataFieldName(String metaDataFieldName) {
		this.metaDataFieldName = metaDataFieldName;
	}
	public String getDisplayOrderNo() {
		return displayOrderNo;
	}
	public void setDisplayOrderNo(String displayOrderNo) {
		this.displayOrderNo = displayOrderNo;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getFieldDisplayName() {
		return fieldDisplayName;
	}
	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getJsonPath() {
		return jsonPath;
	}
	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}
	public String getJsonPathIndex() {
		return jsonPathIndex;
	}
	public void setJsonPathIndex(String jsonPathIndex) {
		this.jsonPathIndex = jsonPathIndex;
	}
	public String getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(String isDisplay) {
		this.isDisplay = isDisplay;
	}
	public String getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	public String getReferenceTable() {
		return referenceTable;
	}
	public void setReferenceTable(String referenceTable) {
		this.referenceTable = referenceTable;
	}
	public String getReferencePath() {
		return referencePath;
	}
	public void setReferencePath(String referencePath) {
		this.referencePath = referencePath;
	}
	public String getReferenceValue() {
		return referenceValue;
	}
	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}
	public String[] getPageName() {
		return pageName;
	}
	public void setPageName(String[] pageName) {
		this.pageName = pageName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}

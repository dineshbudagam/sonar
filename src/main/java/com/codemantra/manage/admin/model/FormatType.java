/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
25-09-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

public class FormatType{

	private String id;
	
	private String formatTypeId;
	private String formatTypeName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFormatTypeId() {
		return formatTypeId;
	}
	public void setFormatTypeId(String formatTypeId) {
		this.formatTypeId = formatTypeId;
	}
	public String getFormatTypeName() {
		return formatTypeName;
	}
	public void setFormatTypeName(String formatTypeName) {
		this.formatTypeName = formatTypeName;
	}
	
	
}

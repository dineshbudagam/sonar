/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

import java.util.List;

import com.codemantra.manage.admin.entity.MBaseEntity;
import com.codemantra.manage.admin.entity.ModuleAccessEntity;

public class Role extends MBaseEntity{
	
	private String roleId;
	private String roleName;
	private Integer level;
	private List<ModuleAccessEntity> module;
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}	

	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public List<ModuleAccessEntity> getModule() {
		return module;
	}
	public void setModule(List<ModuleAccessEntity> module) {
		this.module = module;
	}
	
	
}

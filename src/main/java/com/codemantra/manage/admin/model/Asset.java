/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
04-08-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/

package com.codemantra.manage.admin.model;

import java.util.List;

public class Asset {
	private String permission;
	private List<String> format;
	
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public List<String> getFormat() {
		return format;
	}
	public void setFormat(List<String> format) {
		this.format = format;
	}
}

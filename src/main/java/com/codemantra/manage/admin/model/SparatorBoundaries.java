package com.codemantra.manage.admin.model;

import org.springframework.data.mongodb.core.mapping.Field;

public class SparatorBoundaries {
	
	@Field("beginInd")
	public int beginInd;
	@Field("endInd")
	public int endInd;
	@Field("sparator")
	public String sparator;
	public int getBeginInd() {
		return beginInd;
	}
	public void setBeginInd(int beginInd) {
		this.beginInd = beginInd;
	}
	public int getEndInd() {
		return endInd;
	}
	public void setEndInd(int endInd) {
		this.endInd = endInd;
	}
	public String getSparator() {
		return sparator;
	}
	public void setSparator(String sparator) {
		this.sparator = sparator;
	}

	
}

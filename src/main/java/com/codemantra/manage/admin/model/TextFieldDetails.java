package com.codemantra.manage.admin.model;

import java.util.List;

public class TextFieldDetails {
	
	public String textFieldName;
	
	public String metadataFieldName;

	public boolean isSubElement;
	
	public Transformation transformations;
	
    public List<Attributes> attributes;

	
	public String getTextFieldName() {
		return textFieldName;
	}

	public void setTextFieldName(String textFieldName) {
		this.textFieldName = textFieldName;
	}

	public String getMetadataFieldName() {
		return metadataFieldName;
	}

	public void setMetadataFieldName(String metadataFieldName) {
		this.metadataFieldName = metadataFieldName;
	}

	public boolean isSubElement() {
		return isSubElement;
	}

	public void setSubElement(boolean isSubElement) {
		this.isSubElement = isSubElement;
	}

	public Transformation getTransformations() {
		return transformations;
	}

	public void setTransformations(Transformation transformations) {
		this.transformations = transformations;
	}

	public List<Attributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attributes> attributes) {
		this.attributes = attributes;
	}
}

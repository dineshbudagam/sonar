/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

import java.util.Date;

import com.codemantra.manage.admin.entity.MBaseEntity;

public class StandardDashboardSummary extends MBaseEntity{
	
	private Integer year;
	private Double totalFreeHrs;
	private Date startDate;
	private Date endDate;
	private Double freeHrsUsed;
	private Double paidHrsUsed;
	private Double availableFreeHrs;
	/*private Float freeHrsUsed;
	private Float paidHrsUsed;*/
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Double getTotalFreeHrs() {
		return totalFreeHrs;
	}
	public void setTotalFreeHrs(Double totalFreeHrs) {
		this.totalFreeHrs = totalFreeHrs;
	}
	/*public Float getFreeHrsUsed() {
		return freeHrsUsed;
	}
	public void setFreeHrsUsed(Float freeHrsUsed) {
		this.freeHrsUsed = freeHrsUsed;
	}
	public Float getPaidHrsUsed() {
		return paidHrsUsed;
	}
	public void setPaidHrsUsed(Float paidHrsUsed) {
		this.paidHrsUsed = paidHrsUsed;
	}*/
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getFreeHrsUsed() {
		return freeHrsUsed;
	}
	public void setFreeHrsUsed(Double freeHrsUsed) {
		this.freeHrsUsed = freeHrsUsed;
	}
	public Double getPaidHrsUsed() {
		return paidHrsUsed;
	}
	public void setPaidHrsUsed(Double paidHrsUsed) {
		this.paidHrsUsed = paidHrsUsed;
	}
	public Double getAvailableFreeHrs() {
		return availableFreeHrs;
	}
	public void setAvailableFreeHrs(Double availableFreeHrs) {
		this.availableFreeHrs = availableFreeHrs;
	}
	
}

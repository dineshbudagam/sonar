package com.codemantra.manage.admin.model;


public class FormatDetails {
	
	String formatId;
	String formatNaming;
	String formatNamingPrefix;
	String formatNamingSuffix;
	String appleFileType;
	Boolean compressedIfMultiple;
	Boolean epubValidator;
	String ePubVersion;
	String fieldDisplayName;
	Boolean isEpubFormat;

	
	public FormatDetails() {
		//TO DO
	}
		
	public FormatDetails(String formatId, String formatNaming, String formatNamingPrefix, String formatNamingSuffix,
			String appleFileType, Boolean compressedIfMultiple, Boolean epubValidator, String ePubVersion,
			String fieldDisplayName,Boolean isEpubFormat,Boolean isActive, String isDeleted, String createdOn, String createdBy, String modifiedOn,
			String modifiedBy) {
		super();
		this.formatId = formatId;
		this.formatNaming = formatNaming;
		this.formatNamingPrefix = formatNamingPrefix;
		this.formatNamingSuffix = formatNamingSuffix;
		this.appleFileType = appleFileType;
		this.compressedIfMultiple = compressedIfMultiple;
		this.epubValidator = epubValidator;
		this.ePubVersion = ePubVersion;
		this.fieldDisplayName = fieldDisplayName;
		this.isEpubFormat = isEpubFormat;

		
	}

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}

	public String getFormatNaming() {
		return formatNaming;
	}

	public void setFormatNaming(String formatNaming) {
		this.formatNaming = formatNaming;
	}

	public String getFormatNamingPrefix() {
		return formatNamingPrefix;
	}

	public void setFormatNamingPrefix(String formatNamingPrefix) {
		this.formatNamingPrefix = formatNamingPrefix;
	}

	public String getFormatNamingSuffix() {
		return formatNamingSuffix;
	}

	public void setFormatNamingSuffix(String formatNamingSuffix) {
		this.formatNamingSuffix = formatNamingSuffix;
	}

	public String getAppleFileType() {
		return appleFileType;
	}

	public void setAppleFileType(String appleFileType) {
		this.appleFileType = appleFileType;
	}

	public Boolean getCompressedIfMultiple() {
		return compressedIfMultiple;
	}

	public void setCompressedIfMultiple(Boolean compressedIfMultiple) {
		this.compressedIfMultiple = compressedIfMultiple;
	}
	
	public String getePubVersion() {
		return ePubVersion;
	}

	public void setePubVersion(String ePubVersion) {
		this.ePubVersion = ePubVersion;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}

	public Boolean getEpubValidator() {
		return epubValidator;
	}

	public Boolean getIsEpubFormat() {
		return isEpubFormat;
	}

	public void setEpubValidator(Boolean epubValidator) {
		this.epubValidator = epubValidator;
	}

	public void setIsEpubFormat(Boolean isEpubFormat) {
		this.isEpubFormat = isEpubFormat;
	}
	
	
}

/**********************************************************************************
Date          	Version       Modified By       	Description  
***********		********      *************		    *************	
17-07-2017		v1.0       	  Shahid ul Islam	    Initial Version.
***********************************************************************************/
package com.codemantra.manage.admin.model;

import java.util.List;

import com.codemantra.manage.admin.entity.AccountMappedField;
import com.codemantra.manage.admin.entity.MBaseEntity;

public class Account extends MBaseEntity{
	
	private String accountId;
	private String accountName;
	private List<AccountMappedField> metadataMappedField;	
	
	private List<String> metadataMappedFieldValues;
	

	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public List<String> getMetadataMappedFieldValues() {
		return metadataMappedFieldValues;
	}
	public void setMetadataMappedFieldValues(List<String> metadataMappedFieldValues) {
		this.metadataMappedFieldValues = metadataMappedFieldValues;
	}
	public List<AccountMappedField> getMetadataMappedField() {
		return metadataMappedField;
	}
	public void setMetadataMappedField(List<AccountMappedField> metadataMappedField) {
		this.metadataMappedField = metadataMappedField;
	}
	
}

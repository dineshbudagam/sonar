package com.codemantra.manage.admin.model;

import java.util.List;

public class ReadExcelData {

	private List<String> fieldsToMap;
	private String partnerId;
	private String sheetName;
	private String userId;
	private String filePath;
	private String lookupName;
	
	
	
	public String getLookupName() {
		return lookupName;
	}
	public void setLookupName(String lookupName) {
		this.lookupName = lookupName;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public List<String> getFieldsToMap() {
		return fieldsToMap;
	}
	public String getSheetName() {
		return sheetName;
	}
	public String getUserId() {
		return userId;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFieldsToMap(List<String> fieldsToMap) {
		this.fieldsToMap = fieldsToMap;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	
}

package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.entity.AccountMappedField;
import com.codemantra.manage.admin.model.Account;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir="target/generated-snippets")
public class AccountMasterTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private RestDocumentationContextProvider restDocumentation;

	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", 
						Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}
	
	
	@Test
	public void save() {
		try {
			Account account = new Account();
			account.setAccountName("TestJunitAcc-001");
			String content=objectMapper.writeValueAsString(account);
			mockMvc.perform(post(adminService + "account")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
//					.andDo(print())
					.andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void saveAccountMapping() {
		try {
			Account account = new Account();
			account.setAccountName("Test Account");
			
			AccountMappedField mappedField = new AccountMappedField();
			mappedField.setValue("TS001");
			List<AccountMappedField> metadataMappedField = new ArrayList<>();
			metadataMappedField.add(mappedField);
			
			account.setMetadataMappedField(metadataMappedField);
			String content=objectMapper.writeValueAsString(account);
			mockMvc.perform(put(adminService + "account/AC00145/mapping")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void editAccount(){
		try {
			Account account = new Account();
			account.setAccountName("Account-Updated");
			String content=objectMapper.writeValueAsString(account);
			mockMvc.perform(put(adminService + "account/AC00145")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void deleteAccount(){
		try {
			mockMvc.perform(delete(adminService + "account/AC00143")
					.param("loggedUser", "USR01219"))
					.andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteMapping(){
		try {
			mockMvc.perform(delete(adminService + "account/AC00144/mapping/TS001")
					.param("loggedUser", "USR01219"))
					.andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void addMapping(){
		try {
			Account account = new Account();
			account.setAccountName("Account-Updated");
			String content=objectMapper.writeValueAsString(account);
			mockMvc.perform(post(adminService + "account/mapping/Test")
					.param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Test
	public void retrieveAll() {
		try {
			mockMvc.perform(get(adminService + "account").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieve(){
		try {
			mockMvc.perform(get(adminService + "account/AC00000").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.entity.CPartnerConfigEntity;
import com.codemantra.manage.admin.entity.TransformFieldDetails;
import com.codemantra.manage.admin.model.FieldsToMap;
import com.codemantra.manage.admin.model.ReadExcelData;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class GdsMasterTest {
	
	protected MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	private RestDocumentationContextProvider restDocumentation;
	
	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}
	
	
	@Test
	public void savePartnerConfig() {
		CPartnerConfigEntity partnerConfig= new CPartnerConfigEntity();
		
		List<TransformFieldDetails> transformFieldList = new ArrayList<>();
		TransformFieldDetails trnsDetails = new TransformFieldDetails();
		trnsDetails.setMetaDataFieldName("ExclusiveRightsCountry");
		trnsDetails.setOperation("JOIN");
		trnsDetails.setJoinBy("");
		transformFieldList.add(trnsDetails);
		partnerConfig.setTransformFieldList(transformFieldList);
		
		try {
			String content = objectMapper.writeValueAsString(partnerConfig);
			 mockMvc.perform(
					post(adminService+"partnerconfig")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getAllpartnerconfig() {
		try {
			 mockMvc.perform(
					get(adminService+"getAllpartnerConfig")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveAllPartners() {
		try {
			 mockMvc.perform(
					get(adminService+"exportGds")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getPartnerConfigById() {
		try {
			 mockMvc.perform(
					get(adminService+"getPartnerConfigById/60cb36e86f640d3a70079c9d")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void customTemplateMData() {
		FieldsToMap fieldsToMap = new FieldsToMap();	
		List<String> fields = Arrays.asList("Field_1,Field_2,Field_3");
		fieldsToMap.setFieldsToMap(fields);
		try {
			String content = objectMapper.writeValueAsString(fieldsToMap);
			 mockMvc.perform(
					post(adminService+"customTemplateMData")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void editPartnerConfig() {
		CPartnerConfigEntity partnerConfig= new CPartnerConfigEntity();
		
		List<TransformFieldDetails> transformFieldList = new ArrayList<>();
		TransformFieldDetails trnsDetails = new TransformFieldDetails();
		trnsDetails.setMetaDataFieldName("ExclusiveRightsCountry");
		trnsDetails.setOperation("JOIN");
		trnsDetails.setJoinBy("");
		transformFieldList.add(trnsDetails);
		partnerConfig.setTransformFieldList(transformFieldList);
		
		try {
			String content = objectMapper.writeValueAsString(partnerConfig);
			 mockMvc.perform(
					put(adminService+"editPartnerConfig/60cb36e86f640d3a70079c9d")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deletePartnerConfig() {
		
		try {
			 mockMvc.perform(
					delete(adminService+"deletePartnerConfig/5f4f717ac3701a2e409b8493")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void readLookupExcel() {
		ReadExcelData readExcelData= new ReadExcelData();
		readExcelData.setFilePath("/opt/manage/data/");
		readExcelData.setPartnerId("CP00015");
		List<String> fields = Arrays.asList("Field_1,Field_2,Field_3");
		readExcelData.setFieldsToMap(fields);
		readExcelData.setLookupName("testLookup");
		readExcelData.setUserId("USR01219");
		try {
			String content = objectMapper.writeValueAsString(readExcelData);
			 mockMvc.perform(
					post(adminService+"readLookupExcel")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getAllGdsLookupNames() {
		try {
			 mockMvc.perform(
					get(adminService+"getAllGdsLookupNames")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.model.DistributionStatusUpdate;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir="target/generated-snippets")
public class BaseControllerTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;

	@Autowired
	private RestDocumentationContextProvider restDocumentation;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", 
						Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}
	
	@Test
	public void retrieveImprints() {
		try {
			mockMvc.perform(get(adminService + "imprint")
					.param("loggedUser", "USR01219")
					.param("accounts", new String[] { "AC00000", "AC00001" }))
//					.andDo(print())
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveImprintsFromPG() {

		try {
			mockMvc.perform(get(adminService + "imprintpg")
					.param("loggedUser", "USR01219")
					.param("accounts", new String[] { "AC00000", "AC00001" })).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void retrieveViewerFormats() {

		try {
			mockMvc.perform(get(adminService + "viewerformats")
					.param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void getEligibleFormats() {

		try {
			mockMvc
					.perform(get(adminService + "watermark/eligibleFormats")
							.param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void distributionstatusupdate() {
		DistributionStatusUpdate distribution = new DistributionStatusUpdate();
		distribution.setPartnerId("P00171");
		distribution.setTransactionType("DISTRIBUTION");
		distribution.setStatus("TEST");
		distribution.setRemarks("testing");
		
		try {
			String content = objectMapper.writeValueAsString(distribution);
			mockMvc.perform(put(adminService + "distributionstatusupdate")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAll() {

		try {
			mockMvc.perform(get(adminService + "menu").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

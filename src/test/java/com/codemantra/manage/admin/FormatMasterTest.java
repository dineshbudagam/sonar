package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.model.CApprovalEligibleFormats;
import com.codemantra.manage.admin.model.Format;
import com.codemantra.manage.admin.model.FormatType;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class FormatMasterTest {
	
	protected MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	private RestDocumentationContextProvider restDocumentation;
	
	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}
	
	@Test
	public void retrieveAllFormatTypes() {
		try {
			mockMvc.perform(
					get(adminService+"formattype")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void saveFormatType() {
		FormatType formatType = new FormatType();
		formatType.setFormatTypeName("Junit_Format");
		try {
			String content = objectMapper.writeValueAsString(formatType);
			mockMvc.perform(
					post(adminService+"formattype")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void editFormatType() {
		FormatType formatType = new FormatType();
		formatType.setFormatTypeName("Junit_Format_01");
		try {
			String content = objectMapper.writeValueAsString(formatType);
			mockMvc.perform(
					put(adminService+"formattype/FT00040")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteFormatType() {
		try {
			mockMvc.perform(
					delete(adminService+"formattype/FT00040")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveFormatType() {
		try {
			mockMvc.perform(
					get(adminService+"formattype/FT00040")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveAllFormats() {
		try {
			mockMvc.perform(
					get(adminService+"format")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void saveFormat() {
		Format format = new Format();
		format.setFormatName("Junit_Format");
		format.setFormatTypeId("FT00040");
		try {
			String content = objectMapper.writeValueAsString(format);
			mockMvc.perform(
					post(adminService+"format")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void editFormat() {
		Format format = new Format();
		format.setFormatId("F01041");
		format.setFormatName("Junit_Format_Edited");
		format.setFormatTypeId("FT00040");
		try {
			String content = objectMapper.writeValueAsString(format);
			mockMvc.perform(
					put(adminService+"format/F01041")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteFormat() {
		try {
			mockMvc.perform(
					delete(adminService+"format/F01041")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveFormat() {
		try {
			mockMvc.perform(
					get(adminService+"format/F01041")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveAllCApprovalEligibleFormats() {
		try {
			mockMvc.perform(
					get(adminService+"capprovaleligibleformats")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void saveCApprovalEligibleFormats() {
		CApprovalEligibleFormats format = new CApprovalEligibleFormats();
		format.setFormatName("Junit_Format");
		format.setFormatId("F01041");
		try {
			String content = objectMapper.writeValueAsString(format);
			mockMvc.perform(
					post(adminService+"capprovaleligibleformats")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void editCApprovalEligibleFormats() {
		CApprovalEligibleFormats format = new CApprovalEligibleFormats();
		format.setFormatName("Junit_Format_1");
		format.setFormatId("F01041");
		try {
			String content = objectMapper.writeValueAsString(format);
			mockMvc.perform(
					put(adminService+"capprovaleligibleformats/F01041")
					.param("loggedUser", "USR01219")
					.content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteCApprovalEligibleFormats() {
		try {
			mockMvc.perform(
					delete(adminService+"capprovaleligibleformats/F01041")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retrieveCApprovalEligibleFormats() {
		try {
			mockMvc.perform(
					get(adminService+"capprovaleligibleformats/F01041")
					.param("loggedUser", "USR01219")
					).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

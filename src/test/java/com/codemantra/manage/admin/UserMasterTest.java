package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class UserMasterTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private RestDocumentationContextProvider restDocumentation;

	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}

	@Test
	public void saveUser() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setAccountName("IMF");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "user").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void editUser() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/USR01240").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deleteUser() {
		try {
			mockMvc.perform(delete(adminService + "user/USR01163").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAll() {
		try {
			mockMvc.perform(get(adminService + "user").param("loggedUser", "USR01219")).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieve() {
		try {
			mockMvc.perform(get(adminService + "user/USR01219").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void activateDeactivateUser() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/USR01239/activate/1").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void updateUserInfo() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/saveprofile/USR01240").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void updateProfilePic() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setProfilePicName("profile.png");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/USR01219/updateprofilepic").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void resend() {
		User user = new User();
		user.setEmailId("nambirajan@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/USR01219/resendmail").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void checkForDuplicateUser() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "user/checkforduplicateuser").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void unlockUser() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(put(adminService + "user/unlock/USR01239").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void checkForDuplicatePgName() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		user.setPgName("test");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "user/checkForDuplicatePgName").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getUserColumnFilterSuggestions() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "getUserColumnFilterSuggestions").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllWithFilters() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "filteredusers").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sendInvitationToAllActiveUsers() {
		User user = new User();
		user.setEmailId("junit_test@codemantra.com");
		user.setFirstName("Junit");
		try {
			String content = objectMapper.writeValueAsString(user);
			mockMvc.perform(post(adminService + "sendInvitationToAll/USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllSkip() {
		try {
			mockMvc.perform(get(adminService + "users/5").param("loggedUser", "USR01219")).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

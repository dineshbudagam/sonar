package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.model.Partner;
import com.codemantra.manage.admin.model.PartnerGroup;
import com.codemantra.manage.admin.model.PartnerType;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class PartnerMasterTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private RestDocumentationContextProvider restDocumentation;

	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}

	@Test
	public void savePartnerType() {
		PartnerType partnerType = new PartnerType();
		partnerType.setFormatTypeId("FT00001");
		partnerType.setPartnerTypeName("Junit_Partner");
		try {
			String content = objectMapper.writeValueAsString(partnerType);
			mockMvc.perform(post(adminService + "partnertype").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void editPartnerType() {
		PartnerType partnerType = new PartnerType();
		partnerType.setFormatTypeId("FT00001");
		partnerType.setPartnerTypeName("Junit_Partner_01");
		try {
			String content = objectMapper.writeValueAsString(partnerType);
			mockMvc.perform(put(adminService + "partnertype/PT00114").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deletePartnerType() {
		try {
			mockMvc.perform(delete(adminService + "partnertype/PT00114").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrievePartnerType() {
		try {
			mockMvc.perform(get(adminService + "partnertype/PT00114").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllPartnerTypes() {
		try {
			mockMvc.perform(get(adminService + "partnertype").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void savePartner() {
		Partner partner = new Partner();
		partner.setPartnerId("P00001");
		partner.setAccountId("AC00000");
		partner.setPartnerTypeId("PT00001");
		partner.setChannelName("Preflight");
		partner.setPartnerName("Junit_Partner");
		partner.setFormatId(Arrays.asList("F00001", "F00042"));
		partner.setProductHierarchyIds(Arrays.asList("PH0003"));
		try {
			String content = objectMapper.writeValueAsString(partner);
			mockMvc.perform(post(adminService + "partner").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void editPartner() {
		Partner partner = new Partner();
		partner.setPartnerId("P00001");
		partner.setAccountId("AC00000");
		partner.setPartnerTypeId("PT00001");
		partner.setChannelName("Preflight");
		partner.setPartnerName("Junit_Partner_01");
		partner.setFormatId(Arrays.asList("F00001", "F00042"));
		partner.setProductHierarchyIds(Arrays.asList("PH0003"));
		try {
			String content = objectMapper.writeValueAsString(partner);
			mockMvc.perform(put(adminService + "partner/P00001").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deletePartner() {
		try {
			mockMvc.perform(delete(adminService + "partner/P00001").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrievePartner() {
		try {
			mockMvc.perform(get(adminService + "partner/P00001").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllPartners() {
		try {
			mockMvc.perform(get(adminService + "partner").param("loggedUser", "USR01219")
					.param("exportFormat", "xlsx"))
			.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void activateDeactivatePartner() {
		Partner partner = new Partner();
		partner.setPartnerId("P00001");
		try {
			String content = objectMapper.writeValueAsString(partner);
			mockMvc.perform(put(adminService + "partner/P00001/activate/1").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void savePartnerGroup() {
		PartnerGroup partnerGroup = new PartnerGroup();
		partnerGroup.setPartnerGroupName("Junit_PG");
		partnerGroup.setPartnerId(Arrays.asList("P01005", "P01006"));
		partnerGroup.setPartnerTypeId("PT00001");
		try {
			String content = objectMapper.writeValueAsString(partnerGroup);
			mockMvc.perform(post(adminService + "partnergroup").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void editPartnerGroup() {
		PartnerGroup partnerGroup = new PartnerGroup();
		partnerGroup.setPartnerGroupName("Junit_PG_01");
		partnerGroup.setPartnerId(Arrays.asList("P01005", "P01006"));
		partnerGroup.setPartnerTypeId("PT00001");
		try {
			String content = objectMapper.writeValueAsString(partnerGroup);
			mockMvc.perform(put(adminService + "partnergroup/PG00108").param("loggedUser", "USR01219").content(content)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deletePartnerGroup() {
		try {
			mockMvc.perform(delete(adminService + "partnergroup/PG00108").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrievePartnerGroup() {
		try {
			mockMvc.perform(get(adminService + "partnergroup/PG00108").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllPartnerGroup() {
		try {
			mockMvc.perform(get(adminService + "partnergroup").param("loggedUser", "USR01219"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void changeFtpPassword() {
		Partner partner = new Partner();
		partner.setPartnerId("P00001");
		try {
			String content = objectMapper.writeValueAsString(partner);
			mockMvc.perform(put(adminService + "partner/P00001/changeftppassword").param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

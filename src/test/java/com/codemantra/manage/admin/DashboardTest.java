package com.codemantra.manage.admin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.codemantra.manage.admin.model.StandardDashboardDetails;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class DashboardTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	String adminService = "/manage-admin-service/";
	
	@Value("${spring.data.mongodb.host}")
	private String appURLJunit;

	@Autowired
	private RestDocumentationContextProvider restDocumentation;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
				.alwaysDo(MockMvcRestDocumentation.document("{methodName}", Preprocessors.preprocessRequest(),
						Preprocessors.preprocessResponse(ResponseModifyingPreprocessors.replaceBinaryContent(),
								ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
								Preprocessors.prettyPrint())))
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation).uris().withScheme("http")
						.withHost(appURLJunit).withPort(8085).and().snippets()
						.withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
								HttpDocumentation.httpResponse(), AutoDocumentation.requestFields(),
								AutoDocumentation.pathParameters(), AutoDocumentation.requestParameters(),
								AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
								AutoDocumentation.section()))
				.build();

	}

	@Test
	public void saveDashBoard() {
		StandardDashboardDetails dashboard = new StandardDashboardDetails();
		dashboard.setReleasedOn("2018/04/30");
		dashboard.setFreeHrsUsed(1.0);
		dashboard.setProjectName("TestProject");
		dashboard.setProjectDescription("Test Desc");;
		try {
			String content = objectMapper.writeValueAsString(dashboard);
			 mockMvc.perform(post(adminService + "standarddashboard/details")
					.param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void editDashBoard() {
		StandardDashboardDetails dashboard = new StandardDashboardDetails();
		dashboard.setReleasedOn("2018/04/30");
		dashboard.setFreeHrsUsed(1.0);
		dashboard.setProjectName("TestProject");
		dashboard.setProjectDescription("Test Desc Updated");;
		try {
			String content = objectMapper.writeValueAsString(dashboard);
			 mockMvc.perform(put(adminService + "standarddashboard/details/STDB00007")
					.param("loggedUser", "USR01219")
					.content(content).contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void retrieveAllDetailsForYear() {
		try {
			 mockMvc
					.perform(get(adminService + "standarddashboard/details")
							.param("loggedUser", "USR01219")
							.param("year", "2018"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getUserSuggestions() {
		try {
			 mockMvc
					.perform(get(adminService + "standarddashboard/userSuggestions")
							.param("loggedUser", "USR01219")
							.param("year", "2018")
							.param("searchText", "test"))
					.andExpect(status().isOk());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
